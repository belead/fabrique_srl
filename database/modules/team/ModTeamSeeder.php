<?php

use Illuminate\Database\Seeder;

class ModTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Team',
            'icon' => 'user',
            'reference' => 'team',
            'class_name' => 'Team',
            'is_linkable' => 1,
            'is_multiple' => 1,
            'is_block' => 1,
            'is_targetable_by_slug' => 1,
            'targetable_by' => 'name',
            'orderable_by' => 'created_at,order',
        ]);
    }
}
