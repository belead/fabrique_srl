<?php

use Illuminate\Database\Seeder;

class ModBlocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Content Blocks',
            'icon' => 'layout',
            'reference' => 'content_blocks',
            'class_name' => 'ContentBlock',
            'is_linkable' => 1,
            'is_multiple' => 0,
            'is_block' => 0,
            'is_targetable_by_slug' => 0,
            'targetable_by' => 'name',
            'orderable_by' => 'created_at',
        ]);
    }
}
