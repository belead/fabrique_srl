<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToModDiagnosticsAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_mod_diagnostics_answers', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->string('project_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_mod_diagnostics_answers', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('project_name');
        });
    }
}
