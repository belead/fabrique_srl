<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_galleries')) {
            Schema::create('_mod_galleries', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('partial_id')->nullable()->unsigned();
                $table->index('partial_id');
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->integer('creator_id')->nullable()->unsigned();
                $table->index('creator_id');
                $table->char('lang', 5)->nullable();
                $table->string('name')->nullable();
                $table->string('title')->nullable();
                $table->string('head_title')->nullable();
                $table->text('description')->nullable();
            });
        }

        if(!Schema::hasTable('_mod_galleries_pictures')) {
            Schema::create('_mod_galleries_pictures', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('gallery_id')->nullable()->unsigned();
                $table->index('gallery_id');
                $table->string('photo')->nullable();
                $table->string('title')->nullable();
                $table->string('reason_number')->nullable();
                $table->string('description')->nullable();
                $table->string('video_url')->nullable();
                $table->string('video_title')->nullable();
                $table->text('content')->nullable();
                $table->tinyInteger('has_more_content')->unsigned()->nullable();
                $table->tinyInteger('has_video')->unsigned()->nullable();
                $table->integer('order')->unsigned()->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_galleries');
        Schema::dropIfExists('_mod_galleries_pictures');
    }
}
