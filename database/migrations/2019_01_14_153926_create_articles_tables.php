<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_articles')) {
            Schema::create('_mod_articles', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->integer('creator_id')->nullable()->unsigned();
                $table->index('creator_id');
                $table->integer('category_id')->nullable()->unsigned();
                $table->index('category_id');
                $table->integer('type_id')->nullable()->unsigned();
                $table->index('type_id');
                $table->char('lang', 5)->nullable();
                $table->string('title')->nullable();
                $table->string('slug')->nullable();
                $table->date('custom_date')->nullable();
                $table->string('description')->nullable();
                $table->string('description_short')->nullable();
                $table->string('picture')->nullable();
                $table->mediumText('content')->nullable();
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->string('file')->nullable();
                $table->string('file_name')->nullable();
                $table->string('video_url')->nullable();
                $table->text('tags')->nullable();
            });
        }

        if(!Schema::hasTable('_mod_articles_categories')) {
            Schema::create('_mod_articles_categories', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->char('lang', 5)->nullable();
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('picture')->nullable();
            });
        }

        if(!Schema::hasTable('_mod_articles_types')) {
            Schema::create('_mod_articles_types', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('name')->nullable();
                $table->string('reference')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_articles');
        Schema::dropIfExists('_mod_articles_categories');
        Schema::dropIfExists('_mod_articles_types');
    }
}
