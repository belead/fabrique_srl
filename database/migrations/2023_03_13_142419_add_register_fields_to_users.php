<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRegisterFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable();

            $table->unsignedTinyInteger('founders_count')->nullable();
            $table->unsignedTinyInteger('founders_are_married')->nullable();
            $table->unsignedTinyInteger('founders_have_wedding_contract')->nullable();
            $table->text('founders_nationalities')->nullable();
            $table->text('founders_residential_region')->nullable();

            $table->string('main_contact_first_name')->nullable();
            $table->string('main_contact_last_name')->nullable();
            $table->string('main_contact_email')->nullable();
            $table->string('main_contact_phone')->nullable();
            $table->string('main_contact_address')->nullable();

            $table->string('company_name')->nullable();
            $table->string('company_address')->nullable();
            $table->string('company_launching_at')->nullable();
            $table->string('company_main_sector')->nullable();
            $table->text('company_project_description')->nullable();

            $table->unsignedTinyInteger('business_plan_is_ready')->nullable();
            $table->unsignedTinyInteger('business_plan_needs_quick_feedback')->nullable();
            $table->unsignedTinyInteger('business_plan_needs_financial_formatting')->nullable();
            $table->unsignedTinyInteger('business_plan_needs_finalizing_support')->nullable();
            $table->unsignedTinyInteger('business_plan_needs_full_formatting')->nullable();

            $table->unsignedTinyInteger('sharing_agreement')->nullable();

            $table->text('how')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
