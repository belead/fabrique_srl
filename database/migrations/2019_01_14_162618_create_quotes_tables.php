<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_quotes')) {
            Schema::create('_mod_quotes', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->integer('partial_id')->nullable()->unsigned();
                $table->index('partial_id');
                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_draft')->unsigned()->nullable()->default(1);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->char('lang', 5)->nullable();
                $table->string('name')->nullable();
                $table->string('image')->nullable();
                $table->string('title')->nullable();
                $table->string('head_title')->nullable();
                $table->text('description')->nullable();
                $table->string('quote')->nullable();
                $table->string('author')->nullable();
                $table->string('function')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_quotes');
    }
}
