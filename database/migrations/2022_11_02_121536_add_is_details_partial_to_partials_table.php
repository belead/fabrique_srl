<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDetailsPartialToPartialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partials', function (Blueprint $table) {
            $table->string('child_class')->default(0)->nullable();
            $table->string('hidden_fields')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partials', function (Blueprint $table) {
            $table->dropColumn('child_class');
            $table->dropColumn('hidden_fields');
        });
    }
}
