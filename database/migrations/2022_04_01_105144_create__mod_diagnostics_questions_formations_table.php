<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModDiagnosticsQuestionsFormationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_mod_diagnostics_questions_formations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('formation_id')->index()->nullable();
            $table->unsignedInteger('question_id')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_diagnostics_questions_formations');
    }
}
