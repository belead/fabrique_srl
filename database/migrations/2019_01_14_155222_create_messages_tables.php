<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('_mod_contact_messages')) {
            Schema::create('_mod_contact_messages', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->tinyInteger('is_deleted')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_destroyed')->unsigned()->nullable()->default(0);
                $table->tinyInteger('is_new')->unsigned()->nullable()->default(1);
                $table->integer('site_id')->nullable()->unsigned();
                $table->index('site_id');
                $table->char('lang', 5)->nullable();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('email')->nullable();
                $table->string('phone')->nullable();
                $table->text('content')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_contact_messages');
    }
}
