<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModDiagnosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_mod_diagnostics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->unsignedTinyInteger('is_active')->nullable()->default(0);
        });

        Schema::create('_mod_diagnostics_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('color')->nullable();
            $table->unsignedInteger('position')->nullable();
            $table->unsignedInteger('diagnostic_id')->nullable()->index();
        });

        Schema::create('_mod_diagnostics_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedInteger('diagnostic_id')->nullable()->index();
            $table->unsignedInteger('category_id')->nullable()->index();
            $table->unsignedInteger('position')->nullable();
            $table->string('question')->nullable();
            $table->unsignedInteger('points_weighting')->nullable()->default(1);
        });

        Schema::create('_mod_diagnostics_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedInteger('diagnostic_id')->nullable()->index();
            $table->unsignedInteger('question_id')->nullable()->index();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('note')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_diagnostics');
        Schema::dropIfExists('_mod_diagnostics_categories');
        Schema::dropIfExists('_mod_diagnostics_questions');
        Schema::dropIfExists('_mod_diagnostics_answers');
    }
}
