<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagQuestionsToolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_mod_diagnostics_questions_tools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedInteger('question_id')->nullable()->index();
            $table->unsignedInteger('tool_id')->nullable()->index();
        });

        Schema::create('_mod_diagnostics_questions_fiches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedInteger('question_id')->nullable()->index();
            $table->unsignedInteger('fiche_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mod_diagnostics_questions_tools');
        Schema::dropIfExists('_mod_diagnostics_questions_fiches');
    }
}
