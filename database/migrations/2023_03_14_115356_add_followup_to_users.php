<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFollowupToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedTinyInteger('followup_bank_sent')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_bank_iban')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_bank_signature')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_bank_capital_payment')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_bank_certificate')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_bank_cards')->nullable()->default(0);

            $table->unsignedTinyInteger('followup_business_plan_description')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_business_plan_budget')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_business_plan_cash_flow')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_business_plan_incomes_expenses_explained')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_business_plan_income_statement')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_business_plan_assessment')->nullable()->default(0);

            $table->unsignedTinyInteger('followup_business_court_management_access_in_order')->nullable()->default(0);
            $table->unsignedTinyInteger('followup_business_court_social_in_order')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('followup_bank_sent');
            $table->dropColumn('followup_bank_iban');
            $table->dropColumn('followup_bank_signature');
            $table->dropColumn('followup_bank_capital_payment');
            $table->dropColumn('followup_bank_certificate');
            $table->dropColumn('followup_bank_cards');

            $table->dropColumn('followup_business_plan_description');
            $table->dropColumn('followup_business_plan_budget');
            $table->dropColumn('followup_business_plan_cash_flow');
            $table->dropColumn('followup_business_plan_incomes_expenses_explained');
            $table->dropColumn('followup_business_plan_income_statement');
            $table->dropColumn('followup_business_plan_assessment');

            $table->dropColumn('followup_business_court_management_access_in_order');
            $table->dropColumn('followup_business_court_social_in_order');
        });
    }
}
