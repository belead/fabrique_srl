<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTablesForUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins_socials', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('admin_id')->nullable()->index();
            $table->string('provider')->nullable();
            $table->string('provider_user_id')->nullable();
        });

        // -- Logs table
        if(!Schema::hasTable('admins_logs')) {
            Schema::create('admins_logs', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('admin_id')->nullable()->index();
                $table->string('item_type')->nullable()->index();
                $table->string('item_id')->nullable()->index();
                $table->string('action')->nullable();
                $table->text('action_details')->nullable();
                $table->timestamps();
            });
        }

        // -- Updates table
        if(!Schema::hasTable('updates')) {
            Schema::create('updates', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->timestamps();
                $table->string('name')->nullable();
                $table->string('reference')->nullable();
                $table->string('cms_version')->nullable();
                $table->string('laravel_version')->nullable();
                $table->text('changelog')->nullable();
                $table->unsignedTinyInteger('is_installed')->nullable()->default(0);
                $table->unsignedTinyInteger('is_deprecated')->nullable()->default(0);
                $table->string('file_uri')->nullable();
            });
        }

        if(!Schema::hasTable('updates_logs')) {
            Schema::create('updates_logs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->timestamps();
                $table->unsignedInteger('user_id')->nullable()->index();
                $table->unsignedInteger('update_id')->nullable()->index();
                $table->string('action')->nullable();
                $table->text('description')->nullable();
                $table->text('error')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins_socials');
        Schema::dropIfExists('admins_logs');
        Schema::dropIfExists('updates');
        Schema::dropIfExists('updates_logs');
    }
}
