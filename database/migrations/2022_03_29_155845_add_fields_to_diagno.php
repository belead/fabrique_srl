<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDiagno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_mod_diagnostics', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->string('picture')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_mod_diagnostics', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('picture');
        });
    }
}
