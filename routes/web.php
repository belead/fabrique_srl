<?php

Route::get('belead/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect']
], function() {

    Date::setLocale(App::getLocale());

    // -- Auth
    Route::group(['middleware' => 'guest'], function() {

        // -- Register
        Route::post('/register/step1', '_Modules\SrlController@saveForm')->name('front.register.step1.post');
        Route::get('/creer-mon-srl/etape-2', [\App\Http\Controllers\Front\Auth\RegisterController::class, 'form'])->name('front.register.step2');
        Route::get('/creer-mon-srl/confirmation', [\App\Http\Controllers\Front\Auth\RegisterController::class, 'confirmation'])->name('front.register.confirmation');
        Route::post('/register/step2', [\App\Http\Controllers\Front\Auth\RegisterController::class, 'register'])->name('front.register.step2.post');

        // -- Login
        Route::get('/login', [\App\Http\Controllers\Front\Auth\LoginController::class, 'form'])->name('front.login');
        Route::post('/login', [\App\Http\Controllers\Front\Auth\LoginController::class, 'login']);

    });

    Route::group(['middleware' => 'user_auth'], function() {

        Route::get('/logout', [\App\Http\Controllers\Front\Auth\LoginController::class, 'logout'])->name('front.logout');

        // -- Account
        Route::get('/mon-compte', [\App\Http\Controllers\Front\Auth\AccountController::class, 'index'])->name('front.account');
        Route::post('/mon-compte', [\App\Http\Controllers\Front\Auth\AccountController::class, 'save']);

        // -- Followup
        Route::get('/mon-compte/suivi', [\App\Http\Controllers\Front\Auth\FollowupController::class, 'index'])->name('front.account.followup');

        // -- Documents
        Route::get('/mon-compte/documents', [\App\Http\Controllers\Front\Documents\DocumentsController::class, 'index'])->name('front.account.documents');
        Route::get('/mon-compte/documents/uploader', [\App\Http\Controllers\Front\Documents\DocumentsController::class, 'form'])->name('front.account.documents.form');
        Route::post('/mon-compte/documents/uploader', [\App\Http\Controllers\Front\Documents\DocumentsController::class, 'upload']);

    });

    // -- Diagnostiques
    Route::get('/diagnostiques', 'Front\DiagnosticsController@index')->name('front.diagnostics.index');
    Route::get('/diagnostique/{slug}/retry', 'Front\DiagnosticsController@retry')->name('front.diagnostics.retry');
    Route::get('/diagnostique/{slug}', 'Front\DiagnosticsController@getForm')->name('front.diagnostics.form');
    Route::post('/diagnostique/{slug}', 'Front\DiagnosticsController@save');
    Route::post('/diagnostique/{slug}/register', 'Front\DiagnosticsController@register')->name('front.diagnostics.register');
    Route::post('/diagnostique/{slug}/chart/save', 'Front\DiagnosticsController@saveChart')->name('front.diagnostics.chart.save');

    // -- CMS
    Route::get('/{url?}', 'FrontController@getView')->name('front.page');
    Route::get('/{url}/{reference}/{id_or_slug}', 'FrontController@getModuleDetails')->name('front.page.module.details');

});
