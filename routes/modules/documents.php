<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'can_access:documents'], function() {

    /* Documents */
    Route::resource('documents', '_Modules\Documents\DocumentsController', ['as' => 'admin.modules']);
    Route::post('/documents/search', [\App\Http\Controllers\_Modules\Documents\DocumentsController::class, 'search'])->name('admin.modules.documents.search');
    Route::get('/documents/search/clear', [\App\Http\Controllers\_Modules\Documents\DocumentsController::class, 'clear'])->name('admin.modules.documents.clear');
    Route::get('/documents/{id}/delete', [\App\Http\Controllers\_Modules\Documents\DocumentsController::class, 'delete'])->name('admin.modules.documents.delete');
    Route::get('/documents/{id}/active/toggle', [\App\Http\Controllers\_Modules\Documents\DocumentsController::class, 'toggle'])->name('admin.modules.documents.active.toggle');

});
