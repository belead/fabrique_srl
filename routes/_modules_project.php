<?php
/*
 * CUSTOM PROJECT MODULES
 * */

Route::group(['prefix' => Config::get('_CMS._global.prefix_backend')], function () {
    Route::group(['middleware' => 'admin_auth'], function() {
        Route::group(['prefix' => 'modules'], function() {

            // Diagnostics
            Route::resource('diagnostics', '_Modules\DiagnosticsController', ['as' => 'admin.modules']);
            Route::get('/diagnostics/{id}/delete', '_Modules\DiagnosticsController@delete')->name('admin.modules.diagnostics.delete');
            Route::get('/diagnostics/{id}/active/toggle', '_Modules\DiagnosticsController@toggle')->name('admin.modules.diagnostics.active.toggle');
            Route::get('/diagnostics/{id}/data/export', '_Modules\DiagnosticsController@exportData')->name('admin.modules.diagnostics.data.export');

                // -- Categories
                Route::resource('diagnostics_categories', '_Modules\DiagnosticsCategoriesController', ['as' => 'admin.modules']);
                Route::get('/diagnostics_categories/{id}/delete', '_Modules\DiagnosticsCategoriesController@delete')->name('admin.modules.diagnostics_categories.delete');
                Route::get('/diagnostics_categories/{id}/up', '_Modules\DiagnosticsCategoriesController@up')->name('admin.modules.diagnostics_categories.up');
                Route::get('/diagnostics_categories/{id}/down', '_Modules\DiagnosticsCategoriesController@down')->name('admin.modules.diagnostics_categories.down');

                // -- Questions
                Route::resource('diagnostics_questions', '_Modules\DiagnosticsQuestionsController', ['as' => 'admin.modules']);
                Route::get('/diagnostics_questions/{id}/delete', '_Modules\DiagnosticsQuestionsController@delete')->name('admin.modules.diagnostics_questions.delete');
                Route::get('/diagnostics_questions/{id}/up', '_Modules\DiagnosticsQuestionsController@up')->name('admin.modules.diagnostics_questions.up');
                Route::get('/diagnostics_questions/{id}/down', '_Modules\DiagnosticsQuestionsController@down')->name('admin.modules.diagnostics_questions.down');

            /* Documents */
            include(__DIR__ .  '/modules/documents.php');

        });
    });
});
