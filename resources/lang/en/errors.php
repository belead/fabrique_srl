<?php

return [

    'not_found' => 'This page either does not exist or has been moved/deleted and/or is not active on this server.'

];