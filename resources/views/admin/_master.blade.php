<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('_title'){{config('_CMS._global.name')}}</title>
    <meta name="csrf-token" content="{{csrf_token()}}"/>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/admin/img/favicon.png')}}">

    <!-- CSS Global -->
    {!! \App\Helpers\_CMS\TemplateHelper::styles(true) !!}
    <link href="{{asset('assets/admin/css/remodal.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/css/remodal-default-theme.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/b7dc6524cf.js" crossorigin="anonymous"></script>

    @yield('_styles')
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">
    @stack('_styles')
    @if(Auth::guard('admin')->check())
        <script src="https://cdn.tiny.cloud/1/{{tinymce_key()}}/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script>
            var current_editor = false;
        </script>
    @endif
</head>

<body class="fix-sidebar fix-header">
<div id="wrapper">
    @include('admin.blocks.top_nav')
    @include('admin.blocks.sidebar')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">@yield('_title_section')</h4>
                </div>

                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        @yield('_breadcrumb')
                    </ol>
                </div>
            </div>

            @yield('_content')
        </div>
        @include('admin.blocks.footer')
    </div>
</div>

@include('admin.sections.pages.remodal_medias')
@include('admin.sections.pages.remodal_simple_medias')

<!-- JS Global -->
<script type="text/javascript" src="{{asset('assets/admin/js/jquery.min.js')}}"></script>
{!! \App\Helpers\_CMS\TemplateHelper::scripts(true) !!}

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/quicksearch.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/remodal.js')}}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@include('admin.blocks.tiny_mce')
@include('admin.blocks.scripts')

@yield('_scripts')
@stack('_scripts')
</body>
</html>
