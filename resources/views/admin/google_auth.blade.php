<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('_title'){{config('_CMS._global.name')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/admin/img/favicon.png')}}">

    <!-- CSS Global -->
    {!! \App\Helpers\_CMS\TemplateHelper::styles(true) !!}
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">
</head>

<body>
<section id="wrapper" class="login-register login-google-auth">
    <div class="login-box">
        <div class="white-box">
            <form class="form-horizontal form-material" method="POST" action="{{route('admin.login.google2fa.check')}}">
                {{csrf_field()}}
                <h3 class="box-title">Two-Factor Authentication</h3>

                @if(!is_null($secret))
                    <p>
                        Please scan the QR code with your
                        <br><strong>Google Authenticator</strong> app to confirm your identity.
                    </p>

                    <div style="border: 1px dashed #ddd; text-align: center; margin-bottom: 15px;">
                        @if(strpos($google2fa_url, '<svg') !== false)
                            {!! $google2fa_url !!}
                        @else
                            <img src="{!! $google2fa_url !!}" alt="">
                        @endif
                    </div>
                @else
                    @if(guard_admin()->g2fa_confirmed == 1)
                        <p>Open <strong>Google Authenticator</strong> on your device<br>to get your verification code. <sup>1</sup></p>
                    @else
                        <p>
                            A verification code has been sent to your email address <strong>{{guard_admin()->email}}</strong>
                        </p>
                    @endif
                @endif

                <div class="form-group" style="margin-top: 30px;">
                    <div class="col-xs-12">
                        <input name="google_secret" class="form-control text-center" type="text" required placeholder="XXX XXX" autocomplete="off">
                    </div>
                </div>

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Confirm</button>
                    </div>
                </div>

                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>
                            Download Google Authenticator
                            <br>
                            <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank" class="text-primary m-l-5"><b><i class="fa fa-apple"></i> iOS / Apple</b></a>
                            <br>
                            <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank" class="text-primary m-l-5"><b><i class="fa fa-android"></i> Android</b></a>
                        </p>
                    </div>
                </div>

                @if(guard_admin()->g2fa_confirmed != 2)
                    <div style="margin-top: 30px;">
                        <sup>1</sup> If you lost your app registration, you can <br><a href="{{route('admin.2fa.email.switch')}}">switch to email method</a>
                    </div>
                @else
                    <div style="margin-top: 30px;">
                        Didn't receive your code? You can <a href="{{route('admin.2fa.email.switch')}}">retry</a> the sending.
                    </div>
                @endif
            </form>
        </div>
    </div>
</section>

{!! \App\Helpers\_CMS\TemplateHelper::scripts(true) !!}

@if(session('error'))
    <script>
        $.toast({
            heading: 'Error',
            text: "{{session('error')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'error',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif

</body>
</html>
