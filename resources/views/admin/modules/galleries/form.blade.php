@extends('admin._master')

@section('_title')
    @lang('modules.title_galleries') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.galleries')}}">@lang('modules.title_galleries')</a></li>
    <li class="active">@lang('modules.edit_gallery')</li>
@stop

@section('_title_section')
    @lang('modules.edit_gallery')
@stop

@section('_content')
    @if(isset($gallery->id)) @include('admin.modules.galleries.remodals.upload_images') @endif

    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.galleries')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
            <p class="m-b-0">@lang('modules.gallery_translate_editing', ['lang' => '<strong>' . strtoupper(session('switched_language')) . '</strong>'])</p>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.gallery_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.gallery_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($gallery->id) ? $gallery->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.gallery_label_partial')</label>
                    <div class="col-md-12">
                        <select name="partial_id" class="form-control">
                            <option value="0">-- @lang('modules.gallery_label_partial_select')</option>
                            @foreach($partials as $partial)
                                <option value="{{$partial->id}}" {{isset($gallery->id) && $gallery->partial_id == $partial->id ? 'selected="selected' : ''}}>{{$partial->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.gallery_label_head_title')</label>
                    <div class="col-md-12">
                        <input type="text" name="head_title" class="form-control" value="{{isset($gallery->id) ? $gallery->head_title : old('head_title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.gallery_label_title')</label>
                    <div class="col-md-12">
                        <input type="text" name="title" class="form-control" value="{{isset($gallery->id) ? $gallery->title : old('title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.gallery_label_description')</label>
                    <div class="col-md-12">
                        <textarea name="description" class="form-control editor-cms-small" rows="3">{{isset($gallery->id) ? $gallery->description : old('description')}}</textarea>
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>

            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title m-b-0">@lang('modules.gallery_pictures_title')</h3>
                @if(!isset($gallery->id))
                    <p class="m-b-0">@lang('modules.gallery_save_first')</p>
                @else
                    <div class="col-md-3">
                        <div class="well m-t-10 m-b-0 text-center">
                            <a href="#" data-remodal-target="remodal_upload_images" class="btn btn-rounded btn-success"><i class="ti-upload m-r-5"></i>@lang('modules.gallery_upload_images')</a>
                        </div>
                    </div>
                    <div class="clear"></div>
                    @foreach($gallery->pictures as $picture)
                        <div class="col-md-3">
                            <div class="well m-t-15 m-b-0">
                                <div class="pull-right">#{{$picture->order}}</div>
                                <div class="text-center m-b-15">
                                    @if($picture->order > 1)
                                        <a class="btn btn-circle btn-primary" href="{{route('admin.modules.galleries.picture.up', $picture->id)}}"><i class="fa fa-chevron-left"></i></a>
                                    @endif
                                    @if($picture->order < $gallery->pictures->count())
                                        <a class="btn btn-circle btn-primary" href="{{route('admin.modules.galleries.picture.down', $picture->id)}}"><i class="fa fa-chevron-right"></i></a>
                                    @endif
                                </div>
                                <img src="{{$picture->getPath()}}" class="img m-b-15 h-225">
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="reason_numbers[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.gallery_photo_reason_number_label')" value="{{$picture->reason_number}}">
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="titles[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.gallery_photo_title_label')" value="{{$picture->title}}">
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <textarea name="descriptions[{{$picture->id}}]" class="form-control no-grow" rows="3" placeholder="@lang('modules.gallery_photo_description_label')">{{$picture->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <div class="checkbox checkbox-primary">
                                            <input name="has_more_contents[{{$picture->id}}]" id="check_has_more{{$picture->id}}" type="checkbox" value="1" {{$picture->has_more_content == 1 ? 'checked="checked"' : ''}}>
                                            <label for="check_has_more{{$picture->id}}">@lang('modules.gallery_photo_more_content_label')</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <textarea name="contents[{{$picture->id}}]" class="form-control no-grow editor-cms-small">{{$picture->content}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <div class="checkbox checkbox-primary">
                                            <input name="has_videos[{{$picture->id}}]" id="check_has_video{{$picture->id}}" type="checkbox" value="1" {{$picture->has_video == 1 ? 'checked="checked"' : ''}}>
                                            <label for="check_has_video{{$picture->id}}">@lang('modules.gallery_photo_has_video_label')</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="video_titles[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.gallery_photo_video_title_label')" value="{{$picture->video_title}}">
                                    </div>
                                </div>
                                <div class="form-group m-b-10">
                                    <div class="col-md-12">
                                        <input type="text" name="video_urls[{{$picture->id}}]" class="form-control" placeholder="@lang('modules.gallery_photo_video_url_label')" value="{{$picture->video_url}}">
                                    </div>
                                </div>
                                <button type="submit" class="btn-rounded btn btn-outline btn-success waves-effect waves-light">@lang('backend.update')</button>
                                <a href="{{route('admin.modules.galleries.picture.delete', $picture->id)}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                            </div>
                        </div>
                    @endforeach
                    <div class="clear"></div>
                @endif
            </div>
        </div>
    </form>
@stop

@section('_scripts')
    <script>
        Dropzone.autoDiscover = false;
        $(function() {
            var myDropzone = new Dropzone('#dropzone-galleries', {
                uploadMultiple: true,
                autoProcessQueue: false,
                maxFiles: null,
                parallelUploads: '{{$max_queue_size}}',
                maxFilesize: '{{$max_files_size}}'
            });
            myDropzone.on('completemultiple', function() {
                location.reload();
            });
            myDropzone.on('addedfile', function() {
                $('.processQueue').fadeIn(500);
            });

            $('.processQueue').click(function(e) {
                e.preventDefault();
                myDropzone.processQueue();
            });
        });
    </script>
@stop