@extends('admin._master')

@section('_title')
    @lang('modules.title_team') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.team')}}">@lang('modules.title_team')</a></li>
    <li class="active">@lang('modules.edit_team')</li>
@stop

@section('_title_section')
    @lang('modules.edit_team')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.team')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.team_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($member->id) ? $member->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_slug')</label>
                    <div class="col-md-12">
                        <input type="text" name="slug" class="form-control" value="{{isset($member->id) ? $member->slug : old('slug')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_function')</label>
                    <div class="col-md-12">
                        <input type="text" name="function" class="form-control" value="{{isset($member->id) ? $member->function : old('function')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_quote')</label>
                    <div class="col-md-12">
                        <textarea name="quote" rows="3" class="form-control">{{isset($member->id) ? $member->quote : old('quote')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_biography')</label>
                    <div class="col-md-12">
                        <textarea name="biography" class="editor-cms form-control">{{isset($member->id) ? $member->biography : old('biography')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_image')</label>
                    <div class="col-md-12">
                        @if(isset($member->id) && $member->picture != '')
                            <img src="{{$member->getPicture()}}" class="max-w-300">
                            <div class="checkbox checkbox-danger m-b-5">
                                <input name="delete_picture" id="checkbox_del_picture" type="checkbox" value="1">
                                <label for="checkbox_del_picture">@lang('modules.team_label_image_delete')</label>
                            </div>
                        @endif
                        <input type="file" name="picture" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>

            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.team_contact_info')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_email')</label>
                    <div class="col-md-12">
                        <input type="email" name="email" class="form-control" value="{{isset($member->id) ? $member->email : old('email')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_phone')</label>
                    <div class="col-md-12">
                        <input type="text" name="phone" class="form-control" value="{{isset($member->id) ? $member->phone : old('phone')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_facebook')</label>
                    <div class="col-md-12">
                        <input type="text" name="facebook" class="form-control" value="{{isset($member->id) ? $member->facebook : old('facebook')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_twitter')</label>
                    <div class="col-md-12">
                        <input type="text" name="twitter" class="form-control" value="{{isset($member->id) ? $member->twitter : old('twitter')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_linkedin')</label>
                    <div class="col-md-12">
                        <input type="text" name="linkedin" class="form-control" value="{{isset($member->id) ? $member->linkedin : old('linkedin')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>

            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.team_sorting')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_tags')</label>
                    <div class="col-md-12">
                        <input type="text" name="tags" placeholder="@lang('modules.team_label_add_tags')" data-role="tagsinput" class="form-control" value="{{isset($member->id) ? $member->tags : old('tags')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.team_label_order')</label>
                    <div class="col-md-12">
                        <input type="number" min="0" name="order" class="form-control" value="{{isset($member->id) ? $member->order : old('order')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop