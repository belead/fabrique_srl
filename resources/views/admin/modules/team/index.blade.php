@extends('admin._master')

@section('_title')
    @lang('modules.title_team') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.team')}}">@lang('modules.title_team')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_team')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.team.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('modules.new_team')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($members as $member)
            <div class="col-md-3 col-xs-12 col-sm-6 page-item">
                <div class="white-box">
                    @if(!$member->is_deleted)
                        <a href="{{route('admin.modules.team.active.switch', ['id' => $member->id])}}" type="button" class="switcher-active btn btn-{{$member->is_draft ? 'default' : 'warning'}} btn-circle waves-effect waves-light"><i class="fa fa-lightbulb-o"></i> </a>
                    @endif
                    <div class="user-bg">
                        <div class="overlay-box">
                            <div class="user-content">
                                <img alt="img" class="thumb-lg img-circle" src="{{$member->getPicture()}}">
                                <h4 class="text-white m-t-20 m-b-0 {{$member->is_deleted ? 'is-deleted' : ''}}">
                                    @if($member->is_deleted) <i class="fa fa-trash m-r-5"></i> @endif {{$member->name}}
                                </h4>
                                <h5 class="text-white">{{$member->email}}</h5> </div>
                        </div>
                    </div>
                    <div class="user-btm-box">
                        <div class="stats-row col-md-12 m-t-10 m-b-0 text-center">
                            <div class="stat-item">
                                <b>{{$member->function}}</b>
                                <p>
                                    <strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($member->updated_at))}}
                                </p>
                                @if($member->is_deleted)
                                    <a href="{{route('admin.modules.team.restore', ['id' => $member->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.restore')</a>
                                    <a href="{{route('admin.modules.team.destroy', ['id' => $member->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.destroy')</a>
                                @else
                                    <a href="{{route('admin.modules.team.edit', ['id' => $member->id])}}" class="m-r-5 btn btn-success btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.edit')</a>
                                    <a href="{{route('admin.modules.team.delete', ['id' => $member->id])}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light m-t-20">@lang('backend.delete')</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            {{$members->render()}}
        </div>
    </div>
@stop