@extends('admin._master')

@section('_title')
    Documents -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.documents.index')}}">Documents</a></li>
    <li class="active">Ajouter/Modifier un document</li>
@stop

@section('_title_section')
    Ajouter/Modifier un document
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.documents.index')}}" class="btn btn-danger btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" enctype="multipart/form-data"
          action="{{$document->id ? route('admin.modules.documents.update', $document->id) : route('admin.modules.documents.store')}}">

        @csrf
        @if($document->id) @method('PUT') @endif

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Attributs du document</h3>

                <div class="form-group">
                    <label>Nom *</label>
                    <input required type="text" name="name" class="form-control" value="{{$document->name ?? old('name')}}">
                </div>

                <div class="form-group">
                    <label>Description / Instructions</label>
                    <textarea name="description" class="form-control" rows="3">{{$document->description ?? old('description')}}</textarea>
                </div>

                <div class="form-group">
                    <label for="">Fichier {{!$document->id ? '*' : ''}}</label>
                    <input {{!$document->id ? 'required' : ''}} type="file" name="file" class="form-control">
                </div>

                @if($document->file)
                    <div class="form-group">
                        <p>
                            Fichier actuel : <a href="{{$document->getFile()}}">{{$document->file}}</a>
                        </p>
                        <div class="checkbox checkbox-danger m-b-5">
                            <input name="delete_file" id="checkbox_del_file" type="checkbox" value="1">
                            <label for="checkbox_del_file">Supprimer le fichier</label>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <label>Utilisateur</label>
                    <select name="users[]" class="form-control select-2">
                        <option value="">-- Choisir un utilisateur</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}" {{in_array($user->id, $document->users->modelKeys()) ? 'selected' : ''}}>
                                {{$user->getName()}} ({{$user->email}})
                            </option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop
