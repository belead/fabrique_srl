@extends('admin._master')

@section('_title')
    Documents -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.documents.index')}}">Documents</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    Documents
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.documents.create')}}" class="btn btn-success btn-rounded waves-effect waves-light m-r-5">
                            Ajouter un document
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.blocks.filters', ['_filters' => 'admin.modules.documents.blocks._filters'])

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table m-b-0">
                    <thead>
                        <tr>
                            <th width="50"></th>
                            <th>Nom</th>
                            <th>Fichier</th>
                            <th>Utilisateur</th>
                            <th>Créé le</th>
                            <th style="text-align: center;">Ajouté par l'utilisateur</th>
                            <th style="text-align: right;">@lang('backend.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($documents->count() == 0)
                            <tr>
                                <td colspan="6">Pas de document trouvé</td>
                            </tr>
                        @endif

                        @foreach($documents as $document)
                            <tr>
                                <td>
                                    <a href="{{route('admin.modules.documents.active.toggle', $document->id)}}"
                                       data-toggle="tooltip"
                                       data-html="true"
                                       title="Envoie un email<br>à l'activation"
                                       class="btn btn-{{$document->is_active ? 'warning' : 'default'}} btn-circle waves-effect waves-light">
                                        <i class="fa fa-lightbulb-o"></i>
                                    </a>
                                </td>
                                <td>
                                    <i class="{{$module->icon ?? ''}} m-r-5"></i>
                                    <a href="{{route('admin.modules.documents.edit', $document)}}">{{$document->name}}</a>
                                </td>
                                <td>
                                    <a target="_blank" href="{{$document->getFile()}}">{{$document->file}}</a>
                                </td>
                                <td>
                                    @foreach($document->users as $user)
                                        <a href="{{route('admin.users.edit', ['id' => $user->id])}}">{{$user->getName()}}</a>
                                    @endforeach
                                </td>
                                <td>{{$document->created_at->format('d/m/Y H:i')}}</td>
                                <td style="text-align: center;">
                                    @if($document->by_user_id)
                                        <i class="fa fa-check"></i>
                                    @endif
                                </td>
                                <td style="text-align: right;">
                                    <a href="{{route('admin.modules.documents.edit', $document)}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    <a href="{{route('admin.modules.documents.delete', $document->id)}}" class="_ask btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{$documents->links()}}
            </div>
        </div>
    </div>
@stop
