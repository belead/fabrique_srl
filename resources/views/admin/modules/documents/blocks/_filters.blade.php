<div class="form-group" style="width: 20%;">
    <label for="">Par utilisateur</label>
    <select name="user_id" class="form-control select-2">
        <option value="">-- Tous</option>
        @foreach($users as $user)
            <option value="{{$user->id}}" {{session('search_documents.user_id') == $user->id ? 'selected' : ''}}>{{$user->getName()}}</option>
        @endforeach
    </select>
</div>
