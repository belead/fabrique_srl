@extends('admin._master')

@section('_title')
    Diagnostiques -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.diagnostics.index')}}">Diagnostiques</a></li>
    <li class="active">Créer/Modifier un diagnostique</li>
@stop

@section('_title_section')
    Créer/Modifier un diagnostique
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.diagnostics.index')}}" class="btn btn-danger btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="{{$diagnostic->id ? route('admin.modules.diagnostics.update', $diagnostic->id) : route('admin.modules.diagnostics.store')}}" enctype="multipart/form-data">
        @csrf
        @if($diagnostic->id) @method('PUT') @endif

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Attributs du diagnostique</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Titre *</label>
                            <input required type="text" name="title" class="form-control" value="{{$diagnostic->title ?? old('title')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Sous-titre </label>
                            <input type="text" name="subtitle" class="form-control" value="{{$diagnostic->subtitle ?? old('subtitle')}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Slug (automatique)</label>
                            <input type="text" name="slug" class="form-control" value="{{$diagnostic->slug ?? old('slug')}}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="description" class="form-control" rows="4">{{$diagnostic->description ?? old('description')}}</textarea>
                </div>

                <div class="form-group">
                    <label for="">Image</label>
                    <input type="file" name="picture" class="form-control">
                </div>

                @if($diagnostic->picture)
                    <img src="{{asset('/uploads/img/modules/diagnostics/' . $diagnostic->picture)}}" style="max-width: 200px;">
                    <div class="checkbox checkbox-danger m-b-5">
                        <input name="delete_picture" id="checkbox_del_picture" type="checkbox" value="1">
                        <label for="checkbox_del_picture">@lang('modules.article_label_image_delete')</label>
                    </div>
                @endif

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>

    @if($diagnostic->id)
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Catégories du diagnostique</h3>

                <table class="table m-b-0">
                    <thead>
                        <tr>
                            <th width="60"></th>
                            <th width="50">#</th>
                            <th>Nom</th>
                            <th>Questions</th>
                            <th style="text-align: right;">
                                <a data-remodal-target="_newCategory" href="#" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($diagnostic->categories->count() == 0)
                            <tr>
                                <td colspan="5">Aucune catégorie pour ce diagnostique.</td>
                            </tr>
                        @endif

                        @foreach($diagnostic->categories as $category)
                            <tr>
                                <td>
                                    @if($category->position > 1)
                                        <a class="btn btn-circle btn-primary" href="{{route('admin.modules.diagnostics_categories.up', $category->id)}}"><i class="fa fa-chevron-up"></i></a>
                                    @endif
                                    @if($category->position < $diagnostic->categories->count())
                                        <a class="btn btn-circle btn-primary" href="{{route('admin.modules.diagnostics_categories.down', $category->id)}}" style="margin-top: {{$category->position > 1 ? '2px' : '0'}};"><i class="fa fa-chevron-down"></i></a>
                                    @endif
                                </td>
                                <td>{{$category->position}}</td>
                                <td>
                                    <i class="fa fa-circle" style="color: {{$category->color ?? 'inherit'}}; margin-right: 5px;"></i>
                                    {{$category->name}}
                                </td>
                                <td>
                                    @if($category->questions->count() == 0)
                                        -
                                    @else
                                        <ul style="margin: 0; padding: 0; list-style: none;">
                                            @foreach($category->questions as $question)
                                                <li>
                                                    <strong>{{$question->position}}.</strong> {{$question->question}} ({{$question->points_weighting}} pt{{$question->points_weighting > 1 ? 's' : ''}}.)
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </td>
                                <td style="text-align: right;">
                                    <a href="{{route('admin.modules.diagnostics_categories.edit', $category)}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    <a href="{{route('admin.modules.diagnostics_categories.delete', $category->id)}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        @includeIf('admin.modules.diagnostics.blocks.remodal_new_category')
    @endif
@stop
