@extends('admin._master')

@section('_title')
    Diagnostiques -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.diagnostics.index')}}">Diagnostiques</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    Diagnostiques
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.modules.diagnostics.create')}}" class="btn btn-success btn-rounded waves-effect waves-light">
                            Créer un nouveau diagnostique
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table m-b-0">
                    <thead>
                        <tr>
                            <th width="50"></th>
                            <th>Titre</th>
                            <th>Slug</th>
                            <th style="text-align: right;">@lang('backend.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($diagnostics as $diagnostic)
                            <tr>
                                <td>
                                    <a href="{{route('admin.modules.diagnostics.active.toggle', $diagnostic->id)}}" class="btn btn-{{$diagnostic->is_active ? 'warning' : 'default'}} btn-circle waves-effect waves-light"><i class="fa fa-lightbulb-o"></i> </a>
                                </td>
                                <td><a class="link-u" href="{{route('admin.modules.diagnostics.edit', $diagnostic)}}">{{$diagnostic->title}}</a></td>
                                <td>{{$diagnostic->getSlug()}}</td>
                                <td style="text-align: right;">
                                    <a href="{{route('admin.modules.diagnostics.data.export', ['id' => $diagnostic->id])}}" class="btn btn-primary btn-rounded waves-effect waves-light btn-xs">Exporter les données</a>
{{--                                    <a target="_blank" href="{{route('front.diagnostics.form', $diagnostic->getSlug())}}" class="btn btn-primary btn-outline btn-rounded waves-effect waves-light">Voir</a>--}}
                                    <a href="{{route('admin.modules.diagnostics.edit', $diagnostic)}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    <a href="{{route('admin.modules.diagnostics.delete', $diagnostic->id)}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{$diagnostics->links()}}
            </div>
        </div>
    </div>
@stop
