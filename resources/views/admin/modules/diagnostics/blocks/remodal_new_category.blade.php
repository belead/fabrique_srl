<div class="remodal" data-remodal-id="_newCategory">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Ajouter une catégorie</h1>

    <form method="POST" action="{{route('admin.modules.diagnostics_categories.store')}}">
        @csrf
        <input type="hidden" name="diagnostic_id" value="{{$diagnostic->id}}">

        <div class="text-left" style="margin-top: 15px;">
            <div class="form-group">
                <label for="">Nom *</label>
                <input required type="text" name="name" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Couleur</label>
                <input type="text" name="color" class="form-control colorpicker" autocomplete="off">
            </div>

            <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            <button data-remodal-action="cancel" class="btn-rounded btn btn-danger waves-effect waves-light">Annuler</button>
        </div>
    </form>
</div>
