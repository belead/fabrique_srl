<div class="remodal" data-remodal-id="_newQuestion" id="_newQuestion" style="text-align: left;">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Ajouter une question</h1>

    <form method="POST" action="{{route('admin.modules.diagnostics_questions.store')}}">
        @csrf
        <input type="hidden" name="diagnostic_id" value="{{$category->diagnostic_id}}">
        <input type="hidden" name="category_id" value="{{$category->id}}">

        <div class="text-left" style="margin-top: 15px;">
            <div class="form-group">
                <label for="">Question *</label>
                <input required type="text" name="question" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Pondération *</label>
                <input required type="number" min="0" step="1" name="points_weighting" class="form-control" value="1">
            </div>

            <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            <button data-remodal-action="cancel" class="btn-rounded btn btn-danger waves-effect waves-light">Annuler</button>
        </div>
    </form>
</div>
