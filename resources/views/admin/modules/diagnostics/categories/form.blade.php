@extends('admin._master')

@section('_title')
    Diagnostiques -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.diagnostics.index')}}">Diagnostiques</a></li>
    <li><a href="{{route('admin.modules.diagnostics.edit', $category->diagnostic_id)}}">{{$category->diagnostic->title ?? ''}}</a></li>
    <li class="active">Modifier une catégorie</li>
@stop

@section('_title_section')
    Modifier une catégorie
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.diagnostics.edit', $category->diagnostic_id)}}" class="btn btn-danger btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="{{$category->id ? route('admin.modules.diagnostics_categories.update', $category->id) : route('admin.modules.diagnostics_categories.store')}}" enctype="multipart/form-data">
        @csrf
        @if($category->id) @method('PUT') @endif

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Attributs de la catégorie</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nom *</label>
                            <input required type="text" name="name" class="form-control" value="{{$category->name ?? old('name')}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Couleur</label>
                            <input type="text" name="color" class="form-control colorpicker" value="{{$category->color ?? old('color')}}" autocomplete="off">
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>

    <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
            <h3 class="box-title">Questions de la catégorie</h3>

            <table class="table m-b-0">
                <thead>
                    <tr>
                        <th width="60"></th>
                        <th width="50">#</th>
                        <th>Question</th>
                        <th>Pondération</th>
                        <th style="text-align: right;">
                            <a data-remodal-target="_newQuestion" href="#" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @if($category->questions->count() == 0)
                        <tr>
                            <td colspan="5">Aucune question pour cette catégorie.</td>
                        </tr>
                    @endif

                    @foreach($category->questions as $question)
                        <tr>
                            <td>
                                @if($question->position > 1)
                                    <a class="btn btn-circle btn-primary" href="{{route('admin.modules.diagnostics_questions.up', $question->id)}}"><i class="fa fa-chevron-up"></i></a>
                                @endif
                                @if($question->position < $category->questions->count())
                                    <a class="btn btn-circle btn-primary" href="{{route('admin.modules.diagnostics_questions.down', $question->id)}}" style="margin-top: {{$question->position > 1 ? '2px' : '0'}};"><i class="fa fa-chevron-down"></i></a>
                                @endif
                            </td>
                            <td>{{$question->position}}</td>
                            <td>
                                {{$question->question}}
                            </td>
                            <td>
                                {{$question->points_weighting}} pt{{$question->points_weighting > 1 ? 's' : ''}}
                            </td>
                            <td style="text-align: right;">
                                <a href="{{route('admin.modules.diagnostics_questions.edit', $question)}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                <a href="{{route('admin.modules.diagnostics_questions.delete', $question->id)}}" class="askDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @includeIf('admin.modules.diagnostics.blocks.remodal_new_question')
@stop

@push('_scripts')
    <script>
        $('.select-2').select2({
            width: '100%',
            dropdownParent: $('#_newQuestion')
        });
    </script>
@endpush
