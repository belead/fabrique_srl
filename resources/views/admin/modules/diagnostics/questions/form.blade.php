@extends('admin._master')

@section('_title')
    Diagnostiques -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.diagnostics.index')}}">Diagnostiques</a></li>
    <li><a href="{{route('admin.modules.diagnostics.edit', $question->diagnostic_id)}}">{{$question->diagnostic->title ?? ''}}</a></li>
    <li><a href="{{route('admin.modules.diagnostics_categories.edit', $question->category_id)}}">{{$question->category->name ?? ''}}</a></li>
    <li class="active">Modifier une question</li>
@stop

@section('_title_section')
    Modifier une question
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.diagnostics_categories.edit', $question->category_id)}}" class="btn btn-danger btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="{{$question->id ? route('admin.modules.diagnostics_questions.update', $question->id) : route('admin.modules.diagnostics_questions.store')}}" enctype="multipart/form-data">
        @csrf
        @if($question->id) @method('PUT') @endif

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Attributs de la question</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Question *</label>
                            <input required type="text" name="question" class="form-control" value="{{$question->question ?? old('question')}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Pondération *</label>
                            <input required type="number" min="0" step="1" name="points_weighting" class="form-control" value="{{$question->points_weighting ?? old('points_weighting')}}">
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop

@push('_scripts')
    <script>
        $('.select-2').select2({
            width: '100%',
        });
    </script>
@endpush
