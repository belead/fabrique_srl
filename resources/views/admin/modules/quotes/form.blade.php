@extends('admin._master')

@section('_title')
    @lang('modules.title_quotes') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.quotes')}}">@lang('modules.title_quotes')</a></li>
    <li class="active">@lang('modules.edit_quote')</li>
@stop

@section('_title_section')
    @lang('modules.edit_quote')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.quotes')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.quote_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_name_label')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($quote->id) ? $quote->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>@lang('modules.quote_partial_label')</label>
                        <select required name="partial_id" class="form-control">
                            <option value="0">-- @lang('modules.quote_label_partial_select')</option>
                            @foreach($partials as $partial)
                                <option value="{{$partial->id}}" {{isset($quote->id) && $quote->partial_id == $partial->id ? 'selected="selected' : ''}}>{{$partial->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_head_title_label')</label>
                    <div class="col-md-12">
                        <input required type="text" name="head_title" class="form-control" value="{{isset($quote->id) ? $quote->head_title : old('head_title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_title_label')</label>
                    <div class="col-md-12">
                        <input required type="text" name="title" class="form-control" value="{{isset($quote->id) ? $quote->title : old('title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_description_label')</label>
                    <div class="col-md-12">
                        <textarea name="description" class="form-control editor-cms-small" rows="4">{{isset($quote->id) ? $quote->description : old('description')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_image_label')</label>
                    <div class="col-md-12">
                        @if(isset($quote->id) && $quote->image != '')
                            <img src="{{asset('/uploads/img/modules/quotes/' . $quote->image)}}" class="max-w-300 m-b-15">
                        @endif
                        <input type="file" name="image" class="form-control" {{isset($quote->id) ? '' : 'required'}}>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_value_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="quote" class="form-control" value="{{isset($quote->id) ? $quote->quote : old('quote')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_author_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="author" class="form-control" value="{{isset($quote->id) ? $quote->author : old('author')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.quote_author_function_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="function" class="form-control" value="{{isset($quote->id) ? $quote->function : old('function')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop