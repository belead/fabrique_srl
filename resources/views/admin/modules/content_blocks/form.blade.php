@extends('admin._master')

@section('_title')
    @lang('modules.title_content_blocks') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.content_blocks')}}">@lang('modules.title_content_blocks')</a></li>
    <li class="active">@lang('modules.edit_block')</li>
@stop

@section('_title_section')
    @lang('modules.edit_block')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.content_blocks')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.content_block_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_name_label')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($block->id) ? $block->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>@lang('modules.content_block_partial_label')</label>
                        <select required name="partial_id" class="form-control">
                            <option value="0">-- @lang('modules.content_block_label_partial_select')</option>
                            @foreach($partials as $partial)
                                <option value="{{$partial->id}}" {{isset($block->id) && $block->partial_id == $partial->id ? 'selected="selected' : ''}}>{{$partial->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_head_title_label')</label>
                    <div class="col-md-12">
                        <input required type="text" name="head_title" class="form-control" value="{{isset($block->id) ? $block->head_title : old('head_title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_title_label')</label>
                    <div class="col-md-12">
                        <input required type="text" name="title" class="form-control" value="{{isset($block->id) ? $block->title : old('title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_description_label')</label>
                    <div class="col-md-12">
                        <textarea name="description" class="form-control editor-cms-small" rows="4">{{isset($block->id) ? $block->description : old('description')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_image_label')</label>
                    <div class="col-md-12">
                        @if(isset($block->id) && $block->image != '')
                            <img src="{{asset('/uploads/img/modules/content_blocks/' . $block->image)}}" class="max-w-300 m-b-15">
                        @endif
                        <input type="file" name="image" class="form-control" {{isset($block->id) ? '' : 'required'}}>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_call_action_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="call_to_action_text" class="form-control" value="{{isset($block->id) ? $block->call_to_action_text : old('call_to_action_text')}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>@lang('modules.content_block_page_label')</label>
                        <select name="page_id" class="form-control">
                            <option value="0">-- @lang('modules.content_block_label_page_select')</option>
                            @foreach($pages as $page)
                                <option value="{{$page->id}}" {{isset($block->id) && $block->page_id == $page->id ? 'selected="selected' : ''}}>{{$page->attributes->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_custom_url_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="custom_url" class="form-control" value="{{isset($block->id) ? $block->custom_url : old('custom_url')}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary">
                            <input name="is_video" id="checkbox_video" type="checkbox" value="1" {{isset($block->id) && $block->is_video == 1 ? 'checked="checked"' : ''}}>
                            <label for="checkbox_video">@lang('modules.content_block_is_video')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_video_title_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="video_title" class="form-control" value="{{isset($block->id) ? $block->video_title : old('video_title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_video_url_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="video_url" class="form-control" value="{{isset($block->id) ? $block->video_url : old('video_url')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.content_block_video_time_label')</label>
                    <div class="col-md-12">
                        <input type="text" name="video_time" class="form-control" value="{{isset($block->id) ? $block->video_time : old('video_time')}}">
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop