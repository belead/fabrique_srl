@extends('admin._master')

@section('_title')
    @lang('modules.title_banners') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li>@lang('backend.title_modules')</li>
    <li><a href="{{route('admin.modules.banners')}}">@lang('modules.title_banners')</a></li>
    <li class="active">@lang('modules.edit_banner')</li>
@stop

@section('_title_section')
    @lang('modules.edit_banner')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules.banners')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.content_banner_attributes')</h3>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.banner_name_label')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{isset($banner->id) ? $banner->name : old('name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>@lang('modules.banner_partial_label')</label>
                        <select name="partial_id" class="form-control">
                            <option value="0">-- @lang('modules.banner_partial_select_label')</option>
                            @foreach($partials as $partial)
                                <option value="{{$partial->id}}" {{isset($banner->id) && $banner->partial_id == $partial->id ? 'selected="selected"' : ''}}>{{$partial->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('modules.banner_image_label')</label>
                    <div class="col-md-12">
                        @if(isset($banner->id) && $banner->image != '')
                            <img src="{{asset('/uploads/img/modules/banners/' . $banner->image)}}" class="max-w-300 m-b-15">
                        @endif
                        <input type="file" name="image" class="form-control" {{isset($banner->id) ? '' : 'required'}}>
                    </div>
                </div>
                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        @if(isset($banner->id))
            <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">
                    <h3 class="box-title m-b-0">@lang('modules.content_banner_links')</h3>
                    @if($banner->links->count() == 0)
                        <p>@lang('modules.banner_links_none')</p>
                    @endif
                    <table class="table m-b-15" id="banner_table">
                        <thead>
                            <th>@lang('modules.banner_call_action_text_label')</th>
                            <th>@lang('modules.banner_call_action_type_label')</th>
                            <th>@lang('modules.banner_call_action_page_label')</th>
                            <th>@lang('modules.banner_call_action_custom_label')</th>
                            <th>@lang('modules.banner_call_action_video_label')</th>
                            <th></th>
                            <th><a href="{{href_none()}}" class="btnAddLinkBanner btn btn-circle btn-success"><i class="ti-plus"></i></a></th>
                        </thead>
                        <tbody>
                            @foreach($banner->links as $key => $link)
                                <tr>
                                    <td>
                                        <input type="text" name="banner_call_to_action_texts[]" class="form-control" value="{{$link->call_to_action_text}}">
                                    </td>
                                    <td>
                                        <select name="banner_types[]" class="form-control">
                                            <option value="in" {{$link->type == 'in' ? 'selected="selected"' : ''}}>@lang('modules.banner_in_type')</option>
                                            <option value="out" {{$link->type == 'out' ? 'selected="selected"' : ''}}>@lang('modules.banner_out_type')</option>
                                            <option value="video" {{$link->type == 'video' ? 'selected="selected"' : ''}}>@lang('modules.banner_video_type')</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="banner_page_ids[]" class="form-control">
                                            <option value="0">-- @lang('backend.input_select_page')</option>
                                            @foreach($pages as $page)
                                                <option value="{{$page->id}}" {{$link->page_id == $page->id ? 'selected="selected"' : ''}}>{{$page->attributes->title}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="banner_custom_urls[]" class="form-control" value="{{$link->custom_url}}">
                                    </td>
                                    <td colspan="2">
                                        <input type="text" name="banner_video_urls[]" class="form-control" value="{{$link->video_url}}">
                                    </td>
                                    <td>
                                        <a href="{{href_none()}}" class="btnDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                </div>
            </div>
        @endif
    </form>

    <table style="display: none;">
        <tr id="banner_row_model">
            <td>
                <input type="text" name="banner_call_to_action_texts[]" class="form-control">
            </td>
            <td>
                <select name="banner_types[]" class="form-control">
                    <option value="in">@lang('modules.banner_in_type')</option>
                    <option value="out">@lang('modules.banner_out_type')</option>
                    <option value="video">@lang('modules.banner_video_type')</option>
                </select>
            </td>
            <td>
                <select name="banner_page_ids[]" class="form-control">
                    <option value="0">-- @lang('backend.input_select_page')</option>
                    @foreach($pages as $page)
                        <option value="{{$page->id}}">{{$page->attributes->title}}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <input type="text" name="banner_custom_urls[]" class="form-control">
            </td>
            <td colspan="2">
                <input type="text" name="banner_video_urls[]" class="form-control">
            </td>
            <td>
                <a href="{{href_none()}}" class="btnDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">Delete</a>
            </td>
        </tr>
    </table>
@stop

@section('_scripts')
    <script>
        $('.btnAddLinkBanner').click(function(e) {
            e.preventDefault();
            var $element = $('#banner_row_model').clone();
            $element.attr('id', '');
            $('#banner_table').append($element);
        });
        $('#banner_table').on('click', '.btnDelete', function(e) {
           $(this).parent().parent().remove();
        });
    </script>
@stop