<input type="hidden" name="action_url" value="_Modules\BannersController">
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.content_block_partial_label')</label>
        <select name="block_partial_id" class="form-control">
            @foreach($module->partials as $partial)
                <option value="{{$partial->id}}">{{$partial->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.banner_name_label')</label>
        <input type="text" name="block_name" class="form-control" placeholder="@lang('modules.banner_name_label')">
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.banner_image_label')</label>
        <input type="file" name="block_image" class="form-control">
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label>@lang('modules.banner_links')</label>
        <table class="table m-b-0" id="banner_table">
            <thead>
                <th>@lang('modules.banner_call_action_text_label')</th>
                <th>@lang('modules.banner_call_action_type_label')</th>
                <th>@lang('modules.banner_call_action_page_label')</th>
                <th>@lang('modules.banner_call_action_custom_label')</th>
                <th>@lang('modules.banner_call_action_video_label')</th>
                <th></th>
                <th style="text-align: center;"><a href="{{href_none()}}" class="btnAddLinkBanner btn btn-circle btn-success"><i class="ti-plus"></i></a></th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" name="banner_call_to_action_texts[]" class="form-control">
                    </td>
                    <td>
                        <select name="banner_types[]" class="form-control">
                            <option value="in">@lang('modules.banner_in_type')</option>
                            <option value="out">@lang('modules.banner_out_type')</option>
                            <option value="video">@lang('modules.banner_video_type')</option>
                        </select>
                    </td>
                    <td>
                        <select name="banner_page_ids[]" class="form-control">
                            <option value="0">-- @lang('backend.input_select_page')</option>
                            @foreach($pages as $page)
                                <option value="{{$page->id}}">{{$page->attributes->title}}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="text" name="banner_custom_urls[]" class="form-control">
                    </td>
                    <td colspan="3">
                        <input type="text" name="banner_video_urls[]" class="form-control">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@section('_scripts')
    @parent
    <script>
        $('.btnAddLinkBanner').click(function(e) {
            e.preventDefault();
            var $element = $('#banner_row_model').clone();
            $element.attr('id', '');
            $('#banner_table').append($element);
        });
        $('#banner_table').on('click', '.btnDelete', function(e) {
            $(this).parent().parent().remove();
        });
    </script>
@stop

@section('_content')
    @parent
    <table style="display: none;">
        <tr id="banner_row_model">
            <td>
                <input type="text" name="banner_call_to_action_texts[]" class="form-control">
            </td>
            <td>
                <select name="banner_types[]" class="form-control">
                    <option value="in">@lang('modules.banner_in_type')</option>
                    <option value="out">@lang('modules.banner_out_type')</option>
                    <option value="video">@lang('modules.banner_video_type')</option>
                </select>
            </td>
            <td>
                <select name="banner_page_ids[]" class="form-control">
                    <option value="0">-- @lang('backend.input_select_page')</option>
                    @foreach($pages as $page)
                        <option value="{{$page->id}}">{{$page->attributes->title}}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <input type="text" name="banner_custom_urls[]" class="form-control">
            </td>
            <td colspan="2">
                <input type="text" name="banner_video_urls[]" class="form-control">
            </td>
            <td style="text-align: center;">
                <a href="{{href_none()}}" class="btnDelete btn btn-danger btn-outline btn-rounded waves-effect waves-light">Delete</a>
            </td>
        </tr>
    </table>
@stop