<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-wrapper collapse in">
                <div class="panel-body _filters_form">
                    <form method="POST" class="form-inline" action="{{route('admin.modules.' . str_replace('module_', '', $dash_active) . '.search')}}">
                        @csrf

                        <div class="form-group" style="width: 20%;">
                            <label for="">Par {{$options['default_by'] ?? 'nom'}}</label>
                            <input type="text" name="{{$options['default_name'] ?? 'name'}}"
                                   class="form-control"
                                   placeholder="{{$options['default_placeholder'] ?? 'Par nom'}}"
                                   value="{{session('search_' . str_replace('module_', '', $dash_active) . '.' . ($options['default_name'] ?? 'name'))}}">
                        </div>

                        @includeIf($_filters ?? 'undefined')

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-rounded waves-effect waves-light m-t-20">Filtrer</button>
                            <a href="{{route('admin.modules.' . str_replace('module_', '', $dash_active) . '.clear')}}" class="btn btn-default btn-rounded waves-effect waves-light m-t-20">Annuler</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
