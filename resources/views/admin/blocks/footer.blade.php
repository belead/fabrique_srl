<footer class="footer text-center"> {{date('Y')}} © {{Config::get('_CMS._global.name')}} {{!is_null($configuration) ? $configuration->getCMSVersion() : '*'}} - Laravel {{App::VERSION()}}</footer>
