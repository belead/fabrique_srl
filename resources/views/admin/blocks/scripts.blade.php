<script type="text/javascript">
    $('._ask').click(function(e) {
        e.preventDefault();

        let defaultEvent = $(this).attr('href');
        let title = $(this).attr('data-title');
        let type = $(this).attr('data-type');

        swal({
            title: title ?? 'You sure you want to continue?',
            type: type ?? 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Yes',
            closeOnConfirm: false
        }, function() {
            document.location.href = defaultEvent;
            return true;
        });
    });

    $('.select-2').select2({width: '100%'});
    $('.select-2-tags').select2({width: '100%', tags: true});
    $('.datepicker-cms').datepicker();
    $('.multi-select').multiSelect();
    $('.color-picker').colorpicker();
</script>

@if(session('success'))
    <script>
        $.toast({
            heading: 'Success',
            text: "{{session('success')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif

@if(session('error'))
    <script>
        $.toast({
            heading: 'Error',
            text: "{{session('error')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'error',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif

@if(session('warning'))
    <script>
        $.toast({
            heading: 'Warning',
            text: "{{session('warning')}}",
            position: 'top-right',
            loaderBg:'#4F5467',
            icon: 'warning',
            hideAfter: 3500,
            stack: 6
        });
    </script>
@endif
