@if(Auth::guard('admin')->check())
    <script>
        tinymce.PluginManager.add('belead_media_manager', function (editor, url) {
            editor.ui.registry.addButton('belead_media_manager', {
                icon: 'upload',
                tooltip: 'Medias library',
                onAction: function () {
                    var inst = $('[data-remodal-id=_seeMedia]').remodal();
                    current_editor = editor;
                    inst.open();
                }
            });
        });
    </script>

    <script>
        var editor = tinymce.init({
            selector: '.editor-cms',
            height: 400,
            relative_urls : false,
            remove_script_host : true,
            document_base_url : '{{env('APP_URL')}}',
            //menubar: false,
            menubar: 'view',
            plugins: [
                'advlist autolink lists link image belead_media_manager charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code visualblocks textcolor colorpicker emoticons'
            ],
            toolbar: 'undo redo | insert | styleselect forecolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | belead_media_manager code visualblocks',
            link_list: '{{route('admin.pages.links')}}',
            content_css: '{{asset('assets/admin/css/editors.css')}}',
            style_formats: [
                { title: 'La Fabrique à SRL', items: [
                        { title: 'Boite Verte', selector: 'div,p', block: 'div', classes: ['editor-box-green', 'editor-box'] },
                        { title: 'Boite Orange', selector: 'div,p', block: 'div', classes: ['editor-box-orange', 'editor-box'] },
                        { title: 'Boite Bleue', selector: 'div,p', block: 'div', classes: ['editor-box-blue', 'editor-box'] },
                        { title: 'Citation Grise', selector: 'div,p', block: 'div', classes: ['editor-quote-grey', 'editor-quote'] },
                        { title: 'Citation Orange', selector: 'div,p', block: 'div', classes: ['editor-quote-orange', 'editor-quote'] },
                        { title: 'Citation Bleue', selector: 'div,p', block: 'div', classes: ['editor-quote-blue', 'editor-quote'] },
                        { title: 'Texte Orange', inline: 'span', classes: ['editor-text-orange'] },
                        { title: 'Texte Bleu', inline: 'span', classes: ['editor-text-blue'] }
                    ]}
            ],
            style_formats_merge: true,
            setup: function(ed) {
                ed.on('Dblclick', function () {
                    ed.setContent(ed.getContent() + '<p></p>');
                });
            }
        });

        var editor_small = tinymce.init({
            selector: '.editor-cms-small',
            height: 180,
            relative_urls : false,
            remove_script_host : true,
            document_base_url : '{{env('APP_URL')}}',
            //menubar: false,
            menubar: 'view',
            plugins: [
                'advlist autolink lists link image belead_media_manager charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code visualblocks'
            ],
            toolbar: 'undo redo | insert | styleselect forecolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | belead_media_manager code visualblocks',
            link_list: '{{route('admin.pages.links')}}',
            content_css : '{{asset('assets/admin/css/editors.css')}}',
            setup: function(ed) {
                ed.on('Dblclick', function () {
                    ed.setContent(ed.getContent() + '<p></p>');
                });
            }
        });

        $('body').on('click', '._addToEditor', function(e) {
            e.preventDefault();
            let path = $(this).attr('data-path');
            let ext = $(this).attr('data-ext');

            let images = ['jpg', 'png', 'jpeg', 'gif'];
            if(images.includes(ext)) {
                current_editor.insertContent('<img src=' + path + '>');
            } else {
                current_editor.insertContent('<a target="_blank" href="' + path + '">Download file</a>');
            }
        });
    </script>
@endif
