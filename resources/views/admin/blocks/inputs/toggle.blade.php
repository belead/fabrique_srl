<div class="form-group" style="{{$style ?? ''}}">
    <label class="col-md-12" for="">
        {{$label}}
    </label>

    <div class="col-md-12">
        <div class="d-flex align-items-center">
            <div class="radio radio-success radio-inline mr-4">
                <input type="radio" name="{{$name}}" id="{{$name}}1" value="1" {{$item->$name ? 'checked' : ''}}>
                <label for="{{$name}}1">{{$label_yes ?? 'Oui'}}</label>
            </div>

            <div class="radio radio-danger radio-inline">
                <input type="radio" name="{{$name}}" id="{{$name}}0" value="0" {{!$item->$name ? 'checked' : ''}}>
                <label for="{{$name}}0">{{$label_no ?? 'Non'}}</label>
            </div>
        </div>
    </div>
</div>
