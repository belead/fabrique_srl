@extends('admin._master')

@section('_title')
    Administration -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_dashboard')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-4">
            <a href="{{route('admin.users')}}">
                <div class="white-box">
                    <h3 class="box-title color-belead">@lang('backend.users')</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-people color-belead"></i></li>
                        <li class="text-right"><span class="counter color-belead">{{$counters->users}}</span></li>
                    </ul>
                </div>
            </a>
        </div>

        @if(guard_admin()->level > 97)
            <div class="col-md-4">
                <a href="{{route('admin.operators')}}">
                    <div class="white-box">
                        <h3 class="box-title text-danger">@lang('backend.admins')</h3>
                        <ul class="list-inline two-part">
                            <li><i class="icon-people text-danger"></i></li>
                            <li class="text-right"><span class="counter text-danger">{{$counters->admins}}</span></li>
                        </ul>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a href="{{route('admin.pages')}}">
                    <div class="white-box">
                        <h3 class="box-title text-blue">@lang('backend.title_pages')</h3>
                        <ul class="list-inline two-part">
                            <li><i class="icon-layers text-success text-blue"></i></li>
                            <li class="text-right"><span class="counter text-blue">{{$counters->pages}}</span></li>
                        </ul>
                    </div>
                </a>
            </div>
        @endif
    </div>

    @if(guard_admin()->level > 97)
        <div class="row">
        <div class="col-md-6">
            <div class="white-box bg-belead">
                <h3 class="box-title text-white">
                    <i class="fa fa-circle m-r-5" style="color: #bad7bb;"></i> Visitors <small class="text-lightgreen">(Google Analytics)</small>
                </h3>

                <div id="morris-area-chart" style="max-height: 220px;">
                    @if(!env('ANALYTICS_VIEW_ID') || !env('ANALYTICS_CREDENTIAL_JSON_PATH'))
                        <p style="color:#fff;">
                        Please configure:
                        @if(!env('ANALYTICS_VIEW_ID'))
                            <br><code>env('ANALYTICS_VIEW_ID')</code>
                        @endif
                        @if(!env('ANALYTICS_CREDENTIAL_JSON_PATH'))
                            <br><code>env('ANALYTICS_CREDENTIAL_JSON_PATH')</code>
                        @endif
                        </p>
                    @else
                        @if(!\Storage::exists(env('ANALYTICS_CREDENTIAL_JSON_PATH')))
                            <p style="color:#fff;">File does not exist: <code>{{env('ANALYTICS_CREDENTIAL_JSON_PATH')}}</code></p>
                        @endif
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="white-box bg-darkblue">
                <h3 class="box-title text-white"><i class="fa fa-circle m-r-5" style="color: #8894a3;"></i> Pages views <small class="text-lightgreen">(Google Analytics)</small></h3>
                <div id="morris-area-chart-pageviews" style="max-height: 220px;">
                    @if(!env('ANALYTICS_VIEW_ID') || !env('ANALYTICS_CREDENTIAL_JSON_PATH'))
                        <p style="color:#fff;">
                            Please configure:
                            @if(!env('ANALYTICS_VIEW_ID'))
                                <br><code>env('ANALYTICS_VIEW_ID')</code>
                            @endif
                            @if(!env('ANALYTICS_CREDENTIAL_JSON_PATH'))
                                <br><code>env('ANALYTICS_CREDENTIAL_JSON_PATH')</code>
                            @endif
                        </p>
                    @else
                        @if(!\Storage::exists(env('ANALYTICS_CREDENTIAL_JSON_PATH')))
                            <p style="color:#fff;">File does not exist: <code>{{env('ANALYTICS_CREDENTIAL_JSON_PATH')}}</code></p>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        @foreach($_modules as $module)
            @if(guard_admin()->canAccess($module->reference))
                <div class="col-md-3">
                    <div class="white-box">
                        <a href="{{Route::has('admin.modules.' . $module->reference) ? route('admin.modules.' . $module->reference) : route('admin.modules.' . $module->reference . '.index')}}">
                            <h3 class="box-title">{{$module->name}}</h3>
                        </a>

                        <ul class="list-inline two-part">
                            <li><i class="{{$module->icon ?? 'icon-puzzle'}} text-warning color-belead"></i></li>
                            <li class="text-right"><span class="counter color-belead">{{$module->getCount()}}</span></li>
                        </ul>

                        <p class="m-b-0 text-center">
                            <a href="{{Route::has('admin.modules.' . $module->reference) ? route('admin.modules.' . $module->reference) : route('admin.modules.' . $module->reference . '.index')}}" class="btn btn-outline btn-rounded btn-default">
                                View all {{strtolower(str_replace('¦', ' ', $module->name))}}
                            </a>
                        </p>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@stop

@section('_scripts')
    @if(env('ANALYTICS_VIEW_ID')
        && env('ANALYTICS_CREDENTIAL_JSON_PATH')
        && \Storage::exists(env('ANALYTICS_CREDENTIAL_JSON_PATH')))

        <script>
            $(document).ready(function() {
                Morris.Bar({
                    element: 'morris-area-chart',
                    data: [
                        @foreach($analyticsData ?? [] as $key => $data)
                            {
                                period: '{{($data['date'])->format('Y-m-d')}}',
                                visitors: {{$data['visitors']}}
                            },
                        @endforeach
                    ],
                    xkey: 'period',
                    ykeys: ['visitors'],
                    labels: ['Visitors'],
                    pointSize: 3,
                    fillOpacity: 0,
                    pointStrokeColors:['#bad7bb', '#fdc006', '#2c5ca9'],
                    behaveLikeLine: true,
                    gridLineColor: '#2ea384',
                    lineWidth: 2,
                    hideHover: 'auto',
                    lineColors: ['#bad7bb', '#fdc006', '#2c5ca9'],
                    barColors:['#bad7bb', '#fdc006', '#2c5ca9'],
                    resize: true
                });

                Morris.Bar({
                    element: 'morris-area-chart-pageviews',
                    data: [
                        @foreach($analyticsData ?? [] as $key => $data)
                            {
                                period: '{{($data['date'])->format('Y-m-d')}}',
                                pageviews: {{$data['pageViews']}}
                            },
                        @endforeach
                    ],
                    xkey: 'period',
                    ykeys: ['pageviews'],
                    labels: ['Pages views'],
                    pointSize: 3,
                    fillOpacity: 0,
                    pointStrokeColors:['#8894a3', '#2c5ca9'],
                    behaveLikeLine: true,
                    gridLineColor: '#586b82',
                    lineWidth: 2,
                    hideHover: 'auto',
                    vAxis: {
                        textStyle: {color: '#ffffff'}
                    },
                    lineColors: ['#8894a3', '#2c5ca9'],
                    barColors:['#8894a3', '#2c5ca9'],
                    resize: true
                });
            });
        </script>
    @endif
@endsection
