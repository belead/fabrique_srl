@extends('admin._master')

@section('_title')
    @lang('backend.title_configuration') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.configuration')}}">@lang('backend.title_configuration')</a></li>
    <li class="active">@lang('backend.configuration_edit')</li>
@stop

@section('_title_section')
    @lang('backend.configuration_edit')
@stop

@section('_content')
    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.configuration_attributes')</h3>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_name')</label>
                    <div class="col-md-12">
                        <input required type="text" name="name" class="form-control" value="{{old('name') ?: $configuration->site->name}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_footer')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="footer_copyright">{{old('footer_copyright') ?: $configuration->footer_copyright}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_email')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="email_contact">{{old('email_contact') ?: $configuration->email_contact}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_phone')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="phone_contact">{{old('phone_contact') ?: $configuration->phone_contact}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_address')</label>
                    <div class="col-md-12">
                        <textarea class="form-control editor-footer" name="address_contact">{{old('address_contact') ?: $configuration->address_contact}}</textarea>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Social links</h3>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_facebook')</label>
                    <div class="col-md-12">
                        <input type="text" name="facebook" class="form-control" value="{{old('facebook') ?: $configuration->facebook}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_twitter')</label>
                    <div class="col-md-12">
                        <input type="text" name="twitter" class="form-control" value="{{old('twitter') ?: $configuration->twitter}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_youtube')</label>
                    <div class="col-md-12">
                        <input type="text" name="youtube" class="form-control" value="{{old('youtube') ?: $configuration->youtube}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_linkedin')</label>
                    <div class="col-md-12">
                        <input type="text" name="linkedin" class="form-control" value="{{old('linkedin') ?: $configuration->linkedin}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_instagram')</label>
                    <div class="col-md-12">
                        <input type="text" name="instagram" class="form-control" value="{{old('instagram') ?: $configuration->instagram}}">
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">@lang('backend.configuration_logos')</h3>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_logo_website_default')</label>
                    @if($configuration->logo_website_default)
                        <img class="max-w-300 _cms" src="{{asset('assets/front/img/' . $configuration->logo_website_default)}}">
                        <div>
                            <a href="{{route('admin.configuration.logo.remove', 'logo_website_default')}}" class="_ask btn btn-rounded btn-danger waves-effect waves-light m-t-10 m-b-10">@lang('backend.remove')</a>
                        </div>
                    @endif
                    <input type="file" class="form-control" name="logo_website_default">
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_logo_website_header')</label>
                    @if($configuration->logo_website_header)
                        <img class="max-w-300 _cms" src="{{asset('assets/front/img/' . $configuration->logo_website_header)}}">
                        <div>
                            <a href="{{route('admin.configuration.logo.remove', 'logo_website_header')}}" class="_ask btn btn-rounded btn-danger waves-effect waves-light m-t-10 m-b-10">@lang('backend.remove')</a>
                        </div>
                    @endif
                    <input type="file" class="form-control" name="logo_website_header">
                </div>

                <div class="form-group">
                    <label class="col-md-12">@lang('backend.configuration_label_logo_website_footer')</label>
                    @if($configuration->logo_website_footer)
                        <img class="max-w-300 _cms" src="{{asset('assets/front/img/' . $configuration->logo_website_footer)}}">
                        <div>
                            <a href="{{route('admin.configuration.logo.remove', 'logo_website_footer')}}" class="_ask btn btn-rounded btn-danger waves-effect waves-light m-t-10 m-b-10">@lang('backend.remove')</a>
                        </div>
                    @endif
                    <input type="file" class="form-control" name="logo_website_footer">
                </div>

                <div class="form-group">
                    <label class="col-md-12">Favicon</label>
                    @if($configuration->favicon)
                        <img class="_cms" width="100" src="{{$configuration->getFavicon()}}">
                        <div>
                            <a href="{{route('admin.configuration.logo.remove', 'favicon')}}" class="_ask btn btn-rounded btn-danger waves-effect waves-light m-t-10 m-b-10">@lang('backend.remove')</a>
                        </div>
                    @endif
                    <input type="file" class="form-control" name="favicon">
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Javascript inclusions</h3>

                <div class="form-group">
                    <label class="col-md-12" for="">Include Google Maps JS</label>
                    <div class="col-md-12">
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_google_maps_js" id="has_google_maps_js1" value="1" {{$configuration->has_google_maps_js ? 'checked' : ''}}>
                                    <label for="has_google_maps_js1">Yes</label>
                                </div>
                            </label>

                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_google_maps_js" id="has_google_maps_js0" value="0" {{!$configuration->has_google_maps_js ? 'checked' : ''}}>
                                    <label for="has_google_maps_js0">No</label>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Google Maps API Key</label>
                    <div class="col-md-12">
                        <input type="text" name="google_maps_api_key" class="form-control" value="{{old('google_maps_api_key') ?: $configuration->google_maps_api_key}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Include jQuery CDN</label>
                    <div class="col-md-12">
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_jquery_cdn" id="has_jquery_cdn1" value="1" {{$configuration->has_jquery_cdn ? 'checked' : ''}}>
                                    <label for="has_jquery_cdn1">Yes</label>
                                </div>
                            </label>

                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_jquery_cdn" id="has_jquery_cdn0" value="0" {{!$configuration->has_jquery_cdn ? 'checked' : ''}}>
                                    <label for="has_jquery_cdn0">No</label>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">jQuery CDN url</label>
                    <div class="col-md-12">
                        <input type="text" name="jquery_cdn_url" class="form-control" value="{{old('jquery_cdn_url') ?? ($configuration->jquery_cdn_url ?? config('_CMS._global.jquery_cdn_url'))}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Include reCaptcha JS</label>
                    <div class="col-md-12">
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_recaptcha_js" id="has_recaptcha_js1" value="1" {{$configuration->has_recaptcha_js ? 'checked' : ''}}>
                                    <label for="has_recaptcha_js1">Yes</label>
                                </div>
                            </label>

                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_recaptcha_js" id="has_recaptcha_js0" value="0" {{!$configuration->has_recaptcha_js ? 'checked' : ''}}>
                                    <label for="has_recaptcha_js0">No</label>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Include Stripe JS</label>
                    <div class="col-md-12">
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_stripe_js" id="has_stripe_js1" value="1" {{$configuration->has_stripe_js ? 'checked' : ''}}>
                                    <label for="has_stripe_js1">Yes</label>
                                </div>
                            </label>

                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="has_stripe_js" id="has_stripe_js0" value="0" {{!$configuration->has_stripe_js ? 'checked' : ''}}>
                                    <label for="has_stripe_js0">No</label>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>

    @if(guard_admin()->level >= 100)
        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20" id="updates">
                <div class="_loader">
                    <i class="fa-solid fa-circle-notch fa-spin fa-2x"></i>
                    <br><small>Please wait...</small>
                </div>

                <h3 class="box-title">SYSTEM</h3>
                <p class="alert alert-info">For experimented operators only, these actions can have damaging effects.</p>

                <div class="row">
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <td><small><strong>CMS VERSION</strong></small></td>
                                <td style="text-align: right">
                                    @if($updates->where('is_installed', 1)->first())
                                        v.{{$updates->where('is_installed', 1)->first()->cms_version}}
                                    @else
                                        v.{{config('_CMS._global.version')}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><small><strong>LARAVEL VERSION</strong></small></td>
                                <td style="text-align: right">v.{{App::VERSION()}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <td><small class="ml5"><strong>LAST UPDATED</strong></small></td>
                                <td style="text-align: right">
                                    @if($updates->where('is_installed', 1)->first())
                                        {{$updates->where('is_installed', 1)->first()->updated_at}}
                                    @else
                                        {{$configuration->created_at}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><small class="ml5"><strong>LAST UPDATES CHECK</strong></small></td>
                                <td style="text-align: right">
                                    {{$updates_logs->where('action', 'check')->first()->updated_at ?? 'Never'}}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <h3 class="box-title">Check for updates</h3>
                <table class="table">
                    <tr>
                        <td>
                            @if($updates->where('is_installed', 0)->first())
                                <span class="text-danger">
                                    <i class="ti ti-alert mr5"></i>
                                    <strong>{{$updates->where('is_installed', 0)->count()}}</strong> update(s) available for the CMS
                                </span>
                                <a href="#" class="_toggleUpdatesDetails ml5 link-u">See all updates</a>
                            @else
                                <span class="text-success"><i class="ti ti-check mr5"></i> Your version of the CMS is up to date.</span>
                            @endif
                        </td>
                        <td style="text-align: right;">
                            <a href="{{route('admin.cms.updates.check')}}" class="btn btn-info waves-effect waves-light btn-sm toggleLoader">
                                <small><i class="ti ti-reload mr5"></i> CHECK FOR UPDATES</small>
                            </a>

                            @if($updates->where('is_installed', 0)->first())
                                <a href="{{route('admin.cms.updates.install')}}" class="btn btn-success waves-effect waves-light btn-sm toggleLoader">
                                    <small><i class="ti ti-download mr5"></i> INSTALL ALL UPDATES ({{$updates->where('is_installed', 0)->count()}})</small>
                                </a>
                            @endif
                        </td>
                    </tr>
                </table>

                <table class="table" id="_updatesDetails" style="display: none;">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Reference</th>
                            <th>Date</th>
                            <th>Version</th>
                            <th>Laravel</th>
                            <th>Changelog</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($updates->where('is_installed', 0) as $update)
                            <tr>
                                <td>{{$update->name ?? 'No name'}}</td>
                                <td>#{{$update->reference}}</td>
                                <td>{{$update->updated_at}}</td>
                                <td>v.{{$update->cms_version}}</td>
                                <td>v.{{$update->laravel_version}}</td>
                                <td>
                                    <div style="max-height: 100px; overflow: auto;">
                                        {!! $update->changelog !!}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <h3 class="box-title">Updates logs</h3>
                <div style="max-height: 450px; overflow: auto;">
                    <table class="table" style="margin-bottom: 0;">
                        @foreach($updates_logs as $log)
                            <tr>
                                <td width="125">
                                    @if($log->update_entity)
                                        <a href="#" class="link-u">#{{$log->update_entity->reference}}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td width="100">{{$log->action}}</td>
                                <td>by {{$log->user->getName()}}</td>
                                <td>{!! nl2br($log->description) !!}</td>
                                <td width="400">
                                    @if($log->error)
                                        <i>{{$log->error}}</i>
                                    @else
                                        <i>STATUS - OK</i>
                                    @endif
                                </td>
                                <td style="text-align: right;">{{$log->created_at}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    @endif

    <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
            <h3 class="box-title">PHP server configuration</h3>

            <div style="max-height: 300px; overflow: auto;" class="php-info">
                @php
                    ob_start();
                    phpinfo();
                    $pinfo = ob_get_contents();
                    ob_end_clean();
                    $pinfo = preg_replace( '%^.*<body>(.*)</body>.*$%ms','$1',$pinfo);
                    echo $pinfo;
                @endphp
            </div>
        </div>
    </div>
@stop

@section('_styles')
    <style>
        div.mce-edit-area {
            padding-top: 15px;
        }
    </style>
@stop

@section('_scripts')
    <script>
        $(document).ready(function() {
            if(window.location.hash) {
                let hash = window.location.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top + 300
                }, 500, 'swing');
            }

            $('body')
                .on('click', '._toggleUpdatesDetails', function(e) {
                    e.preventDefault();
                    $('#_updatesDetails').fadeToggle();
                })
                .on('click', '.toggleLoader', function(e) {
                    $('#updates').toggleClass('is_loading');
                });
        });

        var editor = tinymce.init({
            selector: '.editor-footer',
            height: 150,
            menubar: false,
            forced_root_block : '',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code'
            ],
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | link code',
            content_css: '{{asset('assets/admin/css/tinymce.css')}}'
        });
    </script>
@stop
