<tr>
    <td>
        @if($media->isImage())
            <img src="{{$media->getMedia()}}" style="max-width: 45px;">
        @else
            <i class="fa fa-{{$media->getIcon_()}} text-info" style="font-size: 45px;"></i>
        @endif
    </td>
    <td class="text-left">
        <a href="{{$media->getMedia()}}" download>{{$media->file_name}}</a>

        @if($media->original_file_name)
            <br><small style="text-transform: none; font-size: 12px;">{{$media->original_file_name}}</small>
        @endif

        <div style="margin-top: 5px;">
            <code>/uploads/medias/{{$media->file_name}}</code>
        </div>
    </td>
    <td style="text-align: right;">
        <button class="btn btn-default btn-circle waves-effect waves-light _copyClipboard"
                data-clipboard-text="/uploads/medias/{{$media->file_name}}"
                title="Copy"
                data-toggle="tooltip">
            <i class="fa fa-clipboard"></i>
        </button>

        @if(!isset($is_simple))
            <button class="btn btn-default btn-circle _addToEditor"
                    data-path="/uploads/medias/{{$media->file_name}}"
                    data-ext="{{strtolower($media->file_extension)}}"
                    title="Insert to editor"
                    data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
        @endif
    </td>
</tr>
