@extends('admin._master')

@section('_title')
    Medias -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.medias')}}">Medias</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    Medias
@stop

@section('_content')
    @include('admin.sections.medias.body')
@stop
