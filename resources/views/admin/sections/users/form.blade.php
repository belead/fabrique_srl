@extends('admin._master')

@section('_title')
    @lang('backend.title_users') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.users')}}">@lang('backend.title_users')</a></li>
    <li class="active">@lang('backend.user_edit')</li>
@stop

@section('_title_section')
    @lang('backend.user_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.users')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Attributs de l'utilisateur</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-12">Prénom</label>
                                    <div class="col-md-12">
                                        <input type="text" name="first_name" class="form-control" value="{{isset($user->id) ? $user->first_name : old('first_name')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-12">Nom</label>
                                    <div class="col-md-12">
                                        <input type="text" name="last_name" class="form-control" value="{{isset($user->id) ? $user->last_name : old('last_name')}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">E-mail *</label>
                            <div class="col-md-12">
                                <input required type="email" name="email" class="form-control" value="{{isset($user->id) ? $user->email : old('email')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Téléphone</label>
                            <div class="col-md-12">
                                <input type="text" name="phone" class="form-control" value="{{isset($user->id) ? $user->phone : old('phone')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">Nouveau mot de passe</label>
                            <div class="col-md-12">
                                <input type="password" name="password" class="form-control" autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Confirmation du nouveau mot de passe</label>
                            <div class="col-md-12">
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        @if($user->documents->count() > 0)
            <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">
                    <h3 class="box-title">Documents</h3>

                    <table class="table m-b-0 table-bordered">
                        <thead>
                            <tr>
                                <th width="50"></th>
                                <th>Nom</th>
                                <th style="text-align: right;">Ajouté le</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user->documents->sortByDesc('created_at') as $document)
                                <tr>
                                    <td style="text-align: center;">
                                        <a target="_blank" href="{{$document->getFile()}}"><i class="fa fa-file"></i></a>
                                    </td>
                                    <td>
                                        <a href="{{route('admin.modules.documents.edit', $document->id)}}">{{$document->name}}</a>
                                    </td>
                                    <td style="text-align: right;">
                                        {{$document->pivot->updated_at->format('d/m/Y H:i')}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

        <div class="col-sm-6">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Informations pour la création de la SRL</h3>

                <h5 style="margin-bottom: 20px;">1. Les fondateurs.rices</h5>

                <h6>Qui est le contact principal ?</h6>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Nom</label>
                            <div class="col-md-12">
                                <input type="text" name="main_contact_first_name" class="form-control" value="{{$user->main_contact_first_name}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Prénom</label>
                            <div class="col-md-12">
                                <input type="text" name="main_contact_last_name" class="form-control" value="{{$user->main_contact_last_name}}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">E-mail</label>
                    <div class="col-md-12">
                        <input type="email" name="main_contact_email" class="form-control" value="{{$user->main_contact_email}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Téléphone</label>
                    <div class="col-md-12">
                        <input type="text" name="main_contact_phone" class="form-control" value="{{$user->main_contact_phone}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Quelle est votre nationalité ? *</label>
                    <div class="col-md-12">
                        <input type="text" required name="main_contact_nationality" class="form-control" value="{{$user->main_contact_nationality}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Adresse complète</label>
                    <div class="col-md-12">
                        <textarea name="main_contact_address" class="form-control" rows="2">{{$user->main_contact_address}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">Combien y a-t-il de fondateurs.rices ?</label>
                    <div class="col-md-12">
                        <input type="number" name="founders_count" class="form-control" value="{{isset($user->id) ? $user->founders_count : old('founders_count')}}">
                    </div>
                </div>

{{--                <div class="form-group">--}}
{{--                    <label class="col-md-12">Sont-ils/elles marié.es ?</label>--}}
{{--                    <div class="col-md-12">--}}
{{--                        <div class="d-flex align-items-center">--}}
{{--                            <div class="radio radio-primary radio-inline mr-4">--}}
{{--                                <input type="radio" name="founders_are_married" id="founders_are_married1" value="1" {{$user->founders_are_married ? 'checked' : ''}}>--}}
{{--                                <label for="founders_are_married1">Oui</label>--}}
{{--                            </div>--}}

{{--                            <div class="radio radio-primary radio-inline">--}}
{{--                                <input type="radio" name="founders_are_married" id="founders_are_married0" value="0" {{!$user->founders_are_married ? 'checked' : ''}}>--}}
{{--                                <label for="founders_are_married0">Non</label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label class="col-md-12">Si oui, y a t-t-il un contrat de mariage ?</label>--}}
{{--                    <div class="col-md-12">--}}
{{--                        <div class="d-flex align-items-center">--}}
{{--                            <div class="radio radio-primary radio-inline mr-4">--}}
{{--                                <input type="radio" name="founders_have_wedding_contract" id="founders_have_wedding_contract1" value="1" {{$user->founders_have_wedding_contract ? 'checked' : ''}}>--}}
{{--                                <label for="founders_have_wedding_contract1">Oui</label>--}}
{{--                            </div>--}}

{{--                            <div class="radio radio-primary radio-inline">--}}
{{--                                <input type="radio" name="founders_have_wedding_contract" id="founders_have_wedding_contract0" value="0" {{!$user->founders_have_wedding_contract ? 'checked' : ''}}>--}}
{{--                                <label for="founders_have_wedding_contract0">Non</label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <label class="col-md-12" for="">Quelle est leur nationalité ?</label>--}}
{{--                    <div class="col-md-12">--}}
{{--                        <textarea name="founders_nationalities" class="form-control" rows="3">{{$user->founders_nationalities}}</textarea>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="form-group">
                    <label class="col-md-12" for="">Quelle est leur région de résidence ?</label>
                    <div class="col-md-12">
                        <textarea name="founders_residential_region" class="form-control" rows="3">{{$user->founders_residential_region}}</textarea>
                    </div>
                </div>

                <h5 style="margin-bottom: 20px;">2. L'entreprise</h5>

                <div class="form-group">
                    <label class="col-md-12" for="">Nom de l’entreprise à créer</label>
                    <div class="col-md-12">
                        <input type="text" name="company_name" class="form-control" value="{{$user->company_name}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Adresse du siège social de l’entreprise à créer</label>
                    <div class="col-md-12">
                        <textarea name="company_address" class="form-control" rows="2">{{$user->company_address}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Quand voulez-vous lancer l’entreprise ? (mois/année)</label>
                    <div class="col-md-12">
                        <input type="text" name="company_launching_at" class="form-control" value="{{$user->company_launching_at}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Dans quel secteur d’activité principal ?</label>
                    <div class="col-md-12">
                        <input type="text" name="company_main_sector" class="form-control" value="{{$user->company_main_sector}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Expliquez le projet en quelques lignes</label>
                    <div class="col-md-12">
                        <textarea name="company_project_description" class="form-control" rows="4">{{$user->company_project_description}}</textarea>
                    </div>
                </div>

                <h5 style="margin-bottom: 20px;">3. Le plan financier</h5>

                <h6>Il est prêt, complet :</h6>
                <div class="form-group">
                    <label class="col-md-12" for="">
                        Budget, plan de trésorerie, compte de résultat, hypothèses d’exploitation et de financement et situations bilantaires. Je n’ai besoin de rien.
                    </label>
                    <div class="col-md-12">
                        <div class="d-flex align-items-center">
                            <div class="radio radio-primary radio-inline mr-4">
                                <input type="radio" name="business_plan_is_ready" id="business_plan_is_ready1" value="1" {{$user->business_plan_is_ready ? 'checked' : ''}}>
                                <label for="business_plan_is_ready1">Oui</label>
                            </div>

                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="business_plan_is_ready" id="business_plan_is_ready0" value="0" {{!$user->business_plan_is_ready ? 'checked' : ''}}>
                                <label for="business_plan_is_ready0">Non</label>
                            </div>
                        </div>
                    </div>
                </div>

                <h6>Il est quasi prêt et je souhaite :</h6>
                <div class="form-group">
                    <label class="col-md-12" for="">
                        Un feed-back rapide sur le document
                    </label>
                    <div class="col-md-12">
                        <div class="d-flex align-items-center">
                            <div class="radio radio-primary radio-inline mr-4">
                                <input type="radio" name="business_plan_needs_quick_feedback" id="business_plan_needs_quick_feedback1" value="1" {{$user->business_plan_needs_quick_feedback ? 'checked' : ''}}>
                                <label for="business_plan_needs_quick_feedback1">Oui</label>
                            </div>

                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="business_plan_needs_quick_feedback" id="business_plan_needs_quick_feedback0" value="0" {{!$user->business_plan_needs_quick_feedback ? 'checked' : ''}}>
                                <label for="business_plan_needs_quick_feedback0">Non</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">
                        La mise en forme du document au niveau des situations bilantaires
                    </label>
                    <div class="col-md-12">
                        <div class="d-flex align-items-center">
                            <div class="radio radio-primary radio-inline mr-4">
                                <input type="radio" name="business_plan_needs_financial_formatting" id="business_plan_needs_financial_formatting1" value="1" {{$user->business_plan_needs_financial_formatting ? 'checked' : ''}}>
                                <label for="business_plan_needs_financial_formatting1">Oui</label>
                            </div>

                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="business_plan_needs_financial_formatting" id="business_plan_needs_financial_formatting0" value="0" {{!$user->business_plan_needs_financial_formatting ? 'checked' : ''}}>
                                <label for="business_plan_needs_financial_formatting0">Non</label>
                            </div>
                        </div>
                    </div>
                </div>

                <h6>J'ai une base mais je souhaite :</h6>
                <div class="form-group">
                    <label class="col-md-12" for="">
                        Un accompagnement pour finaliser le document
                    </label>
                    <div class="col-md-12">
                        <div class="d-flex align-items-center">
                            <div class="radio radio-primary radio-inline mr-4">
                                <input type="radio" name="business_plan_needs_finalizing_support" id="business_plan_needs_finalizing_support1" value="1" {{$user->business_plan_needs_finalizing_support ? 'checked' : ''}}>
                                <label for="business_plan_needs_finalizing_support1">Oui</label>
                            </div>

                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="business_plan_needs_finalizing_support" id="business_plan_needs_finalizing_support0" value="0" {{!$user->business_plan_needs_finalizing_support ? 'checked' : ''}}>
                                <label for="business_plan_needs_finalizing_support0">Non</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">
                        La mise en forme du document (budget, plan de trésorerie,
                        compte de résultat, hypothèses d’exploitation et de financement
                        et situations bilantaires)
                    </label>
                    <div class="col-md-12">
                        <div class="d-flex align-items-center">
                            <div class="radio radio-primary radio-inline mr-4">
                                <input type="radio" name="business_plan_needs_full_formatting" id="business_plan_needs_full_formatting1" value="1" {{$user->business_plan_needs_full_formatting ? 'checked' : ''}}>
                                <label for="business_plan_needs_full_formatting1">Oui</label>
                            </div>

                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="business_plan_needs_full_formatting" id="business_plan_needs_full_formatting0" value="0" {{!$user->business_plan_needs_full_formatting ? 'checked' : ''}}>
                                <label for="business_plan_needs_full_formatting0">Non</label>
                            </div>
                        </div>
                    </div>
                </div>

                <h5 style="margin-bottom: 20px;">4. La communauté</h5>

                <div class="form-group">
                    <label class="col-md-12" for="">
                        Etes-vous intéressé.e par une mise en avant de votre projet via la communauté
                        Wikipreneurs, une fois l’entreprise lancée ?
                    </label>
                    <div class="col-md-12">
                        <div class="d-flex align-items-center">
                            <div class="radio radio-primary radio-inline mr-4">
                                <input type="radio" name="sharing_agreement" id="sharing_agreement1" value="1" {{$user->sharing_agreement ? 'checked' : ''}}>
                                <label for="sharing_agreement1">Oui</label>
                            </div>

                            <div class="radio radio-primary radio-inline">
                                <input type="radio" name="sharing_agreement" id="sharing_agreement0" value="0" {{!$user->sharing_agreement ? 'checked' : ''}}>
                                <label for="sharing_agreement0">Non</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Comment avez-vous entendu parler de « La Fabrique à SRL » ?</label>
                    <div class="col-md-12">
                        <textarea name="how" class="form-control" rows="2">{{$user->how}}</textarea>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">Tableau de suivi</h3>

                <h5 style="margin-bottom: 20px;">1. Contact avec la banque. Ouverture de compte à la banque</h5>

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_bank_sent',
                    'label' => '1. Envoi de la demande'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_bank_iban',
                    'label' => '2. Acceptation et réception du numéro IBAN'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_bank_signature',
                    'label' => '3. Finalisation et signature'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_bank_capital_payment',
                    'label' => '4. Versement du capital'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_bank_certificate',
                    'label' => '5. Réception de l’attestation bancaire et envoi au notaire'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_bank_cards',
                    'label' => '6. Réception des cartes de banque'
                ])

                <h5 style="margin-bottom: 20px;">2. Check Plan financier</h5>

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_plan_description',
                    'label' => '1. Description du projet'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_plan_budget',
                    'label' => '2. Budget sur 2 ans'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_plan_cash_flow',
                    'label' => '3. Plan de trésorerie et financements sur 2 ans'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_plan_incomes_expenses_explained',
                    'label' => '4. Explications des hypothèses de revenus, de dépenses et de financement'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_plan_income_statement',
                    'label' => '5. Compte de résultat à 12 et 24 mois'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_plan_assessment',
                    'label' => '6. Bilan à 0, 12 et 24 mois'
                ])

                <h5 style="margin-bottom: 20px;">3. Contact avec le guichet d'entreprise</h5>

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_court_management_access_in_order',
                    'label' => '1. En ordre au niveau de l\'accès à la gestion (pour tous) et l\'accès (éventuel) à la profession'
                ])

                @includeIf('admin.blocks.inputs.toggle', [
                    'style' => 'padding-left: 30px;',
                    'item' => $user,
                    'name' => 'followup_business_court_social_in_order',
                    'label' => '2. Après la passation de l\'acte, en ordre au niveau caisse sociale et assurances sociales'
                ])

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>
    </form>
@stop
