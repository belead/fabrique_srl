@extends('admin._master')

@section('_title')
    @lang('backend.title_users') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.users')}}">@lang('backend.title_users')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_users')
@stop

@section('_content')
    @if(guard_admin()->level > 97)
        <div class="row">
            <div class="col-md-3 col-xs-12 col-sm-6">
                <div class="panel panel-default text-center">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <a href="{{route('admin.users.create')}}" class="btn btn-success m-t-10 btn-rounded waves-effect waves-light">@lang('backend.new_user')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <form method="POST" class="form-inline">
                            @csrf

                            <div class="form-group" style="width: 20%;">
                                <label for="">By name</label>
                                <input type="text" name="name" class="form-control" placeholder="By name, email" value="{{session('search_users.name')}}">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-rounded waves-effect waves-light m-t-20">Search</button>
                                <a href="{{route('admin.users.search.clear')}}" class="btn btn-default btn-rounded waves-effect waves-light m-t-20">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table m-b-0">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Créé le</th>
                            <th style="text-align: right;">@lang('backend.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <i class="ti-user m-r-5"></i>
                                    <a class="link-u" href="{{route('admin.users.edit', ['id' => $user->id])}}">
                                        {{$user->getName()}}
                                    </a>
                                </td>
                                <td>{{$user->email}}</td>
                                <td>{{date('d/m/Y H:i', strtotime($user->created_at))}}</td>
                                <td style="text-align: right;">
                                    @if(!$user->password && guard_admin()->level > 97)
                                        <a href="{{route('admin.users.invite', ['id' => $user->id])}}" class="_ask btn btn-warning btn-outline btn-rounded waves-effect waves-light"
                                        data-toggle="tooltip" title="Donner accès à l'espace client">
                                            Inviter
                                        </a>
                                    @endif
                                    <a href="{{route('admin.users.edit', ['id' => $user->id])}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    @if(guard_admin()->level > 97)
                                        <a href="{{route('admin.users.delete', ['id' => $user->id])}}" class="_ask btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{$users->render()}}
            </div>
        </div>
    </div>
@stop
