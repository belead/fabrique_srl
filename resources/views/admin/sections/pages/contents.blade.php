<table class="table template-content">
    <thead>
        <tr>
            <th>@lang('backend.order')</th>
            <th></th>
            <th>@lang('backend.type')</th>
            <th></th>
            <th>Item</th>
            @if(guard_admin()->level >= 100)
                <th>Section id</th>
                <th>Section class</th>
            @endif
            <th></th>
            <th></th>
        </tr>
    </thead>
   <tbody>
        @foreach($page->contents as $content)
            <tr>
                <td width="25">
                    {{$content->order}}.
                </td>
                <td width="100" style="text-align: center;">
                    @if($content->order > 1)
                        <a href="{{route('admin.pages.content.up', $content->id)}}"><i class="fa fa-chevron-up"></i></a>
                    @endif
                    @if($content->order < $page->contents->count())
                        <a href="{{route('admin.pages.content.down', $content->id)}}"><i class="fa fa-chevron-down"></i></a>
                    @endif
                </td>
                <td width="200">
                    @if($content->type)
                        <i class="fa fa-{{$content->type->icon}} m-r-5"></i>{{$content->type->name}}
                    @else
                        <small>Module</small><br>
                        <a href="{{Route::has('admin.modules.' . $content->module->reference) ? route('admin.modules.' . $content->module->reference) : route('admin.modules.' . $content->module->reference . '.index')}}">
                            {{$content->module->name}}
                        </a>
                    @endif
                </td>
                <td>
                    @if($content->module && $content->module_item_id)
                        @if(!empty($content->getItem()->partial->picture))<img src="{{asset('uploads/img/partials/'.$content->getItem()->partial->picture)}}" style="width:125px; height:auto;" class="img-responsive">@endif
                    @endif
                </td>
                <td>
                    @if($content->module && $content->module_item_id)
                        <a href="{{route('admin.modules.' . $content->module->reference . '.edit', $content->module_item_id)}}?page_id={{$page->id}}">
                            {{ $content->getItem()->{$content->module->targetable_by} }}
                        </a>

                        @if($content->module->reference == 'page_blocks')
                            <br>
                            <a class="m-t-3" href="{{route('admin.modules.page_blocks.contents', $content->getItem()->id)}}?page_id={{$page->id}}" style="font-size: 11px;">
                                <i class="fa fa-cube" style="margin-right: 5px;"></i> {{$content->getItem()->contents->count()}} block{{$content->getItem()->contents->count() > 1 ? 's' : ''}}
                            </a>
                        @endif
                    @else
                        -
                    @endif
                </td>
                @if(guard_admin()->level >= 100)
                    <td>
                        {{$content->reference ?? '-'}}
                    </td>
                    <td>
                        @if($content->css_class)
                            <code>{{$content->css_class}}</code>
                        @else
                            -
                        @endif
                    </td>
                @endif
                <td width="150">
                    @if($content->is_draft)
                        <a href="{{route('admin.pages.content.publish', $content->id)}}" class="btn btn-block btn-success btn-rounded">@lang('backend.publish')</a>
                    @else
                        <a href="{{route('admin.pages.content.un_publish', $content->id)}}" class="btn btn-block btn-primary btn-rounded">@lang('backend.un_publish')</a>
                    @endif
                </td>
                <td width="80">
                    <a href="#" data-id="{{$content->id}}" class="toggleForm btn btn-success btn-circle"><i class="fa fa-pencil"></i></a>
                    <a href="{{route('admin.pages.content.remove', $content->id)}}" data-id="{{$content->id}}" class="_ask btn btn-danger btn-circle"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            <tr class="well edit-form-{{$content->id}}">
                <td colspan="{{guard_admin()->level >= 100 ? 9 : 7}}">
                    @php
                        if($content->type) {
                            $action = route('admin.pages.content.update');
                        } else {
                            $action = route('admin.pages.module.update');
                        }
                    @endphp

                    <form action="{{$action}}" method="POST" class="" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <input type="hidden" name="content_id" value="{{$content->id}}">
                        @if($content->type_id) <input type="hidden" name="type_id" value="{{$content->type_id}}"> @endif

                        @if(guard_admin()->level >= 100)
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('backend.input_type_reference') <sup>optional</sup></label>
                                        <input type="text" name="reference" class="form-control" value="{{$content->reference}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Target in layout *</label>
                                        <select name="target" class="form-control select-2" required>
                                            @foreach(config('_project.options.section_targets') ?? [] as $value => $label)
                                                <option value="{{$value}}" {{$content->target === $value ? 'selected' : ''}}>{{$label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('backend.input_type_css_class') <sup>optional</sup></label>
                                        <select name="css_class[]" class="form-control select-2" multiple>
                                            @foreach(config('_project.options.section_classes') ?? [] as $value => $label)
                                                <option value="{{$value}}" {{in_array($value, explode(' ', $content->css_class ?? '')) ? 'selected' : ''}}>{{$label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($content->module && $content->module->is_block)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('backend.input_head_title')</label>
                                        <input type="text" name="head_title" class="form-control" placeholder="@lang('backend.input_head_title')" value="{{$content->head_title}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('backend.input_title')</label>
                                        <input type="text" name="title" class="form-control" placeholder="@lang('backend.input_title')" value="{{$content->title}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>@lang('backend.input_partial')</label>
                                <select name="partial_id" class="form-control" required>
                                    <option value="0">-- @lang('backend.input_partial_select')</option>
                                    @foreach($content->module->partials->where('child_class', null) as $partial)
                                        <option value="{{$partial->id}}" {{$content->partial_id == $partial->id ? 'selected="selected"' : ''}}>{{$partial->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(!$content->type)
                            @if(View::exists('admin.modules.' . $content->module->reference . '.blocks.form_page_content'))
                                <div>
                                    @includeIf('admin.modules.' . $content->module->reference . '.blocks.form_page_content', [
                                        'partials' => $content->module->partials,
                                        'item' => $content->getItem()
                                    ])
                                </div>
                            @else
                                <div class="form-group">
                                    <label>@lang('backend.select_module_content')</label>
                                    @php
                                        $target = $content->module->targetable_by;
                                    @endphp

                                    <select name="module_item_id" class="form-control">
                                        <option value="">-- @lang('backend.select_module_label')</option>
                                        @foreach($content->items as $item)
                                            <option value="{{$item->id}}" {{$content->module_item_id == $item->id ? 'selected="selected"' : ''}}>
                                                {{$item->$target}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        @endif

                        @if($content->module && $content->module->is_multiple)
                            <div class="form-group">
                                <div class="checkbox checkbox-primary">
                                    <input name="module_all_items" id="checkbox_allc_items_{{$content->id}}" type="checkbox" value="1" {{$content->module_all_items ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_allc_items_{{$content->id}}">@lang('backend.module_content_block_all_items')</label>
                                </div>
                            </div>
                        @endif

                        <div class="row m-b-15">
                            @if($content->type && $content->type->input_name == 'content')
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('backend.input_head_title')</label>
                                            <input type="text" name="head_title" class="form-control" placeholder="@lang('backend.input_head_title')" value="{{$content->head_title}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('backend.input_title')</label>
                                            <input type="text" name="title" class="form-control" placeholder="@lang('backend.input_title')" value="{{$content->title}}">
                                        </div>
                                    </div>

                                <div class="form-row">
                                    <label class="col-md-12">
                                        <input type="checkbox" value="1" class="js-two-column-content" {{$content->content_bis ? 'checked' : ''}}>
                                        Content in two columns
                                    </label>
                                </div>
                            @endif
                        </div>

                        <div class="form-group {{ !$content->type && !$content->module->is_multiple ? 'm-b-0' : '' }}">
                            @if($content->type)
                                <label>@lang('backend.input_' . $content->type->reference)</label>
                            @elseif($content->module->is_multiple)
                                <label>@lang('backend.module_content_block')</label>
                            @endif

                            @if($content->type)
                                <div class="row">
                                <div class="col-md-{{$content->content_bis ? '6' : '12'}} js-column-content">
                                    @switch($content->type->input_type)
                                        @case('text')
                                            <input type="text" required name="{{$content->type->input_name}}" class="{{$content->type->input_class}}" placeholder="{{$content->type->input_placeholder}}" value="{{ $content->content }}">
                                        @breakswitch
                                        @case('file')
                                            <img src="{{asset('uploads/img/pages/' . $content->content)}}" class="m-b-15">
                                            <input type="file" name="{{$content->type->input_name}}" class="{{$content->type->input_class}}" placeholder="{{$content->type->input_placeholder}}">
                                        @breakswitch
                                        @case('textarea')
                                            <textarea name="{{$content->type->input_name}}" class="{{$content->type->input_class}}" placeholder="{{$content->type->input_placeholder}}">{!! $content->content !!}</textarea>
                                        @breakswitch
                                    @endswitch
                                </div>
                                @if($content->type->input_name == 'content')
                                    <div class="col-md-6 {{$content->content_bis ? '' : 'hidden'}} js-column-content-bis">
                                        <textarea name="content_bis" class="{{$type->input_class}}" placeholder="{{$type->input_placeholder}}">{!! $content->content_bis !!}</textarea>
                                    </div>
                                @endif
                                </div>
                            @elseif($content->module->is_multiple)
                                <div class="row">
                                    <div class="col-md-3">
                                        @lang('backend.module_content_block_number_items')
                                        <input type="number" name="module_number_items" min="1" class="form-control" value="{{$content->module_number_items ? $content->module_number_items : 1}}">
                                    </div>

                                    <div class="col-md-9">
                                        @lang('backend.module_content_block_matches')

                                        @php
                                            $orders = explode(',', $content->module->orderable_by);
                                        @endphp
                                        <select name="module_order_by" class="form-control">
                                            <option value="">-- @lang('backend.module_content_block_matches_select')</option>
                                            @foreach($orders as $order)
                                                <option value="{{$order}}|ASC" {{$content->module_order_by == ($order . '|ASC') ? 'selected="selected"' : ''}}>@lang('modules.order_' . $order . '_asc')</option>
                                                <option value="{{$order}}|DESC" {{$content->module_order_by == ($order . '|DESC') ? 'selected="selected"' : ''}}>@lang('modules.order_' . $order . '_desc')</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>

                        @if($content->type
                            && ($content->type->reference == 'input-link' || $content->type->input_name == 'call-to-action'))
                            <div class="form-group">
                                <label>@lang('backend.input_select_page_label')</label>
                                <select name="content_page_id" class="form-control">
                                    <option value="0">-- @lang('backend.input_select_page')</option>
                                    @foreach($pages as $page)
                                        <option value="{{$page->id}}" {{$content->content_page_id == $page->id ? 'selected="selected"' : ''}}>{{$page->attributes->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">@lang('backend.input_custom_action_label')</label>
                                <input type="text" name="content_custom_url" class="form-control" placeholder="@lang('backend.input_custom_action_label')" value="{{$content->content_custom_url}}">
                            </div>
                        @endif

                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.update')</button>
                    </form>
                </td>
            </tr>
        @endforeach
   </tbody>
</table>
