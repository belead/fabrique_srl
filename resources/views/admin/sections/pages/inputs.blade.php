@foreach($content_types as $type)
    <section class="well m-b-0 craft-input" id="{{$type->reference}}">
        <form method="POST" action="{{route('admin.pages.content.add')}}" class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="page_id" value="{{$page->id}}">
            <input type="hidden" name="type_id" value="{{$type->id}}">

            @if(guard_admin()->level >= 100)
                <div class="form-group">
                    <div class="col-md-6">
                        <label>@lang('backend.input_type_reference') <sup>optional</sup></label>
                        <input type="text" name="reference" class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label>@lang('backend.input_type_css_class') <sup>optional</sup></label>
                        <select name="css_class[]" class="form-control select-2" multiple>
                            @foreach(config('_project.options.section_classes') ?? [] as $value => $label)
                                <option value="{{$value}}">{{$label}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif

            @if($type->input_name == 'content')
                <div class="form-group">
                    <div class="col-md-6">
                        <label>@lang('backend.input_head_title')</label>
                        <input type="text" name="head_title" class="form-control" placeholder="@lang('backend.input_head_title')" value="">
                    </div>

                    <div class="col-md-6">
                        <label>@lang('backend.input_title')</label>
                        <input type="text" name="title" class="form-control" placeholder="@lang('backend.input_title')" value="">
                    </div>
                </div>

                <div class="m-b-15">
                    <label>
                        <input type="checkbox" value="1" class="js-two-column-content">
                        Content in two columns
                    </label>
                </div>
            @endif

            <div class="form-group">
                <label class="col-md-12">@lang('backend.input_' . $type->reference) *</label>
                <div class="col-md-12 js-column-content">
                    @switch($type->input_type)
                        @case('text')
                            <input type="text" required name="{{$type->input_name}}" class="{{$type->input_class}}" placeholder="{{$type->input_placeholder}}">
                        @breakswitch
                        @case('file')
                            <input type="file" required name="{{$type->input_name}}" class="{{$type->input_class}}" placeholder="{{$type->input_placeholder}}">
                        @breakswitch
                        @case('textarea')
                            <textarea name="{{$type->input_name}}" class="{{$type->input_class}}" placeholder="{{$type->input_placeholder}}"></textarea>
                        @breakswitch
                    @endswitch
                </div>
                @if($type->input_name == 'content')
                    <div class="col-md-6 hidden js-column-content-bis">
                        <textarea name="content_bis" class="{{$type->input_class}}" placeholder="{{$type->input_placeholder}}"></textarea>
                    </div>
                @endif
            </div>

            @if($type->reference == 'input-link' || $type->input_name == 'call-to-action')
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.input_select_page_label')</label>
                    <select name="content_page_id" class="form-control">
                        <option value="0">-- @lang('backend.input_select_page')</option>
                        @foreach($pages as $page)
                            <option value="{{$page->id}}">{{$page->attributes->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="col-md-12">@lang('backend.input_custom_action_label')</label>
                    <input type="text" name="content_custom_url" class="form-control" placeholder="@lang('backend.input_custom_action_label')">
                </div>
            @endif

            <button type="submit" class="btn-rounded btn btn-primary waves-effect waves-light m-r-10">@lang('backend.save_as_draft')</button>
            <button type="submit" name="is_published" value="1" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.publish')</button>
        </form>
    </section>
@endforeach
