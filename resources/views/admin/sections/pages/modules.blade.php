@foreach($modules as $module)
    <section class="well m-b-0 craft-input" id="{{Str::slug($module->reference)}}">
        <form method="POST" action="{{route('admin.pages.module.add')}}" class="" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="page_id" value="{{$page->id}}">
            <input type="hidden" name="module_id" value="{{$module->id}}">

            <div class="row" style="display: {{guard_admin()->level >= 100 ? 'block' : 'none'}};">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>@lang('backend.input_type_reference') <sup>optional</sup></label>
                        <input type="text" name="reference" class="form-control">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>@lang('backend.input_type_css_class') <sup>optional</sup></label>
                        <select name="css_class[]" class="form-control select-2" multiple>
                            @foreach(config('_project.options.section_classes') ?? [] as $value => $label)
                                <option value="{{$value}}">{{$label}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            @if($module->is_block)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('backend.input_head_title') <sup>optional</sup></label>
                            <input type="text" name="head_title" class="form-control" placeholder="@lang('backend.input_head_title')">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('backend.input_title') <sup>optional</sup></label>
                            <input type="text" name="title" class="form-control" placeholder="@lang('backend.input_title')">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>@lang('backend.input_partial')</label>
                    <select name="partial_id" class="form-control select-2">
                        <option value="">-- @lang('backend.input_partial_select')</option>
                        @foreach($module->partials->where('child_class', null) as $partial)
                            <option value="{{$partial->id}}">{{$partial->name}}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            <ul class="nav customtab2 nav-tabs form-group" role="tablist">
                @if(View::exists('admin.modules.' . $module->reference . '.blocks.form_page_content'))
                    <li role="presentation" class="active">
                        <a href="#page_form_{{$module->id}}" aria-controls="page_form_{{$module->id}}" role="tab" data-toggle="tab" aria-expanded="false">
                            Create new
                        </a>
                    </li>
                @endif

                <li role="presentation" @if(!View::exists('admin.modules.' . $module->reference . '.blocks.form_page_content')) class="active" @endif>
                    <a href="#single_item_{{$module->id}}" aria-controls="single_item_{{$module->id}}" role="tab" data-toggle="tab" aria-expanded="true">
                        Existing {{strtolower(\Illuminate\Support\Str::singular(str_replace('Pages ¦', ' ', $module->name)))}}
                    </a>
                </li>

                @if($module->is_multiple)
                    <li role="presentation" class="">
                        <a href="#multiple_items_{{$module->id}}" aria-controls="multiple_items_{{$module->id}}" role="tab" data-toggle="tab" aria-expanded="false">
                            @lang('backend.module_content_block')
                        </a>
                    </li>
                @endif
            </ul>

            <div class="tab-content m-t-0">
                <div role="tabpanel" class="tab-pane fade in @if(!View::exists('admin.modules.' . $module->reference . '.blocks.form_page_content')) active @endif " id="single_item_{{$module->id}}">
                    <div class="form-group">
                        @php
                            $target = $module->targetable_by;
                        @endphp
                        <select name="module_item_id" class="form-control select-2">
                            <option value="">-- Select a {{strtolower(\Illuminate\Support\Str::singular(str_replace('Pages ¦', ' ', $module->name)))}}</option>
                            @foreach($module->items as $_item)
                                <option value="{{$_item->id}}">
                                    <span class="text-muted">#{{$_item->id}}</span>&nbsp;&nbsp;&nbsp;{{$_item->$target}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @if(View::exists('admin.modules.' . $module->reference . '.blocks.form_page_content'))
                    <div role="tabpanel" class="tab-pane fade in active" id="page_form_{{$module->id}}">
                        @includeIf('admin.modules.' . $module->reference . '.blocks.form_page_content', [
                            'partials' => $module->partials,
                            'page_title' => $page->attributes->title
                        ])
                    </div>
                @endif

                @if($module->is_multiple)
                    <div role="tabpanel" class="tab-pane fade" id="multiple_items_{{$module->id}}">
                        <div class="form-group">
                            @lang('backend.module_content_block_matches')
                            <select name="module_order_by" class="form-control">
                                <option value="">-- @lang('backend.module_content_block_matches_select')</option>
                                @foreach($module->orders as $order)
                                    <option value="{{$order}}|ASC">@lang('modules.order_' . $order . '_asc')</option>
                                    <option value="{{$order}}|DESC">@lang('modules.order_' . $order . '_desc')</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row" style="display: flex; align-items: center;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    @lang('backend.module_content_block_number_items')
                                    <input type="number" name="module_number_items" min="1" class="form-control" value="1">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="">
                                    <div class="checkbox checkbox-primary">
                                        <input name="module_all_items" id="checkbox_all_items_{{$module->id}}"  type="checkbox" value="1">
                                        <label for="checkbox_all_items_{{$module->id}}">@lang('backend.module_content_block_all_items')</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <button type="submit" class="btn-rounded btn btn-primary waves-effect waves-light m-r-10">@lang('backend.save_as_draft')</button>
            <button type="submit" name="is_published" value="1" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.publish')</button>
        </form>
    </section>
@endforeach
