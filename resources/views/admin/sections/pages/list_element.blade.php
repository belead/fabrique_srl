@if($page->attributes)
    <tr class="{{isset($depth) ? ('_depth_' . $depth) : ''}}">
        <td class="text-center">
            @if(!$page->is_deleted)
            <a href="{{route('admin.pages.active.switch', ['id' => $page->id])}}" type="button" class="btn btn-{{$page->is_active ? 'warning' : 'default'}} btn-circle waves-effect waves-light"><i class="fa fa-lightbulb-o"></i></a>
            @endif
        </td>
        <td class="{{$page->is_deleted ? 'is-deleted' : ''}} _name">
            @if(isset($depth))
                &boxur; &nbsp;
            @endif

            @if($page->is_home)
                <i class="ti-home m-r-5"></i>
            @endif

            @if($page->is_deleted)
                <i class="fa fa-trash m-r-5"></i>
            @endif

            <a href="{{route('admin.pages.edit', ['id' => $page->id])}}">
                {{$page->attributes->title}}
            </a>
        </td>
        <td><span class="m-r-10 text-muted"><i class="ti-link m-r-5"></i>{{$page->attributes->url}}</span></td>
        <td><strong><i class="ti-user"></i></strong> {{$page->attributes->creator->getName()}}</td>
        <td><strong><i class="ti-pencil"></i></strong> {{date('d/m/Y H:i', strtotime($page->attributes->updated_at))}}</td>
        <td class="text-right">
            @if($page->is_deleted)
                <a href="{{route('admin.pages.restore', ['id' => $page->id])}}" class="btn btn-default btn-outline btn-rounded waves-effect waves-light">@lang('backend.restore')</a>
                <a href="{{route('admin.pages.destroy', ['id' => $page->id])}}" class="_ask btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.destroy')</a>
            @else
                @if($page->is_active)
                    <a href="/{{session('switched_language')}}/{{$page->attributes->url}}" class="btn btn-info btn-outline btn-rounded waves-effect waves-light">View</a>
                @endif
                <a href="{{route('admin.pages.edit', ['id' => $page->id])}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                <a href="{{route('admin.pages.delete', ['id' => $page->id])}}" class="_ask btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                <span>&nbsp;|&nbsp;</span>
                <a href="{{route('admin.pages.copy', ['id' => $page->id])}}" class="btn btn-dark btn-outline btn-rounded waves-effect waves-light _ask"
                    data-title="Are you sure you want to copy this page and all its content?">
                    @lang('backend.copy')
                </a>
            @endif
        </td>
    </tr>
@else
    <tr class="{{isset($depth) ? ('_depth_' . $depth) : ''}}">
        <td></td>
        <td class="{{$page->is_deleted ? 'is-deleted' : ''}} _name" colspan="4">
            @if(isset($depth))
                &boxur; &nbsp;
            @endif

            @if($page->is_home)
                <i class="ti-home m-r-5"></i>
            @endif

            @if($page->is_deleted)
                <i class="fa fa-trash m-r-5"></i>
            @endif

            <span class="text-muted">@lang('backend.not_translated')</span>
        </td>
        <td class="text-right" width="135">
            @if(isset($languages))
                <div style="position: relative;">
                    <button type="button" class="btn waves-effect waves-light btn-info btn-outline dropdown-toggle btn-rounded" data-toggle="dropdown">
                        Duplicate from
                        <span class="caret m-l-5"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @foreach($languages as $language)
                            <li>
                                <a class="_ask" href="{{route('admin.pages.attributes.duplicate.from', [$page->id, $language->code])}}">
                                    {{strtoupper($language->code)}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </td>
    </tr>
@endif
