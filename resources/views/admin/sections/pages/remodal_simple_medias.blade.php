<div class="remodal" data-remodal-id="_seeSimpleMedia">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Medias library</h1>

    @include('admin.sections.medias.body', ['is_modal' => true, 'is_simple' => true])

    <button data-remodal-action="cancel" class="remodal-cancel m-t-15">Close</button>
</div>
