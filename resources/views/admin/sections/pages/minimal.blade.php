@php
    $content = $page->contents->first();
@endphp

<table class="table template-content">
    <tr class="well edit-form-{{$content->id}}" style="display: table-row;">
        <td>
            <form action="{{route('admin.pages.content.update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="content_id" value="{{$content->id}}">
                <input type="hidden" name="type_id" value="{{$content->type_id}}">

                <div class="form-group">
                    <div class="col-md-12">
                        <textarea name="{{$content->type->input_name}}" class="{{$content->type->input_class}} _big" placeholder="{{$content->type->input_placeholder}}">{!! $content->content !!}</textarea>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light">@lang('backend.save')</button>
            </form>
        </td>
    </tr>
</table>
