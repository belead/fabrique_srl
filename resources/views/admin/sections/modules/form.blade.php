@extends('admin._master')

@section('_title')
    @lang('backend.title_modules') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.modules')}}">@lang('backend.title_modules')</a></li>
    <li class="active">@lang('backend.module_edit')</li>
@stop

@section('_title_section')
    @lang('backend.module_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.modules')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.module_attributes')</h3>

                <div class="row">
                    <div class="col-md-{{$module->is_block ? 6 : 12}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-12">
                                        @lang('backend.module_label_reference') *
                                        <br><small>Has to match existing route resource or name</small>
                                    </label>
                                    <div class="col-md-12">
                                        <input required type="text" name="reference" class="form-control" value="{{isset($module->id) ? $module->reference : old('reference')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-12">Display name *</label>
                                    <div class="col-md-12">
                                        <input required type="text" name="name" class="form-control" value="{{isset($module->id) ? $module->name : old('name')}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">
                                @lang('backend.module_label_class') *
                                <br><small>Has to match existing class in App\Models\_Modules</small>
                            </label>
                            <div class="col-md-12">
                                <input required type="text" name="class_name" class="form-control" value="{{isset($module->id) ? $module->class_name : old('class_name')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">
                                @lang('backend.module_label_icon')
                                <br><small>Full icon class</small>
                            </label>
                            <div class="col-md-12">
                                <input type="text" name="icon" class="form-control" value="{{isset($module->id) ? $module->icon : old('icon')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">
                                Labelled by
                                <br><small>Column used in module items select</small>
                            </label>
                            <div class="col-md-12">
                                <input type="text" name="targetable_by" class="form-control" value="{{isset($module->id) ? $module->targetable_by : old('targetable_by')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12" for="">
                                Layout type
                            </label>
                            <div class="col-md-12">
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_block" id="is_block1" value="1" {{$module->is_block ? 'checked' : ''}}>
                                            <label for="is_block1">Block (partial required to encompass the items)</label>
                                        </div>
                                    </label>

                                    <label class="radio-inline">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_block" id="is_block0" value="0" {{!$module->is_block ? 'checked' : ''}}>
                                            <label for="is_block0">Standalone (unique item that includes its own partial)</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12" for="">Targeted by</label>
                            <div class="col-md-12">
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_targetable_by_slug" id="is_targetable_by_slug1" value="1" {{$module->is_targetable_by_slug ? 'checked' : ''}}>
                                            <label for="is_targetable_by_slug1">Slug</label>
                                        </div>
                                    </label>

                                    <label class="radio-inline">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_targetable_by_slug" id="is_targetable_by_slug0" value="0" {{!$module->is_targetable_by_slug ? 'checked' : ''}}>
                                            <label for="is_targetable_by_slug0">Id</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12" for="">Can be linked into pages</label>
                            <div class="col-md-12">
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_linkable" id="is_linkable1" value="1" {{$module->is_linkable ? 'checked' : ''}}>
                                            <label for="is_linkable1">Yes</label>
                                        </div>
                                    </label>

                                    <label class="radio-inline">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_linkable" id="is_linkable0" value="0" {{!$module->is_linkable ? 'checked' : ''}}>
                                            <label for="is_linkable0">No</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12" for="">Multiple items can be fetched</label>
                            <div class="col-md-12">
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_multiple" id="is_multiple1" value="1" {{$module->is_multiple ? 'checked' : ''}}>
                                            <label for="is_multiple1">Yes</label>
                                        </div>
                                    </label>

                                    <label class="radio-inline">
                                        <div class="radio radio-info">
                                            <input type="radio" name="is_multiple" id="is_multiple0" value="0" {{!$module->is_multiple ? 'checked' : ''}}>
                                            <label for="is_multiple0">No</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.module_label_orderable')</label>
                            <p class="col-md-12">
                                @lang('backend.module_label_orderable_info')
                            </p>
                            <div class="col-md-12">
                                <input type="text" name="orderable_by" class="form-control" value="{{isset($module->id) ? $module->orderable_by : old('orderable_by')}}">
                            </div>
                        </div>

                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                    </div>

                    @if($module->is_block)
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Hidden fields <br><small>In backend module form</small></label>
                                <div class="col-md-12">
                                    <select name="hidden_fields[]" class="form-control multi-select" multiple>
                                        @foreach($module->fillable ?? [] as $field)
                                            <option value="{{$field}}" {{in_array($field, $module->hidden_fields) ? 'selected' : ''}}>{{$field}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Add a configuration line</label>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="config_type" class="form-control select-2">
                                                <option value="">-- Select a type</option>
                                                @foreach(config('_CMS._global.modules.configuration.types') ?? [] as $key => $label)
                                                    <option value="{{$key}}">{{$label ?? $key}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <select name="config_field" class="form-control select-2">
                                                <option value="">-- Select a field</option>
                                                @foreach($module->fillable ?? [] as $field)
                                                    <option value="{{$field}}">{{$field}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input type="text" name="config_key" class="form-control" placeholder="key" id="config_key">
                                                <div class="input-group-addon">:</div>
                                                <input type="text" name="config_value" class="form-control" placeholder="value">
                                            </div>

                                            <small>
                                                <a href="#" class="_keyLexicon" data-value="width">width</a>
                                                , <a href="#" class="_keyLexicon" data-value="height">height</a>
                                            </small>
                                        </div>
                                    </div>

                                    <table class="table table-striped" style="margin-bottom: 0; margin-top: 15px;">
                                        <thead>
                                            <tr>
                                                <th width="125">Type</th>
                                                <th>Field</th>
                                                <th style="text-align: right;">Values</th>
                                                <th width="50"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($module->configuration as $type => $values)
                                                @foreach($values as $field => $options)
                                                    @foreach($options as $key => $value)
                                                        <tr>
                                                            <td>{{$type}}</td>
                                                            <td>{{$field}}</td>
                                                            <td style="text-align: right;">{{$key}}:{{$value}}</td>
                                                            <td style="text-align: right;">
                                                                <a href="{{route('admin.modules.configuration.line.remove', [$module->id, ($type . ':' . $field . ':' . $key)])}}" class="btn btn-danger btn-circle waves-effect waves-light _ask"><i class="fa fa-trash"></i> </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </form>
@stop

@push('_scripts')
    <script>
        $('._keyLexicon').click(function(e) {
            e.preventDefault();
            let value = $(this).attr('data-value');
            $('#config_key').val(value);
        });
    </script>
@endpush
