@extends('admin._master')

@section('_title')
    @lang('modules.title_partials') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.partials')}}">@lang('modules.title_partials')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('modules.title_partials')
@stop

@section('_content')
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="panel panel-default text-center">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a href="{{route('admin.partials.create')}}" class="btn btn-success btn-rounded waves-effect waves-light mr5">@lang('modules.new_partial')</a>

                        @if(is_array(config('_CMS._partials')) && count(config('_CMS._partials')) > 0)
                            <a href="{{route('admin.partials.import')}}" class="btn btn-default btn-rounded waves-effect waves-light">
                                Import from config
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <form method="POST" class="form-inline" action="{{route('admin.partials.search')}}">
                            @csrf

                            <div class="form-group" style="width: 20%;">
                                <label for="">By name</label>
                                <input type="text" name="name"
                                       class="form-control"
                                       placeholder="By name"
                                       value="{{session('search_partials.name')}}">
                            </div>

                            <div class="form-group" style="width: 20%;">
                                <label for="">By module</label>
                                <select name="module_id" class="form-control select-2">
                                    <option value="">-- All</option>
                                    @foreach($modules as $module)
                                        <option value="{{$module->id}}" {{session('search_partials.module_id') == $module->id ? 'selected' : ''}}>
                                            {{$module->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-rounded waves-effect waves-light m-t-20">Search</button>
                                <a href="{{route('admin.partials.clear')}}" class="btn btn-default btn-rounded waves-effect waves-light m-t-20">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table m-b-0">
                    <thead>
                        <tr>
                            <th>@lang('modules.partial_label_name')</th>
                            <th>@lang('modules.partial_label_module')</th>
                            <th>@lang('modules.partial_label_path')</th>
                            <th>Image</th>
                            <th>@lang('modules.partial_label_side')</th>
                            <th>@lang('modules.partial_label_created')</th>
                            <th style="text-align: right;">@lang('backend.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($partials->sortBy('module.name') as $partial)
                            <tr>
                                <td>
                                    @if($partial->child_class)
                                        <span class="text-muted">{{$partial->child_class}}</span> >
                                    @endif
                                    <a href="{{route('admin.partials.edit', ['id' => $partial->id])}}">
                                        {{$partial->name}}
                                    </a>
                                </td>
                                <td>{{$partial->module->name}}</td>
                                <td>../front/partials/{{$partial->partial_path}}.blade.php</td>
                                <td>@if(!empty($partial->picture))<img src="{{asset('/uploads/img/partials/'.$partial->picture)}}" alt="" style="max-width: 120px;">@endif</td>
                                <td>{{$partial->side}}</td>
                                <td>{{date('d/m/Y', strtotime($partial->created_at))}}</td>
                                <td style="text-align: right;">
                                    <a href="{{route('admin.partials.edit', ['id' => $partial->id])}}" class="btn btn-success btn-outline btn-rounded waves-effect waves-light">@lang('backend.edit')</a>
                                    <a href="{{route('admin.partials.delete', ['id' => $partial->id])}}" class="_ask btn btn-danger btn-outline btn-rounded waves-effect waves-light">@lang('backend.delete')</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
