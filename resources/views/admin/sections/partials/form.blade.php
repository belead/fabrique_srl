@extends('admin._master')

@section('_title')
    @lang('modules.title_partials') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.partials')}}">@lang('modules.title_partials')</a></li>
    <li class="active">@lang('modules.edit_partial')</li>
@stop

@section('_title_section')
    @lang('modules.edit_partial')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.partials')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('modules.partial_attributes')</h3>

                <div class="row">
                    <div class="col-md-{{!empty($partial->fillable) && in_array('partial_id', $partial->fillable) ? 6 : 12}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-12">@lang('modules.partial_label_module') *</label>
                                    <div class="col-md-12">
                                        <select required name="module_id" class="form-control select-2">
                                            <option value="">-- @lang('modules.partial_label_module_select')</option>
                                            @foreach($modules as $module)
                                                <option value="{{$module->id}}" {{$partial->module_id == $module->id ? 'selected="selected' : ''}}
                                                    data-children="{{json_encode($module->children)}}">
                                                    {{$module->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group _child_box" style="display: {{$partial->child_class ? 'block' : 'none'}};">
                                    <label class="col-md-12">Child model</label>
                                    <div class="col-md-12">
                                        <select name="child_class" class="form-control">
                                            <option value="">-- Select a child model</option>
                                            @if($partial->module_id && $modules->where('id', $partial->module_id)->first())
                                                @foreach($modules->where('id', $partial->module_id)->first()->children ?? [] as $child)
                                                    <option value="{{$child}}" {{$partial->child_class == $child ? 'selected' : ''}}>{{$child}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-12">@lang('modules.partial_label_path') *</label>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-addon">../front/partials/</div>
                                            <input required type="text" name="partial_path" class="form-control" value="{{isset($partial->id) ? $partial->partial_path : old('partial_path')}}">
                                            <div class="input-group-addon">.blade.php</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('modules.partial_label_name') *</label>
                            <div class="col-md-12">
                                <input required type="text" name="name" class="form-control" value="{{isset($partial->id) ? $partial->name : old('name')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('modules.partial_label_side')</label>
                            <div class="col-md-12">
                                <input type="text" name="side" class="form-control" value="{{isset($partial->id) ? $partial->side : old('side')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">CSS class <br><small>Attached to {{'<body>'}} tag</small></label>
                            <div class="col-md-12">
                                <input type="text" name="body_class" class="form-control" value="{{isset($partial->id) ? $partial->body_class : old('body_class')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-primary">
                                    <input name="is_details_partial" id="checkbox_is_details" type="checkbox" value="1" {{isset($partial->id) && $partial->is_details_partial == 1 ? 'checked="checked"' : ''}}>
                                    <label for="checkbox_is_details">@lang('modules.partial_is_details')</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Picture</label>
                            <div class="col-md-12">
                                @if(isset($partial->id) && $partial->picture != '')
                                    <img src="{{$partial->getPicture()}}" style="max-width: 600px;">
                                    <div class="checkbox checkbox-danger m-b-5">
                                        <input name="delete_picture" id="checkbox_del_picture" type="checkbox" value="1">
                                        <label for="checkbox_del_picture">Delete picture</label>
                                    </div>
                                @endif

                                <input type="file" name="picture" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('modules.partial_description_label')</label>
                            <div class="col-md-12">
                                <textarea name="description" class="form-control" rows="4" placeholder="e.g: Used in <name> page">{{isset($partial->id) ? $partial->description : old('description')}}</textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                    </div>

                    @if(!empty($partial->fillable) && in_array('partial_id', $partial->fillable))
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Hidden fields <br><small>In backend module form</small></label>
                                <div class="col-md-12">
                                    <select name="hidden_fields[]" class="form-control multi-select" multiple>
                                        @foreach($partial->fillable as $field)
                                            <option value="{{$field}}" {{in_array($field, $partial->hidden_fields) ? 'selected' : ''}}>{{$field}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Add a configuration line</label>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="config_type" class="form-control select-2">
                                                <option value="">-- Select a type</option>
                                                @foreach(config('_CMS._global.partials.configuration.types') ?? [] as $key => $label)
                                                    <option value="{{$key}}">{{$label ?? $key}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <select name="config_field" class="form-control select-2">
                                                <option value="">-- Select a field</option>
                                                @foreach($partial->fillable as $field)
                                                    <option value="{{$field}}">{{$field}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input type="text" name="config_key" class="form-control" placeholder="key" id="config_key">
                                                <div class="input-group-addon">:</div>
                                                <input type="text" name="config_value" class="form-control" placeholder="value">
                                            </div>

                                            <small>
                                                <a href="#" class="_keyLexicon" data-value="width">width</a>
                                                , <a href="#" class="_keyLexicon" data-value="height">height</a>
                                                @foreach(\App\Models\_CMS\Language::where('is_used', 1)->get() as $language)
                                                    , <a href="#" class="_keyLexicon" data-value="{{$language->code}}">{{$language->code}}</a>
                                                @endforeach
                                            </small>
                                        </div>
                                    </div>

                                    <table class="table table-striped" style="margin-bottom: 0; margin-top: 15px;">
                                        <thead>
                                            <tr>
                                                <th width="125">Type</th>
                                                <th>Field</th>
                                                <th style="text-align: right;">Values</th>
                                                <th width="50"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($partial->configuration as $type => $values)
                                                @foreach($values as $field => $options)
                                                    @foreach($options as $key => $value)
                                                        <tr>
                                                            <td>{{$type}}</td>
                                                            <td>{{$field}}</td>
                                                            <td style="text-align: right;">{{$key}}:{{$value}}</td>
                                                            <td style="text-align: right;">
                                                                <a href="{{route('admin.partials.configuration.line.remove', [$partial->id, ($type . ':' . $field . ':' . $key)])}}" class="btn btn-danger btn-circle waves-effect waves-light _ask"><i class="fa fa-trash"></i> </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                    @endif
                </div>
            </div>
        </div>
    </form>
@stop

@push('_scripts')
    <script>
        $('._keyLexicon').click(function(e) {
            e.preventDefault();
            let value = $(this).attr('data-value');
            $('#config_key').val(value);
        });

        $('select[name="module_id"]').change(function() {
            let option = $(this).find('option:selected');

            $('._child_box').hide();

            let children = option.attr('data-children');
            if(children !== 'null') {
                let options = JSON.parse(children);
                $('._child_box select')
                    .empty()
                    .append('<option value="">-- Select a child model</option>');

                for(let i = 0; i < options.length; i++) {
                    $('._child_box select').append('<option value="' + options[i] + '">' + options[i] + '</option>');
                }

                $('._child_box').show();
            }
        });
    </script>
@endpush
