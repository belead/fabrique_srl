@extends('admin._master')

@section('_title')
    @lang('backend.title_operators') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.operators')}}">@lang('backend.title_operators')</a></li>
    <li class="active">@lang('backend.operator_edit')</li>
@stop

@section('_title_section')
    @lang('backend.operator_edit')
@stop

@section('_content')
    <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="panel panel-default text-center">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <a href="{{route('admin.operators')}}" class="btn btn-danger m-t-10 btn-rounded waves-effect waves-light">@lang('backend.cancel')</a>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" class="form-horizontal">
        {{csrf_field()}}

        <div class="col-sm-12">
            <div class="white-box p-l-20 p-r-20">
                <h3 class="box-title">@lang('backend.operator_attributes')</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_username')</label>
                            <div class="col-md-12">
                                <input required type="text" name="username" class="form-control" value="{{isset($user->id) ? $user->username : old('username')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_first_name')</label>
                            <div class="col-md-12">
                                <input type="text" name="first_name" class="form-control" value="{{isset($user->id) ? $user->first_name : old('first_name')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_last_name')</label>
                            <div class="col-md-12">
                                <input type="text" name="last_name" class="form-control" value="{{isset($user->id) ? $user->last_name : old('last_name')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_email')</label>
                            <div class="col-md-12">
                                <input required type="email" name="email" class="form-control" value="{{isset($user->id) ? $user->email : old('email')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_phone')</label>
                            <div class="col-md-12">
                                <input type="text" name="phone" class="form-control" value="{{isset($user->id) ? $user->phone : old('phone')}}">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">
                                Two Factors confirmed
                            </label>

                            <div class="col-md-12">
                                <div class="radio radio-primary radio-inline">
                                    <input type="radio" name="g2fa_confirmed" id="g2fa_confirmed1" value="1" {{$user->g2fa_confirmed ? 'checked' : ''}}>
                                    <label for="g2fa_confirmed1">Yes</label>
                                </div>

                                <div class="radio radio-primary radio-inline">
                                    <input type="radio" name="g2fa_confirmed" id="g2fa_confirmed0" value="0" {{!$user->g2fa_confirmed ? 'checked' : ''}}>
                                    <label for="g2fa_confirmed0">No</label>
                                </div>
                                
                                <br><br><small><sup>*</sup> Resets QR-code scan</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Access level *</label>
                            <div class="col-md-12">
                                <select required name="level" class="form-control">
                                    <option value="">-- Select a level</option>
                                    <option value="97" {{$user->level == 97 ? 'selected="selected"' : ''}}>Notaire</option>
                                    <option value="98" {{$user->level == 98 ? 'selected="selected"' : ''}}>Moderator (content manager)</option>
                                    @if(guard_admin()->level > 98)
                                        <option value="99" {{$user->level == 99 ? 'selected="selected"' : ''}}>Admin (standard)</option>
                                    @endif
                                    @if(guard_admin()->level > 99)
                                        <option value="100" {{$user->level == 100 ? 'selected="selected"' : ''}}>Superadmin</option>
                                    @endif
                                </select>
                                <br><small><sup>*</sup> Superadmin = Can manage partials, modules and administrators</small>
                                <br><small><sup>*</sup> Admin = Can manage administrators</small>
                                <br><small><sup>*</sup> Notaire = Accès aux utilisateurs et aux documents</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_password')</label>
                            <div class="col-md-12">
                                <input type="password" name="password" class="form-control" autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">@lang('backend.user_label_password_confirm')</label>
                            <div class="col-md-12">
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Socialite</h3>

                <div class="form-group">
                    <label class="col-md-12">Google Provider User Id</label>
                    <div class="col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-google"></i></div>
                            <input type="text" name="google_id" class="form-control" value="{{$user->socials->where('provider', 'google')->first()->provider_user_id ?? null}}">
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
            </div>
        </div>

        @if(isset($user->id))
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Permissions</h3>

                    <div class="form-group">
                        <label class="col-md-12">Grant access to</label>
                        <div class="col-md-12">
                            <select name="permissions[]" class="form-control _multiple" multiple>
                                <optgroup label="CMS">
                                    <option value="medias" {{$user->canAccess('medias', true) ? 'selected="selected"' : ''}}>Medias</option>
                                    <option value="menus" {{$user->canAccess('menus', true) ? 'selected="selected"' : ''}}>Menus</option>
                                    <option value="pages" {{$user->canAccess('pages', true) ? 'selected="selected"' : ''}}>Pages</option>
                                </optgroup>
                                <optgroup label="Modules">
                                    @foreach($modules as $module)
                                        <option value="{{$module->reference}}" {{$user->canAccess($module->reference, true) ? 'selected="selected"' : ''}}>{{$module->name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>

                            <br><small><sup>*</sup> If none are selected, permissions are granted on <u>every modules</u></small>
                        </div>
                    </div>

                    <button type="submit" class="btn-rounded btn btn-success waves-effect waves-light m-r-10">@lang('backend.save')</button>
                </div>
            </div>
        @endif
    </form>
@stop

@section('_scripts')
    <script>
        $('._multiple').multiSelect();
    </script>
@stop
