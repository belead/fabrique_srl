@extends('admin._master')

@section('_title')
    @lang('backend.title_operators') -
@stop

@section('_breadcrumb')
    <li><a href="{{route('admin.dashboard')}}">@lang('backend.title_dashboard')</a></li>
    <li><a href="{{route('admin.operators')}}">@lang('backend.title_operators')</a></li>
    <li class="active">@lang('backend.overview')</li>
@stop

@section('_title_section')
    @lang('backend.title_operators')
@stop

@section('_content')

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <table class="table table-striped m-b-0">
                    <thead>
                    <tr>
                        <th style="width: 50px;"></th>
                        <th style="width: 180px;">@lang('backend.created_at')</th>
                        <th style="width: 200px;">Administrator name</th>
                        <th style="width: 200px;">Concerning</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td class="text-center {{$log->getActionColor()}}"><i class="{{$log->getActionIcon()}}" style="font-size:18px;" title="{{$log->action}}"></i></td>
                            <td>{{date('d/m/Y', strtotime($log->created_at))}} at {{date('H:i', strtotime($log->created_at))}}</td>
                            <td>{{$log->admin->getName()}}</td>
                            <td>{{$log->item_type}}</td>
                            <td>{{$log->action_details}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{$logs->links()}}
            </div>
        </div>
    </div>
@stop
