<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('_title'){{config('_CMS._global.name')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/admin/img/favicon.png')}}">

    <!-- CSS Global -->
    {!! \App\Helpers\_CMS\TemplateHelper::styles(true) !!}
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">
</head>

<body>
<section id="wrapper" class="login-register">
    <div class="login-box login-sidebar">
        <div class="white-box no-shadow">
            <form class="form-horizontal form-material" id="loginform" method="POST">
                {{csrf_field()}}

                <a href="{{href_none()}}" class="text-center db">
                    <p>
                        <img src="{{asset('/assets/admin/img/logo_becms_green_white.svg')}}" style="max-width: 120px; margin-top:60px;"/>
                    </p>
                </a>

                @if(session('error'))
                    <div class="alert alert-danger" style="margin-top: 30px;">
                        {{session('error')}}
                    </div>
                @endif

                <div class="form-group m-t-40">
                    <div class="col-xs-12">
                        <input name="username" class="form-control" type="text" required placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input name="password" class="form-control" type="password" required placeholder="Password">
                    </div>
                </div>

                <div class="form-group text-center m-t-20 m-b-0">
                    <div class="col-xs-12">
                        <button class="btn btn-block btn-rounded btn-success waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

                <div class="login-separator">
                    <span>OR</span>
                </div>

                <div class="kt-login__actions m-t-10">
                    <a href="{{route('social.redirect', 'google')}}" class="btn btn-block btn-rounded btn-primary waves-effect waves-light" style="display: flex; justify-content: center; align-items: center;">
                        <img src="{{asset('assets/admin/img/google.png')}}" width="20" style="margin-right: 8px;">
                        {{ __('Continue with Google') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
</section>

{!! \App\Helpers\_CMS\TemplateHelper::scripts(true) !!}
</body>
</html>
