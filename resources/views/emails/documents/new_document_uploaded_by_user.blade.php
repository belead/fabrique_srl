@extends('emails._template')

@section('_title', 'Nouveau document reçu')

@section('_content')
    <p>
        Un nouveau document "{{$document->name}}" a été envoyé par {{$user->getName()}}
    </p>
@stop