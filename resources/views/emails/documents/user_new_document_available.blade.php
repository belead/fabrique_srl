@extends('emails._template')

@section('_title', 'Un nouveau document est disponible')

@section('_content')
    <p>
        Cher.ère {{$user->getName()}},
    </p>

    <p>Un nouveau document est disponible dans votre espace client.</p>

    <p>
        Vous retrouverez "{{$document->name}}" dans la section Documents.
    </p>

    @if($document->description)
        <p>
            {!! nl2br($document->description) !!}
        </p>
    @endif

    <p>
        Entrepreneurialement,<br>
        La Fabrique à SRL
    </p>
@stop
