@extends('emails._template')

@section('_title', 'Confirmation de votre inscription')

@section('_content')
    <p>
        Cher.ère {{$user->getName()}},
    </p>

    <p>Votre inscription à La Fabrique à SRL a bien été enregistrée...</p>

    <p>
        Entrepreneurialement,<br>
        La Fabrique à SRL
    </p>
@stop
