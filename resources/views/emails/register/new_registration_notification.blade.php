@extends('emails._template')

@section('_title', 'Nouvelle inscription sur la plateforme')

@section('_content')
    <p>
        Nouvelle inscription de : {{$user->getName()}}
    </p>
@stop
