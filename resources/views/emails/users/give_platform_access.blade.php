@extends('emails._template')

@section('_title', 'Accès à votre espace client')

@section('_content')
    <p>
        Cher.ère {{$user->getName()}},
    </p>

    <p>Vous avez maintenant accès à votre espace client.</p>
    <p>
        Vous pouvez vous connecter dès à présent avec ces identifiants :
    </p>

    <p>
        Email : <strong>{{$user->email}}</strong>
        <br>Mot de passe : <strong>{{$password}}</strong>
    </p>

    <p>
        Entrepreneurialement,<br>
        La Fabrique à SRL
    </p>
@stop
