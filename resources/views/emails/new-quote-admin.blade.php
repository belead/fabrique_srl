<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>La Fabrique à SRL</title>
    <style type="text/css">
        .ReadMsgBody { width: 100%; background-color: #FFFFFF; }
        .ExternalClass { width: 100%; background-color: #FFFFFF; }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
        html { width: 100%; }
        body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
        table { border-spacing: 0; table-layout: fixed; margin: 0 auto; }
        table table table { table-layout: auto; }
        .yshortcuts a { border-bottom: none !important; }
        img:hover { opacity: 0.9 !important; }
        a { color: #EA6434; text-decoration: none; }
        .textbutton a { font-family: 'open sans', arial, sans-serif !important;}
        .btn-link a { color:#FFFFFF !important;}

        @media only screen and (max-width: 480px) {
            body { width: auto !important; }
            *[class="table-inner"] { width: 90% !important; text-align: center !important; }
            *[class="table-full"] { width: 100% !important; text-align: center !important; }
            /* image */
            img[class="img1"] { width: 100% !important; height: auto !important; }
        }
    </style>
</head>

<body>
<!--header-->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eceff3">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" align="center">
                        <!--preference-->
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="table-inner">
                            <tr>
                                <td height="30"></td>
                            </tr>
                            <!-- preference -->
                            <!--<tr>-->
                            <!--<td align="center" class="preference-link" style="font-family: 'Open sans', Arial, sans-serif; color:#95a5a6; font-size:11px; line-height: 28px;font-style: italic;"> Try view on your <a href="#">browser</a> or <a href="#">Unsubscribe.</a></td>-->
                            <!--</tr>-->
                            <!-- end preference -->
                            <tr>
                                <td height="10"></td>
                            </tr>
                        </table>
                        <!--end preference-->
                        <table bgcolor="#FFFFFF" style="border-top-left-radius:6px;border-top-right-radius:6px;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table-inner">
                            <tr>
                                <td align="center">
                                    <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="40"></td>
                                        </tr>

                                        <!--end Header Logo-->
                                        <tr>
                                            <td height="10"></td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--end header-->
<!--image-->
<table align="center" bgcolor="#eceff3" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" align="center">
                        <table bgcolor="#FFFFFF" align="center" class="table-inner" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center">
                                    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="table-inner">
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 0px;">
                                                <a href="https://www.fabriquesrl.be/"> <img class="img1" style="display:block; line-height:0px; font-size:0px; border:0px;" src="https://www.fabriquesrl.be/assets/templates/finity/image/logo-fabrique-green.png" width="250px" height="auto" alt="img" /> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--end image-->
<!--1/1 Content-->
<table align="center" bgcolor="#eceff3" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" align="center">
                        <table bgcolor="#FFFFFF" align="center" class="table-inner" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center">
                                    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <!--title-->
                                        <tr>
                                            <td align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#44535A; font-size:22px;font-weight: bold; line-height: 28px;">Confirmation de réception</td>
                                        </tr>
                                        <!--end title-->
                                        <tr>
                                            <td align="center">
                                                <table width="50" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td height="20" style="border-bottom:3px solid #EA6434;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="15"></td>
                                        </tr>
                                        <!--content-->
                                        <tr>
                                            <td style="font-family: 'Open sans', Arial, sans-serif; color:#44535A; font-size:14px; line-height: 28px;">

                                                <p>Bonjour la Wiki Team!</p>

                                                <p>
                                                    Une nouvelle demande de {{$srl->firstname}} {{$srl->lastname}} est disponible !<br>
                                                    Vous pouvez la consulter dans l'administration de Wikipreneurs!
                                                </p>

                                                <p>
                                                    Agréable journée
                                                </p>

                                                <p>
                                                    <i>La Fabrique à SRL</i>
                                                </p>

                                            </td>
                                        </tr>
                                        <!--end content-->
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--end 1/1 Content-->
<!--footer-->
<table align="center" bgcolor="#eceff3" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" align="center">
                        <table align="center" class="table-inner" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="20" bgcolor="#FFFFFF"></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="50" align="right" valign="top">
                                                <table width="50" border="0" align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td height="25" bgcolor="#FFFFFF" style="border-bottom-left-radius:6px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center" background="http://www.digiflow.eu/assets/templates/emails/images/cta-bg.png" style="background-image: url(http://www.digiflow.eu/assets/templates/emails/images/cta-bg.png); background-repeat: repeat-x; background-size: auto; background-position: top;">
                                                <!--button-->
                                                <table style="border-radius:50px;" class="textbutton" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EA6434">
                                                    <tr>
                                                        <td class="btn-link" height="50" align="center" style="padding-left: 15px;padding-right: 15px; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px;color:#FFFFFF;"><a href="https://www.wikipreneurs.be">Accès au site</a></td>
                                                    </tr>
                                                </table>
                                                <!--end button-->
                                            </td>
                                            <td width="50" align="left" valign="top">
                                                <table width="50" border="0" align="left" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td height="25" bgcolor="#FFFFFF" style="border-bottom-right-radius:6px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!--social-->
                            <tr>
                                <td align="center">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center" style="line-height: 0px;">
                                                <a href="https://www.facebook.com/wikipreneurs/"> <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.wikipreneurs.be/assets/templates/mail/images/facebook.svg" alt="img" /> </a>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center" style="line-height: 0px;">
                                                <a href="https://www.instagram.com/wikipreneurs/"> <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.wikipreneurs.be/assets/templates/mail/images/instagram.svg" alt="img" /> </a>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center" style="line-height: 0px;">
                                                <a href="https://www.linkedin.com/company/wikipreneurs/"> <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.wikipreneurs.be/assets/templates/mail/images/linkedin.svg" alt="img" /> </a>
                                            </td>
                                        </tr>
                                        <!--end social-->
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <!--copyright-->
                            <tr>
                                <td align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#60757f; font-size:12px; line-height: 28px;font-style: italic;">Wikipreneurs © {{date('Y')}} All Rights Reserved</td>
                            </tr>
                            <!--end copyright-->
                            <tr>
                                <td height="40"></td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#44535A; font-size:14px; line-height: 28px;">
                                    Wikipreneurs est entièrement gratuit grâce à nos partenaires.<br>
                                    N'hésitez pas à visiter leurs sites web.
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#44535A; font-size:14px; line-height: 28px;">
                                    <a href="%unsubscribe_url%">Je veux me désinscrire et ne plus recevoir d'email</a>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td align="center" style="line-height: 0px; width: 100px;">
                                    <a href="https://www.belfius.be/professional/fr/accompagnement/starters/index.aspx?document.referrer=https:%2F%2Fwww.belfius.be%2Fstarters">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;width:100px;" src="http://www.digiflow.eu/assets/front/img/pictures/Partenaires/Belfius.png" alt="img" />
                                    </a>
                                </td>
                                <td width="15"></td>
                                <td align="center" style="line-height: 0px; width: 100px;">
                                    <a href="https://www.partena-professional.be/fr/">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;width:100px;" src="http://www.digiflow.eu/assets/front/img/pictures/Partenaires/Partena.png" alt="img" />
                                    </a>
                                </td>
                                <td width="15"></td>
                                <td align="center" style="line-height: 0px; width: 100px;">
                                    <a href="https://www.orange.be/fr/independants/orange-pour-professionnels">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;width:100px;" src="http://www.digiflow.eu/assets/front/img/pictures/Partenaires/Orange.png" alt="img" />
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td height="40"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--end footer-->
</body>

</html>
