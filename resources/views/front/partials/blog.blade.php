@if($news->count() > 0)
<section class="our-blog">
    <div class="container">
        <div class="col-sm-12">
            <div class="mb-10 mb-md-0 text-center text-md-left aos-init aos-animate" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                <h2 class="font-size-11 font-weight-medium pr-md-10 pr-xl-0 mb-0">
                    Nos derniers articles
                </h2>
            </div>
        </div>
        <div class="row mt-5">
            @foreach($news as $new)
                <div class="col-md-4 p-4 sameH">
                    <div class="border rounded-8">
                        <div class="position-relative w-100" style="height: 250px;background-image: url('{{$new->getPicture()}}'); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 70px; height: 70px; background-color: #336666;">
                                <small>{{$new->created_at->format('d')}}</small>
                                <small><b>{{$new->created_at->format('M')}}</b></small>
                            </div>
                            {{--                    <a href="#" class="position-absolute px-3 py-2 text-white" style="bottom:10px; left: 10px; background-color: #336666;"><small>PHOTOS</small></a>--}}
                        </div>
                        <div class="px-8 pt-8 pb-8 sameH">
                            <a href="{{route('front.page.module.details', ['news', 'articles', $new->slug])}}" class="d-inline-block"><h4 class="text-dark">{{$new->title}}</h4></a>
                            <p class="tex-secondary">{{$new->description_short}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif

@section('_scripts')
    <script src="{{asset('assets/front/js/matchHeight.js')}}" type="text/javascript"></script>
    <script>
        $('.sameH').matchHeight();
    </script>
@stop