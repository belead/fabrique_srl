<!-- testiMonials Area -->
<div class="pt-10 pt-md-14 pt-lg-22 pb-11 pb-md-14 pb-lg-23 border-top border-default-color">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-9 col-md-11">
                <div class="l4-review-slider mt-0 mt-lg-0 mx-xl-0 mx-lg-10 mx-md-8">
                    <!-- Single Review -->
                    <div class="single-review text-center focus-reset">
                        <p class="font-size-7 font-weight-bold heading-default-color">{!! $content->items->description !!}</p>
                        <div class="mt-13">
                            <div class="mb-8 circle-100 mx-auto overflow-hidden">
                                <img src="{{asset('/uploads/img/modules/content_blocks')}}/{{$content->items->image}}" class="w-100" alt="" />
                            </div>
                            <h5 class="font-size-6 mb-0">{{ $content->items->title }}</h5>
                            <p class="font-size-5">{!! $content->items->head_title !!}</p>
                        </div>
                    </div>
                    <!-- End Single Review -->

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End testiMonials Area -->
