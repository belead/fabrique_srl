<div class="bg-default-4 pb-12 pt-20 pb-md-16 pb-lg-25">
    <a name="comment"></a>
    <div class="container">
        <div class="row justify-content-center justify-content-md-start">
            <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12">
                <div class="section-title aos-init aos-animate" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                    <h2 class="font-size-10 mb-10  text-center text-md-left">{!! $content->items->head_title !!}</h2>
                    <p class="heading-default-color font-size-6 mb-10">@lang('variables.homepage.help.title_one') @lang('variables.help.title_two') @lang('variables.help.title_three')</p>
                </div>
            </div>
        </div>
        <div class="row align-items-lg-center justify-content-center">
            <!-- Right -->
            <div class="col-xl-6 col-lg-6 col-md-5 col-xs-8 aos-init aos-animate" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                <div class="content-image-group-3 mb-17 mb-lg-0 mr-xl-16">
                    <div class="img-1">
                        <img class="w-100 rounded-10" src="{{asset('assets/templates/finity/image/fabrique/etapes1.jpg')}}" alt="">
                        <div class="img-2">
                            <img class="w-100 rounded-10" src="{{asset('assets/templates/finity/image/home-2/png/dot-pattern-black.png')}}" alt="">
                        </div>
                        <div class="img-3 rounded-10">
                            <img class="w-100 mix-blend-multiply" src="{{asset('assets/templates/finity/image/home-2/png/left-circlehalf-shape.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Right -->
            <!-- Left -->
            <div class="col-lg-6 col-md-7 col-xs-10 aos-init aos-animate" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                <div class="mr-lg-10 mr-xl-25">
                    {!! $content->items->description !!}
                    <div class="btn-group mb-12">
                        <a class="btn btn-greenfabrique rounded-5" href="/demander-un-devis">{{$content->items->call_to_action_text}}</a>
                    </div>
                </div>
            </div>
            <!-- Left End -->
        </div>
    </div>
</div>
