@include('front.partials.discover', ['head' => 'Les diagnostics de "La Fabrique à SRL" permettent en deux minutes de  :', 'name'=> 'Les diagnostics de "La Fabrique à SRL" permettent en deux minutes de  :',
            'title' => 'Comprendre si vous devez passer en indépendant / personne physique ou en société SRL en fonction de votre vision & de vos objectifs.',
            'description' => '3, 2, 1… C’est parti. Bon check!',
            'head_size'=>'font-size-6 color-orange',
            'description_size' => 'font-size-7 color-white',
            'title_size'=>'font-size-9 color-white',
            'info_icon' => 'fa-question-circle',
            'hide_info' => 1,
            'button_txt' => 'Démarrer le diagnostic',
            'button_url' => route('front.diagnostics.form', '1-la-bonne-solution-pour-vous-personne-physique-independante-ou-societe-srl'),
            'image' => asset('assets/templates/finity/image/fabrique/diagnostic.jpg'),
            'section_classes' => 'pt-15 pt-md-12 pt-lg-24 pb-15 pb-md-12 pb-lg-24 bg-green',
            ])

<section class="our-blog bg-white">

    <div class="container">
        <div class="row mt-5">
            @foreach($content->items as $diagnostic)
                <div class="col-md-4 p-4 sameH ">
                    <div class="border rounded-8">
                        <div class="position-relative w-100" style="height: 250px;background-image: url('{{$diagnostic->getPicture()}}'); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            {{--                    <a href="#" class="position-absolute px-3 py-2 text-white" style="bottom:10px; left: 10px; background-color: #336666;"><small>PHOTOS</small></a>--}}
                        </div>
                        <div class="px-8 pt-8 pb-8 sameH">
                            <a href="{{route('front.diagnostics.form', $diagnostic->getSlug())}}" class="d-inline-block"><h4 class="text-dark">{{$diagnostic->title}}</h4></a>
                            <p><a href="{{route('front.diagnostics.form', $diagnostic->getSlug())}}" class="btn btn-primary btn-greenfabrique rounded-4 mt-4 btn-block btn-sm">Démarrer</a></p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
