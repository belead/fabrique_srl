<div class="container">
    <section id="services" class="section services-section has-background-white is-clearfix _diagnostic_registration">
        <h2 class="heading-title style-3">
            <small>Etape 0/{{($diagnostic->categories->count() + 1)}}</small>
            <br>Démarrer le diagnostic
        </h2>

        <form method="POST" action="{{route('front.diagnostics.register', $diagnostic->getSlug())}}">
            @csrf

            <div class="diagnostic-register">

                <div class="field">
                    <div class="control is-expanded">
                        <label for="">Nom & prénom *</label>
                        <input required class="input" type="text" name="name" placeholder="John Doe" value="{{old('name')}}" autocomplete="off">
                    </div>
                </div>

                <div class="field">
                    <div class="control is-expanded">
                        <label for="">Nom du projet ou de l'entreprise. *</label>
                        <input required class="input" type="text" name="project_name" placeholder="Wikipreneurs" value="{{old('project_name')}}" autocomplete="off">
                    </div>
                </div>

                <div class="field">
                    <div class="control is-expanded">
                        <label for="">Adresse email *</label>
                        <input required class="input" type="email" name="email" placeholder="Adresse email" value="{{old('email')}}" autocomplete="off">
                    </div>
                </div>

                <div class="field">
                    <div class="control is-expanded">
                        <label for="">Je participe à l'accélérateur Forward</label>
                        <input type="checkbox" name="optin" id="optin" required> J'accepte <a href="{{route('front.page', ['terms-and-conditions'])}}">la politique de confidentialité des données</a>
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        <div class="level">
                            <div class="level-left"></div>
                            <div class="level-right">
                                <button class="button">Continuer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
