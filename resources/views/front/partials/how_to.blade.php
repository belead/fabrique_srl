<div class="bg-images bg-dark-cloud">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-xl-8 col-lg-9 col-md-11">
                <div class="text-center pt-14 pt-md-18 pb-15 pb-md-19 py-lg-20 dark-mode-texts aos-init aos-animate" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                    <h3 class="mb-7">
                        {{$content->items->head_title}} <br>
                    </h3>
                    <h4>{{$content->items->title}}</h4>
                    <p class="font-size-5 line-height-28 px-md-15 px-lg-23 px-xl-25 mb-0">
                        {!! $content->items->description !!}
                    </p>
                    <a href="{{$content->items->custom_url ?? ''}}" class="btn btn-greenfabrique btn-xl h-55 rounded-5">{{$content->items->call_to_action_text}}</a>
                </div>
            </div>
        </div>
    </div>
</div>