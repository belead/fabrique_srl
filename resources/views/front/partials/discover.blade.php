<a name="pourqui"></a>
<div class="{{($section_classes) ?? 'pt-15 pt-md-19 pt-lg-30 pb-15 pb-md-19 pb-lg-26'}}">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-xl-6 col-lg-7 col-md-9 order-2 order-lg-1">
                <div class="mt-13 mt-lg-0" data-aos="fade-right" data-aos-once="true">
                    <h6 class="{{(isset($head_size)) ? $head_size : 'font-size-3 text-orange' }} text-uppercase mb-9 letter-spacing-normal">{{(isset($content->items->name)) ? $content->items->name : ($name ?? '')}}</h6>
                    <h2 class="{{(isset($title_size)) ? $title_size : 'font-size-10' }} mb-8 letter-spacing-n83">{!! (isset($content->items->title)) ? $content->items->title : ($title ?? '') !!}</h2>
                    <div class="{{(isset($description_size)) ? $description_size : '' }}">
                        {!! (isset($content->items->description)) ? $content->items->description : ($description ?? '') !!}
                    </div>

                    @if((isset($content->items->call_to_action_text)))
                    <div class="mt-12">
                        <a class="btn btn-greenfabrique btn-xl h-55 rounded-5" href="/demander-un-devis">{{(isset($content->items->call_to_action_text)) ? $content->items->call_to_action_text : ''}}</a>
                    </div>
                    @endif

                    @if((isset($button_txt)))
                        <div class="mt-12">
                            <a class="btn btn-greenfabrique btn-xl h-55 rounded-5" href="{{$button_url ?? '#'}}">{{$button_txt}}</a>
                        </div>
                    @endif
                </div>
            </div>
            <!-- Right Image -->
            <div class="col-xl-5 offset-xl-1 col-lg-5 col-md-6 col-xs-11 order-1 order-lg-2">
                <div class="l4-content-img-2 ml-lg-10 ml-xl-7 rounded-10 text-center" data-aos="fade-left" data-aos-duration="600" data-aos-once="true">
                    <img class="w-75 w-md-100 rounded-15" src="{{(isset($image)) ? $image : asset('assets/templates/finity/image/fabrique/demarrer1.jpg')}}" alt="">

                    @if(!isset($hide_info))
                    <div class="image-card w-fit-content bg-white rounded-15 d-flex align-items-center shadow-10 p-8">
                        <div class="mr-6 square-59 bg-dodger-blue-1-op1 rounded-10 text-dodger-blue-1 font-size-7">
                            <i class="fas {{($info_icon) ?? 'fa-thumbs-up'}}"></i>
                        </div>
                        <div class="text-left">
                            <h4 class="font-size-6 text-dark-cloud mb-0 line-height-26">Un seul interlocuteur?</h4>
                            <p class="font-size-5 text-stone mb-0">Obtenez les réponses à vos questions<br>grâce à un seul interlocuteur ?</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- End Right Image -->
        </div>
    </div>
</div>
