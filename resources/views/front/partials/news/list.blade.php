<div class="container my-5 pt-20 mt-20 pb-20">
    <div class="text-center mb-5">
        <span class="text-uppercase">Blog</span>
        <h1 class="text-capitalize font-weight-bold mt-2">Nos Derniers <span style="color:#336666">Articles</span></h1>
    </div>
    <div class="row">
        @foreach($content->items as $item)
            <div class="col-md-4 p-4 sameH">
                <div class="border rounded-8">
                    <div class="position-relative w-100" style="height: 250px;background-image: url('{{$item->getPicture()}}'); background-size: cover; background-position: center;">
                        <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                        <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 70px; height: 70px; background-color: #336666;">
                            <small>{{$item->created_at->format('d')}}</small>
                            <small><b>{{$item->created_at->format('M')}}</b></small>
                        </div>
                        {{--                    <a href="#" class="position-absolute px-3 py-2 text-white" style="bottom:10px; left: 10px; background-color: #336666;"><small>PHOTOS</small></a>--}}
                    </div>
                    <div class="px-8 pt-8 pb-8 sameH">
                        <a href="{{route('front.page.module.details', ['news', 'articles', $item->slug])}}" class="d-inline-block"><h4 class="text-dark">{{$item->title}}</h4></a>
                        <p class="tex-secondary" style="font-size: 1rem;">{{$item->description_short}}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
