@if($item->meta_title)
    @section('_title')
        {{$item->meta_title}}
    @endsection
@endif

@if($item->meta_description)
    @section('_meta_desc')
        {{$item->meta_description}}
    @endsection
@endif

<div class="container pt-15 pb-15 mt-20">
    <div class="row justify-content-center">
        <div class="col-xl-10">
            <!-- job-details-content -->
            <div class="text-center pb-0">
                <h2 class="font-size-11 font-weight-bold text-center mb-lg-15 mb-0" style="color: #336666;">{{$item->title}}</h2>
            </div>
        </div>
        <div class="col-xl-10 col-lg-10 px-xl-0 px-lg-6 px-md-0 px-6 pr-0">
            <!-- terms-contents  -->
            <div class="pt-lg-0 pt-10 pl-lg-10 px-xl-15">
                {!! $item->content !!}
            </div>
        </div>
    </div>
</div>