<div class="mb-10" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
    <h1 class="mb-1 font-size-10 letter-spacing-n83">{{$content->items->head_title}}</h1>
    {!! $content->items->description !!}
    <h2 class="mb-1 font-size-9 letter-spacing-n83 mt-15">{{$content->items->title}}</h2>

</div>
