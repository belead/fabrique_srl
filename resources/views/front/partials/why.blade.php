<a name="apropos"></a>
<div class="bg-dark-cloud pt-15 pt-md-18 pt-lg-20 pt-xl-21 pb-15 pb-md-18 pb-lg-20">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <!-- Left Image -->
            <div class="col-xl-6 col-lg-5 col-md-8 col-xs-11">
                <!-- content img start -->
                <div class="l4-content-img-group">
                    <div class="img-1">
                        <img class="w-100 rounded-15" src="{{asset('assets/templates/finity/image/fabrique/apropos1.jpg')}}" alt="" data-aos="fade-right" data-aos-duration="300" data-aos-delay="200" data-aos-once="true">
                    </div>
                    <div class="img-2">
                        <img class="w-100 rounded-15" src="{{asset('assets/templates/finity/image/fabrique/apropos3.jpg')}}" alt="" data-aos="fade-left" data-aos-duration="300" data-aos-delay="400" data-aos-once="true">
                    </div>
                    <div class="img-3">
                        <img class="w-100 rounded-15" src="{{asset('assets/templates/finity/image/fabrique/apropos2.jpg')}}" alt="" data-aos="fade-up" data-aos-duration="300" data-aos-delay="600" data-aos-once="true">
                    </div>
                </div>
            </div>
            <!-- End Left Image -->
            <!-- Right Image -->
            <div class="col-xl-5 offset-lg-1 col-lg-6 col-md-8 mt-n13 mt-md-n8 mt-lg-n0">
                <div class="pt-30 pt-lg-0 mt-lg-0 dark-mode-texts" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                    <h6 class="font-size-3 text-orange text-uppercase mb-9 letter-spacing-normal">{{$content->items->name}}</h6>
                    <h2 class="font-size-10 mb-8 letter-spacing-n83 pr-xs-22 pr-sm-18 pr-md-0">{{$content->items->head_title}}</h2>
                    <p class="font-size-7 mb-5">{{$content->items->title}}</p>
                    {!! $content->items->description !!}
                    <div class="mt-12">
                        <a target="_blank" class="btn btn-greenfabrique btn-xl h-55 rounded-5 font-weight-normal" href="https://www.wikipreneurs.be">{{$content->items->call_to_action_text}}</a>
                    </div>
                </div>
            </div>
            <!-- End Right Image -->
        </div>
    </div>
</div>
