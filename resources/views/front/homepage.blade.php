@extends('front._master', ['no_header' => false, 'no_footer' => false])

@section('_content')
    @if(isset($contents_by_ref['how_to']))
        @php
            $item = $contents_by_ref['how_to'];
        @endphp
        @if($item->partial)
            @include('front/partials/' . $item->partial->partial_path, ['item' => $item, 'content' => $contents_by_ref['how_to']])
        @endif
    @endif

    @if(isset($contents_by_ref['discover']))
        @php
            $item = $contents_by_ref['discover'];
        @endphp
        @if($item->partial)
            @include('front/partials/' . $item->partial->partial_path, ['item' => $item, 'content' => $contents_by_ref['discover']])
        @endif
    @endif

    @include('front.blocks.more')

    @if(isset($contents_by_ref['help']))
        @php
            $item = $contents_by_ref['help'];
        @endphp
        @if($item->partial)
            @include('front/partials/' . $item->partial->partial_path, ['item' => $item, 'content' => $contents_by_ref['help']])
        @endif
    @endif

    @include('front.blocks.steps')

    @if(isset($contents_by_ref['why']))
        @php
            $item = $contents_by_ref['why'];
        @endphp
        @if($item->partial)
            @include('front/partials/' . $item->partial->partial_path, ['item' => $item, 'content' => $contents_by_ref['why']])
        @endif
    @endif

    @if(isset($contents_by_ref['testimonial']))
        @php
            $item = $contents_by_ref['testimonial'];
        @endphp
        @if($item->partial)
            @include('front/partials/' . $item->partial->partial_path, ['item' => $item, 'content' => $contents_by_ref['testimonial']])
        @endif
    @endif

    @include('front/partials/blog')


    @include('front.blocks.create')
    @include('front.blocks.partners')
@endsection
