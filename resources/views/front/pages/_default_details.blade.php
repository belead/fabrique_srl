@extends('front._master', ['no_banner' => true])

@section('_content')
    @include('front/partials/' . $partial_details->partial_path, ['item' => $item])
@stop
