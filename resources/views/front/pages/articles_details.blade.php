@extends('front._master', ["no_banner" => true])

@section('header_title')
    <span style="font-weight:300">{{$item->title}}</span>
@endsection

@section('header_desc')
    {{$item->description}}
@endsection

@section('banner_style', 'min-height:0px!important;')


@section('_content')
    <div class="container" style="padding-top: 20vh; padding-bottom: 15vh;" >
        <div class="row justify-content-center">
            <div class="col-xl-11">
                <div class="text-center pb-0">
                    <h2 class="font-size-11 font-weight-bold text-center mb-lg-15 mb-0">{{$item->title}}</h2>
                </div>
            </div>
            <div class="col-xl-9 col-lg-10 px-xl-0 px-lg-6 px-md-0 px-6 pr-0">
                <div class="pt-lg-0 pt-10 pl-lg-10 px-xl-15">
                    {!! $item->content !!}
                    <small>Publié le <strong>{{$item->created_at->format('d/m/Y à H:i')}}</strong></small>
                </div>
            </div>
        </div>
    </div>

@endsection
