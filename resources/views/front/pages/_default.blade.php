@extends('front._master', ['no_banner' => true])

@section('_content')
    @if($contents)

        @include('front.blocks._content_includer')

    @endif
@stop
