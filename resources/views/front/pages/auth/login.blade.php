@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    <div class="min-height-100vh d-flex align-items-center bg-default-3">
        <div class="container-fluid h-100">
            <div class="row no-gutters h-100">
                <div class="col-xl-7 col-lg-7 col-md-10">
                    <div class="pt-26 pt-md-17 pt-lg-18 pb-md-4 pb-lg-10 max-w-500 mx-auto">
                        <h3>Se connecter à mon compte</h3>
                        <h4 class="text-muted">Entrez vos identifiants</h4>

                        @if(session('error'))
                            <p class="alert alert-danger">
                                {{session('error')}}
                            </p>
                        @endif

                        <form method="POST" style="margin-top: 40px;">
                            @csrf

                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" required name="email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Mot de passe</label>
                                <input type="password" required name="password" class="form-control">
                            </div>

                            <div class="button mb-6 mt-5">
                                <button type="submit" class="btn btn-greenfabrique w-100 rounded-4">
                                    Connexion
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-xl-5 col-lg-5 col-md-10 min-height-lg-100vh" style="padding:20px 10px 20px 0;">
                    <div class="bg-images min-height-100vh d-none d-lg-block" style="background-image: url({{asset('assets/templates/finity/image/fabrique/register.jpg')}}); position: -webkit-sticky; position: sticky;"></div>
                </div>
            </div>
        </div>
    </div>
@stop
