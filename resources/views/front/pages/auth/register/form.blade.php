@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    <div class="min-height-100vh d-flex align-items-center bg-default-3">
        <div class="container-fluid h-100">
            <div class="row no-gutters h-100">
                <div class="col-xl-7 col-lg-7 col-md-10">
                    <div class="pt-26 pt-md-17 pt-lg-18 pb-md-4 pb-lg-10 max-w-500 mx-auto">
                        <h3>Créez votre SRL</h3>
                        <h4 class="text-muted">Étape 2/2</h4>

                        @if(session('error'))
                            <p class="alert alert-danger">
                                {{session('error')}}
                            </p>
                        @endif

                        <form action="{{route('front.register.step2.post')}}" method="POST" style="margin-top: 40px;">
                            @csrf

                            <h5 style="margin-bottom: 20px;">1. Les fondateurs.rices</h5>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nom *</label>
                                        <input type="text" required name="main_contact_first_name" class="form-control" value="{{session('register_user.last_name')}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Prénom *</label>
                                        <input type="text" required name="main_contact_last_name" class="form-control" value="{{session('register_user.first_name')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Email *</label>
                                <input type="email" required name="main_contact_email" class="form-control" value="{{session('register_user.email')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Téléphone *</label>
                                <input type="text" required name="main_contact_phone" class="form-control" value="{{session('register_user.phone')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Quelle est votre nationalité ? *</label>
                                <input type="text" required name="main_contact_nationality" class="form-control" value="{{session('register_user.nationality')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Adresse complète *</label>
                                <textarea required name="main_contact_address" class="form-control" rows="2"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Y a-t-il d'autres fondateurs.rices ? (Si oui, combien ?) *</label>
                                <input name="founders_count" type="number" class="form-control" step="1" min="0">
                            </div>

{{--                            <div class="form-group">--}}
{{--                                <label for="">Sont-ils/elles marié.es ?</label>--}}
{{--                                <div class="d-flex align-items-center">--}}
{{--                                    <div class="radio radio-primary radio-inline mr-4">--}}
{{--                                        <input type="radio" name="founders_are_married" id="founders_are_married1" value="1">--}}
{{--                                        <label for="founders_are_married1">Oui</label>--}}
{{--                                    </div>--}}

{{--                                    <div class="radio radio-primary radio-inline">--}}
{{--                                        <input type="radio" name="founders_are_married" id="founders_are_married0" value="0" checked>--}}
{{--                                        <label for="founders_are_married0">Non</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="">Si oui, y a t-t-il un contrat de mariage ?</label>--}}
{{--                                <div class="d-flex align-items-center">--}}
{{--                                    <div class="radio radio-primary radio-inline mr-4">--}}
{{--                                        <input type="radio" name="founders_have_wedding_contract" id="founders_have_wedding_contract1" value="1">--}}
{{--                                        <label for="founders_have_wedding_contract1">Oui</label>--}}
{{--                                    </div>--}}

{{--                                    <div class="radio radio-primary radio-inline">--}}
{{--                                        <input type="radio" name="founders_have_wedding_contract" id="founders_have_wedding_contract0" value="0">--}}
{{--                                        <label for="founders_have_wedding_contract0">Non</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="">Quelle est leur nationalité ?</label>--}}
{{--                                <textarea name="founders_nationalities" class="form-control" rows="3"></textarea>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="">Quelle est leur région de résidence ?</label>--}}
{{--                                <textarea name="founders_residential_region" class="form-control" rows="3"></textarea>--}}
{{--                            </div>--}}


                            <h5 style="margin-bottom: 20px;">2. L'entreprise</h5>

                            <div class="form-group">
                                <label for="">Nom de l’entreprise à créer *</label>
                                <input type="text" required name="company_name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Adresse du siège social de l’entreprise à créer *</label>
                                <textarea required name="company_address" class="form-control" rows="2"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Quand voulez-vous lancer l’entreprise ? * (mois/année)</label>
                                <input type="text" required name="company_launching_at" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Dans quel secteur d’activité principal ? *</label>
                                <input type="text" required name="company_main_sector" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Expliquez le projet en quelques lignes *</label>
                                <textarea required name="company_project_description" class="form-control" rows="4"></textarea>
                            </div>

                            <h5 style="margin-bottom: 20px;">3. Le plan financier</h5>

                            <p class="text-black">
                                Concernant le plan financier, qui doit être déposé, dont les exigences de fond et de forme ont été renforcées, et qui engage votre responsabilité :
                            </p>

                            <h6>Il est prêt, complet :</h6>
                            <div class="form-group">
                                <label for="">
                                    Budget, plan de trésorerie, compte de résultat, hypothèses d’exploitation et de financement et situations bilantaires. Je n’ai besoin de rien.
                                </label>
                                <div class="d-flex align-items-center">
                                    <div class="radio radio-primary radio-inline mr-4">
                                        <input type="radio" name="business_plan_is_ready" id="business_plan_is_ready1" value="1">
                                        <label for="business_plan_is_ready1">Oui</label>
                                    </div>

                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" name="business_plan_is_ready" id="business_plan_is_ready0" value="0" checked>
                                        <label for="business_plan_is_ready0">Non</label>
                                    </div>
                                </div>
                            </div>

                            <h6>Il est quasi prêt et je souhaite :</h6>
                            <div class="form-group">
                                <label for="">
                                    Un feed-back rapide sur le document
                                </label>
                                <div class="d-flex align-items-center">
                                    <div class="radio radio-primary radio-inline mr-4">
                                        <input type="radio" name="business_plan_needs_quick_feedback" id="business_plan_needs_quick_feedback1" value="1">
                                        <label for="business_plan_needs_quick_feedback1">Oui</label>
                                    </div>

                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" name="business_plan_needs_quick_feedback" id="business_plan_needs_quick_feedback0" value="0" checked>
                                        <label for="business_plan_needs_quick_feedback0">Non</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">
                                    La mise en forme du document au niveau des situations bilantaires
                                </label>
                                <div class="d-flex align-items-center">
                                    <div class="radio radio-primary radio-inline mr-4">
                                        <input type="radio" name="business_plan_needs_financial_formatting" id="business_plan_needs_financial_formatting1" value="1">
                                        <label for="business_plan_needs_financial_formatting1">Oui</label>
                                    </div>

                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" name="business_plan_needs_financial_formatting" id="business_plan_needs_financial_formatting0" value="0" checked>
                                        <label for="business_plan_needs_financial_formatting0">Non</label>
                                    </div>
                                </div>
                            </div>

                            <h6>J'ai une base mais je souhaite :</h6>
                            <div class="form-group">
                                <label for="">
                                    Un accompagnement pour finaliser le document
                                </label>
                                <div class="d-flex align-items-center">
                                    <div class="radio radio-primary radio-inline mr-4">
                                        <input type="radio" name="business_plan_needs_finalizing_support" id="business_plan_needs_finalizing_support1" value="1">
                                        <label for="business_plan_needs_finalizing_support1">Oui</label>
                                    </div>

                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" name="business_plan_needs_finalizing_support" id="business_plan_needs_finalizing_support0" value="0" checked>
                                        <label for="business_plan_needs_finalizing_support0">Non</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">
                                    La mise en forme du document (budget, plan de trésorerie,
                                    compte de résultat, hypothèses d’exploitation et de financement
                                    et situations bilantaires)
                                </label>
                                <div class="d-flex align-items-center">
                                    <div class="radio radio-primary radio-inline mr-4">
                                        <input type="radio" name="business_plan_needs_full_formatting" id="business_plan_needs_full_formatting1" value="1">
                                        <label for="business_plan_needs_full_formatting1">Oui</label>
                                    </div>

                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" name="business_plan_needs_full_formatting" id="business_plan_needs_full_formatting0" value="0" checked>
                                        <label for="business_plan_needs_full_formatting0">Non</label>
                                    </div>
                                </div>
                            </div>

                            <h5 style="margin-bottom: 20px;">4. La communauté</h5>

                            <div class="form-group">
                                <label for="">
                                    Etes-vous intéressé.e par une mise en avant de votre projet via la communauté
                                    Wikipreneurs, une fois l’entreprise lancée ?
                                </label>
                                <div class="d-flex align-items-center">
                                    <div class="radio radio-primary radio-inline mr-4">
                                        <input type="radio" name="sharing_agreement" id="sharing_agreement1" value="1">
                                        <label for="sharing_agreement1">Oui</label>
                                    </div>

                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" name="sharing_agreement" id="sharing_agreement0" value="0" checked>
                                        <label for="sharing_agreement0">Non</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Comment avez-vous entendu parler de « La Fabrique à SRL » ? *</label>
                                <textarea required name="how" class="form-control" rows="2"></textarea>
                            </div>

                            <div class="button mb-6 mt-5">
                                <button type="submit" class="btn btn-greenfabrique w-100 rounded-4">
                                    Valider mes données
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-xl-5 col-lg-5 col-md-10 min-height-lg-100vh" style="padding:20px 10px 20px 0;">
                    <div class="bg-images min-height-100vh d-none d-lg-block" style="background-image: url({{asset('assets/templates/finity/image/fabrique/register.jpg')}}); position: -webkit-sticky; position: sticky;"></div>
                </div>
            </div>
        </div>
    </div>
@stop
