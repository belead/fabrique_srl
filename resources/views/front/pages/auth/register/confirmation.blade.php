@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    <div class="row">
        <div class="col-md-4 offset-md-4 d-flex justify-content-center align-items-end" style="height: 40vh;">
            <i class="fas fa-thumbs-up" style="color:#336666; font-size:10em;"></i>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 d-flex justify-content-center align-items-center mt-5 flex-column">
            <h2>{{$user->getName()}}, merci pour votre demande !</h2>

            <p>
                Nous vous confirmons votre rendez-vous téléphonique avec Xavier De Poorter
                <br>Consultez votre boîte mail ({{$email ?? ''}}) pour retrouver l'invitation.
            </p>

            @if($date)
                <h3 class="text-center d-flex flex-column">
                    <span>{{$date->format('d F Y')}}</span>
                    <span class="mt-2 font-italic text-dark">
                        <i class="fa fa-clock-o mr-1 text-dark"></i>
                        {{$date->format('H:i')}}
                    </span>
                </h3>
            @endif
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12 d-flex justify-content-center">
            <p><small>Vous allez être redirigé vers la page d'accueil...</small></p>
        </div>

        <div class="col-md-12 d-flex justify-content-center">
            <a href="https://lafabriqueasrl.be" class="btn" style="background: #336666; color:white;">Cliquez ici pour être redirigé directement.</a>
        </div>
    </div>
@endsection

@section('_scripts')
    <script>
        setTimeout(function() {
            window.location.replace('https://lafabriqueasrl.be');
        }, 10000)
    </script>
@endsection
