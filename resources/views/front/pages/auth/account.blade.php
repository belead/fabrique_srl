@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    @includeIf('front.pages.auth.blocks.account.header')

    <div class="d-flex align-items-center p-10">
        <div class="container h-100">
            @includeIf('front.pages.auth.blocks.account.navigation', ['active' => 'account'])

            <div style="margin-top: 60px;">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        @if(session('success'))
                            <p class="alert alert-success">
                                {{session('success')}}
                            </p>
                        @endif

                        @if(session('error'))
                            <p class="alert alert-danger">
                                {{session('error')}}
                            </p>
                        @endif

                        <form method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nom *</label>
                                        <input type="text" required name="last_name" class="form-control" value="{{auth()->user()->last_name}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Prénom *</label>
                                        <input type="text" required name="first_name" class="form-control" value="{{auth()->user()->first_name}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">E-mail *</label>
                                <input type="email" required name="email" class="form-control" value="{{auth()->user()->email}}">
                            </div>

                            <div class="form-group">
                                <label for="">Nouveau mot de passe</label>
                                <input type="password" name="password" class="form-control" autocomplete="new-password">
                            </div>

                            <div class="form-group">
                                <label for="">Confirmation du mot de passe</label>
                                <input type="password" name="password_confirmation" class="form-control" autocomplete="new-password">
                            </div>

                            <div class="button text-right">
                                <button type="submit" class="btn btn-greenfabrique rounded-4">
                                    Enregistrer
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
