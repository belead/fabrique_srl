@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    @includeIf('front.pages.auth.blocks.account.header')

    <div class="d-flex align-items-center p-10">
        <div class="container h-100">
            @includeIf('front.pages.auth.blocks.account.navigation', ['active' => 'followup'])

            @php
                $user = auth()->user();
            @endphp
            <div style="margin-top: 60px;">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <table class="table table-hover mb-0">
                            <tr>
                                <td colspan="2">
                                    <strong>1.	Contact avec la banque. 0uverture de compte à la banque</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">1.	Envoi de la demande</td>
                                <td style="text-align: right;">
                                    @if($user->followup_bank_sent)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">2.	Acceptation et réception du numéro IBAN</td>
                                <td style="text-align: right;">
                                    @if($user->followup_bank_iban)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">3.	Finalisation et signature</td>
                                <td style="text-align: right;">
                                    @if($user->followup_bank_signature)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">4.	Versement du capital</td>
                                <td style="text-align: right;">
                                    @if($user->followup_bank_capital_payment)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">5.	Réception de l’attestation bancaire et envoi au notaire</td>
                                <td style="text-align: right;">
                                    @if($user->followup_bank_certificate)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">6.	Réception des cartes de banque</td>
                                <td style="text-align: right;">
                                    @if($user->followup_bank_cards)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong>2.	Check Plan financier</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">1.	Description du projet</td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_plan_description)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">2.	Budget sur 2 ans</td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_plan_budget)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">3.	Plan de trésorerie et financements sur 2 ans</td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_plan_cash_flow)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">4.	Explications des hypothèses de revenus, de dépenses et de financement</td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_plan_incomes_expenses_explained)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">5.	Compte de résultat à 12 et 24 mois</td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_plan_income_statement)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">6.	Bilan à 0, 12 et 24 mois</td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_plan_assessment)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong>3.	Contact avec le guichet d'entreprise</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">1.	En ordre au niveau de l'accès à la gestion (pour tous) et l'accès (éventuel) à la profession</td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_court_management_access_in_order)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 30px;">2.	Après la passation de l'acte, en ordre au niveau caisse sociale et assurances sociales </td>
                                <td style="text-align: right;">
                                    @if($user->followup_business_court_social_in_order)
                                        <span class="text-success"><i class="fa fa-check"></i></span>
                                    @else
                                        <span class="text-muted"><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
