<div class="d-flex align-items-center bg-default-3 p-10">
    <div class="container-fluid h-100">
        <div class="d-flex justify-content-center align-items-center mt-5 flex-column">
            <img src="https://www.fabriquesrl.be/assets/templates/finity/image/logo-fabrique-green.png" alt="" style="width: 250px; margin-bottom: 30px;">
            
            <h3>Bienvenue, {{auth()->user()->getName()}}</h3>
            <h4 class="text-muted">Espace client</h4>

            <div class="mt-6">
                <a href="{{route('front.logout')}}"><small>Se déconnecter</small></a>
            </div>
        </div>
    </div>
</div>
