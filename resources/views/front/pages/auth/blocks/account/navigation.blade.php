<div class="d-flex justify-content-center align-items-center mt-5">
    <div class="mr-4">
        <a class="btn {{$active == 'account' ? 'btn-blue-3' : 'btn-dark'}} btn-2 font-weight-medium rounded-5" href="{{route('front.account')}}">Mes informations</a>
    </div>

    <div class="mr-4">
        <a class="btn {{$active == 'documents' ? 'btn-blue-3' : 'btn-dark'}} btn-2 font-weight-medium rounded-5" href="{{route('front.account.documents')}}">
            Documents ({{auth()->user()->documents->count()}})
        </a>
    </div>

    <div>
        <a class="btn {{$active == 'followup' ? 'btn-blue-3' : 'btn-dark'}} btn-2 font-weight-medium rounded-5" href="{{route('front.account.followup')}}">Tableau de suivi</a>
    </div>
</div>
