@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    <div class="row">
        <div class="col-md-4 offset-md-4 d-flex justify-content-center align-items-end" style="height: 40vh;">
            <i class="fas fa-thumbs-up" style="color:#336666; font-size:10em;"></i>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 d-flex justify-content-center align-items-center mt-5 flex-column">
            <h2>Merci beaucoup pour votre demande</h2>
            <p>Nous vous recontactons prochainement.</p>
            <p>Bonne préparation pour votre projet !</p>
            <p>Au plaisir de faire connaissance.</p>
            <p>
                Entrepreneurialement,
            </p>
            <p><i>L’équipe de Wikipreneurs</i></p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-12 d-flex justify-content-center">
            <p><small>Vous allez être redirigé vers la page d'accueil...</small></p>
        </div>
        <div class="col-md-12 d-flex justify-content-center">
            <a href="https://lafabriqueasrl.be" class="btn" style="background: #336666; color:white;">Cliquez ici pour être redirigé directement.</a>
        </div>
    </div>
@endsection

@section('_scripts')
    <script>
        setTimeout(function() {
            window.location.replace('https://lafabriqueasrl.be');
        }, 10000)

    </script>
@endsection
