@extends('front._master', ['no_header' => true, 'no_footer' => false])

@section('_title', '- Diagnostic : ' . $diagnostic->title)

@section('_content')

    <div class="min-height-100vh d-flex align-items-center bg-default-3">
        <div class="container-fluid h-100">
            <div class="row no-gutters h-100">
                <div class="col-xl-7 col-lg-7 col-md-10">
                    <div class="pt-26 pt-md-17 pt-lg-18 pb-md-4 pb-lg-10 max-w-500 mx-auto">

                        @yield('content_template')

                    </div>
                </div>
                <!-- Right Image -->
                <div class="col-xl-5 col-lg-5 col-md-10 min-height-lg-100vh" style="padding:20px 10px 20px 0;">
                    <div class="bg-images min-height-100vh d-none d-lg-block" style="background-image: url({{$diagnostic->getPicture()}}); position: -webkit-sticky; position: sticky;"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
