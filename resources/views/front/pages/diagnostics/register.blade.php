@extends('front.pages.diagnostics.template')

@section('_title', '- Diagnostic : ' . $diagnostic->title)

@section('content_template')

        <section id="services" class="section services-section has-background-white is-clearfix _diagnostic_registration">
            <h2 class="heading-title style-3">
                <small>Etape 1/{{($diagnostic->categories->count()+2)}}</small>
                <br>Démarrer le diagnostic
            </h2>

            <form method="POST" action="{{route('front.diagnostics.register', $diagnostic->getSlug())}}">
                @csrf

                <div class="diagnostic-register">

                    <div class="field">
                        <div class="control is-expanded">
                            <label for="">Nom & prénom *</label>
                            <input required class="form-control bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" type="text" name="name" placeholder="John Doe" value="{{old('name')}}" autocomplete="off">
                        </div>
                    </div>

                    <div class="field mt-3">
                        <div class="control is-expanded">
                            <label for="">Nom du projet ou de l'entreprise. *</label>
                            <input required class="form-control bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" type="text" name="project_name" placeholder="Wikipreneurs" value="{{old('project_name')}}" autocomplete="off">
                        </div>
                    </div>

                    <div class="field mt-3">
                        <div class="control is-expanded">
                            <label for="">Adresse email *</label>
                            <input required class="form-control bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" type="email" name="email" placeholder="Adresse email" value="{{old('email')}}" autocomplete="off">
                        </div>
                    </div>

                    <div class="field mt-3">
                        <div class="control is-expanded">
                            <label for="">Je participe à l'accélérateur Forward</label>
                            <select name="forward" class="form-control">
                                <option value="">Choisir une option</option>
                                <option value="0">Non</option>
                                <option value="1">Oui et j'accepte de partager mes réponses avec celui-ci</option>
                            </select>
                        </div>
                    </div>

                    <div class="field mt-3">
                        <div class="control">
                            <div class="level">
                                <div class="level-left"></div>
                                <div class="level-right">
                                    <button class="btn btn-greenfabrique btn-sm w-100 rounded-4 mt-10">Continuer</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
@stop
