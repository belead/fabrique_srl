@extends('front.pages.diagnostics.template')

@section('_title', '- Diagnostic : ' . $diagnostic->title)

@section('content_template')
    @if(isset($contents_by_ref['request_a_quote']))
        @php
            $item = $contents_by_ref['request_a_quote'];
        @endphp
        @if($item->partial)
            @include('front/partials/' . $item->partial->partial_path, ['item' => $item, 'content' => $contents_by_ref['request_a_quote']])
        @endif
    @endif

    @if(session('success'))
        <div class="alert alert-primary" role="alert">
            {{session('success')}}
        </div>
    @endif

    @if($category)
        <h2 class="heading-title style-2" style="color: {{$category->color ?? '#000'}}; text-align: left !important;">
            <small>Etape {{$category->position + 1}}/{{($diagnostic->categories->count() + 2)}}</small>
            <br>{{$category->name ?? ''}}
        </h2>

        <p><small><i class="fa fa-info-circle mr-2" style="color:{{$category->color ?? '#000'}};"></i> Evaluez les éléments suivants.</small></p>

        <form method="POST">
            @csrf

            @foreach($category->questions as $question)
                <h3 style="color: #000; font-size: 24px;">
                    {{$question->question}}
                </h3>

                <div class="rb" style="">
                    @for($i = 0; $i <= 1; $i++)
                        <div class="rb-tab tab_{{$question->id}}">
                            <input required class="form-check-input" type="radio" name="questions[{{$question->id}}]"
                                   id="q_{{$question->id}}_{{$i == 0 ? $question->points_weighting : 0}}" value="{{$i == 0 ? $question->points_weighting : 0}}"
                            @if($i > 0) checked @endif>

                            <div class="rb-spot spot_{{$question->id}} spot_{{$question->id}}_{{$i}} {{$i > 0 ? 'rb-tab-active' : ''}}" style="border-color: {{$category->color ?? '#000'}};"
                                 data-value="{{$i == 0 ? $question->points_weighting : 0}}"
                                 data-question="{{$question->id}}">
                                <span class="rb-txt">
                                    {{$i == 0 ? 'Oui' : 'Non'}}
                                </span>
                            </div>
                        </div>
                    @endfor
                </div>
            @endforeach

            <div class="field">
                <div class="control">
                    <div class="level">
                        <div class="level-left"></div>
                        <div class="level-right">
                            <button class="btn btn-greenfabrique w-100 rounded-4 mt-5" type="submit">Suivant</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @else
        <div class="diagnostic-results">
            <div class="bravo">
                <i class="fa fa-thumbs-up"></i>
                <h4 class="">Bravo !</h4>
            </div>

            @if(session()->has('diagnostic.note') && session('diagnostic.note') >= 6)
                <p>D’après tes résultats, tu devrais créer une société SRL.</p>
                <p>La SRL est la forme légale de société souvent considérée comment étant la plus appropriée à une petite ou moyenne entreprise.</p>
                <p>Pour tout savoir sur la société à responsabilité limitée, <a href="https://www.lafabriqueasrl.be/fr/news/articles/pourquoi-choisir-srl-societe-a-responsabilite-limitee" target="_blank">consulte cet article</a>. Besoin d’aide pour lancer ta SRL?</p>
                <p>Nous nous occupons de tout et te déchargeons de la partie administrative avec «La Fabrique à SRL».</p>
            @else
                <p>D’après tes résultats, tu devrais passer en indépendant personne physique.</p>
                <p>Choisir d’exercer en personne physique, c’est créer ton activité en quelques clics, sans capital de départ et avec peu de frais.</p>
                <p>Tu es seul.e et prends donc toutes les décisions sans devoir rendre de compte à d’autres personnes.</p>
                <p>Pour en savoir plus sur le choix de la forme juridique de ton activité, <a href="https://www.lafabriqueasrl.be/fr/news/articles/quelle-forme-juridique-pour-mon-entreprise" target="_blank">consulte cet article.</a> </p>
            @endif
        </div>

        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-sm btn-greenfabrique w-100 rounded-4 mt-5" href="{{route('front.diagnostics.retry', $diagnostic->getSlug())}}">Recommencer le diagnostic</a>
            </div>
            <div class="col-md-6">
                <a class="btn btn-sm btn-greenfabrique w-100 rounded-4 mt-5" href="{{route('front.page')}}">Revenir à l'accueil</a>
            </div>
        </div>
    @endif
@endsection

@if($category)
    @section('_scripts')
        <script>
            $(document).ready(function() {
                $('.rb-spot').click(function() {
                    let question = $(this).attr('data-question');
                    let value = $(this).attr('data-value');

                    $('.spot_' + question).removeClass('rb-tab-active');
                    $(this).addClass('rb-tab-active');
                    $('.tab_' + question + ' input').attr('checked', false);
                    $('input#q_' + question + '_' + value).attr('checked', true);
                });
            });
        </script>
    @endsection
@endif
