@extends('front._master', ['no_header' => true, 'no_footer' => false])

@section('_content')
   <div class="min-height-100vh d-flex align-items-center bg-default-3">
        <div class="container-fluid h-100">
            <div class="row no-gutters h-100">
                <div class="col-xl-7 col-lg-7 col-md-10">
                    <div class="pt-26 pt-md-17 pt-lg-18 pb-md-4 pb-lg-10 max-w-500 mx-auto">
                        @if(isset($contents_by_ref['request_a_quote']))
                            @php
                                $item = $contents_by_ref['request_a_quote'];
                            @endphp

                            @if($item->partial)
                                @include('front/partials/' . $item->partial->partial_path, [
                                    'item' => $item,
                                    'content' => $contents_by_ref['request_a_quote']
                                ])
                            @endif
                        @endif

                        @if(session('success'))
                            <div class="alert alert-primary" role="alert">
                                {{session('success')}}
                            </div>
                        @endif

                        @if(session('error'))
                            <p class="alert alert-danger" role="alert">
                                {{session('error')}}
                            </p>
                        @endif

                        <form action="{{route('front.register.step1.post')}}" method="POST">
                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group mb-6 position-relative">
                                        <input required type="text" name="first_name" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Prénom" id="firstname">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-6 position-relative">
                                        <input required type="text" name="last_name" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Nom" id="lastname">
                                    </div>
                                </div>
                            </div>

                            <!-- Email -->
                            <div class="form-group mb-6 position-relative">
                                <input required type="email" name="email" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="E-mail" id="email">
                            </div>

                            <div class="form-group mb-6 position-relative">
                                <input required type="text" name="phone" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Téléphone" id="phone">
                            </div>

                            <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                {!! \App\Helpers\_CMS\CaptchaHelper::getCaptchaField() !!}

                                @error('g-recaptcha-response')
                                    <div class="invalid-feedback text-danger">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>

                            <div class="button mb-6 mt-5">
                                <button type="submit" class="btn btn-greenfabrique w-100 rounded-4">
                                    Continuer
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Right Image -->

                <div class="col-xl-5 col-lg-5 col-md-10 min-height-lg-100vh" style="padding:20px 10px 20px 0;">
                    <div class="bg-images min-height-100vh d-none d-lg-block" style="background-image: url({{asset('assets/templates/finity/image/fabrique/register.jpg')}}); position: -webkit-sticky; position: sticky;"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

