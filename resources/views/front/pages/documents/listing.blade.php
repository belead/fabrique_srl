@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    @includeIf('front.pages.auth.blocks.account.header')

    <div class="d-flex align-items-center p-10">
        <div class="container h-100">
            @includeIf('front.pages.auth.blocks.account.navigation', ['active' => 'documents'])

            <div class="d-flex align-items-center justify-content-between"
                 style="background-color: rgba(51, 102, 102, 0.2); padding: 20px; margin-top: 60px; border-radius: 10px;">
                <div>
                    <h4>Modèle de document "Statuts de l'entreprise"</h4>
                    <h5 class="text-muted mb-0">À compléter et renvoyer</h5>
                </div>

                <div>
                    <a target="_blank" href="" class="btn btn-secondary btn-sm font-weight-medium rounded-5"
                       style="padding: 5px 12px;">
                        Télécharger le modèle
                    </a>
                </div>
            </div>

            <style>
                table.table td {
                    vertical-align: middle;
                }
                table.table thead tr th {
                    background-color: #F5F7F8;
                }
            </style>

            <div style="margin-top: 60px;">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="mb-4">
                            <a href="{{route('front.account.documents.form')}}" class="btn btn-secondary btn-sm font-weight-medium rounded-5" style="padding: 5px 12px;">
                                Déposer un fichier
                            </a>
                        </div>

                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th>Document</th>
                                    <th>Ajouté le</th>
                                    <th style="text-align: center;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(auth()->user()->documents->count() == 0)
                                    <tr>
                                        <td colspan="3">
                                            Pas de document disponible
                                        </td>
                                    </tr>
                                @endif

                                @foreach(auth()->user()->documents->sortByDesc('created_at') as $document)
                                    <tr>
                                        <td>
                                            <a target="_blank" href="{{$document->getFile()}}">{{$document->name}}</a>
                                            @if($document->description)
                                                <p style="margin-bottom: 0; line-height: 1;">
                                                    <small>{!! nl2br($document->description) !!}</small>
                                                </p>
                                            @endif
                                        </td>
                                        <td>
                                            {{$document->pivot->updated_at->format('d/m/Y H:i')}}
                                        </td>
                                        <td style="text-align: right;">
                                            <a target="_blank" href="{{$document->getFile()}}" class="btn btn-dark btn-sm font-weight-medium rounded-5">Télécharger</a>
{{--                                            @if($document->by_user_id)--}}
{{--                                                <a href="" class="btn btn-danger btn-sm font-weight-medium rounded-5">Supprimer</a>--}}
{{--                                            @endif--}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
