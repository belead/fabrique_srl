@extends('front._master', ['no_header' => true, 'no_footer' => true])

@section('_content')
    @includeIf('front.pages.auth.blocks.account.header')

    <div class="d-flex align-items-center p-10">
        <div class="container h-100">
            @includeIf('front.pages.auth.blocks.account.navigation', ['active' => 'documents'])

            <div style="margin-top: 60px;">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <a href="{{route('front.account.documents')}}">
                            <i class="fa fa-arrow-left"></i>
                            Retour à la liste
                        </a>

                        <h4 style="margin-bottom: 30px;">Ajouter un document</h4>

                        @if(session('success'))
                            <p class="alert alert-success">
                                {{session('success')}}
                            </p>
                        @endif

                        @if(session('error'))
                            <p class="alert alert-danger">
                                {{session('error')}}
                            </p>
                        @endif

                        <form method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="">Nom *</label>
                                <input type="text" required name="name" class="form-control" value="{{old('name')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Fichier *</label>
                                <input type="file" required name="file" class="form-control">
                            </div>

                            <div class="button text-right">
                                <button type="submit" class="btn btn-greenfabrique rounded-4">
                                    Envoyer
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop