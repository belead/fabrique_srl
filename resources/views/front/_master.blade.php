<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="{{$attributes->description ?? ''}}@yield('_meta_desc')">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$attributes->title ?? config('_project.name')}}">
    <meta property="og:url" content="/">
    <meta property="og:image" content="{{asset('assets/front/img/logo.png')}}">
    <meta property="og:site_name" content="{{config('_project.name')}}">

    <title>{{$attributes->title ?? config('_project.name')}}@yield('_title')</title>
    <meta name="csrf-token" content="{{csrf_token()}}"/>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/front/img/favicon.png')}}">

    <!-- CSS Global -->
    {!! \App\Helpers\_CMS\TemplateHelper::styles() !!}

    <!-- Bootstrap , fonts & icons  -->
    <link rel="stylesheet" href="{{asset('assets/templates/finity/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/fonts/icon-font/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/fonts/typography-font/typo.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/fonts/fontawesome-5/css/all.css')}}">
    <!-- Plugin'stylesheets  -->
    <link rel="stylesheet" href="{{asset('assets/templates/finity/plugins/aos/aos.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/plugins/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/plugins/nice-select/nice-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/plugins/slick/slick.min.css')}}">
    <!-- Vendor stylesheets  -->
    <link rel="stylesheet" href="{{asset('assets/templates/finity/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/css/fabrique.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/finity/css/custom.css')}}">
    <!-- Custom stylesheet -->
    @yield('_styles')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{asset('assets/front/js/html5shiv.js')}}"></script>
    <script src="{{asset('assets/front/js/respond.min.js')}}"></script>
</head>

<body class="{{isset($partial_details) ? $partial_details->body_class : (isset($page->body_class) ? $page->body_class : '')}}@yield('_body_class') body-scroll" data-theme="light">
@include('front._template')
<!-- Vendor Scripts -->
<script src="{{asset('assets/templates/finity/js/vendor.min.js')}}"></script>
<!-- Plugin's Scripts -->
<script src="{{asset('assets/templates/finity/plugins/fancybox/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('assets/templates/finity/plugins/nice-select/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('assets/templates/finity/plugins/aos/aos.min.js')}}"></script>
<script src="{{asset('assets/templates/finity/plugins/slick/slick.min.js')}}"></script>
<script src="{{asset('assets/templates/finity/plugins/counter-up/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/templates/finity/plugins/counter-up/jquery.waypoints.min.js')}}"></script>
<!-- Activation Script -->
<script src="{{asset('assets/templates/finity/js/custom.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}"></script>
{!! \App\Helpers\_CMS\TemplateHelper::scripts() !!}
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-7WNWDBMCDW"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-7WNWDBMCDW');
</script>

<!-- Meta Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1947714192099506');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1" style="display:none"
               src=https://www.facebook.com/tr?id=1947714192099506&ev=PageView&noscript=1
    />
</noscript>
<!-- End Meta Pixel Code -->
@yield('_scripts')
</body>
</html>
