<div class="bg-dark-cloud pt-12 pb-10" style="position:relative; bottom:0; width:100%;">
    <div class="container">
        <div class="pb-13">
            <div class="row justify-content-lg-between">
                <!-- Brand Logo -->
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                    <div class="brand-logo mb-5 mb-sm-0 mt-md-4 dark-mode-texts">
                        <a href="/">
                            <img class="mx-auto mx-0 light-version-logo default-logo" src="{{asset('assets/templates/finity/image/logo-fabrique-white.png')}}" alt="">
                            <img src="{{asset('assets/templates/finity/image/logo-fabrique-white.png')}}" alt="" class="dark-version-logo mx-auto mx-0" style="max-width: 200px;">
                        </a>
                    </div>
                    <p class="mt-3 text-white font-size-4 line-height-26">
                        <strong>EDUCA SRL</strong> <br>
                        <strong>Siège social en Wallonie</strong> <br>
                        100 rue Longchamp <br>
                        1420 Braine l'Alleud <br>
                        <strong>Siège d'exploitation à Bruxelles</strong><br>
                        19, Avenue des Volontaires <br>
                        1160 Auderghem <br>
                        <strong>N° d'entreprise </strong><br>
                        BE.0739.869.478
                    </p>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 d-flex flex-column mt-10">
                    <h4 class="text-white">Nos contenus</h4>
                    <ul class="footer_list">
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/news">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/fiches">
                                Fiches
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/outils">
                                Outils
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/videos">
                                Vidéos
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr#">
                                Podcasts
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 d-flex flex-column mt-10">
                    <h4 class="text-white">A propos</h4>
                    <ul class="footer_list">
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/qui-sommes-nous">
                                Qui sommes-nous?
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/ready">
                                Parcours de création d'entreprise
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/services">
                                Services et formations
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/offres">
                                Bons plans
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/ready">
                                Création d'entreprise à Bruxelles
                            </a>
                        </li>
                        <li>
                            <a href="https://www.wikipreneurs.be/fr/ready">
                                Création d'entreprise en Wallonie
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Brand Logo -->
                <!-- Newsletter -->
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-8 col-xs-10 d-flex justify-content-center mt-10">
                    <div class="">
                        <h4 class="text-white">Newsletter</h4>
                        <p class="text-white font-size-5">Restez informé de dernières actualités et outils dédiés aux entrepreneurs</p>
                        <a href="https://www.wikipreneurs.be/fr/newsletter-inscription" class="btn btn-greenfabrique btn px-8 h-55">Inscription à la newsletter</a>
                    </div>
                </div>
                <!-- End Newsletter -->
            </div>
        </div>
        <div class="border-top border-default-color dark-mode-texts d-flex align-items-center justify-content-center justify-content-sm-between flex-column flex-sm-row flex-wrap pt-9">
            <div class="d-flex">
                <p class="d-flex align-items-center list-unstyled mb-5 mb-sm-0 font-size-4">
                    Copyright 2021 EDUCA
                </p>
                <p class="ml-5 align-items-center list-unstyled mb-5 mb-sm-0 font-size-4 pl-3"> -
                    <a href="{{route('front.page', 'terms-and-conditions')}}" class="pl-3">Conditions générale de vente</a>
                </p>
            </div>
            <div class="">
                <ul class="d-flex align-items-center list-unstyled mb-0">
                    <li><a class="pl-5 text-white gr-hover-text-dodger-blue-1 font-size-5 pr-6" href="https://www.facebook.com/wikipreneurs/"><i class="fab fa-facebook"></i></a></li>
                    <li><a class="pl-5 text-white gr-hover-text-dodger-blue-1 font-size-5 pr-6" href="https://www.linkedin.com/company/wikipreneurs/"><i class="fab fa-linkedin"></i></a></li>
                    <li><a class="pl-5 text-white gr-hover-text-dodger-blue-1 font-size-5 pr-6" href="https://www.instagram.com/wikipreneurs/"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@include('front.blocks.modal_cgv')
