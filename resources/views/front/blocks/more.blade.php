<div class="bg-dark-cloud pt-5 pt-md-5 pt-lg-5 pb-13 pb-md-18 pb-lg-26">
    <div class="container">
        <!-- Services -->
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8">
                <div class="pt-13 pt-lg-20" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <div class="square-60 bg-green shadow-dodger-blue-3 rounded-10 text-white font-size-7">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="mt-9">
                        <h4 class="font-size-7 font-color-white mb-5">@lang('variables.homepage.more.block_1.title')</h4>
                        <p class="font-size-5 font-color-lightgray mb-0">
                            @lang('variables.homepage.more.block_1.content')
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8">
                <div class="pt-13 pt-lg-20" data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" data-aos-once="true">
                    <div class="square-60 bg-green shadow-sunset rounded-10 text-white font-size-7">
                        <i class="fa fa-chart-line"></i>
                    </div>
                    <div class="mt-9">
                        <h4 class="font-size-7 font-color-white mb-5">@lang('variables.homepage.more.block_2.title')</h4>
                        <p class="font-size-5 font-color-lightgray mb-0">
                            @lang('variables.homepage.more.block_2.content')
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8">
                <div class="pt-13 pt-lg-20" data-aos="fade-up" data-aos-duration="500" data-aos-delay="400" data-aos-once="true">
                    <div class="square-60 bg-green shadow-dodger-blue-1 rounded-10 text-white font-size-7">
                        <i class="fa fa-clock"></i>
                    </div>
                    <div class="mt-9">
                        <h4 class="font-size-7 font-color-white mb-5">@lang('variables.homepage.more.block_3.title')</h4>
                        <p class="font-size-5 font-color-lightgray mb-0">
                            @lang('variables.homepage.more.block_3.content')
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Services -->
    </div>
</div>
