    <header class="site-header site-header--menu-right px-7 px-lg-10 z-index-99 dynamic-sticky-bg dark-mode-texts--desktops dark-cloud-sticky-bg site-header--absolute site-header--sticky" @if(isset($no_banner)) style="background: #1d293f !important;" @endif>
        <div class="container">
            <nav class="navbar site-navbar offcanvas-active navbar-expand-lg  px-0">
                <!-- Brand Logo-->
                <div class="brand-logo mt-3 mt-md-0 dark-mode-texts"><a href="/">
                        <!-- light version logo (logo must be black)-->
                        <img src="{{asset('assets/templates/finity/image/logo-fabrique-white.png')}}" alt="" class="light-version-logo" style="max-width: 150px;">
                        <!-- Dark version logo (logo must be White)-->
                        <img src="{{asset('assets/templates/finity/image/logo-fabrique-white.png')}}" alt="" class="dark-version-logo" style="max-width: 150px;">
                    </a></div>
                <div class="collapse navbar-collapse" id="mobile-menu">
                    <div class="navbar-nav-wrapper">
                        <ul class="navbar-nav main-menu">
                            {!!menu('main_menu', get_defined_vars(), 'nav-item dropdown nav-link dropdown-toggle gr-toggle-arrow ')!!}
                        </ul>
                    </div>
                    <button class="d-block d-lg-none offcanvas-btn-close" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="true" aria-label="Toggle navigation">
                        <i class="gr-cross-icon"></i>
                    </button>
                </div>
                <div class="header-btn ml-auto ml-lg-6 d-none d-sm-block font-size-3">
                    <a class="btn btn btn-white btn-medium rounded-5 font-size-3" href="/demander-un-devis">
                        @lang('variables.homepage.quote.btn')
                    </a>
                </div>
                <!-- Mobile Menu Hamburger-->
                <button class="navbar-toggler btn-close-off-canvas  hamburger-icon border-0 dark-mode-texts" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <!-- <i class="icon icon-simple-remove icon-close"></i> -->
                    <span class="hamburger hamburger--squeeze js-hamburger">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
                </span>
                </span>
                </button>
                <!--/.Mobile Menu Hamburger Ends-->
            </nav>
        </div>
    </header>

@if(!isset($no_banner) || $no_banner == false)
<div class="bg-green position-relative pt-20 pt-sm-24 pt-md-27 pt-lg-35 pb-15 pb-md-14 pb-lg-33">
    <div class="container position-static" >
        <div class="row position-static">
            <div class="col-xl-4 col-lg-4 col-md-10 position-static">
                <div class="l4-hero-image-group pt-lg-3">
                    <div class="hero-images" data-aos="fade-right" data-aos-duration="600" data-aos-once="true">
                        <div class="img-1">
                            <img class="w-100 w-lg-auto shadow-14 opacity-8 rounded-top-15" src="{{asset('assets/templates/finity/image/fabrique/banner3.jpg')}}" alt="" style="max-width: 835px;">
                        </div>
                        <div class="img-2">
                            <img class="w-100 w-lg-auto shadow-14 rounded-top-15" src="{{asset('assets/templates/finity/image/fabrique/banner2.jpg')}}" alt="" style="max-width: 358px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-10 col-sm-12">
                <div class="dark-mode-texts mt-8 mt-lg-0 pt-lg-16 ml-xl-25 ml-lg-10" data-aos="fade-left" data-aos-duration="600" data-aos-duration="500" data-aos-once="true">
                    <h1 class="font-size-12 mb-9 line-height-84 pr-xs-15 pr-lg-0">@lang('variables.homepage.header.title')</h1>
                    <p class="font-size-8 mb-0 pr-md-10 pr-xl-18">@lang('variables.homepage.header.subtitle')</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
