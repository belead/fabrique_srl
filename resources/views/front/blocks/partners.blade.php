<div class="pt-13 pt-md-17 pt-lg-18 pb-13 pb-md-14 pb-lg-23 goto" id="one-step-down">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 col-md-11">
            <!-- Company Section -->
            <div class="">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-lg-7 text-center text-lg-center">
                                <h5 class="font-size-6 font-weight-normal text-bali-gray mb-0">@lang('variables.homepage.partners.title')</h5>
                            </div>
                        </div>
                    </div>
                    <!-- Brand Logos -->
                    <div class="brand-logo-small d-flex align-items-center justify-content-center justify-content-lg-between flex-wrap">
                        <!-- Single Brand -->
                        <div class="single-brand-logo mx-5 my-6">
                            <a href="https://www.ucm.be" target="_blank">
                                <img src="{{asset('assets/templates/finity/image/fabrique/logos/partner_ucm.svg')}}" alt="" data-aos="fade-in" data-aos-once="true" class="aos-init aos-animate" style="width: 200px;">
                            </a>
                        </div>
                        <!-- Single Brand -->
                        <div class="single-brand-logo mx-5 my-6">
                            <a href="https://subsiconseils.be" target="_blank">
                                <img src="{{asset('assets/templates/finity/image/fabrique/logos/partner_subsiconseils.png')}}" alt="" data-aos="fade-in" data-aos-duration="600" data-aos-delay="500" data-aos-once="true" class="aos-init aos-animate" style="max-height: 100px;">
                            </a>
                        </div>
                        <!-- Single Brand -->
                        <div class="single-brand-logo mx-5 my-6">
                            <a href="https://www.ing.be/fr/retail" target="_blank">
                                <img src="{{asset('assets/templates/finity/image/fabrique/logos/partner_ing.png')}}" alt="" data-aos="fade-in" data-aos-duration="900" data-aos-delay="700" data-aos-once="true" class="aos-init aos-animate" style="max-height: 100px;">
                            </a>
                        </div>
                    </div>
                    <!-- End Brand Logos -->
                </div>
            </div>
            <!-- End Company Section -->
        </div>
    </div>
</div>
