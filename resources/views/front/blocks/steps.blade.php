<a name="etapes"></a>
<div class="pt-10 pt-md-15 pt-lg-20 pb-md-9 pb-lg-21">
    <div class="container">
        <!-- Section Title -->
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-9 col-xs-10">
                <div class="text-center pb-13 pb-lg-12 pr-lg-10 pr-xl-0">
                    <h2 class="font-size-11 mb-0">@lang('variables.homepage.steps.title')</h2>
                </div>
            </div>
        </div>
        <!-- Section Title -->
        <!-- Progress Items -->
        <div class="row justify-content-center">
            <div class="col-sm-12 col-xs-8">
                <div class="timeline-area d-sm-flex justify-content-center justify-content-lg-between flex-wrap flex-lg-nowrap position-relative">
                    <!-- Image Group -->
                    <div class="image-group-3">
                        <div class="arrow-shape-1 d-none d-lg-block absolute-top-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="400" data-aos-once="true">
                            <img src="{{asset('assets/templates/finity/image/home-2/png/arrow-1.png')}}" alt="">
                        </div>
                        <div class="arrow-shape-2 d-none d-lg-block absolute-top-right aos-init aos-animate" data-aos="zoom-in" data-aos-delay="600" data-aos-once="true">
                            <img src="{{asset('assets/templates/finity/image/home-2/png/arrow-2.png')}}" alt="">
                        </div>
                    </div>
                    <!-- Single Progress -->
                    <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="300" data-aos-once="true">
                        <div class="square-97 bg-orange rounded-10 mb-10 shadow-bg-orange-op8 mx-auto mx-md-0">
                            <i class="fa fa-pen-nib fa-2x font-color-white"></i>
                        </div>
                        <div class="">
                            <h3 class="font-size-8 mb-6">1. @lang('variables.homepage.steps.one.title')</h3>
                            <p class="font-size-5 line-height-28 mb-0">@lang('variables.homepage.steps.one.content')</p>
                        </div>
                    </div>
                    <!-- End Single Progress -->
                    <!-- Single Progress -->
                    <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="500" data-aos-once="true">
                        <div class="square-97 bg-orange rounded-10 mb-10 shadow-bg-orange-op8 mx-auto mx-md-0">
                            <i class="fa fa-clipboard-check fa-2x font-color-white"></i>
                        </div>
                        <div class="">
                            <h3 class="font-size-8 mb-6">2. @lang('variables.homepage.steps.two.title')</h3>
                            <p class="font-size-5 line-height-28 mb-0">@lang('variables.homepage.steps.two.content')</p>
                        </div>
                    </div>
                    <!-- End Single Progress -->
                    <!-- Single Progress -->
                    <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="700" data-aos-once="true">
                        <div class="square-97 bg-orange rounded-10 mb-10 shadow-bg-orange-op8 mx-auto mx-md-0">
                            <i class="fa fa-handshake fa-2x font-color-white"></i>
                        </div>
                        <div class="">
                            <h3 class="font-size-8 mb-6">3. @lang('variables.homepage.steps.three.title')</h3>
                            <p class="font-size-5 line-height-28 mb-0">@lang('variables.homepage.steps.three.content')</p>
                        </div>
                    </div>
                    <!-- End Single Progress -->
                </div>
            </div>
        </div>
        <!-- End Progress Items -->

        <!-- Progress Items -->
        <div class="row justify-content-center">
            <div class="col-sm-12 col-xs-8">
                <div class="timeline-area d-sm-flex justify-content-center justify-content-lg-between flex-wrap flex-lg-nowrap position-relative">
                    <!-- Image Group -->
                    <div class="image-group-3">
                        <div class="arrow-shape-1 d-none d-lg-block absolute-top-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="400" data-aos-once="true">
                            <img src="{{asset('assets/templates/finity/image/home-2/png/arrow-1.png')}}" alt="">
                        </div>
                        <div class="arrow-shape-2 d-none d-lg-block absolute-top-right aos-init aos-animate" data-aos="zoom-in" data-aos-delay="600" data-aos-once="true">
                            <img src="{{asset('assets/templates/finity/image/home-2/png/arrow-2.png')}}" alt="">
                        </div>
                    </div>
                    <!-- Single Progress -->
                    <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="300" data-aos-once="true">
                        <div class="square-97 bg-green rounded-10 mb-10 shadow-bg-orange-op8 mx-auto mx-md-0">
                            <i class="fa fa-comment-dots fa-2x font-color-white"></i>
                        </div>
                        <div class="">
                            <h3 class="font-size-8 mb-6">4. @lang('variables.homepage.steps.for.title')</h3>
                            <p class="font-size-5 line-height-28 mb-0">@lang('variables.homepage.steps.for.content')</p>
                        </div>
                    </div>
                    <!-- End Single Progress -->
                    <!-- Single Progress -->
                    <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="500" data-aos-once="true">
                        <div class="square-97 bg-green rounded-10 mb-10 shadow-bg-orange-op8 mx-auto mx-md-0">
                            <i class="fa fa-file-signature fa-2x font-color-white"></i>
                        </div>
                        <div class="">
                            <h3 class="font-size-8 mb-6">5. @lang('variables.homepage.steps.fifth.title')</h3>
                            <p class="font-size-5 line-height-28 mb-0">@lang('variables.homepage.steps.fifth.content')</p>
                        </div>
                    </div>
                    <!-- End Single Progress -->
                    <!-- Single Progress -->
                    <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left aos-init aos-animate" data-aos="zoom-in" data-aos-delay="700" data-aos-once="true">
                        <div class="square-97 bg-green rounded-10 mb-10 shadow-bg-orange-op8 mx-auto mx-md-0">
                            <i class="fa fa-users fa-2x font-color-white"></i>
                        </div>
                        <div class="">
                            <h3 class="font-size-8 mb-6">6. @lang('variables.homepage.steps.six.title')</h3>
                            <p class="font-size-5 line-height-28 mb-0">@lang('variables.homepage.steps.six.content')</p>
                        </div>
                    </div>
                    <!-- End Single Progress -->
                </div>
            </div>
        </div>
        <!-- End Progress Items -->
    </div>
</div>
