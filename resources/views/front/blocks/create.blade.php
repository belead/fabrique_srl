<div class="bg-images" style="background-image:url({{asset('assets/templates/finity/image/fabrique/cta1.jpg')}})">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-xl-8 col-lg-9 col-md-11">
                <div class="text-center pt-14 pt-md-18 pb-15 pb-md-19 py-lg-31 dark-mode-texts aos-init aos-animate" data-aos="zoom-in" data-aos-duration="500" data-aos-once="true">
                    <h2 class="font-size-11 mb-7">@lang('variables.homepage.create.title')</h2>
                    <a class="btn btn-greenfabrique rounded-5 mt-12" href="/demander-un-devis">@lang('variables.homepage.create.request_a_quote')</a>
                </div>
            </div>
        </div>
    </div>
</div>
