<div class="modal fade" id="cgvModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Conditions générales de ventes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>&nbsp;</p>
                <p><strong>CONDITIONS GENERALES</strong></p>
                <p><br /><br /></p>
                <p><span style="font-weight: 400;">Article 1: G&eacute;n&eacute;ralit&eacute;s.</span></p>
                <p>&nbsp;</p>
                <ol>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">La FABRIQUE A SRL est une d&eacute;nomination commerciale appartenant &agrave; la SRL EDUCA, dont le si&egrave;ge social est &eacute;tabli rue de Longchamps, 100 &agrave; 1420 BRAINE L&rsquo;ALLEUD (Belgique). Toutes les offres et conventions souscrites par EDUCA sont soumises aux pr&eacute;sentes conditions g&eacute;n&eacute;rales, sauf d&eacute;rogation &eacute;crite et expresse.</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">L&rsquo;ensemble des droits de propri&eacute;t&eacute; intellectuelle, en ce compris la marque, les droits d&rsquo;auteur, les droits pr&eacute;vus par les lois sp&eacute;cifiques en mati&egrave;re de programmes d&rsquo;ordinateurs et tous les autres droits de propri&eacute;t&eacute; intellectuelle qui pourraient exister sur la plateforme &laquo;&nbsp;www.lafabriqueasrl.be&nbsp;&raquo;, ainsi que sur les codes sources, les tables et la documentation, sont et restent la propri&eacute;t&eacute; exclusive d'EDUCA.</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Par le seul fait de sa commande, le client adh&egrave;re aux pr&eacute;sentes conditions g&eacute;n&eacute;rales de vente, renon&ccedil;ant aux conditions g&eacute;n&eacute;rales et particuli&egrave;res mentionn&eacute;es dans ses propres bons de commande, dans ses lettres ou sur ses documents commerciaux, encore ceux-ci fussent-ils &eacute;tablis &agrave; la main ant&eacute;rieurement ou post&eacute;rieurement &agrave; l'&eacute;tablissement de nos documents.</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Les conditions g&eacute;n&eacute;rales du client seront opposables &agrave; EDUCA dans la mesure o&ugrave; celle-ci les aurait express&eacute;ment accept&eacute;es par &eacute;crit.</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Les commandes, engagements ou transactions conclues &agrave; l'intervention des agents, repr&eacute;sentants ou pr&eacute;pos&eacute;s d&rsquo;EDUCA, ne lui seront valables qu'apr&egrave;s ratification &eacute;crite par elle.</span></li>
                </ol>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Article 2: D&eacute;lais.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">2.1. </span> <span style="font-weight: 400;">Les d&eacute;lais de prestations des services ne sont donn&eacute;s qu'&agrave; titre indicatif, sauf si le contrat pr&eacute;cise express&eacute;ment et par &eacute;crit une date formelle de prestation qui soit de rigueur.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">2.2. </span> <span style="font-weight: 400;">Les retards &eacute;ventuels d'ex&eacute;cution ne peuvent en aucun cas donner lieu &agrave; la r&eacute;solution du contrat, ni &agrave; des dommages et int&eacute;r&ecirc;ts.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Article 3&nbsp;: Prestation des services</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">3.1.</span> <span style="font-weight: 400;">EDUCA s&rsquo;engage &agrave; accomplir ses prestations de service avec conscience et probit&eacute;, dans le respect des lois et r&egrave;glements applicables.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">3.2. </span> <span style="font-weight: 400;">Le client s&rsquo;engage &agrave; fournir en temps utile &agrave; EDUCA toutes les informations et instructions n&eacute;cessaires pour permettre une ex&eacute;cution correcte des prestations faisant l&rsquo;objet du contrat de services. Plus particuli&egrave;rement, le client s&rsquo;engage &agrave; r&eacute;pondre avec diligence &agrave; toutes les communications d&rsquo;EDUCA et &agrave; lui fournir toutes les informations requises par EDUCA ou toutes autres informations utiles dans le d&eacute;lai convenu. Sauf stipulation expresse contraire, EDUCA ne pourra &ecirc;tre tenu responsable d&rsquo;un quelconque retard dans la r&eacute;alisation de ses Prestations.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">3.3</span> <span style="font-weight: 400;">Le client sera seul responsable des dommages &eacute;ventuels qui pourraient r&eacute;sulter d&rsquo;informations impr&eacute;cises, erron&eacute;es ou incompl&egrave;tes transmises &agrave; EDUCA. En cas de manquement du client dans la communication des informations demand&eacute;es, EDUCA peut suspendre l&rsquo;ex&eacute;cution de ses prestations en tout ou en partie et ne pourra &ecirc;tre tenu responsable d&rsquo;aucun dommage pouvant r&eacute;sulter directement ou indirectement de la suspension des prestations.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Article 4 : Responsabilit&eacute;.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">4.1. </span> <span style="font-weight: 400;">EDUCA prend les dispositions n&eacute;cessaires et utiles pour ex&eacute;cuter correctement ses prestations, dans les d&eacute;lais convenus. Toutefois, les obligations souscrites par EDUCA sont des obligations de moyen. EDUCA ne sera tenue que de son dol ou sa faute lourde. Elle n&rsquo;est pas responsable du dol ou de la faute lourde de ses commettants et en r&egrave;gle g&eacute;n&eacute;rale de ses agents d&rsquo;ex&eacute;cution.&nbsp;</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">4.2. </span> <span style="font-weight: 400;">Seuls les dommages directs pourront, le cas &eacute;ch&eacute;ant, faire l&rsquo;objet d&rsquo;une r&eacute;paration &agrave; l&rsquo;exclusion des dommages indirects tels que la perte de client&egrave;le ou la perte d&rsquo;exploitation. En tout &eacute;tat de cause, la responsabilit&eacute; d&rsquo;EDUCA sera limit&eacute;e au montant pay&eacute; par le client en ex&eacute;cution du contrat.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">4.3.</span> <span style="font-weight: 400;">EDUCA ne supporte aucune responsabilit&eacute; quant &agrave; l&rsquo;usage ult&eacute;rieur des d&eacute;veloppements, services et produits que les entrepreneurs feront suite &agrave; la cr&eacute;ation de leur SRL.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">4.4.</span> <span style="font-weight: 400;">EDUCA n&rsquo;est aucunement responsable des incidents pouvant se produire suite &agrave; la survenance d&rsquo;un cas de force majeure. Par &laquo; force majeure &raquo; il convient d&rsquo;entendre &laquo; un &eacute;v&egrave;nement &agrave; caract&egrave;re insurmontable, et selon certains impr&eacute;visible, ind&eacute;pendamment de toute faute de celui qui l&rsquo;invoque, qui emp&ecirc;che ce dernier d&rsquo;ex&eacute;cuter ses obligations d&eacute;coulant du contrat conclu avec EDUCA et dont les cons&eacute;quences n'auraient pas pu &ecirc;tre &eacute;vit&eacute;es malgr&eacute; toute la diligence d&eacute;ploy&eacute;e par celui qui l&rsquo;invoque &raquo;.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Sans que cette liste ne soit limitative, constituent un cas de force majeure les &eacute;v&eacute;nements suivants : une gr&egrave;ve, une pand&eacute;mie, une crise sanitaire, un arr&ecirc;t de fourniture d&rsquo;&eacute;nergie, une d&eacute;faillance des r&eacute;seaux, une guerre, des intemp&eacute;ries susceptibles d&rsquo;entraver le fonctionnement des instruments de t&eacute;l&eacute;communication et d&rsquo;acc&egrave;s &agrave; Internet, une perte de connectivit&eacute; Internet dues aux op&eacute;rateurs publics et priv&eacute;s dont d&eacute;pend EDUCA.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Lorsqu&rsquo;un cas de force majeure survient, le contrat conclu avec EDUCA pourra connaitre les sorts suivant :</span></p>
                <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">&ecirc;tre suspendu d&egrave;s lors que le cas de force majeure entraine une impossibilit&eacute; d&rsquo;ex&eacute;cution temporaire du contrat et que la reprise de l&rsquo;ex&eacute;cution de celui-ci pr&eacute;sente encore une utilit&eacute; pour les parties lorsque le cas de force majeure aura pris fin. Cette solution sera pr&eacute;f&eacute;r&eacute;e et envisag&eacute;e avant toute autre.&nbsp;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">&ecirc;tre modifi&eacute; et entrainer une ren&eacute;gociation des conditions du contrat conclu avec EDUCA &agrave; l&rsquo;aune des circonstances et d&rsquo;un commun accord de EDUCA et du client ;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">&ecirc;tre dissout d&egrave;s lors que le cas de force majeure emp&ecirc;che l&rsquo;ex&eacute;cution du contrat conclu avec EDUCA de mani&egrave;re d&eacute;finitive. Ainsi, le client sera rembours&eacute; de la partie du contrat non-ex&eacute;cut&eacute;e, c&rsquo;est-&agrave;-dire les prestation de service non-effectu&eacute;es &agrave; son profit, au moment o&ugrave; le cas de force majeure survient. Ce montant sera calcul&eacute; proportionnellement en fonction du prix total convenu dans le contrat.</span></li>
                </ul>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">4.3. </span> <span style="font-weight: 400;">Toute r&eacute;clamation &eacute;ventuelle concernant les prestations accomplies par EDUCA doit &ecirc;tre adress&eacute;e par &eacute;crit dans les quinze jours qui suivent l&rsquo;accomplissement des prestations faisant l&rsquo;objet de la r&eacute;clamation. Pass&eacute; ce d&eacute;lai, la responsabilit&eacute; d&rsquo;EDUCA ne pourra &ecirc;tre mise en &oelig;uvre.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Article 5 : Paiement</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">5.1. </span> <span style="font-weight: 400;">Les factures d&rsquo;EDUCA sont payables au grand comptant.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">6.2. </span> <span style="font-weight: 400;">Le non-paiement d'une somme &agrave; son &eacute;ch&eacute;ance entra&icirc;ne de plein droit et sans mise en demeure pr&eacute;alable la d&eacute;bition d&rsquo;une indemnit&eacute; forfaitaire &eacute;quivalente &agrave; 10 % du montant total de la facture avec un minimum de 150 EUR.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">6.3. </span> <span style="font-weight: 400;">Toute facture &eacute;chue portera en outre, de plein droit, int&eacute;r&ecirc;t au taux d&eacute;termin&eacute; par la loi du 2 ao&ucirc;t 2002 sur la lutte contre les retards de paiement dans les transactions commerciales.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Article 6&nbsp;: Politique de protection des donn&eacute;es</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">6.1. </span> <span style="font-weight: 400;">Afin de garantir la protection des donn&eacute;es personnelles de ses clients, prospects et salari&eacute;s, EDUCA s&rsquo;engage &agrave; respecter le r&egrave;glement (UE) 2016/679 du Parlement europ&eacute;en et du Conseil relatif &agrave; la protection des personnes physiques &agrave; l&rsquo;&eacute;gard du traitement des donn&eacute;es &agrave; caract&egrave;re personnel et &agrave; la libre circulation de ces donn&eacute;es (RGPD) entr&eacute; en vigueur le 25 mai 2018 et &agrave; s&rsquo;assurer de son respect par les sous-traitants effectuant pour son compte les op&eacute;rations de traitement desdites donn&eacute;es.&nbsp;</span></p>
                <p><br /><br /></p>
                <p><span style="font-weight: 400;">Article 7&nbsp;: Litiges et droit applicable</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">7.1. </span> <span style="font-weight: 400;">Les contrats conclus avec EDUCA en vertu des pr&eacute;sentes conditions g&eacute;n&eacute;rales de vente sont soumis au droit belge.&nbsp;</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">7.2. </span> <span style="font-weight: 400;">En cas de litige relatif aux contrats conclus avec EDUCA, n'ayant pu faire l'objet d'un accord &agrave; l'amiable, le cas &eacute;ch&eacute;ant par le biais d&rsquo;une conciliation ou d&rsquo;une m&eacute;diation, seuls les tribunaux de l&rsquo;arrondissement judiciaire du Brabant wallon sont comp&eacute;tents pour trancher toute question litigieuse (notamment d&rsquo;interpr&eacute;tation ou d&rsquo;ex&eacute;cution) qui r&eacute;sulterait des contrats conclus avec EDUCA en vertu des pr&eacute;sentes conditions g&eacute;n&eacute;rales de vente.&nbsp;&nbsp;</span></p>
                <p><br /><br /><br /></p>
                <p><strong>Annexe 1&nbsp; - d&eacute;claration de conformit&eacute; au R&egrave;glement (UE) 2016/679 (RGPD)</strong></p>
                <p>&nbsp;</p>
                <p><strong>D&Eacute;CLARATION DE CONFORMIT&Eacute; AU R&Egrave;GLEMENT (UE) 2016/679</strong></p>
                <p><strong>DU PARLEMENT EUROP&Eacute;EN ET DU CONSEIL RELATIF &Agrave; LA PROTECTION DES PERSONNES PHYSIQUES &Agrave; L&rsquo;&Eacute;GARD DU TRAITEMENT DES DONN&Eacute;ES &Agrave; CARACT&Egrave;RE PERSONNEL ET &Agrave; LA LIBRE CIRCULATION DE CES DONN&Eacute;ES (RGPD)</strong></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Afin de garantir la protection des donn&eacute;es personnelles de ses clients, prospects et salari&eacute;s, EDUCA s&rsquo;engage &agrave; respecter le r&egrave;glement (UE) 2016/679 du Parlement europ&eacute;en et du Conseil relatif &agrave; la protection des personnes physiques &agrave; l&rsquo;&eacute;gard du traitement des donn&eacute;es &agrave; caract&egrave;re personnel et &agrave; la libre circulation de ces donn&eacute;es (RGPD) entr&eacute; en vigueur le 25 mai 2018 et &agrave; s&rsquo;assurer de son respect par les sous-traitants effectuant pour son compte les op&eacute;rations de traitement desdites donn&eacute;es.&nbsp;</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Conform&eacute;ment &agrave; ce r&egrave;glement, EDUCA et ses sous-traitants veillent en permanence &agrave; se conformer aux r&egrave;gles en vigueur en mati&egrave;re de collecte, de traitement, d&rsquo;h&eacute;bergement et de transfert des donn&eacute;es &agrave; caract&egrave;re personnel de ses clients, formateurs et membres du personnel.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Par donn&eacute;e personnelle, nous entendons toute donn&eacute;e pouvant &ecirc;tre rattach&eacute;e, de pr&egrave;s ou de loin, &agrave; un citoyen : nom/pr&eacute;nom, adresse, lieu et date de naissance, carte de paiement, num&eacute;ro de t&eacute;l&eacute;phone, donn&eacute;es biom&eacute;triques, photo, num&eacute;ro de s&eacute;curit&eacute; sociale, &hellip;</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">A cette fin, EDUCA veille tout particuli&egrave;rement &agrave; respecter les &eacute;l&eacute;ments suivants :</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">1)</span> <span style="font-weight: 400;">Privacy by design : la plateforme de gestion et d&rsquo;information de EDUCA qui lui permet de g&eacute;rer ses divers processus d&rsquo;entreprise (ERP), comme chaque technologie &agrave; laquelle EDUCA recourt, int&egrave;grent, depuis leur conception et lors de chaque utilisation, le plus haut niveau possible de protection des donn&eacute;es.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">2)</span> <span style="font-weight: 400;">Privacy by default : EDUCA, en tant que responsable du traitement de donn&eacute;es personnelles, met tout en &oelig;uvre pour garantir par d&eacute;faut le plus haut niveau possible de protection des donn&eacute;es : nous gardons toujours &agrave; l&rsquo;esprit la protection des donn&eacute;es et nous assurons que nos employ&eacute;s et collaborateurs soient toujours form&eacute;s et inform&eacute;s sur la question.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">3)</span> <span style="font-weight: 400;">Registre des traitements : EDUCA tient &agrave; jour un registre des traitements contenant plusieurs informations essentielles, dont les coordonn&eacute;es du responsable du traitement, les finalit&eacute;s du traitement, les diff&eacute;rentes cat&eacute;gories de personnes concern&eacute;es par le traitement, les gestionnaires des donn&eacute;es (acteurs internes ou externes), le parcours des donn&eacute;es, les d&eacute;lais de destruction des donn&eacute;es et la description des mesures de s&eacute;curit&eacute; pour limiter les risques de fuite et pour optimiser la protection de ces donn&eacute;es.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">4)</span> <span style="font-weight: 400;">Conformit&eacute; des sous-traitants : lorsqu&rsquo;il fait recours &agrave; un sous-traitant, EDUCA s&rsquo;assure que celui-ci pr&eacute;sente toutes les garanties n&eacute;cessaires &agrave; la s&eacute;curit&eacute; et au bon traitement des donn&eacute;es qui lui sont confi&eacute;es. En cas de contr&ocirc;le, il devra pouvoir apporter toutes les preuves justifiant qu&rsquo;il est en parfaite conformit&eacute; avec le RGPD.</span></p>
                <p><span style="font-weight: 400;">&nbsp;</span></p>
                <p><span style="font-weight: 400;">Les sous-traitants de EDUCA ont en outre l&rsquo;obligation de suivre strictement ses instructions, lesquelles sont clairement &eacute;nonc&eacute;es dans un contrat &eacute;crit . Sur ce dernier sont renseign&eacute;es des informations essentielles, comme la finalit&eacute;, la dur&eacute;e, la nature et les modalit&eacute;s des traitements concern&eacute;s, le type de donn&eacute;es, ou encore le public cibl&eacute;.&nbsp;</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Par ailleurs, les &eacute;ventuels sous-traitants de EDUCA ne peuvent agir et prendre des d&eacute;cisions sans son accord explicite. S&rsquo;ils veulent eux-m&ecirc;mes recourir aux services d&rsquo;un autre sous-traitant pour r&eacute;aliser leur mission, ils sont tenus d&rsquo;en avertir EDUCA pour obtenir son autorisation, apr&egrave;s avoir d&eacute;montr&eacute; que leur partenaire pr&eacute;sente toutes les garanties n&eacute;cessaires. De m&ecirc;me, s&rsquo;ils proc&egrave;de &agrave; des transferts de donn&eacute;es, c&rsquo;est uniquement sur ordre, et non de leur propre initiative.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">5)</span> <span style="font-weight: 400;">R&eacute;colte explicite des donn&eacute;es : EDUCA veille &agrave; ce que toute personne concern&eacute;e par un traitement de donn&eacute;es personnelles ait donn&eacute; son consentement &agrave; l&rsquo;utilisation de celui-ci. EDUCA recueille ainsi le consentement de l&rsquo;utilisateur &agrave; l&rsquo;utilisation des donn&eacute;es personnelles qu&rsquo;il transmet par un acte positif clair . EDUCA,&nbsp; en utilisant un langage clair, facilement compr&eacute;hensible et sans tournure n&eacute;gative, s&rsquo;assure que le consentement de l&rsquo;utilisateur soit &eacute;clair&eacute; et univoque (il ne donne pas lieu &agrave; plusieurs interpr&eacute;tations).&nbsp;</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Respectant le principe de minimisation des donn&eacute;es, EDUCA ne demande par ailleurs que les donn&eacute;es n&eacute;cessaires pour la finalit&eacute; du traitement des donn&eacute;es et s&rsquo;engage &agrave; ce que ces donn&eacute;es ne soient pas trait&eacute;es ult&eacute;rieurement de mani&egrave;re incompatible avec cette finalit&eacute;. Si nous souhaitons utiliser des donn&eacute;es r&eacute;colt&eacute;es pour toute autre finalit&eacute; que celle pour laquelle nous les avons obtenues, nous demandons &agrave; nouveau le consentement de l&rsquo;utilisateur pour ce nouveau traitement.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">6)</span> <span style="font-weight: 400;">Limitation de la dur&eacute;e de conservation des donn&eacute;es : EDUCA s&rsquo;engage &agrave; ne conserver les donn&eacute;es personnelles r&eacute;colt&eacute;es que le temps n&eacute;cessaire &agrave; l&rsquo;accomplissement de l&rsquo;objectif qui &eacute;tait poursuivi lors de leur collecte. En dehors des cas dans lesquels il existe une obligation d'archivage, les donn&eacute;es qui ne pr&eacute;sentent plus d&rsquo;int&eacute;r&ecirc;t sont supprim&eacute;es sans d&eacute;lai. En cas de proc&eacute;dure de suppression automatique, le responsable du fichier s&rsquo;assure &eacute;galement que les donn&eacute;es sont effectivement supprim&eacute;es.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Les donn&eacute;es personnelles qui peuvent, et dans certains cas, doivent, faire l&rsquo;objet d&rsquo;un archivage lorsqu&rsquo;elles pr&eacute;sentent encore un int&eacute;r&ecirc;t sont archiv&eacute;es de trois fa&ccedil;ons :</span></p>
                <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">dans une base active (autrement appel&eacute;e &laquo; archives courantes &raquo;) ;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">dans des archives interm&eacute;diaires avec un acc&egrave;s restreint (&eacute;tape interm&eacute;diaire avant suppression) ;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">dans des archives d&eacute;finitives (pour les donn&eacute;es pr&eacute;sentant un int&eacute;r&ecirc;t justifiant qu&rsquo;elles ne fassent l&rsquo;objet d&rsquo;aucune destruction).</span></li>
                </ul>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Lorsque nous sommes soumis &agrave; une obligation d'archivage, nous veillons &agrave; n&rsquo;archiver que les&nbsp;</span></p>
                <p><span style="font-weight: 400;">donn&eacute;es utiles au respect de l&rsquo;obligation pr&eacute;vue ou pour faire valoir un droit en justice en effectuant un tri parmi la totalit&eacute; des donn&eacute;es collect&eacute;es pour ne garder que les seules donn&eacute;es indispensables.&nbsp;</span></p>
                <p><span style="font-weight: 400;">Notons que les donn&eacute;es archiv&eacute;es par EDUCA pour r&eacute;pondre &agrave; une obligation l&eacute;gale ou r&eacute;glementaire ne le seront que le temps n&eacute;cessaire &agrave; l&rsquo;accomplissement de l&rsquo;obligation en cause. Les donn&eacute;es archiv&eacute;es sont ensuite supprim&eacute;es lorsque le motif justifiant leur archivage n&rsquo;a plus raison d&rsquo;&ecirc;tre.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">7)</span> <span style="font-weight: 400;">Droit &agrave; l&rsquo;information, d&rsquo;acc&egrave;s direct, de rectification, d&rsquo;opposition, de portabilit&eacute; et d&rsquo;effacement des donn&eacute;es :</span></p>
                <p>&nbsp;</p>
                <ol>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Droit &agrave; l&rsquo;information : EDUCA s&rsquo;engage &agrave; r&eacute;pondre &agrave; toute personne concern&eacute;e par une collecte de donn&eacute;es qui souhaite savoir si nous d&eacute;tenons ou non des donn&eacute;es la concernant.</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Droit d&rsquo;acc&egrave;s direct : toute personne concern&eacute;e par une collecte de donn&eacute;es de la part d&rsquo; EDUCA peut recevoir, de mani&egrave;re compr&eacute;hensible, une copie de ses donn&eacute;es faisant l'objet d'un traitement ainsi que l&rsquo;information disponible sur l'origine des donn&eacute;es.</span></li>
                </ol>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Pour exercer son droit d'acc&egrave;s, l&rsquo;utilisateur doit adresser une demande &agrave; EDUCA par envoi recommand&eacute;, en faisant la preuve de son identit&eacute;.&nbsp;</span></p>
                <p><span style="font-weight: 400;">EDUCA r&eacute;pond dans les 30 jours de la r&eacute;ception de la demande en d&eacute;taillant &agrave; la personne concern&eacute;e, le cas &eacute;ch&eacute;ant, les informations telles que la p&eacute;riode de r&eacute;tention des donn&eacute;es, le droit de d&eacute;poser une plainte, le transfert des donn&eacute;es vers un pays tiers, non-membre de l'UE&hellip;</span></p>
                <p>&nbsp;</p>
                <ol>
                    <li><span style="font-weight: 400;">Droit de rectification : l&rsquo;utilisateur peut, sans frais, faire rectifier par EDUCA les donn&eacute;es inexactes qui se rapportent &agrave; lui et faire effacer ou interdire l&rsquo;utilisation de donn&eacute;es incompl&egrave;tes, non pertinentes ou interdites.</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li><span style="font-weight: 400;">Droit d&rsquo;opposition : l&rsquo;utilisateur peut s'opposer &agrave; ce que les donn&eacute;es le concernant trait&eacute;es par EDUCA fassent l'objet d'un traitement, mais il doit invoquer des raisons s&eacute;rieuses et l&eacute;gitimes. L&rsquo;utilisateur ne peut cependant pas faire valoir son droit d'opposition pour les traitements de EDUCA n&eacute;cessaires &agrave; la conclusion ou &agrave; l'ex&eacute;cution d'un contrat. Les personnes concern&eacute;es ne peuvent pas non plus s'opposer au traitement de leurs donn&eacute;es par EDUCA impos&eacute; par une obligation l&eacute;gale ou r&eacute;glementaire.</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li><span style="font-weight: 400;">Droit de portabilit&eacute; : la personne concern&eacute;e peut demander &agrave; EDUCA que ses donn&eacute;es soit d&eacute;tenues dans un format &eacute;lectronique et d'en obtenir une copie, lui permettant de les transf&eacute;rer facilement vers un autre fournisseur de service.&nbsp;</span></li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li><span style="font-weight: 400;">Droit d&rsquo;effacement des donn&eacute;es : la personne concern&eacute;e peut demander &agrave; EDUCA l&rsquo;effacement de ses donn&eacute;es d&egrave;s qu&rsquo;un des motifs suivants se pr&eacute;sente :</span></li>
                    <li><span style="font-weight: 400;">les donn&eacute;es ne sont plus n&eacute;cessaires au regard des finalit&eacute;s pour lesquelles elles ont &eacute;t&eacute; collect&eacute;es ou trait&eacute;es par EDUCA;</span></li>
                    <li><span style="font-weight: 400;">la personne souhaite retirer son consentement et il n&rsquo;existe pas d&rsquo;autre fondement juridique &agrave; ce traitement ;</span></li>
                </ol>
                <p><span style="font-weight: 400;">iii.</span> <span style="font-weight: 400;">la personne concern&eacute;e s&rsquo;oppose au traitement n&eacute;cessaire &agrave; l&rsquo;ex&eacute;cution d&rsquo;une mission d&rsquo;int&eacute;r&ecirc;t public ou relevant de l&rsquo;exercice de l&rsquo;autorit&eacute; publique dont est investi EDUCA ou s&rsquo;oppose au traitement n&eacute;cessaire aux fins des int&eacute;r&ecirc;ts l&eacute;gitimes poursuivis par EDUCA ou par un tiers ;</span></p>
                <ol>
                    <li><span style="font-weight: 400;">la personne concern&eacute;e s&rsquo;oppose au traitement de ses donn&eacute;es &agrave; des fins de prospection ;</span></li>
                    <li><span style="font-weight: 400;">les donn&eacute;es ont fait l&rsquo;objet d&rsquo;un traitement illicite ;</span></li>
                    <li><span style="font-weight: 400;">les donn&eacute;es doivent &ecirc;tre effac&eacute;es pour respecter une obligation l&eacute;gale qui est pr&eacute;vue par le droit de l&rsquo;Union ou par le droit belge ;</span></li>
                </ol>
                <p><span style="font-weight: 400;">vii.</span> <span style="font-weight: 400;">les donn&eacute;es ont &eacute;t&eacute; collect&eacute;es dans le cadre d&rsquo;une offre de services de EDUCA &agrave; des enfants.</span></p>
                <p><span style="font-weight: 400;">Lorsqu&rsquo;une telle demande est formul&eacute;e, EDUCA prend, dans les meilleurs d&eacute;lais et au plus tard dans le mois de la demande, les mesures raisonnables en vue d&rsquo;effacer tout lien vers ces donn&eacute;es ainsi que toute copie ou reproduction existantes de celles-ci.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Toute information compl&eacute;mentaire relative &agrave; la politique de EDUCA quant au r&egrave;glement sur le traitement des donn&eacute;es &agrave; caract&egrave;re personnel et &agrave; la libre circulation de ces donn&eacute;es (RGPD) peut &ecirc;tre obtenu sur simple demande &agrave; EDUCA.</span></p>
                <p><br /><br /><br /></p>
                <p><span style="font-weight: 400;">Pour EDUCA,</span></p>
                <p><br /><br /></p>
                <p><span style="font-weight: 400;">Xavier DE POORTER</span></p>
                <p><span style="font-weight: 400;">Administrateur</span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
