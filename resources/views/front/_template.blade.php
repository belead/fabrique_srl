<div class="site-wrapper overflow-hidden ">
    <div id="loading">
        <img src="{{asset('assets/templates/finity/image/preloader.gif')}}" alt="">
    </div>
    @if(!isset($no_header) || !$no_header)
        @include('front.blocks.header')
    @endif
    <main class="site-content" role="main">
        @yield('_content')
    </main>
    @if(!isset($no_footer) || !$no_footer)
        @include('front.blocks.footer')
    @endif
    @include('cookieConsent::index')
</div>
