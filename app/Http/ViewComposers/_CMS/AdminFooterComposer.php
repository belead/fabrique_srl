<?php namespace App\Http\ViewComposers\_CMS;

use App\Repositories\_CMS\Configuration\ConfigurationRepository;
use Illuminate\View\View;

class AdminFooterComposer {

    protected $configuration_repository;

    public function __construct(ConfigurationRepository $confr) {
        $this->configuration_repository = $confr;
    }

    public function compose(View $view) {
        $configuration = $this->configuration_repository->getCurrent();
        $view->with('configuration', $configuration);
    }

}
