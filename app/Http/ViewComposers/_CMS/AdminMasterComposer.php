<?php namespace App\Http\ViewComposers\_CMS;

use App\Models\_CMS\Media;
use Illuminate\View\View;

class AdminMasterComposer {
    public function compose(View $view) {
        $medias = Media::with('user')->orderBy('created_at', 'DESC')->get();
        $total_size = Media::sum('file_size');
        $view->with('medias', $medias);
        $view->with('total_size', $total_size);
    }
}
