<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class GoogleAuthMiddleware {

    public function handle($request, Closure $next, $guard = 'admin') {
        if(config('_CMS._global.google2FA')) {

            $admin = Auth::guard($guard)->user();
            if(session('socialite.admin')
                && $admin->socials
                    ->where('provider', session('socialite.admin.provider'))
                    ->where('provider_user_id', session('socialite.admin.user_id'))
                    ->first()) {

                // -- Bypass 2FA
                return $next($request);
            }

            if((is_null($request->cookie('google2FA'))) || (is_null($admin->g2fa_secret))) {
                if(!$request->session()->has('google_authentication')) {
                    return redirect()->route('admin.login.google2fa');
                }
            }
        }

        return $next($request);
    }

}
