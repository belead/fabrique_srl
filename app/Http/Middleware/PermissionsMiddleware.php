<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class PermissionsMiddleware {

    public function handle($request, Closure $next, $key) {
        if(!guard_admin()->canAccess($key)) {
            session()->flash('warning', 'You are not allowed to access this area.');
            return redirect()->route('admin.dashboard');
        }
        return $next($request);
    }

}
