<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class WipMiddleware {

    public function handle($request, Closure $next) {
        if(env('APP_WIP')
            && str_replace('/', '', $request->route()->getPrefix()) != config('_CMS._global.prefix_backend')
            && $request->route()->getPrefix() != '/cms/setup'
            && $request->route()->getPrefix() != config('_CMS._global.prefix_backend') . '/modules') {
            return response()->view('front.pages._wip', [], 503);
        }
        return $next($request);
    }

}
