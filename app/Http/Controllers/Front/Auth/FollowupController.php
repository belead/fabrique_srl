<?php namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;

class FollowupController extends Controller {

    public function index() {
        auth()->user()->load(['documents' => function($query) {
            $query
                ->where('is_active', 1);
        }]);

        return view('front.pages.auth.followup');
    }

}
