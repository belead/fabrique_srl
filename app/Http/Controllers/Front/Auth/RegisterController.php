<?php namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Mail\Register\NewRegistrationNotification;
use App\Mail\Register\UserConfirmation;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller {

    protected $user_repository;

    public function __construct(UserRepository $userr) {
        $this->user_repository = $userr;
    }

    public function form() {
        $user = session('register_user');
        if(!$user)
            return redirect()->route('front.page', 'demander-un-devis');

        return view('front.pages.auth.register.form');
    }

    public function register() {
        $user = session('register_user');
        if(!$user)
            return redirect_error('Une erreur est survenue');

        $data = request()->all();
        $user->update($data);

        try {

            // -- Mail to User
            Mail::to($user->email)->send(new UserConfirmation($user));

            // -- Mail to Organization
            Mail::to('xavier.depoorter@e-duca.be')
                ->cc(['info@belead.com'])
                ->send(new NewRegistrationNotification($user));

        } catch(\Throwable $t) {
            Log::error(get_class() . ': ' . $t->getMessage());
        }

        $url = env('CALENDLY_URL', 'https://calendly.com/belead/petit-point');
        $year = date('Y');
        $month = date('m');

        $url = "$url?month=$year-$month";

        session()->forget('register_user');

        return redirect()->to($url);
    }

    public function confirmation() {//calendly callback
        $email = request()->get('invitee_email');
        $date = request()->get('event_start_time');
        if($date)
            $date = new \DateTime($date);

        $user = $this->user_repository->findBy('email', $email);

        return view('front.pages.auth.register.confirmation', compact('user', 'date'));
    }

}
