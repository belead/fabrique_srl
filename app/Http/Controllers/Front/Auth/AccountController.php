<?php namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller {

    protected $user_repository;

    public function __construct(UserRepository $userr) {
        $this->user_repository = $userr;
    }

    public function index() {
        auth()->user()->load(['documents' => function($query) {
            $query
                ->where('is_active', 1);
        }]);

        return view('front.pages.auth.account');
    }

    public function save() {
        $user = $this->user_repository->find(auth()->id());

        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password_confirmation' => 'required_with:password'
        ];
        if(request()->filled('password')) {
            $rules['password'] = 'confirmed';
        }

        if($user->email != request()->input('email')) {
            $rules['email'] = 'unique:users';
        }

        $data = request()->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        if(request()->filled('password'))
            $data['password'] = bcrypt(request()->input('password'));
        else
            unset($data['password']);

        $this->user_repository->updateOrCreate(['id' => $user->id], $data);

        return redirect_success('Données enregistrées.');
    }

}
