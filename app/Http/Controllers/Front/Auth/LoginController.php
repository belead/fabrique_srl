<?php namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;

class LoginController extends Controller {

    public function form() {
        return view('front.pages.auth.login');
    }

    public function login() {
        if(auth()->guard('user')->attempt([
            'email' => request()->input('email'),
            'password' => request()->input('password')
        ])) {
            return redirect()->route('front.account');
        } else {
            return redirect()->back()->with('error', 'Email et/ou mot de passe erronés');
        }
    }

    public function logout() {
        auth()->guard('user')->logout();
        return redirect_success('');
    }

}
