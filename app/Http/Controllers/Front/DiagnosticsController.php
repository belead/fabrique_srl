<?php namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\SendDiagnosticResult;
use App\Repositories\DiagnosticRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DiagnosticsController extends Controller {

    protected $diagnostic_repository;

    public function __construct(DiagnosticRepository $diagr) {
        $this->diagnostic_repository = $diagr;
    }

    public function index() {
        $diagnostics = $this->diagnostic_repository->getFrontList();

        return view('front.v2.pages.diagnostics.list', compact('diagnostics'));
    }

    public function getForm($slug) {
        [$id, $slug] = explode('-', $slug, 2);
        $diagnostic = $this->diagnostic_repository->findBy('slug', $slug);

        if(!session('diagnostic_email'))
            return view('front.pages.diagnostics.register', compact('diagnostic'));

        if(session('current_diagnostic.id') != $id)
            session()->forget('current_diagnostic');

        if(!session('current_diagnostic'))
            session([
                'current_diagnostic' => [
                    'id' => $diagnostic->id,
                    'slug' => $diagnostic->slug,
                    'category' => $diagnostic->categories->first()->position ?? 1
                ]
            ]);

        $category = $diagnostic->categories->where('position', session('current_diagnostic.category'))->first();

        $recommended = [];

        if(!$category) {
            // -- End of the diagnostic
            foreach($diagnostic->categories as $categ) {
                foreach($categ->questions as $question) {
                    $answer = $diagnostic->answers->where('question_id', $question->id)->first();
                    if($answer && $answer->note < 3) {
                            $recommended[$categ->name]['questions'][] = [
                                'question' => $question->question,
                                'color' => $categ->color,
                                'score' => $answer->note
                            ];
                    }
                }
            }

            if(!session('current_diagnostic.email_sent')) {
                try {
                    $email = session('diagnostic_email');
                    $emails_copy[] = 'info@wikipreneurs.be';

                    if(session('diagnostic_forward')) {
                        Mail::to($emails_copy)
                            ->send(new SendDiagnosticResult($email, $recommended, $diagnostic, false, session('diagnostic.note')));
                    }

                    Mail::to($email)
                        ->send(new SendDiagnosticResult($email, $recommended, $diagnostic, false, session('diagnostic.note')));

                    session([
                        'current_diagnostic.email_sent' => 1
                    ]);

                } catch(\Throwable $t) {
                    Log::error($t->getMessage());
                }
            }
        }

        return view('front.pages.diagnostics.form', compact('diagnostic', 'category', 'recommended'));
    }

    public function save($slug) {
        [$id, $slug] = explode('-', $slug, 2);
        $diagnostic = $this->diagnostic_repository->findBy('slug', $slug);

        foreach(request()->input('questions') as $id => $note) {
            $diagnostic->answers()->updateOrCreate([
                'question_id' => $id,
                'email' => session('diagnostic_email')
            ], [
                'name' => session('name'),
                'project_name' => session('project_name'),
                'note' => $note,
            ]);

            session([
                'diagnostic.note' => (session('diagnostic.note') ?? 0) + $note
            ]);
        }

        session([
            'current_diagnostic.category' => session('current_diagnostic.category') + 1
        ]);

        return redirect_success('Réponses enregistrées.');
    }

    public function register($slug) {
        $data = request()->all();
        $rules = [
            'email' => 'required|email',
        ];

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation, trans('variables.form.has_errors'));

        session([
            'diagnostic_email' => request()->input('email'),
            'name' => request()->input('name'),
            'project_name' => request()->input('project_name'),
            'diagnostic_forward'  => request()->input('forward') ?? false
        ]);

        return redirect()->route('front.diagnostics.form', $slug);
    }

    public function retry($slug) {
        session()->forget('current_diagnostic');
        session()->forget('diagnostic.note');
        return redirect_success('');
    }

    public function saveChart($slug) {
        [$id, $slug] = explode('-', $slug, 2);
        $diagnostic = $this->diagnostic_repository->findBy('slug', $slug);

        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', request()->input('b64')));
        $email = auth()->check() ? auth()->user()->email : session('diagnostic_email');

        $directory = public_path() . '/uploads/img/diagnostics/charts/' . $diagnostic->id . '/';
        if(!file_exists($directory))
            mkdir($directory, 0777, true);

        $file_name = Str::slug($email, '_') . '.jpg';

        file_put_contents($directory . $file_name, $data);
        return 'success';
    }

}
