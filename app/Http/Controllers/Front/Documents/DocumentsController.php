<?php namespace App\Http\Controllers\Front\Documents;

use App\Http\Controllers\Controller;
use App\Mail\Documents\NewDocumentUploadedByUser;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class DocumentsController extends Controller {

    public function index() {
        auth()->user()->load(['documents' => function($query) {
            $query
                ->where('is_active', 1);
        }]);

        return view('front.pages.documents.listing');
    }

    public function form() {
        auth()->user()->load(['documents' => function($query) {
            $query
                ->where('is_active', 1);
        }]);

        return view('front.pages.documents.form');
    }

    public function upload() {
        $user = auth()->user();

        $data = request()->all();
        if(request()->hasFile('file')) {
            $directory = public_path() . '/uploads/files/modules/documents/';
            if(!file_exists($directory))
                mkdir($directory, 0777, true);

            $file = request()->file('file');
            $name = time() . '_' . str_replace(' ', '', $file->getClientOriginalName());
            $file->move($directory, $name);

            $data['file'] = $name;
        }

        $data['by_user_id'] = $user->id;
        $data['is_active'] = 1;

        $document = $user->documents()->create($data);

        try {

            Mail::to('xavier.depoorter@e-duca.be')
                ->cc(['chloe@e-duca.be'])
                ->send(new NewDocumentUploadedByUser($user, $document));

        } catch (\Throwable $t) {
            Log::error(get_class() . ': ' . $t->getMessage());
        }

        return redirect_success('Fichier envoyé.');
    }

}
