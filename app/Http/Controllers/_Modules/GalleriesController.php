<?php namespace App\Http\Controllers\_Modules;

use App\Http\Controllers\CRUDController;
use App\Models\_CMS\Module;
use App\Models\_CMS\Partial;
use App\Models\_Modules\GalleryPicture;
use App\Models\_Modules\Gallery;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;

class GalleriesController  extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_Modules\Gallery';

    public function index() {
        $dash_active = 'module_galleries';
        $galleries = Gallery::with('pictures')->where('site_id', session('switched_site.id'))
            ->where('lang', session('switched_language'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return view('admin.modules.galleries.index', compact('dash_active', 'galleries'));
    }

    public function switchDraftState($id) {
        $gallery = Gallery::find($id);
        $gallery->is_draft = !$gallery->is_draft;
        $gallery->save();
        return redirect_success('Draft state changed.');
    }

    public function getForm($id = false) {
        $dash_active = 'module_galleries';
        $gallery = $id ? Gallery::find($id) : new Gallery();

        $max_queue_size = 15;
        $max_files_size = 20;

        $module = Module::where('reference', 'galleries')->first();
        $partials = Partial::where('module_id', $module->id)->orderBy('name')->get();

        return view('admin.modules.galleries.form', compact('dash_active', 'gallery', 'max_queue_size', 'max_files_size', 'partials'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'partial_id' => 'required',
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $gallery = $id ? Gallery::find($id) : new Gallery();
            $gallery->name = $request->input('name');
            $gallery->title = $request->input('title');
            $gallery->head_title = $request->input('head_title');
            $gallery->partial_id = $request->input('partial_id');
            $gallery->description = $request->input('description');
            if(!$id) {
                $gallery->creator_id = guard_admin()->id;
                $gallery->lang = session('switched_language');
                $gallery->site_id = session('switched_site.id');
            }
            $gallery->save();
            if($request->has('titles')) {
                foreach($request->input('titles') as $key => $title) {
                    $picture = GalleryPicture::find($key);
                    $picture->title = $title;
                    $picture->description = $request->input('descriptions')[$key];
                    $picture->reason_number = $request->input('reason_numbers')[$key];
                    $picture->content = $request->input('contents')[$key];
                    $picture->video_title = $request->input('video_titles')[$key];
                    $picture->video_url = $request->input('video_urls')[$key];
                    if(isset($request->input('has_more_contents')[$key])) $picture->has_more_content = 1;
                    else $picture->has_more_content = 0;
                    if(isset($request->input('has_videos')[$key])) $picture->has_video = 1;
                    else $picture->has_video = 0;
                    $picture->save();
                }
            }
            return redirect_success('Gallery updated.');
        }
    }

    public function upItem($id) {
        $item = GalleryPicture::find($id);
        $before = GalleryPicture::where('order', $item->order - 1)->where('gallery_id', $item->gallery_id)->first();
        $before->order++;
        $before->save();
        $item->order--;
        $item->save();
        return redirect_success('Pictures order has been updated.');
    }

    public function downItem($id) {
        $item = GalleryPicture::find($id);
        $after = GalleryPicture::where('order', $item->order + 1)->where('gallery_id', $item->gallery_id)->first();
        $after->order--;
        $after->save();
        $item->order++;
        $item->save();
        return redirect_success('Pictures order has been updated.');
    }

    public function deleteItem($id) {
        $item = GalleryPicture::find($id);
        $after_items = GalleryPicture::where('order', '>', $item->order)->where('gallery_id', $item->gallery_id)->get();
        foreach($after_items as $after) {
            $after->order--;
            $after->save();
        }
        $item->delete();
        return redirect_success('Picture removed from the gallery.');
    }

    public function uploadPictures(Request $request) {
        $gallery_id = $request->input('gallery_id');
        $order = GalleryPicture::where('gallery_id', $gallery_id)->max('order');

        $directory = public_path() . '/uploads/img/modules/galleries/';
        if(!file_exists($directory)) mkdir($directory, 0777, true);
        if($request->hasFile('file')) {
            foreach($request->file('file') as $picture) {
                $order++;
                $photo = new GalleryPicture();
                $photo->order = $order;
                $photo->gallery_id = $gallery_id;

                $name = time() . '_' . $picture->getClientOriginalName();
                $picture->move($directory, $name);
                $photo->photo = $name;

                $photo->save();
            }
        } else return 'error';
        return 'success';
    }

}