<?php namespace App\Http\Controllers\_Modules;

use App\Models\_Modules\Team;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class TeamController extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_Modules\Team';

    public function index() {
        $dash_active = 'module_team';
        $members = Team::where('site_id', session('switched_site.id'))
            ->where('lang', session('switched_language'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return view('admin.modules.team.index', compact('dash_active', 'members'));
    }

    public function switchActiveState($id) {
        $member = Team::find($id);
        $member->is_draft = !$member->is_draft;
        $member->save();
        return redirect_success('Member active state changed.');
    }

    public function getForm($id = false) {
        $dash_active = 'module_team';
        $member = $id ? Team::find($id) : new Team();
        return view('admin.modules.team.form', compact('dash_active', 'member'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $member = $id ? Team::find($id) : new Team();
            $member->name = $request->input('name');
            if($request->has('slug') && !empty($request->input('slug'))) {
                $member->slug = \Str::slug($request->input('slug'));
            } else $member->slug = \Str::slug($member->name);
            $member->function = $request->input('function');
            $member->quote = $request->input('quote');
            $member->biography = $request->input('biography');
            $member->email = $request->input('email');
            $member->phone = $request->input('phone');
            $member->facebook = $request->input('facebook');
            $member->twitter = $request->input('twitter');
            $member->linkedin = $request->input('linkedin');
            $member->tags = $request->input('tags');
            $member->order = $request->input('order');
            if(!$id) {
                $member->lang = session('switched_language');
                $member->site_id = session('switched_site.id');
            }
            if($request->has('delete_picture')) {
                $member->picture = '';
            }
            if($request->hasFile('picture')) {
                $directory = public_path() . '/uploads/img/modules/team/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('picture')->getClientOriginalName();
                $request->file('picture')->move($directory, $name);
                $member->picture = $name;
            }
            $member->save();
            return redirect_success('Team member updated.');
        }
    }

}
