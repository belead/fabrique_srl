<?php namespace App\Http\Controllers\_Modules;

use App\Exports\DiagnosticsFormExport;
use App\Http\Controllers\Controller;
use App\Models\_Modules\Diagnostic;
use App\Repositories\DiagnosticRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class DiagnosticsController extends Controller {

    protected $diagnostic_repository;

    public function __construct(DiagnosticRepository $diagr) {
        $this->diagnostic_repository = $diagr;
    }

    public function index() {
        $dash_active = 'module_diagnostics';
        $diagnostics = $this->diagnostic_repository->getBackendList();

        return view('admin.modules.diagnostics.index', compact('dash_active', 'diagnostics'));
    }

    public function create() {
        return $this->form();
    }
    public function store() {
        return $this->saveForm();
    }
    public function edit($id) {
        return $this->form($id);
    }
    public function update($id) {
        return $this->saveForm($id);
    }

    public function form($id = null) {
        $dash_active = 'module_diagnostics';

        $diagnostic = $id ? $this->diagnostic_repository->find($id) : $this->diagnostic_repository->new();
        if($id)
            $diagnostic->load('categories.questions');

        return view('admin.modules.diagnostics.form', compact('dash_active', 'diagnostic'));
    }

    public function saveForm($id = null) {
        $data = request()->except('_token', '_method');

        $rules = [
            'title' => 'required',
        ];

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $data['slug'] = Str::slug($data['slug'] ?? $data['title']);

        if(request()->hasFile('picture')) {
            $directory = public_path() . '/uploads/img/modules/diagnostics/';
            if(!file_exists($directory)) mkdir($directory, 0777, true);

            $name = time() . '_' . request()->file('picture')->getClientOriginalName();
            request()->file('picture')->move($directory, $name);
            $data['picture'] = $name;
        }

        if(request()->has('delete_picture'))
            $data['picture'] = null;

        $this->diagnostic_repository->updateOrCreate(compact('id'), $data);

        return redirect_success('Diagnostique mis à jour.');
    }

    public function delete($id) {
        $this->diagnostic_repository->delete($id);
        return redirect_success('Diagnostique supprimé.');
    }

    public function toggle($id) {
        $this->diagnostic_repository->switchActiveState($id);
        return redirect_success('Statut du diagnostique modifié.');
    }

    public function exportData($id) {
        $diagnostic = Diagnostic::find($id);
        return Excel::download(new DiagnosticsFormExport($diagnostic), 'diagnostics_data.xlsx');
    }

}
