<?php namespace App\Http\Controllers\_Modules;

use App\Models\_CMS\Content;
use App\Models\_CMS\Module;
use App\Models\_CMS\Page;
use App\Models\_CMS\Partial;
use App\Models\_Modules\ContentBlock;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\CRUDController;
use Validator;

class ContentBlocksController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_Modules\ContentBlock';

    public function index() {
        $dash_active = 'module_content_blocks';
        $blocks = ContentBlock::where('site_id', session('switched_site.id'))
            ->where('lang', session('switched_language'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return view('admin.modules.content_blocks.index', compact('dash_active', 'blocks'));
    }

    public function getForm($id = false) {
        $dash_active = 'module_content_blocks';
        $block = $id ? ContentBlock::find($id) : new ContentBlock();
        $pages = Page::with('attributes')
            ->has('attributes')
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'))
            ->orderBy('is_home', 'DESC')
            ->get();
        $module = Module::where('reference', 'content_blocks')->first();
        $partials = Partial::where('module_id', $module->id)->orderBy('name')->get();
        return view('admin.modules.content_blocks.form', compact('dash_active', 'block', 'partials', 'pages'));
    }

    public function switchDraftState($id) {
        $block = ContentBlock::find($id);
        $block->is_draft = !$block->is_draft;
        $block->save();
        return redirect_success('Draft state changed.');
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'title' => 'required',
            'head_title' => 'required',
            'description' => 'required',
            'partial_id' => 'required|not_in:0',
        ];
        if(!$id) $rules['image'] = 'required';
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            $block = $id ? ContentBlock::find($id) : new ContentBlock();
            if(!$id) {
                $block->lang = session('switched_language');
                $block->site_id = session('switched_site.id');
            }
            $block->partial_id = $request->input('partial_id');
            $block->name = $request->input('name');
            $block->head_title = $request->input('head_title');
            $block->title = $request->input('title');
            $block->description = $request->input('description');
            $block->call_to_action_text = $request->input('call_to_action_text');
            if($request->has('page_id') && $request->input('page_id') != '0') $block->page_id = $request->input('page_id');
            else {
                $block->custom_url = $request->input('custom_url');
                $block->page_id = null;
            }
            if($request->has('is_video')) {
                $block->is_video = 1;
                $block->video_title = $request->input('video_title');
                $block->video_url = $request->input('video_url');
                $block->video_time = $request->input('video_time');
            }
            if($request->hasFile('image')) {
                $directory = public_path() . '/uploads/img/modules/content_blocks/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('image')->getClientOriginalName();
                $request->file('image')->move($directory, $name);
                $block->image = $name;
            }
            $block->save();
            return redirect_success('Content Block updated.');
        }
    }

    public function create_new(Request $request) {
        $block = new ContentBlock();
            $block->lang = session('switched_language');
            $block->site_id = session('switched_site.id');
            $block->partial_id = $request->input('block_partial_id');
            $block->name = $request->input('block_name');
            $block->head_title = $request->input('block_head_title');
            $block->title = $request->input('block_title');
            $block->description = $request->input('block_description');
            $block->call_to_action_text = $request->input('block_call_to_action');
            if($request->has('block_page_id') && $request->input('block_page_id') != '0') $block->page_id = $request->input('block_page_id');
            else {
                $block->custom_url = $request->input('block_custom_url');
                $block->page_id = null;
            }
            if($request->has('block_is_video')) {
                $block->is_video = 1;
                $block->video_title = $request->input('block_video_title');
                $block->video_url = $request->input('block_video_url');
                $block->video_time = $request->input('block_video_time');
            }
            if($request->hasFile('block_image')) {
                $directory = public_path() . '/uploads/img/modules/content_blocks/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $name = time() . '_' . $request->file('block_image')->getClientOriginalName();
                $request->file('block_image')->move($directory, $name);
                $block->image = $name;
            }
            $block->is_draft = 0;
        $block->save();

        $order = Content::where('page_id', $request->input('page_id'))->where('lang', session('switched_language'))->max('order');
        $module = Module::find($request->input('module_id'));

        $content = new Content();
            $content->reference = $request->input('reference');
            if($request->has('css_class')) $content->css_class = $request->input('css_class');
            $content->module_id = $module->id;
            $content->lang = session('switched_language');
            $content->page_id = $request->input('page_id');
            $content->order = $order + 1;
            $content->module_item_id = $block->id;
            if($request->has('is_published')) {
                $content->is_draft = 0;
            }
        $content->save();
        return redirect_success('Module content created.');
    }

}