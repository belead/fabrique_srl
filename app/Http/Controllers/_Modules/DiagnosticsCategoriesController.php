<?php namespace App\Http\Controllers\_Modules;

use App\Http\Controllers\Controller;
use App\Models\_Modules\Fiche;
use App\Models\_Modules\Tool;
use App\Repositories\DiagnosticCategoryRepository;
use Illuminate\Support\Facades\Validator;

class DiagnosticsCategoriesController extends Controller {

    protected $diagnostic_category_repository;

    public function __construct(DiagnosticCategoryRepository $diagcr) {
        $this->diagnostic_category_repository = $diagcr;
    }

    public function store() {
        return $this->saveForm();
    }
    public function edit($id) {
        return $this->form($id);
    }
    public function update($id) {
        return $this->saveForm($id);
    }

    public function form($id = null) {
        $dash_active = 'module_diagnostics';
        $category = $id ? $this->diagnostic_category_repository->find($id) : $this->diagnostic_category_repository->new();

        return view('admin.modules.diagnostics.categories.form', compact('dash_active', 'category'));
    }

    public function saveForm($id = null) {
        $data = request()->except('_token', '_method');

        $rules = [
            'name' => 'required',
        ];

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        if(!$id) {
            $max = $this->diagnostic_category_repository->getMaxPosition('position', [], $data['diagnostic_id']);
            $data['position'] = $max + 1;
        }

        $this->diagnostic_category_repository->updateOrCreate(compact('id'), $data);

        return redirect_success('Catégorie mise à jour.');
    }

    public function delete($id) {
        $category = $this->diagnostic_category_repository->find($id);

        $this->diagnostic_category_repository
            ->model()
            ->where('diagnostic_id', $category->diagnostic_id)
            ->where('position', '>', $category->position)
            ->decrement('position');

        $category->delete();
        return redirect_success('Catégorie supprimée.');
    }

    public function up($id) {
        $category = $this->diagnostic_category_repository->find($id);

        $this->diagnostic_category_repository
            ->model()
            ->where('diagnostic_id', $category->diagnostic_id)
            ->where('position', $category->position - 1)
            ->increment('position');

        $category->decrement('position');
        return redirect_success('Catégorie déplacée.');
    }

    public function down($id) {
        $category = $this->diagnostic_category_repository->find($id);

        $this->diagnostic_category_repository
            ->model()
            ->where('diagnostic_id', $category->diagnostic_id)
            ->where('position', $category->position + 1)
            ->decrement('position');

        $category->increment('position');
        return redirect_success('Catégorie déplacée.');
    }

}
