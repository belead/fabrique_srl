<?php namespace App\Http\Controllers\_Modules\Documents;

use App\Http\Controllers\_CMS\Resources\ResourcesController;
use App\Http\Controllers\_CMS\Resources\ResourcesInterface;
use App\Mail\Documents\UserNewDocumentAvailable;
use App\Repositories\_Modules\Document\DocumentRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class DocumentsController extends ResourcesController implements ResourcesInterface {

    protected $document_repository;
    protected $user_repository;

    public function __construct(DocumentRepository $docr,
                                UserRepository $userr) {
        $this->document_repository = $docr;
        $this->user_repository = $userr;

        $this->dash_active = 'module_documents';
    }

    public function index() {
        $dash_active = $this->dash_active;

        $documents = $this->document_repository->getBackendList();
        $users = $this->user_repository->getSelectableList();

        return view('admin.modules.documents.index', compact('dash_active', 'documents', 'users'));
    }

    public function form($id = null) {
        $dash_active = $this->dash_active;

        $document = $id ? $this->document_repository->find($id) : $this->document_repository->new();
        $users = $this->user_repository->getSelectableList();

        return view('admin.modules.documents.form', compact('dash_active', 'document', 'users'));
    }

    public function saveForm($id = null) {
        $rules = [
            'name' => 'required',
        ];
        if(!$id)
            $rules['file'] = 'required';

        $data = request()->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        if(request()->has('delete_file')) {
            $data['file'] = null;
        }

        if(request()->hasFile('file')) {
            $directory = public_path() . '/uploads/files/modules/documents/';
            if(!file_exists($directory))
                mkdir($directory, 0777, true);

            $file = request()->file('file');
            $name = time() . '_' . str_replace(' ', '', $file->getClientOriginalName());
            $file->move($directory, $name);

            $data['file'] = $name;
        }

        $document = $this->document_repository->updateOrCreate(compact('id'), $data);

        // -- Users
        $document->users()->sync(request()->input('users'));

        return redirect_success('Document updated.');
    }

    public function toggle($id) {
        $this->document_repository->switchActiveState($id);

        $document = $this->document_repository->find($id);
        if($document->is_active) {
            foreach($document->users as $user) {
                try {

                    Mail::to($user->email)->send(new UserNewDocumentAvailable($document, $user));

                } catch(\Throwable $t) {
                    Log::error(get_class() . ': ' . $t->getMessage());
                }
            }
        }

        return redirect_success('Document updated.');
    }

    public function delete($id) {
        $this->document_repository->delete($id);
        return redirect_success('Document deleted.');
    }


}
