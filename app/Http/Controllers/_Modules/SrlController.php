<?php namespace App\Http\Controllers\_Modules;

use App\Helpers\_CMS\CaptchaHelper;
use App\Http\Controllers\Controller;
use App\Mail\NewQuoteRequestAdmin;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class SrlController extends Controller {

    protected $model = '_Modules\Srl';

    protected $user_repository;

    public function __construct(UserRepository $userr) {
        $this->user_repository = $userr;
    }

    public function saveForm(Request $request) {
        $rules = [
            'first_name' => 'required',
			'email' => 'required|unique:users',
            'g-recaptcha-response' => 'required'
        ];

        $inputs = $request->all();
        $captchaIsValid = CaptchaHelper::captchaIsValid(request()->input('g-recaptcha-response'));

        $validation = Validator::make($inputs, $rules);
        if($validation->fails() || !$captchaIsValid)
            return redirect_error_form($validation);

        $inputs['site_id'] = 1;

        $user = $this->user_repository->create($inputs);
        session([
            'register_user' => $user
        ]);

        return redirect()->route('front.register.step2');
    }
}
