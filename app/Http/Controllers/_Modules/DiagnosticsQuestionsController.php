<?php namespace App\Http\Controllers\_Modules;

use App\Http\Controllers\Controller;
use App\Models\_Modules\Fiche;
use App\Models\_Modules\Formation;
use App\Models\_Modules\Tool;
use App\Repositories\DiagnosticQuestionRepository;
use Illuminate\Support\Facades\Validator;

class DiagnosticsQuestionsController extends Controller {

    protected $diagnostic_question_repository;

    public function __construct(DiagnosticQuestionRepository $diagqr) {
        $this->diagnostic_question_repository = $diagqr;
    }

    public function store() {
        return $this->saveForm();
    }
    public function edit($id) {
        return $this->form($id);
    }
    public function update($id) {
        return $this->saveForm($id);
    }

    public function form($id = null) {
        $dash_active = 'module_diagnostics';
        $question = $id ? $this->diagnostic_question_repository->find($id) : $this->diagnostic_question_repository->new();

        return view('admin.modules.diagnostics.questions.form', compact('dash_active', 'question'));
    }

    public function saveForm($id = null) {
        $data = request()->except('_token', '_method');

        $rules = [
            'question' => 'required',
        ];

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        if(!$id) {
            $max = $this->diagnostic_question_repository->getMaxPosition('position', [], $data['category_id']);
            $data['position'] = $max + 1;
        }

        $question = $this->diagnostic_question_repository->updateOrCreate(compact('id'), $data);

        return redirect_success('Question mise à jour.');
    }

    public function delete($id) {
        $question = $this->diagnostic_question_repository->find($id);

        // -- Decrement positions
        $this->diagnostic_question_repository
            ->model()
            ->where('category_id', $question->category_id)
            ->where('position', '>', $question->position)
            ->decrement('position');

        $question->delete();

        return redirect_success('Question supprimée.');
    }

    public function up($id) {
        $question = $this->diagnostic_question_repository->find($id);

        $this->diagnostic_question_repository
            ->model()
            ->where('category_id', $question->category_id)
            ->where('position', $question->position - 1)
            ->increment('position');

        $question->decrement('position');
        return redirect_success('Question déplacée.');
    }

    public function down($id) {
        $question = $this->diagnostic_question_repository->find($id);

        $this->diagnostic_question_repository
            ->model()
            ->where('category_id', $question->category_id)
            ->where('position', $question->position + 1)
            ->decrement('position');

        $question->increment('position');
        return redirect_success('Question déplacée.');
    }

}
