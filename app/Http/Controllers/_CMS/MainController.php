<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\Controller;
use App\Models\_CMS\Admin;
use App\Models\_CMS\Configuration;
use App\Models\_CMS\Language;
use App\Models\_CMS\Site;
use Illuminate\Http\Request;
use Validator;
use DB;
use Schema;
use Artisan;
use PDO;
use Session;

class MainController extends Controller {

    public function setup() {
        // -- Check database
        $database = env('DB_DATABASE', false);
        if(!$database)
            return $this->setupDatabase();

        try {
            DB::select('SELECT 1 FROM sites');
        } catch (\PDOException $e) {
            session()->flash('error', 'The database "' . env('DB_DATABASE') . '" is missing or incomplete.');
            return $this->setupDatabase();
        }

        Session::put('environment_checked', true);

        // -- Check default site
        $site = Site::where('is_default', 1)->first();
        if(!$site)
            return $this->setupSite();

        // -- Check default configuration
        $configuration = Configuration::where('site_id', $site->id)->first();
        if(!$configuration)
            return $this->setupConfig();

        // -- Everything is OK
        return redirect()->route('front.page');
    }

    private function setupDatabase() {
        $step_number = 0;
        return view('cms.setup.database', compact('step_number'));
    }

    public function postDatabaseSetup(Request $request) {
        $validation = Validator::make($request->all(), [
            'host' => 'required',
            'db_name' => 'required',
            'username' => 'required',
        ]);
        if($validation->fails())
            return redirect_error_form('Please, fill all the required fields.');

        $path = base_path('.env');
        if(file_exists($path)) {
            file_put_contents($path, str_replace(
                'DB_HOST=' . env('DB_HOST'), 'DB_HOST=' . $request->input('host'), file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'DB_DATABASE=' . env('DB_DATABASE'), 'DB_DATABASE=' . $request->input('db_name'), file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'DB_USERNAME=' . env('DB_USERNAME'), 'DB_USERNAME=' . $request->input('username'), file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'DB_PASSWORD=' . env('DB_PASSWORD'), 'DB_PASSWORD=' . $request->input('db_pwd'), file_get_contents($path)
            ));

            Artisan::call('config:clear');
        }

        $this->migrateAndSeedDatabase();

        return redirect()->route('cms.setup');
    }

    private function migrateAndSeedDatabase() {
        try {

            $pdo = $this->getPDOConnection(env('DB_HOST'), env('DB_PORT'), env('DB_USERNAME'), env('DB_PASSWORD'));

            $pdo->exec(sprintf(
                'CREATE DATABASE IF NOT EXISTS %s CHARACTER SET %s COLLATE %s;',
                env('DB_DATABASE'),
                'utf8mb4',
                'utf8mb4_unicode_ci'
            ));

        } catch (PDOException $exception) {
            throw new \Exception(sprintf('Failed to create "%s" database, %s', env('DB_DATABASE'), $exception->getMessage()));
        }
        Artisan::call('migrate');
        Artisan::call('db:seed');

        $language = Language::where('default_backend', 1)->first();
        Session::put('switched_language', $language->code);
    }

    private function getPDOConnection($host, $port, $username, $password) {
        return new PDO(sprintf('mysql:host=%s;port=%d;', $host, $port), $username, $password);
    }

    private function setupSite() {
        return view('cms.setup.site');
    }

    public function postSiteSetup(Request $request) {
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if($validation->fails())
            return redirect_error_form($validation, 'Please, provide a website name.');

        $site = new Site();
            $site->name = $request->input('name');
            $site->is_default = 1;
        $site->save();

        Session::put('switched_site', $site);

        return redirect()->route('cms.setup');
    }

    private function setupConfig() {
        $step_number = 2;
        return view('cms.setup.config', compact('step_number'));
    }

    public function postConfigSetup(Request $request) {
        $rules = [
            'email' => 'required|unique:admins',
            'username' => 'required|unique:admins',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ];

        $validation = Validator::make($request->all(), $rules);
        if($validation->fails())
            return redirect_error_form($validation, 'Please, fill all the required fields.');

        $admin = new Admin();
            $admin->username = $request->input('username');
            $admin->email = $request->input('email');
            $admin->level = 100;
            $admin->password = bcrypt($request->input('password'));
        $admin->save();

        $site = Site::where('is_default', 1)->first();
        $configuration = new Configuration();
            $configuration->site_id = $site->id;
            $configuration->email_contact = $request->input('email_contact');
            $configuration->phone_contact = $request->input('phone_contact');
            $directory = public_path() . '/assets/front/img/';
            if(!file_exists($directory)) mkdir($directory, 0777, true);
            if($request->hasFile('logo_website_default')) {
                $file = 'logo_website_default.' . $request->file('logo_website_default')->getClientOriginalExtension();
                $request->file('logo_website_default')->move($directory, $file);
                $configuration->logo_website_default = $file;
            }
        $configuration->save();

        return redirect()->route('cms.setup');
    }

}
