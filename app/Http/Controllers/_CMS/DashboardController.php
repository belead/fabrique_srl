<?php namespace App\Http\Controllers\_CMS;

use App\Repositories\_CMS\Admin\AdminLogRepository;
use App\Repositories\_CMS\Admin\AdminRepository;
use App\Repositories\_CMS\Page\PageRepository;
use App\Repositories\_CMS\Site\SiteRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Analytics;
use Spatie\Analytics\Period;

class DashboardController extends Controller {

    protected $admin_repository;
    protected $user_repository;
    protected $page_repository;
    protected $site_repository;
    protected $admin_log_repository;

    public function __construct(AdminRepository $admr,
                                UserRepository $usr,
                                PageRepository $pager,
                                SiteRepository $sitr,
                                AdminLogRepository $adminLogRepository) {
        $this->admin_repository = $admr;
        $this->user_repository = $usr;
        $this->page_repository = $pager;
        $this->site_repository = $sitr;
        $this->admin_log_repository = $adminLogRepository;
    }

    public function index() {
        $dash_active = 'dashboard';

        $counters = new \StdClass();
        $counters->admins = $this->admin_repository->getCount();
        $counters->users = $this->user_repository->getCount();
        $counters->pages = $this->page_repository->getCount();

        $analyticsData = null;

        if(env('ANALYTICS_VIEW_ID')
            && env('ANALYTICS_CREDENTIAL_JSON_PATH')
            && Storage::exists(env('ANALYTICS_CREDENTIAL_JSON_PATH'))) {

            try {
                $analyticsData = Analytics::fetchTotalVisitorsAndPageViews(Period::days(30));
            } catch (\Throwable $t) {
                Log::error($t->getMessage());
            }

        }

        return view('admin.dashboard', compact('dash_active', 'counters', 'analyticsData'));
    }

    public function logout($guard = 'admin') {
        auth()->guard($guard)->logout();
        return redirect()->to('/');
    }

    public function getLoginForm($guard = 'admin') {
        if(auth()->guard($guard)->check()) {
            return redirect()->to('/admin');
        }
        return view('admin.login');
    }

    public function login(Request $request, $guard = 'admin') {
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $inputs = $request->all();

        $validation = Validator::make($inputs, $rules);
        if($validation->fails())
            return redirect()->back()->with('error', 'The form has invalid inputs.');

        if(auth()->guard($guard)->attempt([
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ])) {
            Log::info(guard_admin()->getName() . ' has logged in.');
            $this->admin_log_repository->createLog('admins', guard_admin()->id, 'login', guard_admin()->email);
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->back()->with('error', 'Invalid username and/or password.');
        }
    }

    public function switchLanguage($code) {
        session([
            'switched_language' => $code
        ]);
        return redirect()->back()->with('success', 'Language switched');
    }

    public function switchSite($id) {
        session([
            'switched_site' => $this->site_repository->find($id)
        ]);
        return redirect()->back()->with('success', 'Site switched');
    }

    public function flushSession() {
        $user = guard_admin();

        $site = session('switched_site');
        $language_code = session('switched_language');
        $socialite = session('socialite');

        session()->flush();

        session([
            'switched_site' => $site,
            'switched_language' => $language_code,
            'socialite' => $socialite,
        ]);

        auth()->guard('admin')->login($user);
        return redirect()->back();
    }

}
