<?php namespace App\Http\Controllers\_CMS\Pages;

use App\Helpers\_CMS\PageHelper;
use App\Http\Controllers\CRUDController;
use App\Helpers\_CMS\SearchHelper;
use App\Repositories\_CMS\Admin\AdminLogRepository;
use App\Repositories\_CMS\Content\ContentRepository;
use App\Repositories\_CMS\Content\ContentTypeRepository;
use App\Repositories\_CMS\Language\LanguageRepository;
use App\Repositories\_CMS\Menu\MenuRepository;
use App\Repositories\_CMS\Module\ModuleRepository;
use App\Repositories\_CMS\Page\PageAttributeRepository;
use App\Repositories\_CMS\Page\PageRepository;
use App\Repositories\_CMS\Search\SearchEngineRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PagesController extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_CMS\Page';

    protected $page_repository;
    protected $page_attribute_repository;
    protected $language_repository;
    protected $content_repository;
    protected $content_type_repository;
    protected $module_repository;
    protected $menu_repository;
    protected $search_engine_repository;
    protected $admin_log_repository;

    public function __construct(PageRepository $pager,
                                PageAttributeRepository $pattrr,
                                LanguageRepository $langr,
                                ContentRepository $contr,
                                ContentTypeRepository $conttr,
                                ModuleRepository $modr,
                                AdminLogRepository $adminLogRepository,
                                MenuRepository $menur,
                                SearchEngineRepository $searchr) {
        $this->page_repository = $pager;
        $this->page_attribute_repository = $pattrr;
        $this->language_repository = $langr;
        $this->content_repository = $contr;
        $this->content_type_repository = $conttr;
        $this->module_repository = $modr;
        $this->menu_repository = $menur;
        $this->search_engine_repository = $searchr;
        $this->admin_log_repository = $adminLogRepository;

        $this->dash_active = 'pages';
    }

    public function index() {
        $dash_active = 'pages';

        $pages = $this->page_repository->getBackendList();
        $languages = $this->language_repository->in('code', session('switched_language'), ['is_used' => 1], true);

        $pages_count = PageHelper::getPagesCount($pages->where('is_deleted', 0));

        return view('admin.sections.pages.index', compact(
            'dash_active', 'pages', 'languages', 'pages_count'
        ));
    }

    public function switchActiveState($id) {
        $page = $this->page_repository->switchActiveState($id);
        $this->admin_log_repository->createLog('page', $page->id, ($page->is_active == 0) ? 'deactivate' : 'activate', $page->attributes->title);

        return redirect()->back()->with('success', 'Active state switched.');
    }

    public function getForm($id = false) {
        $dash_active = 'pages';

        $permissions = [99, 100];
        $can_edit_configuration = in_array(guard_admin()->level, $permissions);
        $has_security = false;

        $page = $id ? $this->page_repository->find($id) : $this->page_repository->new();

        if($id && !$page->attributes)
            return redirect()->route('admin.pages')->with('error', 'This content does not exist.');

        $languages = $this->language_repository->in('code', session('switched_language'), ['is_used' => 1], true);
        $content_types = $this->content_type_repository->getSelectable();
        $pages = $this->page_repository->getSelectableList(true);
        $modules = $this->module_repository->getSelectableForPageForm();
        $menus = $this->menu_repository->getSelectableForPageForm();

        // -- Edition mode
        if($id && $page->edition_mode == 'minimal') {
            if($page->contents->where('lang', session('switched_language'))->count() == 0) {
                // -- Create first default content
                $page->contents()->create([
                    'order' => 1,
                    'is_draft' => 0,
                    'type_id' => 1,
                    'lang' => session('switched_language'),
                    'reference' => uniqid(),
                ]);
            } elseif(!$page->contents->whereNotNull('type_id')->first()) {
                $page->update([
                    'edition_mode' => 'full'
                ]);
            }
        }

        // -- Contents
        if($id) {
            $page->load([
                'attributes',
                'contents.type',
                'contents.module.partials',
                'menus'
            ]);

            $clean_order = 1;
            foreach($page->contents as $content) {
                if($content->module) {
                    $class_name = '\App\Models\_Modules\\' . $content->module->class_name;
                    $class = new $class_name;
                    $items = $class->getLinkableContents($content->module->targetable_by);
                    $content->items = $items;
                }

                $this->content_repository->update([
                    'order' => $clean_order++
                ], $content->id);
            }
        }

        return view('admin.sections.pages.form', compact(
            'page', 'pages', 'dash_active', 'can_edit_configuration', 'has_security',
            'content_types', 'languages', 'modules', 'menus'
        ));
    }

    public function saveForm(Request $request, $id = false) {
        $data = $request->except('_token');

        $validation = Validator::make($data, [
            'title' => 'required'
        ]);
        if($validation->fails())
            return redirect_error_form($validation);

        $data['site_id'] = session('switched_site.id');
        $data['body_class'] = implode(' ', $request->input('body_class') ?? []);
        $data['is_deleted'] = $request->has('is_deleted');
        $data['is_secured'] = $request->has('is_secured');
        $data['is_guest'] = $request->has('is_guest');

        $data['is_home'] = $request->has('is_home');
        if(guard_admin()->level > 98) {
            if($data['is_home'])
                $this->page_repository->model()->where('is_home', 1)->update([
                    'is_home' => 0
                ]);
        }

        // -- Page
        $page = $this->page_repository->updateOrCreate(compact('id'), $data);
        $page->menus()->sync($request->input('menus') ?? []);

        if($request->filled('parent_id')) {
            $max = $this->page_repository->getMaxFamilyOrder($request->input('parent_id'));
            $page->parents()->sync([
                $request->input('parent_id') => [
                    'order' => $max + 1
                ]
            ]);
        } else {
            $page->parents()->detach();
        }

        // -- Attributes
        if(!$id)
            $data['creator_id'] = guard_admin()->id;

        $data['url'] = Str::slug($request->input('url') ?? $data['title']);

        if($request->has('delete_picture')) {
            $data['image'] = null;
        }
        if($request->hasFile('image')) {
            $directory = public_path() . '/uploads/img/pages/';
            if(!file_exists($directory)) mkdir($directory, 0777, true);

            $name = time() . '_' . $request->file('image')->getClientOriginalName();
            $request->file('image')->move($directory, $name);
            $data['image'] = $name;
        }

        $attr = $this->page_attribute_repository->updateOrCreate([
            'page_id' => $page->id,
            'lang' => session('switched_language')
        ], $data);

        // -- Search engine
        if(config('_project.options.search_engine') && env('APP_ENV') != 'local' && $id) {
            $url = '/' . session('switched_language') . '/' . $attr->url;

            $this->search_engine_repository->updateOrCreate([
                'element_type' => 'page',
                'element_id' => $page->id,
                'lang' => session('switched_language'),
                'site_id' => session('switched_site.id')
            ], [
                'url' => $url,
                'is_secured' => $page->is_secured,
                'content' => SearchHelper::loadContent(url($url)),
                'title' => $data['title']
            ]);
        }

        $this->admin_log_repository->createLog('page', $page->id, ($id ? 'update' : 'create'), $data['title']);

        return redirect()
            ->route('admin.pages.edit', $page->id)
            ->with('success', 'Page updated with success');
    }

    public function changeEditionMode($id, $mode) {
        $this->page_repository->update([
            'edition_mode' => $mode
        ], $id);
        return redirect_success('Edition mode changed.');
    }

    public function duplicateAttributes($id, $code) {
        $duplicated = $this->page_attribute_repository->duplicateInLocale($id, $code);
        if(!$duplicated)
            return redirect_error('Missing attributes.');
        return redirect_success('Page duplicated.');
    }

    public function duplicateAttributesFrom($id, $code) {
        $duplicated = $this->page_attribute_repository->duplicateFromLocale($id, $code);
        if(!$duplicated)
            return redirect_error('Missing attributes.');
        return redirect_success('Page duplicated.');
    }

    public function deleteTranslated($id) {
        $this->page_attribute_repository->delete($id);
        return redirect_success('Page deleted.');
    }

    public function editTranslated($id) {
        $attributes = $this->page_attribute_repository->find($id);
        session([
            'switched_language' => $attributes->lang
        ]);
        return redirect()->route('admin.pages.edit', $attributes->page->id);
    }

    public function beforeDelete($id) {
        $page = $this->page_repository->find($id);
        $page->parents()->detach();

        if($page->attributes) {
            $this->page_attribute_repository->update([
                'url' => '#' . $page->attributes->url
            ], $page->attributes->id);

            $this->admin_log_repository->createLog('page', $page->id, 'delete', $page->attributes->title);
        }

        return true;
    }

    public function beforeRestore($id) {
        $page = $this->page_repository->find($id);
        if($page->attributes) {
            $this->page_attribute_repository->update([
                'url' => str_replace('#', '', $page->attributes->url)
            ], $page->attributes->id);

            $this->admin_log_repository->createLog('page', $page->id, 'restore', $page->attributes->title);
        }
        return true;
    }

    public function getPagesLinks() {
        $pages = $this->page_repository
            ->model()
            ->with('attributes')
            ->where('site_id', session('switched_site.id'))
            ->where('is_destroyed', 0)
            ->where('is_deleted', 0)
            ->orderBy('is_home', 'DESC')
            ->get();

        $links = [];
        foreach($pages as $page) {
            if($page->attributes) {
                $p['title'] = ($page->attributes->title) ?? 'No title available';
                $p['value'] = (env('APP_URL') . '/' . session('switched_language') . '/' . $page->attributes->url) ?? '#';
                $links[] = $p;
            }
        }

        return response()->json($links);
    }

    public function copy($id) {
        $page = $this->page_repository->find($id);

        // -- Page
        $copy_page = $page->replicate();
        $copy_page->save();

        // -- Attributes
        $copy_attributes = $page->attributes->replicate();
        $copy_attributes->title .= ' (Copy)';
        $copy_attributes->url .= '-copy';
        $copy_attributes->page_id = $copy_page->id;
        $copy_attributes->save();

        // -- Contents
        foreach($page->contents as $content) {
            try {
                $copy_content = $content->replicate();
                $copy_content->page_id = $copy_page->id;
                $copy_content->save();

                if($content->module_id && $content->module_item_id) {
                    $module = $content->module;
                    if($module) {
                        $class_name = '\App\Models\_Modules\\' . $module->class_name;

                        if(class_exists($class_name)) {
                            $class = new $class_name;
                            $item = $class->find($content->module_item_id);

                            if($item) {
                                // -- Module item
                                $copy_item = $item->replicate();
                                $copy_item->{$module->targetable_by} .= ' (Copy)';
                                if($copy_item->slug) $copy_item->slug .= '-copy';
                                $copy_item->save();

                                $copy_content->update([
                                    'module_item_id' => $copy_item->id
                                ]);

                                // -- Module item relationships
                                $module->copyRelationships($class, $item, $copy_item, session('switched_language'));
                            } else {
                                throw new \Exception('Item with id ' . $content->module_item_id . ' not found.');
                            }
                        } else {
                            throw new \Exception($class_name . ' not found.');
                        }
                    }
                }
            } catch(\Throwable $t) {
                Log::error('_CMS\PagesController: ' . $t->getMessage() . $t->getTraceAsString());
            }
        }

        return redirect_success('Page copied.');
    }

}
