<?php namespace App\Http\Controllers\_CMS\Pages;

use App\Http\Controllers\Controller;
use App\Repositories\_CMS\Admin\AdminLogRepository;
use App\Repositories\_CMS\Content\ContentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PagesModulesController extends Controller {

    protected $content_repository;
    protected $admin_log_repository;

    public function __construct(ContentRepository $contr, AdminLogRepository $adminLogRepository) {
        $this->content_repository = $contr;
        $this->admin_log_repository = $adminLogRepository;
    }

    public function saveForm(Request $request) {
        if((!$request->has('module_item_id') || $request->input('module_item_id') == 0)
            && !$request->has('module_all_items')
            && $request->has('external_controller')) {

            return resolve('\\App\Http\Controllers\\' . $request->input('external_controller'))
                ->saveForm($request->input('item_id'));
        }

        $rules = [
            'module_item_id' => 'required_without:module_all_items|required_without:module_order_by',
            'module_all_items' => 'required_without:module_item_id',
            'module_order_by' => 'required_without:module_item_id'
        ];

        if(!$request->has('module_all_items') && !$request->has('module_order_by'))
            $rules['module_item_id'] .= '|not_in:0';

        $validation = Validator::make($request->all(), $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $data = $request->all();
        $id = $request->input('content_id');

        if(!$id) {
            $max = $this->content_repository->getMaxPosition('order', [
                'lang' => session('switched_language'),
                'page_id' => $request->input('page_id')
            ]);
            $data['order'] = $max + 1;

            $data['lang'] = session('switched_language');
            $data['is_draft'] = !$request->has('is_published');
        }

        $data['reference'] = $request->input('reference') ?: uniqid();
        $data['css_class'] = implode(' ', request()->input('css_class') ?? []);

        if($request->input('module_item_id'))
            $data['module_number_items'] = null;

        $data['module_all_items'] = !$request->input('module_item_id')
            && $request->has('module_all_items');

        $content = $this->content_repository->updateOrCreate(compact('id'), $data);

        // -- Logs
        $this->admin_log_repository->createLog('page_module', $content->id, ($id ? 'update' : 'create'), $content->page->attributes->title ?? '');

        return redirect_success('Module content updated.');
    }

}
