<?php namespace App\Http\Controllers\_CMS\Pages;

use App\Http\Controllers\Controller;
use App\Repositories\_CMS\Admin\AdminLogRepository;
use App\Repositories\_CMS\Content\ContentRepository;
use App\Repositories\_CMS\Content\ContentTypeRepository;
use Illuminate\Http\Request;

class PagesContentsController extends Controller {

    protected $content_repository;
    protected $content_type_repository;
    protected $admin_log_repository;

    public function __construct(ContentRepository $contr,
                                ContentTypeRepository $conttr,
                                AdminLogRepository $adminLogRepository) {
        $this->content_repository = $contr;
        $this->content_type_repository = $conttr;
        $this->admin_log_repository = $adminLogRepository;
    }

    public function saveForm(Request $request) {
        $data = $request->except('_token');
        $id = $request->input('content_id');

        $content_type = $this->content_type_repository->find($request->input('type_id'));
        $name = $content_type->input_name;

        if(!$id) {
            $max = $this->content_repository->getMaxPosition('order', [
                'page_id' => $request->input('page_id'),
                'lang' => session('switched_language')
            ]);
            $data['order'] = $max + 1;
            $data['lang'] = session('switched_language');
            $data['is_draft'] = !$request->has('is_published');
        }

        $data['reference'] = $request->input('reference') ?: uniqid();
        $data['css_class'] = implode(' ', request()->input('css_class') ?? []);

        if($content_type->input_type == 'file' && $request->hasFile($name)) {
            $directory = public_path() . '/uploads/img/pages/';
            if(!file_exists($directory)) mkdir($directory, 0777, true);

            $file = time() . '_' . $request->file($name)->getClientOriginalName();
            $request->file($name)->move($directory, $file);
            $data['content'] = $file;
        } else {
            $data['content'] = $request->input($name);
        }

        $content = $this->content_repository->updateOrCreate(compact('id'), $data);

        // -- Logs
        $this->admin_log_repository->createLog('page_content', $content->page->id, ($id ? 'update' : 'create'), $content->page->attributes->title ?? '');

        return redirect_success('Content updated.');
    }

    public function remove($id) {
        $content = $this->content_repository->find($id);
        if($content) {
            $this->admin_log_repository->createLog('page_content', $content->page->id, 'delete', $content->page->attributes->title);
            $this->content_repository->delete($id);
        }
        return redirect_success('Content removed from the page.');
    }

    public function up($id) {
        $this->content_repository->up($id, 'order');
        return redirect_success('Contents order has been updated.');
    }

    public function down($id) {
        $this->content_repository->down($id, 'order');
        return redirect_success('Contents order has been updated.');
    }

    public function unpublish($id) {
        $this->content_repository->update([
            'is_draft' => 1
        ], $id);
        return redirect_success('Content marked as draft.');
    }

    public function publish($id) {
        $this->content_repository->update([
            'is_draft' => 0
        ], $id);
        return redirect_success('Content published.');
    }

}
