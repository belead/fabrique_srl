<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\CRUDController;
use App\Models\_CMS\ContentTemplate;
use App\Models\_CMS\Media;
use App\Models\_CMS\Menu;
use App\Models\_CMS\PageAttribute;
use App\Models\_CMS\ContentDraft;
use App\Models\_CMS\ContentType;
use App\Models\_CMS\Language;
use App\Models\_CMS\PageMenu;
use Illuminate\Http\Request;
use App\Models\_CMS\Content;
use App\Models\_CMS\Module;
use App\Models\_CMS\Page;
use App\Http\Requests;
use Validator;
use Session;
use Auth;

class PagesController extends CRUDController {

    protected $soft_delete = true;
    protected $model = '_CMS\Page';

    public function index() {
        $dash_active = 'pages';
        $pages = Page::with('attributes.creator')
            ->where('site_id', session('switched_site.id'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('is_home', 'DESC')
            ->where(function($query) {
                if(session('search_pages.title')) {
                    $query->whereHas('attributes', function($query) {
                        $query->where('title', 'LIKE', '%' . session('search_pages.title') . '%');
                    });
                }
            })
            ->get();

        return view('admin.sections.pages.index', compact('dash_active', 'pages'));
    }

    public function search(Request $request) {
        session([
            'search_pages' => [
                'title' => $request->input('title')
            ]
        ]);
        return redirect()->route('admin.pages');
    }

    public function clear() {
        session()->forget('search_pages');
        return redirect()->route('admin.pages');
    }

    public function switchActiveState($id) {
        $page = Page::find($id);
        $page->is_active = !$page->is_active;
        $page->save();
        return redirect()->back()->with('success', 'Active state switched.');
    }

    public function getForm($id = false) {
        $dash_active = 'pages';
        $permissions = [99, 100];

        if($id) $page = Page::with('attributes', 'contents.type', 'contents.draft', 'contents.module', 'menus')->find($id);
        else $page = new Page();

        if($id && !$page->attributes)
            return redirect()->route('admin.pages')->with('error', 'This content does not exist.');

        $can_edit_configuration = in_array(guard_admin()->level, $permissions);
        $has_security = false;
        $content_types = ContentType::orderBy('name')->where('available', 1)->get();
        $languages = Language::where('is_used', 1)->where('code', '!=', session('switched_language'))->get();

        $modules = Module::with('partials')->where('is_active', 1)->where('is_linkable', 1)->get();
        foreach($modules as $key => $module) {
            $class_name = '\App\Models\_Modules\\' . $module->class_name;
            if(class_exists($class_name)) {
                $class = new $class_name;
            } else {
                $modules->forget($key);
                continue;
            }
            if(method_exists($class, 'getLinkableContents')) {
                $items = $class->getLinkableContents($module->targetable_by);
                $module->items = $items;
                $module->orders = explode(',', $module->orderable_by);
                $module->view_name = 'admin.modules.' . $module->reference . '.content_form';
                $module->is_volatile = view()->exists($module->view_name);
            } else {
                $modules->forget($key);
                continue;
            }
        }

        if($id) {
            foreach($page->contents as $content) {
                if($content->module) {
                    $class_name = '\App\Models\_Modules\\' . $content->module->class_name;
                    $class = new $class_name;
                    $items = $class->getLinkableContents($content->module->targetable_by);
                    $content->items = $items;
                }
            }
        }

        $linked_menus = [];
        $menus = Menu::with('items')->where('site_id', session('switched_site.id'))->where('is_everywhere', 0)->get();
        if($id && $page->menus) {
            foreach($page->menus as $menu) {
                array_push($linked_menus, $menu->menu_id);
            }
        }

        $pages = Page::with('attributes')
            ->has('attributes')
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'))
            ->orderBy('is_home', 'DESC')
            ->get();

        $medias = Media::with('user')->orderBy('created_at', 'DESC')->get();
        $total_size = Media::sum('file_size');

        return view('admin.sections.pages.form', compact('page', 'pages', 'dash_active', 'can_edit_configuration', 'has_security', 'content_types', 'languages', 'modules', 'menus', 'linked_menus', 'medias', 'total_size'));
    }

    public function saveForm(Request $request, $id = false) {
        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if($validation->fails()) {
            return redirect_error_form($validation);
        } else {
            if($id) $page = Page::with('attributes')->find($id);
            else $page = new Page();
            $page->body_class = $request->input('body_class');
            $page->directory = $request->input('directory');
            if($request->has('level_access')) $page->level_access = $request->input('level_access');
            if($request->has('is_active')) $page->is_active = $request->input('is_active');
            if($request->has('is_deleted')) $page->is_deleted = 1;
            else $page->is_deleted = 0;
            if($request->has('is_secured')) $page->is_secured = 1;
            else $page->is_secured = 0;
            if($request->has('is_guest')) $page->is_guest = 1;
            else $page->is_guest = 0;
            if($request->has('is_home')) {
                $before = Page::where('is_home', 1)->first();
                if(($before && $id && $before->id != $id) || ($before && !$id)) {
                    $before->is_home = 0;
                    $before->save();
                }
                $page->is_home = 1;
            } else $page->is_home = 0;
            if(!$id) $page->site_id = session('switched_site.id');
            $page->save();

            if($request->has('menus') && !empty($request->input('menus'))) {
                PageMenu::where('page_id', $page->id)->delete();
                foreach($request->input('menus') as $item) {
                    $hook = new PageMenu();
                        $hook->page_id = $page->id;
                        $hook->menu_id = $item;
                    $hook->save();
                }
            } else PageMenu::where('page_id', $page->id)->delete();

            if($id) $attr = $page->attributes;
            else $attr = new PageAttribute();
                if(!$id) $attr->lang = session('switched_language');
                if(!$id) $attr->page_id = $page->id;
                $attr->title = $request->input('title');
                if($request->has('description')) $attr->description = $request->input('description');
                if($request->has('meta_title')) $attr->meta_title = $request->input('meta_title');
                if($request->has('meta_description')) $attr->meta_description = $request->input('meta_description');
                if(!$request->filled('url')) $attr->url = \Str::slug($request->input('title'));
                else $attr->url = $request->input('url');
                if(!$id) $attr->creator_id = guard_admin()->id;
                if($request->has('delete_picture')) {
                    $attr->image = null;
                }
                if($request->hasFile('image')) {
                    $directory = public_path() . '/uploads/img/pages/';
                    if(!file_exists($directory)) mkdir($directory, 0777, true);

                    $name = time() . '_' . $request->file('image')->getClientOriginalName();
                    $request->file('image')->move($directory, $name);
                    $attr->image = $name;
                }
            $attr->save();

            return redirect()->route('admin.pages.edit', $page->id)->with('success', 'Page updated with success');
        }
    }

    public function addModule(Request $request) {
        if((!$request->has('module_item_id') || $request->input('module_item_id') == 0)
            && !$request->has('module_all_items') && $request->has('action_url')) {
            return resolve('\\App\Http\Controllers\\' . $request->input('action_url'))->create_new($request);
        }

        $rules = [
            'module_item_id' => 'required_without:module_all_items|required_without:module_order_by',
            'module_all_items' => 'required_without:module_item_id',
            'module_order_by' => 'required_without:module_item_id'
        ];

        if(!$request->has('module_all_items') && !$request->has('module_order_by'))
            $rules['module_item_id'] .= '|not_in:0';

        $validation = Validator::make($request->all(), $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $order = Content::where('page_id', $request->input('page_id'))->where('lang', session('switched_language'))->max('order');
        $module = Module::find($request->input('module_id'));

        $content = new Content();
            $content->reference = $request->input('reference') ?: uniqid();
            $content->head_title = $request->input('head_title');
            $content->title = $request->input('title');
            $content->partial_id = $request->input('partial_id');
            $content->css_class = $request->input('css_class');
            $content->module_id = $module->id;
            $content->lang = session('switched_language');
            $content->page_id = $request->input('page_id');
            $content->order = $order + 1;
            if($request->has('module_item_id') && $request->input('module_item_id') != 0) $content->module_item_id = $request->input('module_item_id');
            else {
                if($request->has('module_all_items')) {
                    $content->module_all_items = 1;
                } else if($request->has('module_number_items')) $content->module_number_items = $request->input('module_number_items');
                $content->module_order_by = $request->input('module_order_by');
            }
            if($request->has('is_published')) {
                $content->is_draft = 0;
            }
        $content->save();

        return redirect_success('Module content added to the page.');
    }

    public function updateModule(Request $request) {
        $content = Content::find($request->input('content_id'));
            $content->reference = $request->input('reference') ?: uniqid();
            $content->head_title = $request->input('head_title');
            $content->title = $request->input('title');
            $content->css_class = $request->input('css_class');
            if($request->has('partial_id') && $request->input('partial_id') != 0) $content->partial_id = $request->input('partial_id');
            if($request->has('module_all_items')) {
                $content->module_all_items = 1;
                $content->module_item_id = null;
                $content->module_number_items = null;
                $content->module_order_by = $request->input('module_order_by');
            } else if($request->has('module_item_id') && $request->input('module_item_id') != 0) {
                $content->module_item_id = $request->input('module_item_id');
                $content->module_number_items = null;
                $content->module_order_by = null;
                $content->module_all_items = 0;
            } elseif($request->has('module_number_items') && $request->has('module_order_by')) {
                $content->module_number_items = $request->input('module_number_items');
                $content->module_order_by = $request->input('module_order_by');
                $content->module_item_id = null;
                $content->module_all_items = 0;
            }
        $content->save();

        return redirect_success('Module content updated.');
    }

    public function addContent(Request $request) {
        $order = Content::where('page_id', $request->input('page_id'))->where('lang', session('switched_language'))->max('order');
        $content_type = ContentType::find($request->input('type_id'));
        $name = $content_type->input_name;

        $content = new Content();
            $content->reference = $request->input('reference') ?: uniqid();
            $content->css_class = $request->input('css_class');
            $content->type_id = $content_type->id;
            $content->lang = session('switched_language');
            $content->page_id = $request->input('page_id');
            $content->order = $order + 1;
            if($content_type->input_type == 'file' && $request->hasFile($name)) {
                $directory = public_path() . '/uploads/img/pages/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $file = time() . '_' . $request->file($name)->getClientOriginalName();
                $request->file($name)->move($directory, $file);
                $content->content = $file;
            } else $content->content = $request->input($name);
            if($request->has('is_published')) {
                $content->is_draft = 0;
            }
            if($request->has('content_custom_url') && !empty($request->input('content_custom_url'))) {
                $content->content_custom_url = $request->input('content_custom_url');
            } elseif($request->has('content_page_id') && $request->input('content_page_id') != 0) {
                $content->content_page_id = $request->input('content_page_id');
            }
        $content->save();

        return redirect_success('Content added.');
    }

    public function updateContent(Request $request, $from_draft = false) {
        $content_type = ContentType::find($request->input('type_id'));
        $name = $content_type->input_name;

        $content = Content::find($request->input('content_id'));
            if($request->has('is_draft')) {
                if(!$request->has('draft_id')) {
                    ContentDraft::where('content_id', $content->id)->delete();
                    $draft = new ContentDraft();
                } else $draft = ContentDraft::find($request->input('draft_id'));

                if(!$request->has('draft_id')) $draft->content_id = $content->id;
                $draft->reference = $request->input('reference');
                if($request->has('css_class')) $draft->css_class = $request->input('css_class');
                if($content_type->input_type == 'file' && $request->hasFile($name)) {
                    $directory = public_path() . '/uploads/img/pages/';
                    if(!file_exists($directory)) mkdir($directory, 0777, true);
                    $file = time() . '_' . $request->file($name)->getClientOriginalName();
                    $request->file($name)->move($directory, $file);
                    $draft->content = $file;
                } else $draft->content = $request->input($name);
                if($request->has('content_custom_url') && !empty($request->input('content_custom_url'))) {
                    $draft->content_custom_url = $request->input('content_custom_url');
                    $draft->content_page_id = null;
                } elseif($request->has('content_page_id') && $request->input('content_page_id') != 0) {
                    $draft->content_page_id = $request->input('content_page_id');
                    $draft->content_custom_url = null;
                }
                $draft->save();

                if(!$from_draft) return redirect_success('Draft created/updated with success.');
                else return true;
            }

            $content->reference = $request->input('reference') ?: uniqid();
            $content->css_class = $request->input('css_class');
            if($content_type->input_type == 'file' && $request->hasFile($name)) {
                $directory = public_path() . '/uploads/img/pages/';
                if(!file_exists($directory)) mkdir($directory, 0777, true);
                $file = $request->file($name)->getClientOriginalName();
                $request->file($name)->move($directory, $file);
                $content->content = $file;
            } elseif($content_type->input_type != 'file') $content->content = $request->input($name);
            if($request->has('content_custom_url') && !empty($request->input('content_custom_url'))) {
                $content->content_custom_url = $request->input('content_custom_url');
                $content->content_page_id = null;
            } elseif($request->has('content_page_id') && $request->input('content_page_id') != 0) {
                $content->content_page_id = $request->input('content_page_id');
                $content->content_custom_url = null;
            }
        $content->save();

        return redirect_success('Content updated.');
    }

    public function removeContent($id) {
        $content = Content::find($id);
            $after_contents = Content::where('order', '>', $content->order)->where('page_id', $content->page_id)->get();
            foreach($after_contents as $after) {
                $after->order--;
                $after->save();
            }
            if($content->draft) $content->draft->delete();
        $content->delete();

        return redirect_success('Content removed from the page.');
    }

    public function upContent($id) {
        $content = Content::find($id);
            $page_id = $content->page_id;
            $before = Content::where('order', $content->order - 1)->where('page_id', $page_id)->first();
            $before->order++;
            $before->save();
            $content->order--;
        $content->save();

        return redirect_success('Contents order has been updated.');
    }

    public function downContent($id) {
        $content = Content::find($id);
            $page_id = $content->page_id;
            $after = Content::where('order', $content->order + 1)->where('page_id', $page_id)->first();
            $after->order--;
            $after->save();
            $content->order++;
        $content->save();

        return redirect_success('Contents order has been updated.');
    }

    public function unPublishContent($id) {
        $content = Content::find($id);
            $content->is_draft = 1;
        $content->save();

        return redirect_success('Content marked as draft.');
    }

    public function publishContent($id) {
        $content = Content::find($id);
            $content->is_draft = 0;
        $content->save();

        return redirect_success('Content published.');
    }

    public function duplicateAttributes($id, $code) {
        $page = Page::find($id);
        $duplicate = $page->attributes->replicate();
            $duplicate->lang = $code;
            $duplicate->creator_id = guard_admin()->id;
        $duplicate->save();

        $contents = $page->contents;
        foreach($contents as $content) {
            if($content->module_id && $content->module_item_id) continue;
            $duplicate = $content->replicate();
            $duplicate->lang = $code;
            $duplicate->save();
        }

        return redirect_success('Page duplicated.');
    }

    public function deleteTranslated($id) {
        $attributes = PageAttribute::find($id);
        $attributes->delete();
        Content::where('page_id', $attributes->page->id)->where('lang', $attributes->lang)->delete();
        return redirect_success('Page deleted.');
    }

    public function editTranslated($id) {
        $attributes = PageAttribute::find($id);
        Session::put('switched_language', $attributes->lang);
        return redirect()->route('admin.pages.edit', $attributes->page->id);
    }

    public function updateDraft(Request $request) {
        $draft_id = $request->input('draft_id');
        $draft = ContentDraft::find($draft_id);
        if($request->has('is_deleted')) {
            $draft->delete();
            return redirect_success('Draft deleted.');
        }
        if($request->has('is_published')) {
            $this->updateContent($request, true);
            $content = $draft->rel_content;
            $content->content = $draft->content;
            $content->reference = $draft->reference;
            $content->css_class = $draft->css_class;
            $content->content_custom_url = $draft->content_custom_url;
            $content->content_page_id = $draft->content_page_id;
            $content->save();
            $draft->delete();
            return redirect_success('Draft published.');
        }
        $content_type = ContentType::find($request->input('type_id'));
        $name = $content_type->input_name;
        $draft->reference = $request->input('reference');
        if($request->has('css_class')) $draft->css_class = $request->input('css_class');
        if($content_type->input_type == 'file' && $request->hasFile($name)) {
            $directory = public_path() . '/uploads/img/pages/';
            if(!file_exists($directory)) mkdir($directory, 0777, true);
            $file = time() . '_' . $request->file($name)->getClientOriginalName();
            $request->file($name)->move($directory, $file);
            $draft->content = $file;
        } else $draft->content = $request->input($name);
        if($request->has('content_custom_url') && !empty($request->input('content_custom_url'))) {
            $draft->content_custom_url = $request->input('content_custom_url');
            $draft->content_page_id = null;
        } elseif($request->has('content_page_id') && $request->input('content_page_id') != 0) {
            $draft->content_page_id = $request->input('content_page_id');
            $draft->content_custom_url = null;
        }
        $draft->save();
        return redirect_success('Draft updated.');
    }

    public function beforeDelete($id) {
        $page = Page::with('attributes')->find($id);
        $url = '#' . $page->attributes->url;
        $page->attributes->url = $url;
        $page->attributes->save();

        return true;
    }

    public function beforeRestore($id) {
        $page = Page::with('attributes')->find($id);
        $url = str_replace('#', '', $page->attributes->url);
        $page->attributes->url = $url;
        $page->attributes->save();

        return true;
    }

}
