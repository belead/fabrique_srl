<?php namespace App\Http\Controllers\_CMS;

use App\Mail\TwoFactors;
use App\Repositories\_CMS\Admin\AdminLogRepository;
use App\Repositories\_CMS\Admin\AdminRepository;
use App\Repositories\_CMS\Module\ModuleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FAQRCode\Google2FA;

class AdminsController extends CRUDController {

    protected $model = '_CMS\Admin';

    protected $admin_repository;
    protected $module_repository;
    protected $admin_log_repository;

    public function __construct(AdminRepository $admr,
                                ModuleRepository $modr,
                                AdminLogRepository $adminLogRepository) {
        $this->admin_repository = $admr;
        $this->module_repository = $modr;
        $this->admin_log_repository = $adminLogRepository;
    }

    public function index() {
        $dash_active = 'operators';
        $operators = $this->admin_repository->all();

        return view('admin.sections.operators.index', compact('dash_active', 'operators'));
    }

    public function getForm($id = false) {
        $dash_active = 'operators';

        $user = $id ? $this->admin_repository->find($id) : $this->admin_repository->new();
        $modules = $this->module_repository->getSelectable();

        return view('admin.sections.operators.form', compact('dash_active', 'user', 'modules'));
    }

    public function saveForm(Request $request, $id = false) {
        $user = $id ? $this->admin_repository->find($id) : $this->admin_repository->new();

        $rules = [
            'username' => 'required',
            'email' => 'required|email',
            'password_confirmation' => 'required_with:password'
        ];

        if($request->filled('password')) {
            $rules['password'] = 'confirmed';
        }
        if(!$id) {
            $rules['password'] = 'required';
            $rules['email'] = 'unique:admins';
            $rules['username'] = 'unique:admins';
        } else {
            if($user->email != $request->input('email')) {
                $rules['email'] = 'unique:admins';
            }
            if($user->username != $request->input('username')) {
                $rules['username'] = 'unique:admins';
            }
        }

        $data = $request->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $data['first_name'] = ucfirst($request->input('first_name'));
        $data['last_name'] = ucfirst($request->input('last_name'));
        $data['permissions'] = json_encode($request->input('permissions') ?? []);
        if($request->filled('password'))
            $data['password'] = bcrypt($request->input('password'));
        else
            unset($data['password']);

        $admin = $this->admin_repository->updateOrCreate(compact('id'), $data);

        if(request()->filled('google_id')) {
            $admin->socials()->updateOrCreate([
                'provider' => 'google',
            ], [
                'provider_user_id' => $data['google_id']
            ]);
        } else {
            $admin->socials()->where('provider', 'google')->delete();
        }

        $this->admin_log_repository->createLog('admins', $admin->id, ($id ? 'update' : 'create'), $admin->email);

        return redirect_success('Admin updated.');
    }

    public function delete($id) {
        if(guard_admin()->level < 100)
            return redirect_error('You have no permission to delete this operator.');

        return parent::delete($id);
    }

    public function getGoogle2FAForm() {
        $admin = guard_admin();

        if(!$admin->g2fa_confirmed) {
            $google2fa = new Google2FA();
            $secret = $google2fa->generateSecretKey(32);

            $admin->g2fa_secret = $secret;
            $admin->save();

            $google2fa_url = $google2fa->getQRCodeInline(
                config('_project.name') . ' | ' . config('_CMS._global.name'),
                $admin->email,
                $secret
            );
        } else {
            $secret = null;
            $google2fa_url = null;
        }

        return view('admin.google_auth', compact('secret', 'google2fa_url'));
    }

    public function checkGoogle2FAForm(Request $request) {
        $secret = $request->input('google_secret');
        if($request->has('google_secret')) {
            try {
                if(guard_admin()->g2fa_confirmed == 2) {
                    $valid = $secret == guard_admin()->token_2fa;
                } else {
                    $google2fa = new Google2FA();
                    $window = config('_CMS._global.google2FA_window', 0);
                    $valid = $google2fa->verifyKey(guard_admin()->g2fa_secret, $secret, $window);
                }

                if($valid) {
                    $admin = guard_admin();
                    $admin->update([
                        'g2fa_confirmed' => 1,
                        'token_2fa' => null,//email method
                    ]);

                    $request->session()->put('google_authentication', true);

                    return redirect()
                        ->route('admin.dashboard')
                        ->with('success', 'Google 2FA Authentication succeeded.')
                        ->cookie('google2FA', 1, config('_CMS._global.google2FA_cookie_lifespan', 60));
                } else
                    return redirect()->back()->with('error', 'Failed : The code does not match.');

            } catch(\ErrorException $e){
                return redirect()->route('admin.google')->with('error', 'An error has occurred.');
            }
        } else return redirect()->back()->with('error', 'Please enter the code.');
    }

    public function switchToEmail() {
        $admin = guard_admin();

        $token = mt_rand(100000, 999999);
        $admin->update([
            'g2fa_confirmed' => 2,
            'token_2fa' => $token
        ]);

        try {

            Mail::to($admin)->send(new TwoFactors($admin, $token));

        } catch(\Throwable $t) {}

        return redirect_success('Check your emails for the authentication code.');
    }

}
