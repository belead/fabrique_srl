<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\Controller;
use App\Providers\SocialAccountServiceProvider;
use App\Repositories\_CMS\Admin\AdminLogRepository;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller {

    protected $admin_log_repository;

    public function __construct(AdminLogRepository $adminLogRepository){
        $this->admin_log_repository = $adminLogRepository;
    }

    public function redirect($provider) {
        return Socialite::driver($provider)->redirect();
    }

    public function callback(SocialAccountServiceProvider $service, $provider) {
        try {

            $provider_user = Socialite::driver($provider)->stateless()->user();
            $admin = $service->getAdmin($provider_user, $provider);
            if($admin === false) {
                session()->flash('error', 'Account not found.');
                return redirect()->route('admin.login');
            }

            auth()->guard('admin')->login($admin);
            session([
                'socialite' => [
                    'admin' => [
                        'provider' => $provider,
                        'user_id' => $provider_user->getId()
                    ]
                ]
            ]);

            $this->admin_log_repository->createLog('admins', $admin->id, 'login', $admin->email);

            return redirect()->route('admin.dashboard');

        } catch (\Throwable $e){
            Log::error('_CMS\SocialAuthController: ' . $e->getMessage());

            session()->flash('error', $e->getMessage());
            return redirect()->route('admin.login');
        }
    }

}
