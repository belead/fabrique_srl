<?php namespace App\Http\Controllers\_CMS;

use App\Repositories\_CMS\Language\LanguageRepository;
use App\Repositories\_CMS\Language\VariableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Validator;

class LanguagesController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Language';

    protected $language_repository;
    protected $variable_repository;

    public function __construct(LanguageRepository $langr,
                                VariableRepository $varr) {
        $this->language_repository = $langr;
        $this->variable_repository = $varr;
    }

    public function index() {
        $dash_active = 'languages';

        $languages = $this->language_repository->all();
        $variables = $this->variable_repository->getBackendList();

        $can_operate = in_array(guard_admin()->level, [100]);

        return view('admin.sections.languages.index', compact('dash_active', 'languages', 'variables', 'can_operate'));
    }

    public function getForm($id = false) {
        $dash_active = 'languages';
        $language = $id ? $this->language_repository->find($id) : $this->language_repository->new();

        return view('admin.sections.languages.form', compact('dash_active', 'language'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'code' => 'required',
            'name' => 'required'
        ];
        $data = $request->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $data['code'] = strtolower($data['code']);
        $data['name'] = ucfirst($data['name']);
        $data['default_backend'] = $request->has('default_backend');
        $data['default_frontend'] = $request->has('default_frontend');

        if($data['default_backend'])
            $this->language_repository->model()->whereNotNull('id')->update(['default_backend' => 0]);

        if($data['default_frontend'])
            $this->language_repository->model()->whereNotNull('id')->update(['default_frontend' => 0]);

        // -- Check default backend
        $default_backend = $this->language_repository->findBy('default_backend', 1);
        if(!$data['default_backend'] && $default_backend->id == $id)
            $data['default_backend'] = 1;

        $this->language_repository->updateOrCreate(compact('id'), $data);

        return redirect_success('Language updated.');
    }

    public function switchActiveState($id) {
        $this->language_repository->switchActiveState($id, 'is_used');
        return redirect()->back()->with('success', 'Active state switched.');
    }

}
