<?php namespace App\Http\Controllers\_CMS;

use App\Helpers\_CMS\MenuHelper;
use App\Repositories\_CMS\Admin\AdminLogRepository;
use App\Repositories\_CMS\Menu\MenuItemRepository;
use App\Repositories\_CMS\Menu\MenuRepository;
use App\Repositories\_CMS\Page\PageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MenusController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Menu';

    protected $menu_repository;
    protected $menu_item_repository;
    protected $page_repository;
    protected $admin_log_repository;

    public function __construct(MenuRepository $menur,
                                MenuItemRepository $menuir,
                                PageRepository $pagr,
                                AdminLogRepository $adminLogRepository) {
        $this->menu_repository = $menur;
        $this->menu_item_repository = $menuir;
        $this->page_repository = $pagr;
        $this->admin_log_repository = $adminLogRepository;
    }

    public function index() {
        $dash_active = 'menus';
        $menus = $this->menu_repository->getBackendList();

        return view('admin.sections.menus.index', compact('dash_active', 'menus'));
    }

    public function getForm($id = false) {
        $dash_active = 'menus';

        $menu = $id ? $this->menu_repository->find($id) : $this->menu_repository->new();
        if($id) $menu->load('items.page.attributes', 'items.sub_menu.menu');

        $pages = $this->page_repository->getSelectableList();
        $nestable = MenuHelper::buildBackendNestable($menu, $pages);

        return view('admin.sections.menus.form', compact('menu', 'dash_active', 'pages', 'nestable'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'reference' => 'required',
            'name' => 'required'
        ];
        $data = $request->except('_token');

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        if(!$id)
            $data['site_id'] = session('switched_site.id');

        $data['is_everywhere'] = $request->has('is_everywhere');
        $data['reference'] = Str::slug($data['reference'], '_');

        $m = $this->menu_repository->updateOrCreate(compact('id'), $data);

        $this->admin_log_repository->createLog('menu', $m->id, ($id ? 'update' : 'create'), $m->name);

        return redirect_success('Menu updated.');
    }

    private function buildMenu($menu_id, $raw_data, $request) {
        // -- Delete all items
        $this->menu_item_repository->model()
            ->where('lang', session('switched_language'))
            ->where('menu_id', $menu_id)
            ->delete();

        $order = 1;
        foreach($raw_data as $raw) {
            $id = $raw->id;

            $data = [
                'title' => $request->input('title')[$id],
                'page_anchor' => $request->input('page_anchor')[$id],
                'menu_id' => $menu_id,
                'order' => $order++,
                'lang' => session('switched_language')
            ];
            if(isset($request->input('is_void')[$id])) {
                $data['is_void'] = 1;
            } else {
                if($request->input('custom_url')[$id]) {
                    $data['custom_url'] = $request->input('custom_url')[$id];
                } else {
                    $data['page_id'] = $request->input('page_id')[$id];
                }
            }

            // -- Create item
            $item = $this->menu_item_repository->create($data);

            if(isset($raw->children) && !empty($raw->children)) {
                $reference = $menu_id . '_sub_menu_' . Str::slug($item->title, '_');

                $menu = $this->menu_repository->updateOrCreate(compact('reference'), [
                    'name' => 'Sub Menu of ' . $item->title,
                    'site_id' => session('switched_site.id'),
                    'is_everywhere' => 0
                ]);

                $item->sub_menu()->create([
                    'menu_id' => $menu->id
                ]);

                // -- Recursively build sub-menus
                $this->buildMenu($menu->id, $raw->children, $request);
            }
        }
    }

    public function updateItems(Request $request) {
        $raw_data = json_decode($request->input('nestableData'));

        $this->buildMenu($request->input('menu_id'), $raw_data, $request);
        $this->menu_repository->clearOrphans();

        $menu = $this->menu_repository->find($request->input('menu_id'));
        if($menu)
            $this->admin_log_repository->createLog('menu', $menu->id, 'update', $menu->name);

        return redirect_success('Menu updated.');
    }

    public function delete($id, $internal_call = false, $is_recursive = false) {
        $menu = $this->menu_repository->find($id);
        $this->menu_repository->removeSubMenusWhere('menu_id', $menu->id);

        foreach($menu->items as $item) {
            if($item->sub_menu) {
                $this->delete($item->sub_menu->menu_id, true, true);
                $item->sub_menu->delete();
            }
            $item->delete();
        }

        $this->admin_log_repository->createLog('menu', $id, 'delete', $menu->name);

        if($is_recursive || !$internal_call) {
            $menu->delete();
        }

        if(!$internal_call) {
            Log::info(guard_admin()->getName() . ' has deleted a menu and its items. ' . json_encode(compact('id')));

            return redirect_success('Menu and items deleted.');
        }
    }

    public function buildItem(Request $request) {
        $item = $request->input('item');
        $item['is_void'] = (boolean) json_decode(strtolower($item['is_void'] ?? 0));

        $pages = $this->page_repository->getSelectableList();

        return view('admin.sections.menus.blocks.nestable_item', compact('item', 'pages'));
    }

}
