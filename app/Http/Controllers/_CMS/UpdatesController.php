<?php namespace App\Http\Controllers\_CMS;

use App\Helpers\_CMS\FileHelper;
use App\Http\Controllers\Controller;
use App\Models\_CMS\Update;
use App\Models\_CMS\UpdateLog;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Artisan;

class UpdatesController extends Controller {

    //protected static $base_url = 'http://127.0.0.1:8005';
    protected static $base_url = 'https://updates.belead.com';
    protected static $api_url = 'api/cms/updates/check';


    public function check($from_reference = null) {
        $error  = null;
        // -- Check for updates on BeLead server
        try {

            $client = new Client();
            $res = $client->request('POST', self::$base_url . '/' . self::$api_url . ($from_reference ? '/' . $from_reference : ''));
            if($res->getStatusCode() == 201 || $res->getStatusCode() == 200) {
                $response_data = json_decode($res->getBody()->getContents());
                foreach($response_data->updates as $update) {
                    Update::updateOrCreate(['reference' => $update->reference], (array) $update);
                }
            }

        } catch(\Throwable $t) {
            $error = 'ERROR ' . $t->getCode() . ' - ' . $t->getMessage();
        }

        // -- Create log
        $log = 'Checking for CMS updates';
        $this->addLog('check', $log, $error);

        session()->flash('success', 'Updates check done.');
        return redirect()->to(route('admin.configuration') . '#updates');
    }

    public function install() {
        set_time_limit(0);

        // -- Backup
        if(env('APP_ENV') !== 'local') {
            Artisan::call('backup:run');
        }

        // -- Install updates
        $updates = Update::where('is_installed', 0)
            ->where('is_deprecated', 0)
            ->orderBy('id')
            ->get();

        foreach($updates as $update) {

            $error = null;
            try {

                $log = 'Installing update - ' . $update->name;
                $this->addLog('install', $log, null, $update->id);

                $dir = public_path() . '/updates/';
                if(!file_exists($dir))
                    mkdir($dir, 0775, true);

                // -- Get zip from server
                copy(self::$base_url . $update->file_uri, $dir . $update->reference . '.zip');

                $zipArchive = new \ZipArchive();
                $res = $zipArchive->open($dir . $update->reference . '.zip');
                if($res === true) {
                    $dir .= $update->reference;
                    if(!file_exists($dir))
                        mkdir($dir, 0775, true);

                    $zipArchive->extractTo($dir);
                    $zipArchive->close();
                }

                // -- Copy files
                $files_path = $dir . '/files';
                if(file_exists($files_path)) {
                    $files = FileHelper::getDirContents($files_path);
                    foreach($files as $file) {
                        if(!is_dir($file)) {
                            $parts = explode('\\files\\', $file);
                            $to = base_path() . '/' . $parts[1];

                            $path_info = pathinfo($to);
                            if(!file_exists($path_info['dirname']))
                                mkdir($path_info['dirname'], 0775, true);

                            copy($file, $to);

                            $log = 'Copying file "' . $parts[1] . '"';
                            $this->addLog('file', $log, null, $update->id);
                        }
                    }
                }

                // -- Run commands
                $commands = $dir . '/commands.txt';
                if(file_exists($commands)) {
                    $lines = fopen($commands, 'r');
                    while(!feof($lines))  {
                        $cmd = fgets($lines);
                        $cmd = str_replace(["\n", "\r"], '', $cmd);

                        if(strpos($cmd, 'composer') !== false)
                            $cmd_ = $cmd . ' -d ' . base_path();
                        else
                            $cmd_ = str_replace('artisan', base_path() . '/artisan', $cmd);

                        $output = shell_exec($cmd_);

                        $log = 'Executing command "' . $cmd . '" - ' . str_replace(['[39m', '[32m'], '', $output);
                        $this->addLog('command', $log, null, $update->id);
                    }
                    fclose($lines);
                }

                // -- Remove folder
                FileHelper::removeDir($dir);

            } catch(\Throwable $t) {
                $error = 'ERROR ' . $t->getCode() . ' - ' . $t->getMessage();

                // -- Rollback?
                //...

                $log = 'Failure while installing update - ' . $update->name;
                $this->addLog('install', $log, $error, $update->id);

                break;
            }

            if(!$error) {
                // -- Mark as installed
                $update->update([
                    'is_installed' => 1
                ]);

                $log = 'Finished installing update - ' . $update->name;
                $this->addLog('install', $log, null, $update->id);
            }
        }

        session()->flash('success', 'Updates installed successfully!');
        return redirect()->to(route('admin.configuration') . '#updates');
    }

    public function addLog($action, $description, $error = null, $update_id = null) {
        UpdateLog::create([
            'user_id' => guard_admin()->id,
            'action' => $action,
            'description' => $description,
            'error' => $error,
            'update_id' => $update_id
        ]);
        sleep(1);
    }

}
