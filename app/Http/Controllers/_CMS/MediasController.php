<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\Controller;
use App\Repositories\_CMS\Media\MediaRepository;
use Illuminate\Http\Request;

class MediasController extends Controller {

    protected $media_repository;

    public function __construct(MediaRepository $medr) {
        $this->media_repository = $medr;
    }

    public function index() {
        $dash_active = 'medias';

        $medias = $this->media_repository->getBackendList();
        $total_size = $this->media_repository->getTotalSize();

        return view('admin.sections.medias.index', compact('dash_active', 'medias', 'total_size'));
    }

    public function upload(Request $request) {
        if($request->hasFile('file')) {
            $directory = public_path() . '/uploads/medias/';
            if(!file_exists($directory)) mkdir($directory, 0775, true);

            $medias = collect();
            foreach($request->file('file') as $file) {
                $size = $file->getSize();

                $name = strtolower(uniqid() . '.' . strtolower($file->getClientOriginalExtension()));
                $file->move($directory, $name);

                $media = $this->media_repository->create([
                    'file_name' => $name,
                    'original_file_name' => $file->getClientOriginalName(),
                    'file_size' => $size,
                    'file_extension' => strtolower($file->getClientOriginalExtension()),
                    'user_id' => guard_admin()->id,
                ]);

                $medias->push(view('admin.sections.medias.row', compact('media'))->render());
            }

            return json_encode($medias);
        } else
            return 'error';
    }

    public function delete($id) {
        $media = $this->media_repository->find($id);

        try {
            $file = public_path() . '/uploads/medias/' . $media->file_name;
            if(file_exists($file)) unlink($file);
        } catch(\Throwable $t) {}

        $media->delete();

        return redirect_success('Media deleted.');
    }

}
