<?php namespace App\Http\Controllers\_CMS;

use App\Mail\Users\GivePlatformAccess;
use App\Repositories\_CMS\Admin\AdminLogRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsersController extends CRUDController {

    protected $soft_delete = true;
    protected $model = 'User';

    protected $user_repository;
    protected $admin_log_repository;

    public function __construct(UserRepository $userr,
                                AdminLogRepository $adminLogRepository) {
        $this->user_repository = $userr;
        $this->admin_log_repository = $adminLogRepository;

        $this->dash_active = 'users';
    }

    public function index() {
        $dash_active = 'users';
        $users = $this->user_repository->getBackendList();

        return view('admin.sections.users.index', compact('dash_active', 'users'));
    }

    public function getForm($id = false) {
        $dash_active = 'users';
        $user = $id ? $this->user_repository->find($id) : $this->user_repository->new();

        return view('admin.sections.users.form', compact('dash_active', 'user'));
    }

    public function saveForm(Request $request, $id = false) {
        $user = $id ? $this->user_repository->find($id) : $this->user_repository->new();

        $rules = [
            'email' => 'required|email',
            'password_confirmation' => 'required_with:password'
        ];
        if($request->filled('password')) {
            $rules['password'] = 'confirmed';
        }

        if(!$id) {
            $rules['password'] = 'required';
            $rules['email'] = 'unique:users';
        } else {
            if($user->email != $request->input('email')) {
                $rules['email'] = 'unique:users';
            }
        }

        $data = $request->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
             return redirect_error_form($validation);

        if(!$id)
            $data['site_id'] = session('switched_site.id');

        if($request->filled('password'))
            $data['password'] = bcrypt($request->input('password'));
        else
            unset($data['password']);

        $u = $this->user_repository->updateOrCreate(compact('id'), $data);

        // -- Logs
        $this->admin_log_repository->createLog('user', $u->id, ($id ? 'update' : 'create'), $u->getName());

        return redirect_success('User Updated.');
    }

    public function invite($id) {
        $user = $this->user_repository->find($id);

        $password = Str::random(10);
        try {

            Mail::to($user->email)->send(new GivePlatformAccess($user, $password));

            $user->update([
                'password' => bcrypt($password)
            ]);

        } catch(\Throwable $t) {
            Log::error(get_class() . ': ' . $t->getMessage());
            return redirect_error('Something went wrong: ' . $t->getMessage());
        }

        return redirect_success('User invited.');
    }

}
