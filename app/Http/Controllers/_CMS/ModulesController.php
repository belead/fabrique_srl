<?php namespace App\Http\Controllers\_CMS;

use App\Repositories\_CMS\Language\LanguageRepository;
use App\Repositories\_CMS\Module\ModuleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Validator;

class ModulesController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Module';

    protected $module_repository;
    protected $language_repository;

    public function __construct(ModuleRepository $modr,
                                LanguageRepository $langr) {
        $this->module_repository = $modr;
        $this->language_repository = $langr;
    }

    public function index() {
        if(guard_admin()->level < 100)
            return redirect()->route('admin.dashboard')->with('error', 'You can\'t access this location.');

        $dash_active = 'modules';
        $modules = $this->module_repository->getBackendList();
        $languages = $this->language_repository->in('code', session('switched_language'), ['is_used' => 1], true);

        return view('admin.sections.modules.index', compact('dash_active', 'modules', 'languages'));
    }

    public function getForm($id = false) {
        $dash_active = 'modules';

        $module = $id ? $this->module_repository->find($id) : $this->module_repository->new();
        $class_name = '\App\Models\_Modules\\' . $module->class_name;
        if(class_exists($class_name)) {
            $class = new $class_name;
            $module->fillable = $class->getFillable();
        }

        return view('admin.sections.modules.form', compact('dash_active', 'module'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'reference' => 'required',
            'class_name' => 'required',
        ];
        $data = $request->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
             return redirect_error_form($validation);

        if(!request()->filled('hidden_fields'))
            $data['hidden_fields'] = null;

        $data['targetable_by'] = str_replace(' ', '', trim($request->input('targetable_by')));
        $data['orderable_by'] = str_replace(' ', '', trim($request->input('orderable_by')));

        $module = $this->module_repository->updateOrCreate(compact('id'), $data);

        if(request()->filled('config_type')
            && request()->filled('config_field')
            && request()->filled('config_key')
            && request()->filled('config_value')) {

            // -- Configuration
            $config = $module->configuration;
            $config[$data['config_type']][$data['config_field']][$data['config_key']] = $data['config_value'];

            $module->update([
                'configuration' => $config
            ]);
        }

        return redirect_success('Module updated.');
    }

    public function switchActiveState($id) {
        $this->module_repository->switchActiveState($id);
        return redirect()->back()->with('success', 'Active state switched.');
    }

    public function removeInConfig($id, $values) {
        $module = $this->module_repository->find($id);
        $config = $module->configuration;

        list($type, $field, $key) = explode(':', $values, 3);

        if(isset($config[$type][$field][$key])) {
            unset($config[$type][$field][$key]);

            if(empty($config[$type][$field]))
                unset($config[$type][$field]);

            if(empty($config[$type]))
                unset($config[$type]);
        }

        $module->update([
            'configuration' => $config
        ]);

        return redirect_success('Module updated.');
    }

    public function duplicateInLocale($id, $locale) {
        $module = $this->module_repository->find($id);

        $class_name = '\App\Models\_Modules\\' . $module->class_name;
        if(class_exists($class_name)) {
            $class = new $class_name;
            $items = $class->where('lang', session('switched_language'))->get();

            if($items->count() == 0)
                return redirect_error('No items found.');

            foreach($items as $item) {
                $copy_item = $item->replicate();
                $copy_item->lang = $locale;
                $copy_item->save();

                $module->copyRelationships($class, $item, $copy_item, $locale);
            }

            return  redirect_success('Items duplicated in: ' . strtoupper($locale));
        } else {
            return redirect_error($class_name . ' not found.');
        }
    }

}
