<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\Controller;
use App\Repositories\_CMS\Admin\AdminLogRepository;

class AdminsLogsController extends Controller {

    protected $admin_log_repository;

    public function __construct(AdminLogRepository $logr) {
        $this->admin_log_repository = $logr;
    }

    public function index() {
        $dash_active = 'operators_logs';
        $logs = $this->admin_log_repository->getBackendList();

        return view('admin.sections.operators.logs', compact('dash_active', 'logs'));
    }

}
