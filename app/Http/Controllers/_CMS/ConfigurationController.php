<?php namespace App\Http\Controllers\_CMS;

use App\Http\Controllers\Controller;
use App\Models\_CMS\Update;
use App\Models\_CMS\UpdateLog;
use App\Repositories\_CMS\Configuration\ConfigurationRepository;
use App\Repositories\_CMS\Site\SiteRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConfigurationController extends Controller {

    protected $configuration_repository;
    protected $site_repository;

    public function __construct(ConfigurationRepository $confr,
                                SiteRepository $sitr) {
        $this->configuration_repository = $confr;
        $this->site_repository = $sitr;
    }

    public function index() {
        $dash_active = 'configuration';

        $configuration = $this->configuration_repository->getCurrent();
        if(!$configuration)
            return redirect_error('Configuration row missing in database.');

        $updates = Update::orderBy('id', 'DESC')->where('is_deprecated', 0)->get();
        $updates_logs = UpdateLog::with('user', 'update_entity')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('admin.sections.configuration.form', compact('dash_active', 'configuration', 'updates', 'updates_logs'));
    }

    public function saveForm(Request $request) {
        $rules = [
            'name' => 'required'
        ];
        $data = $request->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $configuration = $this->configuration_repository->getCurrent();

        // -- Update site name
        $configuration->site->update([
            'name' => $request->input('name')
        ]);

        // -- Logos
        $directory = public_path() . '/assets/front/img/';
        if(!file_exists($directory)) mkdir($directory, 0777, true);

        if($request->hasFile('logo_website_default')) {
            $file = 'logo_website_default.' . $request->file('logo_website_default')->getClientOriginalExtension();
            $request->file('logo_website_default')->move($directory, $file);
            $data['logo_website_default'] = $file;
        }

        if($request->hasFile('logo_website_header')) {
            $file = 'logo_website_header.' . $request->file('logo_website_header')->getClientOriginalExtension();
            $request->file('logo_website_header')->move($directory, $file);
            $data['logo_website_header'] = $file;
        }

        if($request->hasFile('logo_website_footer')) {
            $file = 'logo_website_footer.' . $request->file('logo_website_footer')->getClientOriginalExtension();
            $request->file('logo_website_footer')->move($directory, $file);
            $data['logo_website_footer'] = $file;
        }

        if($request->hasFile('favicon')) {
            $file = 'favicon.' . $request->file('favicon')->getClientOriginalExtension();
            $request->file('favicon')->move($directory, $file);
            $data['favicon'] = $file;
        }

        // -- Update configuration
        $configuration->update($data);

        return redirect_success('Configuration updated.');
    }

    public function removeLogo($reference) {
        $configuration = $this->configuration_repository->getCurrent();

        try {
            $directory = public_path() . '/assets/front/img/';
            if(file_exists($directory . $configuration->$reference))
                unlink($directory . $configuration->$reference);
        } catch(\Throwable $t) {}

        $configuration->update([
            $reference => null
        ]);

        return redirect_success('Logo removed.');
    }

}
