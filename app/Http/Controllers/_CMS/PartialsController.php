<?php namespace App\Http\Controllers\_CMS;

use App\Repositories\_CMS\Module\ModuleRepository;
use App\Repositories\_CMS\Partial\PartialRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PartialsController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Partial';

    protected $partial_repository;
    protected $module_repository;

    public function __construct(PartialRepository $partr,
                                ModuleRepository $modr) {
        $this->partial_repository = $partr;
        $this->module_repository = $modr;
        $this->dash_active = 'partials';
    }

    public function index() {
        $dash_active = 'partials';

        $partials = $this->partial_repository->getBackendList();
        $modules = $this->module_repository->getSelectable();

        return view('admin.sections.partials.index', compact('dash_active', 'partials', 'modules'));
    }

    public function getForm($id = false) {
        $dash_active = 'partials';

        $partial = $id ? $this->partial_repository->find($id) : $this->partial_repository->new();

        $partial->fillable = [];
        if($partial && $partial->module) {
            $class_name = '\App\Models\_Modules\\' . ($partial->child_class ?? $partial->module->class_name);
            if(class_exists($class_name)) {
                $class = new $class_name;
                $partial->fillable = $class->getFillable();
            }
        }

        $modules = $this->module_repository->getSelectable();
        foreach($modules as $module) {
            $class_name = '\App\Models\_Modules\\' . $module->class_name;
            if(class_exists($class_name)) {
                $class = new $class_name;
                $module->children = $class->children;
            }
        }

        return view('admin.sections.partials.form', compact('dash_active', 'partial', 'modules'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'name' => 'required',
            'partial_path' => 'required',
            'module_id' => 'required|not_in:0',
        ];
        $data = $request->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $data['is_details_partial'] = $request->has('is_details_partial');
        if(!request()->filled('hidden_fields'))
            $data['hidden_fields'] = null;

        if(request()->hasFile('picture')) {
            $directory = public_path() . '/uploads/img/partials/';
            if(!file_exists($directory)) mkdir($directory, 0777, true);

            $file = request()->file('picture');
            $name = strtolower(uniqid() . '.' . strtolower($file->getClientOriginalExtension()));

            $img = Image::make($file->getRealPath());
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($directory . '/' . $name);

            $data['picture'] = $name;
        }

        $partial = $this->partial_repository->updateOrCreate(compact('id'), $data);

        if(request()->filled('config_type')
            && request()->filled('config_field')
            && request()->filled('config_key')
            && request()->filled('config_value')) {

            // -- Configuration
            $config = $partial->configuration;
            $config[$data['config_type']][$data['config_field']][$data['config_key']] = $data['config_value'];

            $partial->update([
                'configuration' => $config
            ]);
        }

        return redirect_success('Partial updated.');
    }

    public function deletePartial($id) {
        $this->partial_repository->delete($id);
        return redirect_success('Partial removed.');
    }

    public function removeInConfig($id, $values) {
        $partial = $this->partial_repository->find($id);
        $config = $partial->configuration;

        list($type, $field, $key) = explode(':', $values, 3);

        if(isset($config[$type][$field][$key])) {
            unset($config[$type][$field][$key]);

            if(empty($config[$type][$field]))
                unset($config[$type][$field]);

            if(empty($config[$type]))
                unset($config[$type]);
        }

        $partial->update([
            'configuration' => $config
        ]);

        return redirect_success('Partial updated.');
    }

    public function import() {
        foreach (config('_CMS._partials') as $partial) {
            $module = $this->module_repository->findBy('reference', $partial['module_reference']);
            if($module) {
                $partial['module_id'] = $module->id;

                if(isset($partial['hidden_fields']))
                    $partial['hidden_fields'] = json_decode($partial['hidden_fields']);

                if(isset($partial['configuration']))
                    $partial['configuration'] = json_decode($partial['configuration']);

                $this->partial_repository->updateOrCreate([
                    'partial_path' => $partial['partial_path'],
                    'side' => $partial['side'] ?? null,
                ], $partial);
            }
        }
        return redirect_success('Partials imported from config.');
    }

}
