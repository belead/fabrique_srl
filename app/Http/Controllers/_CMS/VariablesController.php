<?php namespace App\Http\Controllers\_CMS;

use App\Repositories\_CMS\Language\LanguageRepository;
use App\Repositories\_CMS\Language\VariableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Validator;

class VariablesController extends CRUDController {

    protected $soft_delete = false;
    protected $model = '_CMS\Variable';

    protected $variable_repository;
    protected $language_repository;

    public function __construct(VariableRepository $varr,
                                LanguageRepository $langr) {
        $this->variable_repository = $varr;
        $this->language_repository = $langr;
    }

    public function getForm($id = false) {
        $dash_active = 'languages';

        $variable = $id ? $this->variable_repository->find($id) : $this->variable_repository->new();
        $languages = $this->language_repository->getSelectable();

        return view('admin.sections.variables.form', compact('dash_active', 'variable', 'languages'));
    }

    public function saveForm(Request $request, $id = false) {
        $rules = [
            'value' => 'required',
        ];
        if(!$id) {
            $rules['reference'] = 'required';
            $rules['lang'] = 'required|not_in:0';
        }
        $data = $request->all();

        $validation = Validator::make($data, $rules);
        if($validation->fails())
            return redirect_error_form($validation);

        $this->variable_repository->updateOrCreate(compact('id'), $data);

        return redirect_success('Variable updated.');
    }

    public function sync($code) {
        $variables = $this->variable_repository->getSyncable($code);

        if($variables->count() == 0)
            return redirect_error('Variables are already synced.');

        foreach($variables as $variable) {
            $sync = $variable->replicate();
                $sync->lang = $code;
            $sync->save();
        }

        return redirect_success('Variables successfully synced.');
    }

}
