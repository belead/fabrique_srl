<?php namespace App\Http\Controllers\_CMS\Resources;

interface ResourcesInterface {

    public function index();
    public function form($id = null);
    public function saveForm($id = null);
    public function toggle($id);
    public function delete($id);

}
