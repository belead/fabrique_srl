<?php namespace App\Http\Controllers\_CMS\Resources;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class ResourcesController extends Controller {

    protected $dash_active;
    protected $module;
    protected $uploads_directory_path;

    public function create() {
        return $this->form();
    }
    public function store() {
        return $this->saveForm();
    }
    public function edit($id) {
        return $this->form($id);
    }
    public function update($id) {
        return $this->saveForm($id);
    }

    public function search() {
        $data = request()->except('_token');
        $key = 'search_' . str_replace('module_', '', $this->dash_active);
        foreach($data as $field => $value) {
            session([$key . '.' . $field => $value]);
        }
        return redirect_success('');
    }

    public function clear() {
        session()->forget('search_' . str_replace('module_', '', $this->dash_active));
        return redirect_success('');
    }

    public function uploadFile($configuration, $item, $request, $key, $is_picture = false) {
        $directory = public_path() . '/uploads/'
            . ($is_picture ? 'img' : 'files')
            . '/modules/'
            . ($this->uploads_directory_path ?? $this->module->reference) . '/';

        if(!file_exists($directory))
            mkdir($directory, 0777, true);

        $name = time() . '_' . $request->file($key)->getClientOriginalName();

        $is_picture = $is_picture
            && !in_array($request->file($key)->getClientOriginalExtension(), ['svg', 'SVG']);

        if($is_picture) {
            $img = Image::make($request->file($key)->getRealPath());

            // -- Resize/Fit picture
            if(isset($configuration['picture.resize'][$key])) {
                $method = 'resize';
            } elseif(isset($configuration['picture.fit'][$key])) {
                $method = 'fit';
            } else
                $method = null;

            if($method) {
                try {
                    $img->$method(
                        $configuration['picture.' . $method][$key]['width'] ?? null,
                        $configuration['picture.' . $method][$key]['height'] ?? null
                    );
                } catch(\Throwable $t) {
                    Log::error($t->getMessage());
                }
            }

            $img->save($directory . $name);
        } else {
            $request->file($key)->move($directory, $name);
        }

        $item->update([
            $key => $name
        ]);
    }

}
