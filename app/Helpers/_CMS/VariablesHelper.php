<?php namespace App\Helpers\_CMS;

use App;
use Session;
use App\Models\_CMS\Variable;

class VariablesHelper {

    public static function getVariablesArray() {
        $locale = App::getLocale();
        if(!Session::has('variables_' . $locale)) {
            $variables = Variable::where('lang', $locale)->get();
            if(!$variables) return [];
            Session::put('variables_' . $locale, $variables->mapWithKeys(function($item) {
                return [$item['reference'] => $item['value']];
            })->toArray());
        }
        return Session::get('variables_' . $locale);
    }

}