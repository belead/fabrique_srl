<?php namespace App\Helpers\_CMS;

use Config;

class TemplateHelper {

    protected static $path = '/assets/templates/';
    protected static $admin_template = 'pixel';
    protected static $front_template = '';

    public static function styles($for_admin = false) {
        self::$front_template = Config::get('_CMS._global.front_template');
        if($for_admin) {
            $template = self::$admin_template;
            $elements = Config::get('_CMS.styles');
        } else {
            $template = self::$front_template;
            $elements = Config::get('_project.styles');
        }
        $styles = '';
        foreach($elements as $key => $element) {
            if($key != 0) $styles .= "\r\n\t";
            $styles .= '<link href="' . asset(self::$path . $template . '/' . $element) . '" rel="stylesheet">';
        }
        return $styles;
    }

    public static function scripts($for_admin = false) {
        self::$front_template = Config::get('_CMS._global.front_template');
        if($for_admin) {
            $template = self::$admin_template;
            $elements = Config::get('_CMS.scripts');
        } else {
            $template = self::$front_template;
            $elements = Config::get('_project.scripts');
        }
        $scripts = '';
        foreach($elements as $key => $element) {
            if($key != 0) $scripts .= "\r\n\t";
            $scripts .= '<script type="text/javascript" src="' . asset(self::$path . $template . '/' . $element) . '"></script>';
        }
        return $scripts;
    }

}