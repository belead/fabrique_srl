<?php namespace App\Helpers\_CMS;

use App\Models\_CMS\PageAttribute;
use App\Repositories\_CMS\Language\LanguageRepository;

class PageHelper {

    public static function getChildrenRows($data, $page, $depth = 1) {
        $html = '';
        ++$depth;
        foreach($page->children->sortBy('order') as $child) {
            $rendered = view('admin.sections.pages.list_element', array_merge(['page' => $child, 'depth' => $depth], $data))->render();
            $html .= $rendered;
            $html .= self::getChildrenRows($data, $child, $depth);
        }
        return $html;
    }

    public static function getChildrenOptions($page, $current_page_id, $parents = [], $depth = 1) {
        $html = '';
        ++$depth;
        foreach($page->children->sortBy('order') as $child) {
            $html .= '<option class="_depth_' . $depth . '" value="' . $child->id . '"'
                . (in_array($child->id, $parents) ? 'selected' : '')
                . ($current_page_id == $child->id ? 'disabled' : '')
                . '>'
                . ($depth > 1 ? (self::getPadding($depth) . '&boxur; &nbsp;') : '') . $child->attributes->title
                . '</option>';
            $html .= self::getChildrenOptions($child, $current_page_id, $parents, $depth);
        }
        return $html;
    }

    public static function getPagesCount($pages) {
        $count = 0;
        foreach($pages as $page) {
            $count++;
            $count += self::getPagesCount($page->children);
        }
        return $count;
    }

    public static function getPadding($depth) {
        $value = '';
        for($i = 1; $i <= $depth; $i++) {
            $value .= '&nbsp;&nbsp;';
        }
        return $value;
    }

    public static function findLocaleUrlFromAny($url) {
        if(!is_array($url))
            $url = [$url];

        $attributes = PageAttribute::with('page')
            ->whereIn('url', $url)
            ->whereHas('page', function($query) {
                $query
                    ->with('attributes')
                    ->has('attributes')
                    ->where('is_active', 1)
                    ->where('is_deleted', 0)
                    ->where('is_destroyed', 0);
            })
            ->first();

        return $attributes->page->attributes->url ?? '';
    }

}
