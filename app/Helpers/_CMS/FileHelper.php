<?php namespace App\Helpers\_CMS;

class FileHelper {

    public static function getFormattedSize($bytes) {
        if($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = 0;
        }

        return $bytes;
    }

    public static function getIcon($file_type) {
        if(!empty($file_type)) {
            if(strpos($file_type, '.') !== false) {
                $arrayFile = explode('.', $file_type);
                $file_type = end($arrayFile);
            }
        }

        switch(strtolower($file_type)) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'gif':
                return 'file-image-o';
            case 'pdf':
                return 'file-pdf-o';
            case 'doc':
            case 'docx':
                return 'file-word-o';
            case 'xls':
            case 'xlsx':
                return 'file-excel-o';
            default:
                return 'file-o';
        }
    }

    public static function getDirContents($dir, &$results = []) {
        $files = scandir($dir);
        foreach($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if(!is_dir($path)) {
                $results[] = $path;
            } else if($value != '.' && $value != '..') {
                self::getDirContents($path, $results);
                $results[] = $path;
            }
        }
        return $results;
    }

    public static function removeDir($dir) {
        if(!file_exists($dir)) {
            return true;
        }
        if(!is_dir($dir)) {
            return unlink($dir);
        }
        foreach(scandir($dir) as $item) {
            if($item == '.' || $item == '..') {
                continue;
            }
            if(!self::removeDir($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }
        return rmdir($dir);
    }

}
