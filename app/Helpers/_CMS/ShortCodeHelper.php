<?php namespace App\Helpers\_CMS;

use App\Models\_Modules\InteractiveMap;
use Illuminate\Support\Str;

class ShortCodeHelper {

    public static function convertShortCode($tag, $text) {
        $find = [
            '~\[' . $tag . '\](.*?)\[/' . $tag . '\]~s'
        ];
        $method = 'get' . ucfirst(Str::camel($tag)) . 'View';
        return preg_replace_callback($find, [(new \App\Helpers\_CMS\ShortCodeHelper), $method], $text);
    }

    public function getMapView($matches) {
        if(isset($matches[1])) {
            $imap = InteractiveMap::find($matches[1]);
            if($imap)
                return view('front.partials.interactive_maps.map', ['item' => $imap]);
            else
                return $matches[0];
        } else return $matches[0];
    }

}
