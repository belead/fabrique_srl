<?php namespace App\Helpers\_CMS;

class DataTableHelper {

    public static function orderLink($name, $route, $order_by, $session) {
        $direction = 'ASC';
        $class = '';

        if($order_by == ($session['order_by'] ?? null)) {
            $direction = (($session['direction'] ?? null) == 'ASC') ? 'DESC' : 'ASC';
            $class = strtolower($direction);
        }

        $link = '<a href="' . route($route, [$order_by, $direction]) . '"';
        $link .= ' class="' . ($class ? ($class . ' text-info') : '') . '">';
        $link .= $name;

        if(!empty($class)) {
            $link .= ($class == 'asc') ? '<i class="fa fa-sort-up m-l-5" style="font-size: 14px;"></i>' : '<i class="fa fa-sort-down m-l-5" style="font-size: 14px;"></i>';
        } else {
            $link .= ' <i class="fa fa-sort m-l-5"></i>';
        }
        $link .= '</a>';

        return $link;
    }

}
