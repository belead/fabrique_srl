<?php namespace App\Helpers\_CMS;

class SearchHelper {

    /**
     * @param $url
     * @param string $path_override
     * @return mixed|string
     */
    public static function loadContent($url, $path_override = '') {
        try {
            $data = @file_get_contents($url);
            $dom = new \DOMDocument();
            $content = $data;
            if(@$dom->loadHTML($data)) {
                $xpath = new \DOMXPath($dom);
                $path = (empty($path_override)) ? '//body/div[1]/main' : $path_override;
                $rows = $xpath->query($path);
                if(isset($rows->item(0)->textContent))
                    $content = self::removeReturns($rows->item(0)->textContent);
                else
                    $content = '';
            }

            $content = trim(self::removeEmoji($content));
        } catch(\Exception $e) {
            $content = '';
            \Log::info('Error scrapping Search engine : ' . $e->getMessage());
        }

        return $content;
    }

    /**
     * @param $text
     * @return mixed
     */
    static function removeReturns($text) {
        $in = array("\t", "\n", "\r");
        $text = str_replace($in, " ", $text);
        return $text;
    }

    /**
     * @param $text
     * @return mixed|string
     */
    public static function removeEmoji($text) {
        $clean_text = '';

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        return $clean_text;
    }

    /**
     * @param $keywords
     * @return array
     */
    public static function getKeywords($keywords) {
        $keywords = str_replace(['-', '\'', '"', '.', ',', '!', '?', '%', '$', '€', ';', ':', '=', '+', '*', '(', ')', '&', '@', '#'], ' ', $keywords);
        $keywords = explode(' ', $keywords);
        $k = [];

        foreach($keywords as $keyword) {
            if(strlen($keyword) > 2) $k[] = $keyword;
        }

        return $k;
    }

    /**
     * @param $keywords
     * @return string
     */
    public static function formatKeywords($keywords) {
        $search = '';
        foreach($keywords as $keyword) {
            if(!empty($search)) $search .= ' ';
            $search .= '+' . addslashes(strip_tags($keyword)) . '*';
        }
        return $search;
    }

    /**
     * @param $text
     * @param $keywords
     * @return mixed
     */
    public static function highlightKeywords($text, $keywords) {
        foreach($keywords as $keyword) {
            $text = self::highlightStr($text, $keyword);
        }
        return $text;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return mixed
     */
    private static function highlightStr($haystack, $needle) {
        if(strlen($haystack) < 1 || strlen($needle) < 1) {
            return $haystack;
        }
        $haystack = preg_replace("/($needle+)/i", '<strong>$1</strong>', $haystack);

        return $haystack;
    }

    /**
     * @param $text
     * @return string
     */
    public static function trimText($text) {
        $text = self::trim_all($text);
        $extends = 200;
        $posIn = strpos($text, '<strong>');
        if($posIn !== false) $posIn -= $extends;
        $posOut = strpos($text, '</strong>');
        if($posOut !== false) $posOut += $extends;

        if($posIn !== false) {
            return substr($text, $posIn, ($posOut + 9) - $posIn);
        } else {
            return substr($text, 0, 200);
        }
    }

    /**
     * @param $str
     * @param null $what
     * @param string $with
     * @return string
     */
    public static function trim_all($str, $what = NULL, $with = ' ') {
        if($what === NULL) {
            //  Character      Decimal      Use
            //  "\0"            0           Null Character
            //  "\t"            9           Tab
            //  "\n"           10           New line
            //  "\x0B"         11           Vertical Tab
            //  "\r"           13           New Line in Mac
            //  " "            32           Space

            $what = "\\x00-\\x20";    //all white-spaces and control chars
        }

        return trim(preg_replace("/[" . $what . "]+/", $with, $str), $what);
    }

}
