<?php namespace App\Helpers\_CMS;

class CaptchaHelper {

    private static $APP_SECRET_KEY = '6Leyyq8fAAAAAGUyD4hSlsC7bZGvLLq0bIFjy9-f';
    private static $APP_PUBLIC_KEY = '6Leyyq8fAAAAAJBwXaj8qQ119a2Pn9CN-hclDxH3';

    public static function captchaIsValid($code, $ip = null, $timeout = 1) {
        if(empty($code)) { return false; }
        $params = [
            'secret'    => self::$APP_SECRET_KEY,
            'response'  => $code
        ];
        if($ip){ $params['remoteip'] = $ip; }
        $url = "https://www.google.com/recaptcha/api/siteverify?" . http_build_query($params);
        $response = file_get_contents($url);
        if(empty($response) || is_null($response)) {
            return false;
        }
        $json = json_decode($response);
        return $json->success;
    }

    public static function getCaptchaField() {
        return '<div class="g-recaptcha" data-sitekey="'. self::$APP_PUBLIC_KEY . '"></div>';
    }

}
