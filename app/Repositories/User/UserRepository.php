<?php namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\Repository;

class UserRepository extends Repository {

    public function __construct(User $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->when(session('search_users.name'), function($query) {
                $query->where(function($query) {
                        $query
                            ->where('first_name', 'LIKE', '%' . session('search_users.name') . '%')
                            ->orWhere('last_name', 'LIKE', '%' . session('search_users.name') . '%')
                            ->orWhere('email', 'LIKE', '%' . session('search_users.name') . '%');
                    });
            })
            ->where('is_destroyed', 0)
            ->where('is_deleted', 0)
            ->orderBy('first_name')
            ->orderBy('last_name')
            ->paginate(30);
    }

    public function getSelectableList() {
        return $this->model
            ->where('is_destroyed', 0)
            ->where('is_deleted', 0)
            ->orderBy('first_name')
            ->orderBy('last_name')
            ->get();
    }

}
