<?php namespace App\Repositories\_Modules\Article;

use App\Models\_Modules\ArticleCategory;
use App\Repositories\Repository;

class ArticleCategoryRepository extends Repository {

    public function __construct(ArticleCategory $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->where('lang', session('switched_language'))
            ->orderBy('name')
            ->paginate(30);
    }

    public function getSelectable() {
        return $this->model
            ->where('lang', session('switched_language'))
            ->orderBy('name')
            ->get();
    }

}
