<?php namespace App\Repositories\_Modules\Article;

use App\Models\_Modules\Article;
use App\Repositories\Repository;

class ArticleRepository extends Repository {

    public function __construct(Article $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->with('category')
            ->where('lang', session('switched_language'))
            ->when(session('search_articles.title'), function($query) {
                $query
                    ->where('title', 'LIKE', '%' . session('search_articles.title') . '%');
            })
            ->orderBy('custom_date', 'DESC')
            ->paginate(30);
    }

    public function getSelectable() {
        return $this->model
            ->where('lang', session('switched_language'))
            ->where('is_active', 1)
            ->orderBy('title')
            ->get();
    }

}
