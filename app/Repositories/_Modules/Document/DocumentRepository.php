<?php namespace App\Repositories\_Modules\Document;

use App\Models\_Modules\Document\Document;
use App\Repositories\Repository;

class DocumentRepository extends Repository {

    public function __construct(Document $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->with('users')
            ->when(session('search_documents.name'), function($query) {
                $query
                    ->where('name', 'LIKE', '%' . session('search_documents.name') . '%');
            })
            ->when(session('search_documents.user_id'), function($query) {
                $query->whereHas('users', function($query) {
                    $query
                        ->where('users.id', session('search_documents.user_id'));
                });
            })
            ->orderBy('name')
            ->paginate(30);
    }

}
