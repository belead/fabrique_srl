<?php namespace App\Repositories\_Modules\ContentBlock;

use App\Models\_Modules\ContentBlock;
use App\Repositories\Repository;

class ContentBlockRepository extends Repository {

    public function __construct(ContentBlock $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->with('partial')
            ->when(session('search_content_blocks.name'), function($query) {
                $query
                    ->where('name', 'LIKE', '%' . session('search_content_blocks.name') . '%');
            })
            ->when(session('search_content_blocks.partial_id'), function($query) {
                $query
                    ->where('partial_id', session('search_content_blocks.partial_id'));
            })
            ->where('lang', session('switched_language'))
            ->orderBy('created_at', 'DESC')
            ->paginate(30);
    }

}
