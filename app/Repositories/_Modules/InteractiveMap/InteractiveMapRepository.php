<?php namespace App\Repositories\_Modules\InteractiveMap;

use App\Models\_Modules\InteractiveMap;
use App\Repositories\Repository;

class InteractiveMapRepository extends Repository {

    public function __construct(InteractiveMap $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->paginate(30);
    }

}
