<?php namespace App\Repositories\_Modules\Newsletter;

use App\Models\_Modules\NewsletterBlock;
use App\Repositories\Repository;

class NewsletterBlockRepository extends Repository {

    public function __construct(NewsletterBlock $model) {
        $this->model = $model;
    }

    public function delete($id) {
        $block = $this->find($id);
        $block->articles()->sync([]);

        $this->model
            ->where('newsletter_id', $block->newsletter_id)
            ->where('position', '>', $block->position)
            ->decrement('position');
        return parent::delete($id);
    }

}
