<?php namespace App\Repositories\_Modules\Newsletter;

use App\Models\_Modules\Newsletter;
use App\Repositories\Repository;

class NewsletterRepository extends Repository {

    public function __construct(Newsletter $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->with('blocks')
            ->orderBy('created_at', 'DESC')
            ->paginate(30);
    }

    public function delete($id) {
        $newsletter = $this->find($id);
        foreach($newsletter->blocks as $block) {
            $block->articles()->sync([]);
        }
        $newsletter->blocks()->delete();
        return $newsletter->delete();
    }

    public function getSendable($period) {
        return $this->model
            ->with([
                'blocks.articles',
                'blocks.partial'
            ])
            ->where('is_active', 1)
            ->whereNotNull('provider')
            ->whereDate('planned_date', date('Y-m-d'))
            ->where('planned_timeslot', $period)
            ->where(function($query) {
                $query
                    ->whereNotNull('lists')
                    ->orWhereNotNull('recipients');
            })
            ->whereNull('sent_at')
            ->whereHas('blocks', function($query) {
                $query
                    ->where('_mod_newsletters_blocks.is_active', 1);
            })
            ->get();
    }

}
