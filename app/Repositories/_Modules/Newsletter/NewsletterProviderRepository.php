<?php namespace App\Repositories\_Modules\Newsletter;

use App\Repositories\_Modules\Newsletter\Provider\MailchimpProviderRepository;
use App\Repositories\_Modules\Newsletter\Provider\MailjetProviderRepository;
use Illuminate\Support\Facades\Log;

class NewsletterProviderRepository {

    protected $mailjet_repository;
    protected $mailchimp_repository;

    public function __construct(MailjetProviderRepository $mailjetr,
                                MailchimpProviderRepository $mailchimpr) {
        $this->mailjet_repository = $mailjetr;
        $this->mailchimp_repository = $mailchimpr;
    }

    public function getLists($provider) {
        if(!$provider)
            return [];

        try {

            if(!session($provider . '_lists')) {
                $lists  = $this->{$provider . '_repository'}->getLists();
                session([
                    $provider . '_lists' => $lists
                ]);
            }
            return session($provider . '_lists');

        } catch(\Throwable $t) {
            Log::error($t->getMessage());
            return [];
        }
    }

    public function getUnsubLink($provider, $newsletter) {
        if(!$provider)
            return null;

        try {
            return $this->{$provider . '_repository'}->getUnsubLink($newsletter);
        } catch(\Throwable $t) {
            Log::error($t->getMessage());
            return null;
        }
    }

    public function send($provider, $newsletter) {
        if(!$provider)
            throw new \Exception('Provider is missing.');

        try {
            return $this->{$provider . '_repository'}->send($newsletter);
        } catch(\Throwable $t) {
            throw new \Exception($t->getMessage());
        }
    }

}
