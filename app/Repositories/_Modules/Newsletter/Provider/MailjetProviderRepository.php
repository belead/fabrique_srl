<?php namespace App\Repositories\_Modules\Newsletter\Provider;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Mailjet\Resources;

class MailjetProviderRepository {

    public function getLists() {
        $results = [];
        $lists = \Mailjet\LaravelMailjet\Facades\Mailjet::getAllLists([])->getBody()['Data'];

        foreach($lists as $list) {
            if(!$list['IsDeleted']) {
                $results[] = [
                    'id' => $list['ID'],
                    'name' => $list['Name'] . ' (' . $list['SubscriberCount'] . ' subscribers)'
                ];
            }
        }

        return $results;
    }

    public function getUnsubLink($newsletter) {
        return '[[UNSUB_LINK_' . strtoupper($newsletter->lang) . ']]';
    }

    public function send($newsletter) {
        $mj = new \Mailjet\Client(
            config('services.mailjet.key'),
            config('services.mailjet.secret')
        );

        $body = [
            'Locale' => 'fr_FR',
            'Sender' => env('APP_NAME'),
            'SenderEmail' => env('MAIL_FROM_ADDRESS'),
            'Subject' => $newsletter->subject,
            'Title' => $newsletter->name
        ];

        // -- Using lists
        foreach($newsletter->lists as $list) {
            $body['ContactsListID'] = $list;

            try {
                $this->makeCampaign($mj, $body, $newsletter);
            } catch(\Throwable $t) {
                throw new \Exception($t->getMessage());
            }
        }
    }

    private function makeCampaign($mj, $body, $newsletter) {
        $response = $mj->post(Resources::$Campaigndraft, ['body' => $body]);
        $response->success() && $res = $response->getData();

        if(isset($res[0]['ID'])) {
            $unsub_link = $this->getUnsubLink($newsletter);

            $body = [
                'Headers' => 'object',
                'Html-part' => view('admin.modules.newsletters.templates.default.body', compact('newsletter', 'unsub_link'))->render(),
                'MJMLContent' => '',
            ];

            $response = $mj->post(Resources::$CampaigndraftDetailcontent, ['id' => $res[0]['ID'], 'body' => $body]);
            $response->success() && Log::info('Campaign content attached.');
        } else
            throw new \Exception($response->getData()['ErrorMessage'] ?? 'Something went wrong.');

        $response = $mj->post(Resources::$CampaigndraftSend, ['id' => $res[0]['ID']]);

        if($response->success() && !$newsletter->retry)
            $newsletter->update([
                'sent_at' => Carbon::now()
            ]);
    }

}
