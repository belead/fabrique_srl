<?php namespace App\Repositories\_Modules\Newsletter\Provider;

use MailchimpMarketing\ApiClient;

class MailchimpProviderRepository {

    public function getLists() {
        $results = [];

        $client = new ApiClient();
        $client->setConfig([
            'apiKey' => env('MAILCHIMP_APIKEY'),
            'server' => env('MAILCHIMP_SERVER'),
        ]);

        $lists = $client->lists->getAllLists(null, null, 1000);

        foreach ($lists->lists as $list) {
            $results[] = [
                'id' => $list->id,
                'name' => $list->name . ' (' . $list->stats->member_count . ' subscribers)'
            ];
        }

        return $results;
    }

    public function send($newsletter) {
        $client = new ApiClient();
        $client->setConfig([
            'apiKey' => env('MAILCHIMP_APIKEY'),
            'server' => env('MAILCHIMP_SERVER'),
        ]);

        foreach ($newsletter->lists as $list) {
            try {

                // -- Create campaign
                $campaign = $client->campaigns->create([
                    'type' => 'regular',
                    'recipients' => [
                        'list_id' => $list
                    ],
                    'settings' => [
                        'subject_line' => $newsletter->subject,
                        'reply_to' => env('MAIL_FROM_ADDRESS'),
                        'from_name' => env('MAIL_FROM_NAME'),
                        'title' => $newsletter->name
                    ]
                ]);

                // -- Attach html content
                $unsub_link = $this->getUnsubLink($newsletter);
                $client->campaigns->setContent($campaign->id, [
                    'html' => view('admin.modules.newsletters.templates.default.body', compact('newsletter', 'unsub_link'))->render()
                ]);

                // -- Send campaign
                $client->campaigns->send($campaign->id);

            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                //$decoded = json_decode($responseBodyAsString);

                throw new \Exception($responseBodyAsString);
            } catch (\Throwable $t) {
                throw new \Exception($t->getMessage());
            }
        }
    }

    public function getUnsubLink($newsletter) {
        return '*|UNSUB|*';
    }

}
