<?php namespace App\Repositories\_Modules\PageBlock;

use App\Models\_Modules\PageBlock;
use App\Repositories\Repository;

class PageBlockRepository extends Repository {

    public function __construct(PageBlock $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->paginate(30);
    }

}
