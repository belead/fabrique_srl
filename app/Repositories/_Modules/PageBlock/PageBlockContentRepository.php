<?php namespace App\Repositories\_Modules\PageBlock;

use App\Models\_Modules\PageBlockContent;
use App\Repositories\Repository;

class PageBlockContentRepository extends Repository {

    public function __construct(PageBlockContent $model) {
        $this->model = $model;
    }

    public function getBackendList($page_block_id = false) {
        return $this->model
            ->when($page_block_id, function($query) use ($page_block_id) {
                $query
                    ->where('page_block_id', $page_block_id);
            })
            ->orderBy('order')
            ->paginate(30);
    }

    public function decreaseOrder($page_block_id, $order_removed) {
        return $this->model
            ->where('page_block_id', $page_block_id)
            ->where('order', '>', $order_removed)
            ->update(['order' => \DB::raw('(_mod_page_blocks_contents.order - 1)')]);
    }

}
