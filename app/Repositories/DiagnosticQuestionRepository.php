<?php namespace App\Repositories;

use App\Models\_Modules\DiagnosticQuestion;

class DiagnosticQuestionRepository extends Repository {

    public function __construct(DiagnosticQuestion $model) {
        $this->model = $model;
    }

    public function getMaxPosition($field = 'position', $wheres = [], $category_id = false) {
        return $this->model
            ->where('category_id', $category_id)
            ->max('position');
    }

}
