<?php namespace App\Repositories;

class Repository implements RepositoryInterface {

    protected $model;

    // -- Interface
    public function all() {
        return $this->model->all();
    }

    public function create(array $data) {
        return $this->model->create($data);
    }

    public function update(array $data, $id) {
        return $this->model->find($id)->update($data);
    }

    public function delete($id) {
        return $this->model->destroy($id);
    }

    public function find($id) {
        return $this->model->find($id);
    }

    // -- Custom
    public function new() {
        return new $this->model;
    }

    public function model() {
        return $this->model;
    }

    public function updateOrCreate($search = [], $data = []) {
        return $this->model->updateOrCreate($search, $data);
    }

    public function findBy($by, $value) {
        return $this->model->where($by, $value)->first();
    }

    public function in($field, $values, $wheres = [], $not_in = false) {
        if(!is_array($values))
            $values = [$values];

        $query = $this->model;
        foreach($wheres as $key => $val) {
            $query = $query->where($key, $val);
        }

        if($not_in)
            return $query->whereNotIn($field, $values)->get();

        return $query->whereIn($field, $values)->get();
    }

    // -- CMS
    public function getBackendList() {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->paginate(30);
    }

    public function switchActiveState($id, $key = 'is_active') {
        $model = $this->model->find($id);
        $model->update([
            $key => !$model->$key
        ]);
        return $model;
    }

    public function up($id, $field = 'position', $wheres = []) {
        $model = $this->find($id);
        $this->model
            ->where($field, $model->$field - 1)
            ->when(!empty($wheres), function($query) use($wheres) {
                foreach($wheres as $key => $val) {
                    $query
                        ->where($key, $val);
                }
            })
            ->increment($field);
        return $model->decrement($field);
    }

    public function down($id, $field = 'position', $wheres = []) {
        $model = $this->find($id);
        $this->model
            ->where($field, $model->$field + 1)
            ->when(!empty($wheres), function($query) use($wheres) {
                foreach($wheres as $key => $val) {
                    $query
                        ->where($key, $val);
                }
            })
            ->decrement($field);
        return $model->increment($field);
    }

    public function getMaxPosition($field = 'position', $wheres = []) {
        return $this->model
            ->when(!empty($wheres), function($query) use($wheres) {
                foreach($wheres as $key => $val) {
                    $query
                        ->where($key, $val);
                }
            })
            ->max($field);
    }

    public function getCount() {
        return $this->model->count();
    }

}
