<?php namespace App\Repositories\_CMS\Search;

use App\Models\_Modules\Search;
use App\Repositories\Repository;

class SearchEngineRepository extends Repository {

    public function __construct(Search $model) {
        $this->model = $model;
    }

    public function hasBeenScrappedFrom($type, $id, $lang, $period) {
        return $this->model
            ->where('element_type', $type)
            ->where('element_id', $id)
            ->where('lang', $lang)
            ->where('updated_at', '>', $period->format('Y-m-d H:i:s'))
            ->first();
    }

}
