<?php namespace App\Repositories\_CMS\Media;

use App\Models\_CMS\Media;
use App\Repositories\Repository;

class MediaRepository extends Repository {

    public function __construct(Media $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->with('user')
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
    }

    public function getTotalSize() {
        return $this->model
            ->sum('file_size');
    }

}
