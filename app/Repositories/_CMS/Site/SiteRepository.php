<?php namespace App\Repositories\_CMS\Site;

use App\Models\_CMS\Site;
use App\Repositories\Repository;

class SiteRepository extends Repository {

    public function __construct(Site $model) {
        $this->model = $model;
    }

}
