<?php namespace App\Repositories\_CMS\Language;

use App\Models\_CMS\Variable;
use App\Repositories\Repository;

class VariableRepository extends Repository {

    public function __construct(Variable $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->with('siblings')
            ->where('lang', session('switched_language'))
            ->orderBy('reference')
            ->get();
    }

    public function getSyncable($locale) {
        return $this->model
            ->where('lang', session('switched_language'))
            ->whereDoesntHave('siblings', function($query) use($locale) {
                $query->where('lang', $locale);
            })
            ->get();
    }

}
