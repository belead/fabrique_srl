<?php namespace App\Repositories\_CMS\Language;

use App\Models\_CMS\Language;
use App\Repositories\Repository;

class LanguageRepository extends Repository {

    public function __construct(Language $model) {
        $this->model = $model;
    }

    public function getSelectable() {
        return $this->model
            ->where('is_used', 1)
            ->get();
    }

}
