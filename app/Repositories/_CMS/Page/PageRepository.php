<?php namespace App\Repositories\_CMS\Page;

use App\Models\_CMS\Page;
use App\Models\_CMS\PageFamily;
use App\Repositories\Repository;

class PageRepository extends Repository {

    protected $model_page_family;

    public function __construct(Page $model,
                                PageFamily $model_page_family) {
        $this->model = $model;
        $this->model_page_family = $model_page_family;
    }

    public function getCount() {
        return $this->model
            ->where('site_id', session('switched_site.id'))
            ->where('is_deleted', 0)
            ->count();
    }

    public function getBackendList() {
        return $this->model
            ->with([
                'attributes.creator',
                'children.attributes.creator'//first depth
            ])
            ->where('site_id', session('switched_site.id'))
            ->where('is_destroyed', 0)
            ->orderBy('is_deleted')
            ->orderBy('is_home', 'DESC')
            ->when(session('search_pages.title'), function($query) {
                $query->whereHas('attributes', function($query) {
                    $query
                        ->where('title', 'LIKE', '%' . session('search_pages.title') . '%');
                });
            })
            ->when(!session('search_pages.title'), function($query) {
                $query
                    ->doesntHave('parents');
            })
            ->get();
    }

    public function getSelectableList($only_parents = false) {
        return $this->model
            ->when($only_parents, function($query) {
                $query
                    ->with('children.attributes')
                    ->doesntHave('parents');
            })
            ->with('attributes')
            ->has('attributes')
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'))
            ->orderBy('is_home', 'DESC')
            ->get();
    }

    public function getMaxFamilyOrder($id) {
        return $this->model_page_family->where('parent_id', $id)->max('order');
    }

}
