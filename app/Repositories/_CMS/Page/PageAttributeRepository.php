<?php namespace App\Repositories\_CMS\Page;

use App\Models\_CMS\Content;
use App\Models\_CMS\Page;
use App\Models\_CMS\PageAttribute;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Log;

class PageAttributeRepository extends Repository {

    protected $model_page;
    protected $model_content;

    public function __construct(PageAttribute $model,
                                Page $model_page,
                                Content $model_content) {
        $this->model = $model;
        $this->model_page = $model_page;
        $this->model_content = $model_content;
    }

    public function duplicateInLocale($page_id, $locale) {
        $page = $this->model_page->find($page_id);

        if(!$page->attributes)
            return false;

        return $this->duplicateDataInLocale($page->attributes, $page->contents, $locale);
    }

    public function duplicateFromLocale($page_id, $locale) {
        $page = $this->model_page->find($page_id);

        $attributes = $page->raw_attributes->where('lang', $locale)->first();
        if(!$attributes)
            return false;

        return $this->duplicateDataInLocale($attributes, $page->contents, session('switched_language'));
    }

    public function delete($id) {
        $attributes = $this->find($id);

        $this->model_content
            ->where('page_id', $attributes->page->id)
            ->where('lang', $attributes->lang)
            ->delete();

        return parent::delete($id);
    }

    private function duplicateDataInLocale($attributes, $contents, $locale) {
        $copy_attributes = $attributes->replicate();
        $copy_attributes->lang = $locale;
        $copy_attributes->creator_id = guard_admin()->id;
        $copy_attributes->save();

        foreach($contents as $content) {
            try {

                $copy_content = $content->replicate();
                $copy_content->lang = $locale;
                $copy_content->save();

                if($content->module_id && $content->module_item_id) {
                    $module = $content->module;
                    if($module) {
                        $class_name = '\App\Models\_Modules\\' . $module->class_name;
                        if(class_exists($class_name)) {
                            $class = new $class_name;

                            $item = $class->find($content->module_item_id);
                            if($item) {
                                $copy_item = $item->replicate();
                                $copy_item->lang = $locale;
                                $copy_item->save();

                                $copy_content->update([
                                    'module_item_id' => $copy_item->id
                                ]);

                                $module->copyRelationships($class, $item, $copy_item, $locale);
                            } else {
                                throw new \Exception('Item with id ' . $content->module_item_id . ' not found.');
                            }
                        } else {
                            throw new \Exception($class_name . ' not found.');
                        }
                    }
                }

            } catch(\Throwable $t) {
                Log::error('_CMS\Page\PageAttributeRepository: ' . $t->getMessage() . $t->getTraceAsString());
            }
        }

        return true;
    }

    public function getPagesByLang($lang) {
        return $this->model
            ->with('page')
            ->whereHas('page', function($query) {
                $query
                    ->where('is_active', 1)
                    ->where('is_deleted', 0)
                    ->where('is_destroyed', 0);
            })
            ->where('lang', $lang)
            ->get();
    }

}
