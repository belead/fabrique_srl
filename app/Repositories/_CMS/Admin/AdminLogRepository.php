<?php namespace App\Repositories\_CMS\Admin;

use App\Models\_CMS\AdminLog;
use App\Repositories\Repository;

class AdminLogRepository extends Repository {

    public function __construct(AdminLog $model) {
        $this->model = $model;
    }

    public function createLog($item_type, $item_id, $action, string $title = '', string $description = '') {
        $this->create(
            [
                'admin_id' => guard_admin()->id,
                'item_type' => $item_type,
                'item_id' => $item_id,
                'action' => $action,
                'action_details' => (!empty($description) ? $description : $this->generateDescription($item_type, $item_id, $action, $title)),
            ]
        );
    }

    public function generateDescription($item_type, $item_id, $action, $title) {
        $description = $this->getItemLabel($item_type)
            . (!empty($title) ? ' "' . $title . '"' : '')
            . ' has been '
            . $this->getActionLabel($action);
        return $description;
    }

    public function getActionLabel($action) {
        $labels = [
            'create' => 'created',
            'update' => 'updated',
            'delete' => 'removed',
            'hide' => 'hidden',
            'show' => 'shown',
            'login' => 'logged in',
            'activate' => 'activated',
            'deactivate' => 'deactivated',
            'restore' => 'restored',
        ];
        return (isset($labels[$action])) ? $labels[$action] : $action;
    }

    public function getItemLabel($action) {
        $labels = [
            'page' => 'The page',
            'page_content' => 'A content for the page',
            'page_block' => 'A page block for the page',
            'admins' => 'The admin',
            'users' => 'The user',
        ];
        return (isset($labels[$action])) ? $labels[$action] : 'The ' . $action;
    }

}
