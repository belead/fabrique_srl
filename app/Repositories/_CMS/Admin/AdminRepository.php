<?php namespace App\Repositories\_CMS\Admin;

use App\Models\_CMS\Admin;
use App\Repositories\Repository;

class AdminRepository extends Repository {

    public function __construct(Admin $model) {
        $this->model = $model;
    }

}
