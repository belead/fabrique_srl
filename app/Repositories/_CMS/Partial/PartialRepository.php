<?php namespace App\Repositories\_CMS\Partial;

use App\Models\_CMS\Partial;
use App\Repositories\Repository;

class PartialRepository extends Repository {

    public function __construct(Partial $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->with('module')
            ->when(session('search_partials.name'), function($query) {
                $query
                    ->where('name', 'LIKE', '%' . session('search_partials.name') . '%');
            })
            ->when(session('search_partials.module_id'), function($query) {
                $query->whereHas('module', function($query) {
                    $query
                        ->where('module_id', session('search_partials.module_id'));
                });
            })
            ->orderBy('name')
            ->get();
    }

}
