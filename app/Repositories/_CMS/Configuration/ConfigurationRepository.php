<?php namespace App\Repositories\_CMS\Configuration;

use App\Models\_CMS\Configuration;
use App\Repositories\Repository;

class ConfigurationRepository extends Repository {

    public function __construct(Configuration $model) {
        $this->model = $model;
    }

    public function getCurrent() {
        return $this->model
            ->with('site')
            ->whereHas('site', function($query) {
                $query
                    ->where('id', session('switched_site.id') ?? 1);
            })
            ->first();
    }

}
