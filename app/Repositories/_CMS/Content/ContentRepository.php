<?php namespace App\Repositories\_CMS\Content;

use App\Models\_CMS\Content;
use App\Repositories\Repository;

class ContentRepository extends Repository {

    public function __construct(Content $model) {
        $this->model = $model;
    }

    public function delete($id) {
        $content = $this->find($id);
        $this->model
            ->where('page_id', $content->page_id)
            ->where('order', '>', $content->order)
            ->decrement('order');
        return parent::delete($id);
    }

    public function up($id, $field = 'position', $wheres = []) {
        $model = $this->find($id);
        $this->model
            ->where($field, $model->$field - 1)
            ->where('page_id', $model->page_id)
            ->where('lang', session('switched_language'))
            ->when(!empty($wheres), function($query) use($wheres) {
                foreach($wheres as $key => $val) {
                    $query
                        ->where($key, $val);
                }
            })
            ->increment($field);
        return $model->decrement($field);
    }

    public function down($id, $field = 'position', $wheres = []) {
        $model = $this->find($id);
        $this->model
            ->where($field, $model->$field + 1)
            ->where('page_id', $model->page_id)
            ->where('lang', session('switched_language'))
            ->when(!empty($wheres), function($query) use($wheres) {
                foreach($wheres as $key => $val) {
                    $query
                        ->where($key, $val);
                }
            })
            ->decrement($field);
        return $model->increment($field);
    }

}
