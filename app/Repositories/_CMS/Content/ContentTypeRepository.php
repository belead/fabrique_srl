<?php namespace App\Repositories\_CMS\Content;

use App\Models\_CMS\ContentType;
use App\Repositories\Repository;

class ContentTypeRepository extends Repository {

    public function __construct(ContentType $model) {
        $this->model = $model;
    }

    public function getSelectable() {
        return $this->model
            ->where('available', 1)
            ->orderBy('name')
            ->get();
    }

}
