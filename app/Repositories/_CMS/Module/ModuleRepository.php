<?php namespace App\Repositories\_CMS\Module;

use App\Models\_CMS\Module;
use App\Repositories\Repository;

class ModuleRepository extends Repository {

    public function __construct(Module $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->orderBy('is_active', 'DESC')
            ->orderBy('name')
            ->get();
    }

    public function getSelectableForPageForm() {
        $modules = $this->model
            ->with('partials')
            ->where('is_active', 1)
            ->where('is_linkable', 1)
            ->orderBy('name')
            ->get();

        foreach($modules as $key => $module) {
            $class_name = '\App\Models\_Modules\\' . $module->class_name;
            if(class_exists($class_name)) {
                $class = new $class_name;
            } else {
                $modules->forget($key);
                continue;
            }

            if(method_exists($class, 'getLinkableContents')) {
                $items = $class->getLinkableContents($module->targetable_by);
                $module->items = $items;
                $module->orders = explode(',', $module->orderable_by);
            } else {
                $modules->forget($key);
            }
        }

        return $modules;
    }

    public function getSelectable() {
        return $this->model
            ->where('is_active', 1)
            ->orderBy('name')
            ->get();
    }

}
