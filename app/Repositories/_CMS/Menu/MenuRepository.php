<?php namespace App\Repositories\_CMS\Menu;

use App\Models\_CMS\Menu;
use App\Models\_CMS\MenuItem;
use App\Models\_CMS\MenuSubMenu;
use App\Repositories\Repository;

class MenuRepository extends Repository {

    protected $model_sub_menu;
    protected $model_item;

    public function __construct(Menu $model, MenuSubMenu $model_sub_menu, MenuItem $model_item) {
        $this->model = $model;
        $this->model_sub_menu = $model_sub_menu;
        $this->model_item = $model_item;
    }

    public function getSelectableForPageForm() {
        return $this->model
            ->with('items')
            ->where('site_id', session('switched_site.id'))
            ->where('is_everywhere', 0)
            ->get();
    }

    public function getBackendList() {
        return $this->model
            ->where('site_id', session('switched_site.id'))
            ->doesntHave('is_child')
            ->get();
    }

    public function clearOrphans() {
        $this->model_sub_menu
            ->doesntHave('item')
            ->delete();

        // -- Clear orphan sub-menus and items
        $this->model
            ->where('name', 'LIKE', 'Sub Menu' . '%')
            ->doesntHave('is_child')
            ->delete();

        $this->model_item
            ->doesntHave('menu')
            ->delete();

        return true;
    }

    public function removeSubMenusWhere($field, $value) {
        return $this->model_sub_menu
            ->where($field, $value)
            ->delete();
    }

}
