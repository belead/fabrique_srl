<?php namespace App\Repositories\_CMS\Menu;

use App\Models\_CMS\MenuItem;
use App\Repositories\Repository;

class MenuItemRepository extends Repository {

    public function __construct(MenuItem $model) {
        $this->model = $model;
    }

}
