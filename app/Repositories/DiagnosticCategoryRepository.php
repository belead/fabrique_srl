<?php namespace App\Repositories;

use App\Models\_Modules\DiagnosticCategory;

class DiagnosticCategoryRepository extends Repository {

    public function __construct(DiagnosticCategory $model) {
        $this->model = $model;
    }

    public function getMaxPosition($field = 'position', $wheres = [], $diagnostic_id = false) {
        return $this->model
            ->where('diagnostic_id', $diagnostic_id)
            ->max('position');
    }

}
