<?php namespace App\Repositories;

use App\Models\_Modules\Diagnostic;

class DiagnosticRepository extends Repository {

    public function __construct(Diagnostic $model) {
        $this->model = $model;
    }

    public function getBackendList() {
        return $this->model
            ->paginate(30);
    }

    public function getFrontList() {
        return $this->model
            ->where('is_active', 1)
            ->orderBy('created_at', 'ASC')
            ->paginate(15);
    }

    public function getFrontDetails($id, $slug) {
        return $this->model()
            ->has('categories')
            ->with(['categories' => function($query) {
                $query
                    ->has('questions');
            }])
            ->when(session('diagnostic_email'), function($query) {
                $query->with([
                    'answers' => function($query) {
                        $query
                            ->where('email', session('diagnostic_email'));
                    }
                ]);
            })
            ->where('slug', $slug)
            ->where('is_active', 1)
            ->find($id);
    }

}
