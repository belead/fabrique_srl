<?php namespace App\Models\_Modules;

use Illuminate\Database\Eloquent\Model;

class DiagnosticQuestion extends Model {

    protected $table = '_mod_diagnostics_questions';
    protected $fillable = [
        'category_id', 'diagnostic_id', 'position', 'question', 'points_weighting'
    ];

    public function diagnostic() {
        return $this->belongsTo(Diagnostic::class, 'diagnostic_id');
    }

    public function category() {
        return $this->belongsTo(DiagnosticCategory::class, 'category_id');
    }


}
