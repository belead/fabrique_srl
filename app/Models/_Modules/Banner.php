<?php namespace App\Models\_Modules;

use App\Models\_CMS\Partial;

class Banner extends _Module {

    protected $table = '_mod_banners';

    public function links() {
        return $this->hasMany(BannerLink::class, 'banner_id');
    }

    public function partial() {
        return $this->belongsTo(Partial::class, 'partial_id');
    }

    public function getImage() {
        return asset('uploads/img/modules/banners/' . $this->image);
    }

}
