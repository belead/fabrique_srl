<?php namespace App\Models\_Modules;

use Illuminate\Database\Eloquent\Model;

class DiagnosticCategory extends Model {

    protected $table = '_mod_diagnostics_categories';
    protected $fillable = [
        'name', 'color', 'diagnostic_id', 'position'
    ];

    public function diagnostic() {
        return $this->belongsTo(Diagnostic::class, 'diagnostic_id');
    }

    public function questions() {
        return $this->hasMany(DiagnosticQuestion::class, 'category_id')->orderBy('position');
    }

}
