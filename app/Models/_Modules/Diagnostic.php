<?php namespace App\Models\_Modules;

use Illuminate\Database\Eloquent\Model;

class Diagnostic extends _Module {

    protected $table = '_mod_diagnostics';
    protected $fillable = [
        'title', 'is_active', 'slug', 'description', 'picture', 'subtitle'
    ];

    /* Relationships */

    public function categories() {
        return $this->hasMany(DiagnosticCategory::class, 'diagnostic_id')->orderBy('position');
    }

    public function answers() {
        return $this->hasMany(DiagnosticAnswer::class, 'diagnostic_id');
    }

    /* Methods */

    public function getSlug() {
        return $this->id . '-' . $this->slug;
    }

    public function getPicture() {
        return asset('/uploads/img/modules/diagnostics/' . $this->picture);
    }

    /* Override */

    public function getLinkableContents($order, $sorting = 'ASC', $is_front = false, $take = false) {
        $query = $this
            ->where('is_active', 1)
            ->orderBy($order, $sorting);
        if($take) return $query->take($take)->get();
        else return $query->get();
    }

}
