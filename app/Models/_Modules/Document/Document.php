<?php namespace App\Models\_Modules\Document;

use App\Models\_Modules\_Module;
use App\Models\User;

class Document extends _Module {

    protected $table = '_mod_documents';
    protected $fillable = [
        'name', 'description', 'file', 'is_active', 'by_user_id'
    ];

    /* Relationships */

    public function users() {
        return $this
            ->belongsToMany(User::class, 'users_documents', 'document_id', 'user_id')
            ->withTimestamps();
    }

    public function by_user() {
        return $this->belongsTo(User::class, 'by_user_id');
    }

    /* Methods */

    public function getFile() {
        return asset('/uploads/files/modules/documents/' . $this->file);
    }

}
