<?php namespace App\Models\_Modules;

use App\Models\_CMS\Partial;

class Quote extends _Module {

    protected $table = '_mod_quotes';

    public function partial() {
        return $this->belongsTo(Partial::class, 'partial_id');
    }

    public function getPicture() {
        return asset('/uploads/img/modules/quotes/' . $this->image);
    }

}
