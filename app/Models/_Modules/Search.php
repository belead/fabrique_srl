<?php namespace App\Models\_Modules;

class Search extends _Module {

    protected $table = '_mod_search';
    protected $fillable = [
        'element_type', 'element_id', 'lang', 'site_id', 'url', 'content', 'title'
    ];

}
