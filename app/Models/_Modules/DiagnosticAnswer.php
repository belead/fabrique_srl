<?php namespace App\Models\_Modules;

use Illuminate\Database\Eloquent\Model;

class DiagnosticAnswer extends Model {

    protected $table = '_mod_diagnostics_answers';
    protected $fillable = [
        'diagnostic_id', 'question_id', 'user_id', 'note', 'email', 'name', 'project_name'
    ];

    public function question() {
        return $this->belongsTo(DiagnosticQuestion::class, 'question_id');
    }
    public function diagnostic() {
        return $this->belongsTo(Diagnostic::class, 'diagnostic_id');
    }

}
