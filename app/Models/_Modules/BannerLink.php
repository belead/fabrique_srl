<?php namespace App\Models\_Modules;

use App\Models\_CMS\Page;

class BannerLink extends _Module {

    protected $table = '_mod_banners_links';

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

}
