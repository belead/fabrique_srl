<?php namespace App\Models\_Modules;

class ContactMessage extends _Module {

    protected $table = '_mod_contact_messages';

    public function getName() {
        return strtoupper($this->last_name) . ' ' . ucfirst($this->first_name);
    }

}
