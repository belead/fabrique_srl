<?php namespace App\Models\_Modules;

use App\Models\_CMS\Page;
use App\Models\_CMS\Partial;

class ContentBlock extends _Module {

    protected $table = '_mod_content_blocks';

    public function partial() {
        return $this->belongsTo(Partial::class, 'partial_id');
    }

    public function getPicture() {
        return asset('/uploads/img/modules/content_blocks/' . $this->image);
    }

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

}
