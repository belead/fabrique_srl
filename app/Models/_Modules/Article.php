<?php namespace App\Models\_Modules;

use App;

class Article extends _Module {

    protected $table = '_mod_articles';

    public function category() {
        return $this->belongsTo(ArticleCategory::class, 'category_id');
    }

    public function type() {
        return $this->belongsTo(ArticleType::class, 'type_id');
    }

    public function getPicture() {
        return asset('/uploads/img/modules/articles/' . $this->picture);
    }

    public function getDetailsContent($module, $id_or_slug)
    {
        $query = $this
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('is_draft', 0)
            ->where('lang', App::getLocale())
            ->where('site_id', session('switched_site.id'));

        $item = $query->where('id', $id_or_slug)->orWhere('slug', $id_or_slug)->first();

        return $item;
    }

}
