<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model {

    protected $table = 'configurations';
    protected $fillable = [
        'email_contact', 'phone_contact', 'logo_website_default', 'logo_website_header', 'logo_website_footer',
        'footer_copyright', 'address_contact', 'facebook', 'twitter', 'linkedin', 'instagram', 'youtube', 'favicon',
        'has_google_maps_js', 'has_recaptcha_js', 'has_stripe_js', 'has_jquery_cdn',
        'google_maps_api_key', 'jquery_cdn_url', 'site_id'
    ];

    /* Relationships */

    public function site() {
        return $this->belongsTo(Site::class, 'site_id');
    }

    /* Methods */

    public function getLogoDefault() {
        return asset('/assets/front/img/' . $this->logo_website_default);
    }

    public function getLogoHeader() {
        return asset('/assets/front/img/' . $this->logo_website_header);
    }

    public function getLogoFooter() {
        return asset('/assets/front/img/' . $this->logo_website_footer);
    }

    public function getFavicon() {
        return asset('/assets/front/img/' . $this->favicon);
    }


    public function getCMSVersion() {
        $last_update = Update::where('is_installed', 1)->latest()->first();
        return $last_update->cms_version ?? config('_CMS._global.version');
    }

}
