<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class PageFamily extends Model {

    protected $table = 'pages_families';
    protected $fillable = [
        'parent_id', 'page_id', 'order'
    ];

    public function parent() {
        return $this->belongsTo(Page::class, 'parent_id');
    }

}
