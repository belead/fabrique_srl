<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class AdminSocial extends Model {

    protected $table = 'admins_socials';
    protected $fillable = [
        'admin_id', 'provider', 'provider_user_id'
    ];

}
