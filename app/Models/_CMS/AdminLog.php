<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{

    protected $table = 'admins_logs';
    protected $fillable = [
        'admin_id', 'item_type', 'item_id', 'action', 'action_details'
    ];

    /* Relationships */

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function getActionIcon(){
        $icons = [
            'delete' => 'fa fa-trash-can',
            'login' => 'fa fa-user-check',
            'create' => 'fa fa-square-plus',
            'update' => 'fa fa-square-pen',
            'activate' => 'fa fa-lightbulb',
            'deactivate' => 'fa fa-lightbulb-o',
            'restore' => 'fa fa-trash-can-arrow-up',
        ];
        return (isset($icons[$this->action])) ? $icons[$this->action] : 'fa fa-square-pen';
    }

    public function getActionColor(){
        $colors = [
            'delete' => 'text-danger',
            'create' => 'text-success',
            'activate' => 'text-warning',
        ];
        return (isset($colors[$this->action])) ? $colors[$this->action] : '';
    }

}
