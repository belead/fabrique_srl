<?php namespace App\Models\_CMS;

use App\Models\People;

class Admin extends People {

    protected $table = 'admins';
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'is_support', 'g2fa_confirmed',
        'level', 'permissions', 'password', 'avatar', 'phone', 'g2fa_secret', 'token_2fa'
    ];
    public $label_field = 'email';

    /* Relationships */

    public function socials() {
        return $this->hasMany(AdminSocial::class, 'admin_id');
    }

    /* Methods */

    public function getAvatar() {
        if($this->avatar)
            return asset('/assets/admin/img/users/avatars/' . $this->avatar);

        return asset('/assets/admin/img/users/avatars/default.jpg');
    }

    public function canAccess($reference, $for_select = false) {
        $permissions = json_decode($this->permissions);
        if(!$permissions || empty($permissions))
            return !$for_select;

        return in_array($reference, $permissions);
    }

}
