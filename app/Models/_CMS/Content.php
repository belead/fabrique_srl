<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Content extends Model {

    protected $table = 'contents';
    protected $fillable = [
        'page_id', 'partial_id', 'head_title', 'title', 'lang',
        'type_id', 'module_id', 'reference', 'css_class', 'order',
        'content', 'module_item_id', 'content_page_id', 'module_all_items',
        'is_draft', 'module_number_items', 'module_order_by', 'content_custom_url',
        'content_bis', 'target'
    ];

    public function type() {
        return $this->belongsTo(ContentType::class, 'type_id');
    }

    public function module() {
        return $this->belongsTo(Module::class, 'module_id');
    }

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function targeted_page() {
        return $this->belongsTo(Page::class, 'content_page_id');
    }

    public function partial() {
        return $this->belongsTo(Partial::class, 'partial_id');
    }

    public function getItem() {
        $module = $this->module;
        try {

            if(!$this->item) {
                $class_name = '\App\Models\_Modules\\' . $module->class_name;
                $class = new $class_name;
                $this->item = $class->find($this->module_item_id);
            }
            return $this->item;

        } catch(\Throwable $t) {}
        return null;
    }

}
