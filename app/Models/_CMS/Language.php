<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Language extends Model {

    protected $table = 'languages';
    protected $fillable = [
        'name', 'code', 'is_used', 'default_backend', 'default_frontend',
    ];

}
