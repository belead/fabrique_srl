<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model {

    protected $table = 'menus_items';
    protected $fillable = [
        'menu_id', 'page_id', 'page_anchor', 'custom_url', 'title', 'is_void', 'lang', 'order'
    ];

    public function menu() {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function sub_menu() {
        return $this->hasOne(MenuSubMenu::class, 'parent_id');
    }

    public function isActiveDueToChildItem($current_url) {
        if(!$this->sub_menu) return false;
        if(!$this->sub_menu->menu) return false;
        foreach($this->sub_menu->menu->f_items as $item) {
            if(!$item->page) continue;
            if(!$item->page->f_attributes) continue;
            if($current_url == $item->page->f_attributes->url) return true;
        }
        return false;
    }

}
