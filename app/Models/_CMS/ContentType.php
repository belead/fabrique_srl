<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model {

    protected $table = 'contents_types';

}
