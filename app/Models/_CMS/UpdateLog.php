<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class UpdateLog extends Model {

    protected $table = 'updates_logs';
    protected $fillable = [
        'user_id', 'update_id', 'action', 'description', 'error'
    ];

    public function user() {
        return $this->belongsTo(Admin::class, 'user_id');
    }

    public function update_entity() {
        return $this->belongsTo(Update::class, 'update_id');
    }

}
