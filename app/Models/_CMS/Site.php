<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Site extends Model {

    protected $table = 'sites';
    protected $fillable = [
        'name', 'is_default'
    ];

    public function configuration() {
        return $this->hasOne(Configuration::class, 'site_id');
    }

}
