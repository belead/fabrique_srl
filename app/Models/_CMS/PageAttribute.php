<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class PageAttribute extends Model {

    protected $table = 'pages_attributes';
    protected $fillable = [
        'lang', 'title', 'image', 'url', 'meta_title', 'meta_description',
        'description', 'page_id', 'creator_id', 'display_title'
    ];

    public function creator() {
        return $this->belongsTo(Admin::class, 'creator_id');
    }

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function getImage() {
        return asset('uploads/img/pages/' . $this->image);
    }

}
