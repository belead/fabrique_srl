<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;
use App;

class Page extends Model {

    protected $table = 'pages';
    protected $fillable = [
        'level_access', 'site_id', 'directory', 'body_class', 'is_active', 'is_home',
        'is_secured', 'is_guest', 'is_deleted', 'is_destroyed', 'edition_mode'
    ];

    /* BACKEND */

    public function raw_attributes() {
        return $this->hasMany(PageAttribute::class, 'page_id');
    }

    public function attributes() {
        return $this->hasOne(PageAttribute::class, 'page_id')->where('lang', session('switched_language'));
    }

    public function menus() {
        return $this->belongsToMany(Menu::class, 'pages_menus', 'page_id', 'menu_id')->withTimestamps();
    }

    public function raw_contents() {
        return $this->hasMany(Content::class, 'page_id')->orderBy('order');
    }

    public function contents() {
        return $this->hasMany(Content::class, 'page_id')->orderBy('order')->where('lang', session('switched_language'));
    }

    public function logs() {
        return$this->hasMany(PageLog::class, 'page_id');
    }

    public function children() {
        return $this->belongsToMany(Page::class, 'pages_families', 'parent_id', 'page_id')
            ->withTimestamps()
            ->withPivot('order');
    }

    public function parents() {
        return $this->belongsToMany(Page::class, 'pages_families', 'page_id', 'parent_id')
            ->withTimestamps()
            ->withPivot('order');
    }

    public function isTranslated($language) {
        return PageAttribute::where('page_id', $this->id)->where('lang', $language)->first();
    }

    /* FRONT */

    public function f_attributes() {
        return $this->hasOne(PageAttribute::class, 'page_id')->where('lang', App::getLocale());
    }

    public function f_contents() {
        return $this->hasMany(Content::class, 'page_id')->orderBy('order')->where('lang', App::getLocale())->where('is_draft', 0);
    }

    /* SCOPES */

    public function scopeFront($query) {
        return $query
            ->where('is_active', 1)
            ->where('is_deleted', 0)
            ->where('is_destroyed', 0)
            ->where('site_id', session('switched_site.id'));
    }

}
