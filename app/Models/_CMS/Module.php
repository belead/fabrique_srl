<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

    protected $table = 'modules';
    protected $fillable = [
        'name', 'reference', 'class_name', 'description', 'targetable_by', 'orderable_by', 'icon',
        'is_targetable_by_slug', 'is_linkable', 'is_multiple', 'is_block', 'is_active',
        'hidden_fields', 'configuration'
    ];

    /* Mutators */

    public function getHiddenFieldsAttribute($value) {
        return !empty($value) ? json_decode($value) : [];
    }

    public function setHiddenFieldsAttribute($value) {
        $this->attributes['hidden_fields'] = is_array($value) && !empty($value) ? json_encode($value) : null;
    }

    public function getConfigurationAttribute($value) {
        return !empty($value) ? json_decode($value, true) : [];
    }

    public function setConfigurationAttribute($value) {
        $this->attributes['configuration'] = is_array($value) && !empty($value) ? json_encode($value) : null;
    }

    /* Relationships */

    public function partials() {
        return $this->hasMany(Partial::class, 'module_id')->orderBy('name');
    }

    /* Methods */

    public function getCount() {
        $class_name = '\App\Models\_Modules\\' . $this->class_name;
        if(class_exists($class_name)) {
            $class = new $class_name;
            return $class->getCount();
        }
        return 0;
    }

    public function hasFillable($key) {
        $class_name = '\App\Models\_Modules\\' . $this->class_name;
        if(class_exists($class_name)) {
            $class = new $class_name;
            return in_array($key, $class->getFillable());
        }
        return false;
    }

    public function hasFieldDisplayed($reference) {
        return !in_array($reference, $this->hidden_fields);
    }

    public function copyRelationships($class, $item, $copy_item, $locale) {
        if(is_array($class->copy_relationships) && !empty($class->copy_relationships)) {
            foreach($class->copy_relationships as $type => $relations) {
                if(!is_array($relations))
                    $relations = [$relations];

                foreach($relations  as $relation) {
                    [$relation_name, $relation_key] = explode(':', $relation, 2);

                    switch($type) {
                        case 'hasMany':
                            foreach($item->$relation_name as $child) {
                                $copy_child = $child->replicate();
                                $copy_child->$relation_key = $copy_item->id;
                                if(in_array('lang', $child->getFillable()))
                                    $copy_child->lang = $locale;
                                $copy_child->save();

                                $this->copyRelationships($child, $child, $copy_child, $locale);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

}
