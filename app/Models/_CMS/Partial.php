<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Partial extends Model {

    protected $table = 'partials';
    protected $fillable = [
        'name', 'description', 'side', 'partial_path', 'module_id', 'body_class',
        'is_details_partial', 'picture', 'hidden_fields', 'configuration', 'child_class'
    ];

    /* Mutators */

    public function getHiddenFieldsAttribute($value) {
        return !empty($value) ? json_decode($value) : [];
    }

    public function setHiddenFieldsAttribute($value) {
        $this->attributes['hidden_fields'] = is_array($value) && !empty($value) ? json_encode($value) : null;
    }

    public function getConfigurationAttribute($value) {
        return !empty($value) ? json_decode($value, true) : [];
    }

    public function setConfigurationAttribute($value) {
        $this->attributes['configuration'] = is_array($value) && !empty($value) ? json_encode($value) : null;
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }

    /* Relationships */

    public function module() {
        return $this->belongsTo(Module::class, 'module_id');
    }

    /* Methods */

    public function getPicture() {
        return asset('/uploads/img/partials/' . $this->picture);
    }

    public function getConfig($reference = null) {
        $config = $this->configuration;
        if(!empty($config) && $reference)
            return $config[$reference] ?? [];
        return $config;
    }

}
