<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class Update extends Model {

    protected $table = 'updates';
    protected $fillable = [
        'name', 'reference', 'cms_version', 'laravel_version', 'changelog',
        'is_installed', 'is_deprecated', 'file_uri'
    ];

    public function logs() {
        return $this->hasMany(UpdateLog::class, 'update_id');
    }

}
