<?php namespace App\Models\_CMS;

use Illuminate\Database\Eloquent\Model;

class ContentDraft extends Model {

    protected $table = 'contents_drafts';

    public function rel_content() {
        return $this->belongsTo(Content::class, 'content_id');
    }

}
