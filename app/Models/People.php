<?php namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class People extends Authenticatable {

    public function getName() {
        return $this->first_name . ' ' . $this->last_name;
    }
}
