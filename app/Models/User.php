<?php namespace App\Models;

use App\Models\_Modules\Document\Document;

class User extends People {

    protected $table = 'users';
    protected $fillable = [
        'first_name', 'last_name', 'email', 'avatar',
        'is_deleted', 'is_destroyed', 'site_id', 'password', 'phone',
        'founders_count', 'founders_are_married', 'founders_have_wedding_contract', 'founders_nationalities', 'founders_residential_region',
        'main_contact_first_name', 'main_contact_last_name', 'main_contact_email', 'main_contact_phone', 'main_contact_address',
        'main_contact_nationality',
        'company_name', 'company_address', 'company_launching_at', 'company_main_sector', 'company_project_description',
        'business_plan_is_ready', 'business_plan_needs_quick_feedback', 'business_plan_needs_financial_formatting',
        'business_plan_needs_finalizing_support', 'business_plan_needs_full_formatting',
        'sharing_agreement', 'how',
        'followup_bank_sent', 'followup_bank_iban', 'followup_bank_signature', 'followup_bank_capital_payment', 'followup_bank_certificate', 'followup_bank_cards',
        'followup_business_plan_description', 'followup_business_plan_budget', 'followup_business_plan_cash_flow', 'followup_business_plan_incomes_expenses_explained',
        'followup_business_plan_income_statement', 'followup_business_plan_assessment',
        'followup_business_court_management_access_in_order', 'followup_business_court_social_in_order',
    ];

    /* Relationships */

    public function documents() {
        return $this
            ->belongsToMany(Document::class, 'users_documents', 'user_id', 'document_id')
            ->withTimestamps();
    }

}
