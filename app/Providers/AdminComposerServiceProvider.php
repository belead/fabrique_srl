<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AdminComposerServiceProvider extends ServiceProvider {

    public function boot() {
        view()->composer(
            'admin.blocks.top_nav', 'App\Http\ViewComposers\_CMS\AdminNavComposer'
        );
        view()->composer(
            ['admin.blocks.sidebar', 'admin.dashboard'],
            'App\Http\ViewComposers\_CMS\AdminSidebarComposer'
        );
        view()->composer(
            ['admin._master'],
            'App\Http\ViewComposers\_CMS\AdminMasterComposer'
        );
        view()->composer(
            ['admin.blocks.footer'],
            'App\Http\ViewComposers\_CMS\AdminFooterComposer'
        );
    }


    public function register() {

    }

}