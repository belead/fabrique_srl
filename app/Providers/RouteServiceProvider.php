<?php namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

    protected $namespace = 'App\Http\Controllers';

    public function boot() {
        Route::pattern('id', '[0-9]+');
        parent::boot();
    }

    public function map() {
        $this->mapCMSRoutes();
        $this->mapModulesRoutes();
        $this->mapCustomModulesRoutes();
        $this->mapWebRoutes();
    }

    protected function mapWebRoutes() {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    protected function mapCMSRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/_cms.php'));
    }

    protected function mapModulesRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/_modules.php'));
    }

    protected function mapCustomModulesRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/_modules_project.php'));
    }

}
