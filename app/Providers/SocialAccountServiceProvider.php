<?php namespace App\Providers;

use App\Repositories\_CMS\Admin\AdminRepository;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccountServiceProvider {

    protected $admin_repository;

    public function __construct(AdminRepository $adminr) {
        $this->admin_repository = $adminr;
    }

    public function getAdmin(ProviderUser $provider_user, $provider) {
        $id = $provider_user->getId();
        $email = $provider_user->getEmail();

        $admin = $this->admin_repository
            ->model()
            ->whereHas('socials', function($query) use($provider, $id) {
                $query
                    ->where('provider', $provider)
                    ->where('provider_user_id', $id);
            })
            ->first();

        if(!$admin) {
            $admin = $this->admin_repository->findBy('email', $email);
            if(!$admin)
                return false;
        }

        if(!$admin->socials->where('provider', $provider)->first()) {
            $admin->socials()->create([
                'provider' => $provider,
                'provider_user_id' => $provider_user->getId(),
            ]);
        }

        return $admin;
    }

}
