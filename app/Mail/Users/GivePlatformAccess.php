<?php namespace App\Mail\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GivePlatformAccess extends Mailable {
    use Queueable, SerializesModels;

    public $user;
    public $password;

    public function __construct($user, $password) {
        $this->user = $user;
        $this->password = $password;
    }

    public function build() {
        return $this
            ->subject('LaFabrique - Accès à votre espace client')
            ->view('emails.users.give_platform_access');
    }

}
