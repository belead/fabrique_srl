<?php namespace App\Mail\Documents;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewDocumentUploadedByUser extends Mailable {
    use Queueable, SerializesModels;

    public $user;
    public $document;

    public function __construct($user, $document) {
        $this->user = $user;
        $this->document = $document;
    }

    public function build() {
        return $this
            ->subject('LaFabrique - Nouveau document reçu')
            ->view('emails.documents.new_document_uploaded_by_user');
    }

}
