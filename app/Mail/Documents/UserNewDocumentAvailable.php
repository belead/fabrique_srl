<?php namespace App\Mail\Documents;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserNewDocumentAvailable extends Mailable {
    use Queueable, SerializesModels;

    public $document;
    public $user;

    public function __construct($document, $user) {
        $this->document = $document;
        $this->user = $user;
    }

    public function build() {
        return $this
            ->subject('LaFabrique - Un nouveau document est disponible')
            ->view('emails.documents.user_new_document_available');
    }

}
