<?php namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TwoFactors extends Mailable {
    use Queueable, SerializesModels;

    public $token;
    public $user;

    public function __construct($user, $token) {
        $this->token = $token;
        $this->user = $user;
    }

    public function build() {
        return $this
            ->subject('Two factors authentication verification')
            ->view('emails.two_factors');
    }

}
