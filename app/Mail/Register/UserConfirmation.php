<?php namespace App\Mail\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserConfirmation extends Mailable {
    use Queueable, SerializesModels;

    public $user;

    public function __construct($user) {
        $this->user = $user;
    }

    public function build() {
        return $this
            ->subject('LaFabrique - Confirmation de votre inscription')
            ->view('emails.register.user_confirmation');
    }

}
