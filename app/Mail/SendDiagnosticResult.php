<?php namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDiagnosticResult extends Mailable {
    use Queueable, SerializesModels;

    public $email;
    public $recommended;
    public $diagnostic;
    public $user_followed;
    public $note;

    public function __construct($email, $recommended, $diagnostic, $user_followed = false, $note) {
        $this->email = $email;
        $this->recommended = $recommended;
        $this->diagnostic = $diagnostic;
        $this->user_followed = $user_followed;
        $this->note = $note;
    }

    public function build() {
        return $this
            ->subject('Résultat de votre diagnostic : "' . $this->diagnostic->title . '"')
            ->view('emails.diagnostics.results');
    }

}
