<?php namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DiagnosticsFormExport implements FromCollection, WithHeadings, WithMapping {

    public $diagnostic;

    public function __construct($diagnostic) {
        $this->diagnostic = $diagnostic;
    }

    public function collection() {
        return $this->diagnostic->answers;
    }

    public function headings(): array {
        return [
            'date',
            'question',
            'note',
            'email',
            'name',
            'project',
        ];
    }

    public function map($row): array {
        return [
            $row->created_at->format('d/m/Y'),
            $row->question->question ?? null,
            $row->note,
            $row->email,
            $row->name,
            $row->project_name,
        ];
    }

}