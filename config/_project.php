<?php

return [

    'styles' => [

    ],
    'scripts' => [

    ],

    'name' => 'La Fabrique à SRL',

    'body_classes' => [
        //-- Available classes appendable to front-end body tag (pages form)
        //'example' => 'This is an example',

    ],
    'section_classes' => [
        //-- Available classes appendable to front-end sections
        //'example' => 'This is an example',

    ],
    'section_targets' => [
        //-- Available targets in front-end layout

        '_content' => 'Default _content container',
        '_before_content' => '_before_content',
        '_after_content' => '_after_content',
    ]

];