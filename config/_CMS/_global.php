<?php

return [

    'name' => 'BeLead CMS',
    'version' => '3.6',
    'front_template' => 'default',

    'prefix_backend' => 'admin',

    'google2FA' => false,
    'google2FA_window' => 4,
    'google2FA_cookie_lifespan' => 60,

    'partials' => [
        'configuration' => [
            'types' => [
                'picture.resize' => null,//description can be added
                'picture.fit' => null,
                'label.rename' => null,
            ]
        ]
    ],
    'modules' => [
        'configuration' => [
            'types' => [
                'picture.resize' => null,//description can be added
                'picture.fit' => null,
            ]
        ]
    ],

    'newsletters' => [
        'providers' => [
            'mailchimp',
            'mailjet',
            //'mailgun',
        ],
        'variables' => [
            'mailjet' => [
                '[[data:first_name]]' => 'First Name',
                '[[data:last_name]]' => 'Last Name',
            ],
            'mailchimp' => [
                '*|FNAME|*' => 'First Name',
                '*|LNAME|*' => 'Last Name',
                '*|EMAIL|*' => 'Email',
                '*|CURRENT_YEAR|*' => 'Current Year',
            ]
        ]
    ]

];
